﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PayLoanForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PayLoanForm))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.OrdernoTextBox = New System.Windows.Forms.TextBox()
        Me.PaymentTextBox = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DiscountTextBox = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.AmountpaidtTextBox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.RemainingpaymentTextBox = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.AmountToPayTextBox = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.ContinueToPayButton = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(21, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(81, 19)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Order No:"
        '
        'OrdernoTextBox
        '
        Me.OrdernoTextBox.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OrdernoTextBox.Location = New System.Drawing.Point(190, 25)
        Me.OrdernoTextBox.Name = "OrdernoTextBox"
        Me.OrdernoTextBox.ReadOnly = True
        Me.OrdernoTextBox.Size = New System.Drawing.Size(228, 27)
        Me.OrdernoTextBox.TabIndex = 1
        Me.OrdernoTextBox.TabStop = False
        '
        'PaymentTextBox
        '
        Me.PaymentTextBox.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PaymentTextBox.Location = New System.Drawing.Point(190, 73)
        Me.PaymentTextBox.Name = "PaymentTextBox"
        Me.PaymentTextBox.ReadOnly = True
        Me.PaymentTextBox.Size = New System.Drawing.Size(228, 27)
        Me.PaymentTextBox.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(21, 76)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 19)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Payment:"
        '
        'DiscountTextBox
        '
        Me.DiscountTextBox.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DiscountTextBox.Location = New System.Drawing.Point(190, 106)
        Me.DiscountTextBox.Name = "DiscountTextBox"
        Me.DiscountTextBox.ReadOnly = True
        Me.DiscountTextBox.Size = New System.Drawing.Size(228, 27)
        Me.DiscountTextBox.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(21, 109)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 19)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Discount:"
        '
        'AmountpaidtTextBox
        '
        Me.AmountpaidtTextBox.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AmountpaidtTextBox.Location = New System.Drawing.Point(190, 139)
        Me.AmountpaidtTextBox.Name = "AmountpaidtTextBox"
        Me.AmountpaidtTextBox.ReadOnly = True
        Me.AmountpaidtTextBox.Size = New System.Drawing.Size(228, 27)
        Me.AmountpaidtTextBox.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(21, 142)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(107, 19)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Amount Paid:"
        '
        'RemainingpaymentTextBox
        '
        Me.RemainingpaymentTextBox.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RemainingpaymentTextBox.ForeColor = System.Drawing.Color.Red
        Me.RemainingpaymentTextBox.Location = New System.Drawing.Point(190, 172)
        Me.RemainingpaymentTextBox.Name = "RemainingpaymentTextBox"
        Me.RemainingpaymentTextBox.ReadOnly = True
        Me.RemainingpaymentTextBox.Size = New System.Drawing.Size(228, 27)
        Me.RemainingpaymentTextBox.TabIndex = 9
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(21, 175)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(156, 19)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Remaining Payment:"
        '
        'AmountToPayTextBox
        '
        Me.AmountToPayTextBox.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AmountToPayTextBox.ForeColor = System.Drawing.Color.Black
        Me.AmountToPayTextBox.Location = New System.Drawing.Point(190, 241)
        Me.AmountToPayTextBox.Name = "AmountToPayTextBox"
        Me.AmountToPayTextBox.Size = New System.Drawing.Size(228, 27)
        Me.AmountToPayTextBox.TabIndex = 0
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(21, 244)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(163, 19)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Enter Amount to Pay:"
        '
        'ContinueToPayButton
        '
        Me.ContinueToPayButton.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContinueToPayButton.Location = New System.Drawing.Point(190, 297)
        Me.ContinueToPayButton.Name = "ContinueToPayButton"
        Me.ContinueToPayButton.Size = New System.Drawing.Size(228, 36)
        Me.ContinueToPayButton.TabIndex = 12
        Me.ContinueToPayButton.Text = "&Continue to Pay"
        Me.ContinueToPayButton.UseVisualStyleBackColor = True
        '
        'PayLoanForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.ROJBackground
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(441, 357)
        Me.Controls.Add(Me.ContinueToPayButton)
        Me.Controls.Add(Me.AmountToPayTextBox)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.RemainingpaymentTextBox)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.AmountpaidtTextBox)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.DiscountTextBox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.PaymentTextBox)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.OrdernoTextBox)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "PayLoanForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Payment Form"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents OrdernoTextBox As TextBox
    Friend WithEvents PaymentTextBox As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents DiscountTextBox As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents AmountpaidtTextBox As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents RemainingpaymentTextBox As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents AmountToPayTextBox As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents ContinueToPayButton As Button
End Class
