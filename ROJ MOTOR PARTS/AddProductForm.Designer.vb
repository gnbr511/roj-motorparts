﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddProductForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AddProductForm))
        Me.ProductNameTextBox = New System.Windows.Forms.TextBox()
        Me.AvailStockTextBox = New System.Windows.Forms.TextBox()
        Me.MinStockLimitTextBox = New System.Windows.Forms.TextBox()
        Me.PurchaseCostTextBox = New System.Windows.Forms.TextBox()
        Me.SalePriceTextBox = New System.Windows.Forms.TextBox()
        Me.MeasuringUnitComboBox = New System.Windows.Forms.ComboBox()
        Me.AddButton = New System.Windows.Forms.Button()
        Me.CancelAddProductButton = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ProductBrandTextBox = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.ProductDescriptionTextBox = New System.Windows.Forms.TextBox()
        Me.BrowsePicButton = New System.Windows.Forms.Button()
        Me.ProductPhotoPictureBox = New System.Windows.Forms.PictureBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.SupRetPriceTextBox = New System.Windows.Forms.TextBox()
        Me.ProductNoTextBox = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        CType(Me.ProductPhotoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ProductNameTextBox
        '
        Me.ProductNameTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ProductNameTextBox.Location = New System.Drawing.Point(163, 186)
        Me.ProductNameTextBox.Name = "ProductNameTextBox"
        Me.ProductNameTextBox.Size = New System.Drawing.Size(185, 23)
        Me.ProductNameTextBox.TabIndex = 0
        '
        'AvailStockTextBox
        '
        Me.AvailStockTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AvailStockTextBox.Location = New System.Drawing.Point(163, 331)
        Me.AvailStockTextBox.Name = "AvailStockTextBox"
        Me.AvailStockTextBox.Size = New System.Drawing.Size(185, 23)
        Me.AvailStockTextBox.TabIndex = 4
        '
        'MinStockLimitTextBox
        '
        Me.MinStockLimitTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MinStockLimitTextBox.Location = New System.Drawing.Point(163, 368)
        Me.MinStockLimitTextBox.Name = "MinStockLimitTextBox"
        Me.MinStockLimitTextBox.Size = New System.Drawing.Size(185, 23)
        Me.MinStockLimitTextBox.TabIndex = 5
        '
        'PurchaseCostTextBox
        '
        Me.PurchaseCostTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PurchaseCostTextBox.Location = New System.Drawing.Point(163, 443)
        Me.PurchaseCostTextBox.Name = "PurchaseCostTextBox"
        Me.PurchaseCostTextBox.Size = New System.Drawing.Size(185, 23)
        Me.PurchaseCostTextBox.TabIndex = 7
        '
        'SalePriceTextBox
        '
        Me.SalePriceTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SalePriceTextBox.Location = New System.Drawing.Point(163, 478)
        Me.SalePriceTextBox.Name = "SalePriceTextBox"
        Me.SalePriceTextBox.Size = New System.Drawing.Size(185, 23)
        Me.SalePriceTextBox.TabIndex = 8
        '
        'MeasuringUnitComboBox
        '
        Me.MeasuringUnitComboBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MeasuringUnitComboBox.FormattingEnabled = True
        Me.MeasuringUnitComboBox.Items.AddRange(New Object() {"Feet", "Kg", "Ltr", "Meter", "Pack", "Pcs"})
        Me.MeasuringUnitComboBox.Location = New System.Drawing.Point(163, 294)
        Me.MeasuringUnitComboBox.Name = "MeasuringUnitComboBox"
        Me.MeasuringUnitComboBox.Size = New System.Drawing.Size(185, 24)
        Me.MeasuringUnitComboBox.TabIndex = 3
        '
        'AddButton
        '
        Me.AddButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AddButton.Location = New System.Drawing.Point(144, 528)
        Me.AddButton.Name = "AddButton"
        Me.AddButton.Size = New System.Drawing.Size(99, 32)
        Me.AddButton.TabIndex = 9
        Me.AddButton.Text = "Add Product"
        Me.AddButton.UseVisualStyleBackColor = True
        '
        'CancelAddProductButton
        '
        Me.CancelAddProductButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CancelAddProductButton.Location = New System.Drawing.Point(249, 528)
        Me.CancelAddProductButton.Name = "CancelAddProductButton"
        Me.CancelAddProductButton.Size = New System.Drawing.Size(99, 32)
        Me.CancelAddProductButton.TabIndex = 10
        Me.CancelAddProductButton.Text = "Cancel"
        Me.CancelAddProductButton.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(69, 189)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(88, 16)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Product Name"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(63, 334)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(94, 16)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Available Stock"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(31, 371)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(126, 16)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "Minimum Stock Limit"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(64, 297)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(93, 16)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Measuring Unit"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(68, 446)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(89, 16)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Purchase Cost"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(92, 481)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(65, 16)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Sale Price"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(79, 225)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 16)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Brand Name"
        '
        'ProductBrandTextBox
        '
        Me.ProductBrandTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ProductBrandTextBox.Location = New System.Drawing.Point(163, 222)
        Me.ProductBrandTextBox.Name = "ProductBrandTextBox"
        Me.ProductBrandTextBox.Size = New System.Drawing.Size(185, 23)
        Me.ProductBrandTextBox.TabIndex = 1
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(39, 261)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(118, 16)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "Product Description"
        '
        'ProductDescriptionTextBox
        '
        Me.ProductDescriptionTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ProductDescriptionTextBox.Location = New System.Drawing.Point(163, 258)
        Me.ProductDescriptionTextBox.Name = "ProductDescriptionTextBox"
        Me.ProductDescriptionTextBox.Size = New System.Drawing.Size(185, 23)
        Me.ProductDescriptionTextBox.TabIndex = 2
        '
        'BrowsePicButton
        '
        Me.BrowsePicButton.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BrowsePicButton.Location = New System.Drawing.Point(285, 107)
        Me.BrowsePicButton.Name = "BrowsePicButton"
        Me.BrowsePicButton.Size = New System.Drawing.Size(63, 25)
        Me.BrowsePicButton.TabIndex = 19
        Me.BrowsePicButton.Text = "Browse"
        Me.BrowsePicButton.UseVisualStyleBackColor = True
        '
        'ProductPhotoPictureBox
        '
        Me.ProductPhotoPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ProductPhotoPictureBox.Image = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.uploadphoto
        Me.ProductPhotoPictureBox.Location = New System.Drawing.Point(119, 20)
        Me.ProductPhotoPictureBox.Name = "ProductPhotoPictureBox"
        Me.ProductPhotoPictureBox.Size = New System.Drawing.Size(160, 112)
        Me.ProductPhotoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.ProductPhotoPictureBox.TabIndex = 20
        Me.ProductPhotoPictureBox.TabStop = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(43, 410)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(114, 16)
        Me.Label9.TabIndex = 22
        Me.Label9.Text = "Sup. Retailer Price"
        '
        'SupRetPriceTextBox
        '
        Me.SupRetPriceTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SupRetPriceTextBox.Location = New System.Drawing.Point(163, 407)
        Me.SupRetPriceTextBox.Name = "SupRetPriceTextBox"
        Me.SupRetPriceTextBox.Size = New System.Drawing.Size(185, 23)
        Me.SupRetPriceTextBox.TabIndex = 6
        '
        'ProductNoTextBox
        '
        Me.ProductNoTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ProductNoTextBox.Location = New System.Drawing.Point(163, 149)
        Me.ProductNoTextBox.Name = "ProductNoTextBox"
        Me.ProductNoTextBox.Size = New System.Drawing.Size(185, 23)
        Me.ProductNoTextBox.TabIndex = 0
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(87, 152)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(70, 16)
        Me.Label10.TabIndex = 24
        Me.Label10.Text = "Product No"
        '
        'AddProductForm
        '
        Me.AcceptButton = Me.AddButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.ROJBackground
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(383, 576)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.ProductNoTextBox)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.SupRetPriceTextBox)
        Me.Controls.Add(Me.ProductPhotoPictureBox)
        Me.Controls.Add(Me.BrowsePicButton)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.ProductDescriptionTextBox)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.ProductBrandTextBox)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CancelAddProductButton)
        Me.Controls.Add(Me.AddButton)
        Me.Controls.Add(Me.SalePriceTextBox)
        Me.Controls.Add(Me.PurchaseCostTextBox)
        Me.Controls.Add(Me.MinStockLimitTextBox)
        Me.Controls.Add(Me.AvailStockTextBox)
        Me.Controls.Add(Me.ProductNameTextBox)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.MeasuringUnitComboBox)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "AddProductForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add Product"
        CType(Me.ProductPhotoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ProductNameTextBox As TextBox
    Friend WithEvents AvailStockTextBox As TextBox
    Friend WithEvents MinStockLimitTextBox As TextBox
    Friend WithEvents PurchaseCostTextBox As TextBox
    Friend WithEvents SalePriceTextBox As TextBox
    Friend WithEvents MeasuringUnitComboBox As ComboBox
    Friend WithEvents AddButton As Button
    Friend WithEvents CancelAddProductButton As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents ProductBrandTextBox As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents ProductDescriptionTextBox As TextBox
    Friend WithEvents BrowsePicButton As Button
    Friend WithEvents ProductPhotoPictureBox As PictureBox
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents Label9 As Label
    Friend WithEvents SupRetPriceTextBox As TextBox
    Friend WithEvents ProductNoTextBox As TextBox
    Friend WithEvents Label10 As Label
End Class
