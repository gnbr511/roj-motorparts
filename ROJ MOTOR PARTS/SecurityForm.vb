﻿'Programmed By: Ginber Candelario, BSIT

Imports MySql.Data.MySqlClient
Public Class SecurityForm
    Friend GINBAHoption, GINBAHoption2 As String

    Private Sub SecurityForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SecurityusernameTextBox.Focus()
        Label1.Text = "Enter Administrator Username and Password To " & GINBAHoption2
    End Sub
    Private Sub SecurityForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        SecurityusernameTextBox.Clear()
        SecuritypasswordTextBox.Clear()
    End Sub
    Private Sub SecurityForm_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        SecurityusernameTextBox.Focus()
        Label1.Text = "Enter Administrator Username and Password To " & GINBAHoption2
    End Sub

    Private Sub SecEnterButton_Click(sender As Object, e As EventArgs) Handles SecEnterButton.Click
        Try
            OpenConnection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Dim GINBAH As String
        Try
            Query = "select * from users where username = '" _
                & SecurityusernameTextBox.Text _
                & "' and password = '" & SecuritypasswordTextBox.Text & "'"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                GINBAH = Reader.GetString("type")
            End While
            Reader.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        If GINBAHoption = "GinbahInventory" Then
            If GINBAH = "Administrator" Then
                InventoryForm.ShowDialog()
                Me.Close()
            Else
                MessageBox.Show("Must enter a correct administrator username and password!", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        ElseIf GINBAHoption = "GinbahRecords" Then
            If GINBAH = "Administrator" Then
                RecordsForm.ShowDialog()
                Me.Close()
            Else
                MessageBox.Show("Must enter a correct administrator username and password!", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        ElseIf GINBAHoption = "GinbahBackup" Then
            If GINBAH = "Administrator" Then
                BackUpForm.Backupfilename = "rojmotorparts"
                BackUpForm.ShowDialog()
                Me.Close()
            Else
                MessageBox.Show("Must enter a correct administrator username and password!", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        ElseIf GINBAHoption = "GinbahRestore" Then
            If GINBAH = "Administrator" Then
                RestoreDataForm.ShowDialog()
                Me.Close()
            Else
                MessageBox.Show("Must enter a correct administrator username and password!", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        ElseIf GINBAHoption = "GinbahUsersSettings" Then
            If GINBAH = "Administrator" Then
                UserSettingsForm.ShowDialog()
                Me.Close()
            Else
                MessageBox.Show("Must enter a correct administrator username and password!", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Else
            If GINBAH = "Administrator" Then
                BackupLocationForm.ShowDialog()
                Me.Close()
            End If
        End If
        MysqlCon.Close()
    End Sub
End Class