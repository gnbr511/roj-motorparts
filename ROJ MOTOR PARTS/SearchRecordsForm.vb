﻿'Program Name: ePOSIRMS (Electronic Point of Sale, Inventory, and Record Management System)
'Program Description: This program is a thesis project of Ginber Candelario and Ian Jay Lomlom for ROJ Motorparts.
'Programmer: GINBER J. CANDELARIO, BSIT

Imports MySql.Data.MySqlClient
Imports System.Drawing.Printing
Public Class SearchRecordsForm
    Private TOTGROSSSALE, TOTEXPENSES, TOTDISCOUNT, TOTNETSALE As Decimal

    Private Sub MakebackupButton_Click(sender As Object, e As EventArgs) Handles MakebackupButton.Click
        BackUpForm.Backupfilename = FromDateTimePicker.Text & " to " & ToDateTimePicker.Text & " Records"
        BackUpForm.FromDate = FromDateTimePicker.Text
        BackUpForm.ToDate = ToDateTimePicker.Text
        BackUpForm.ShowDialog()
    End Sub

    Private Sub DeleteSlctdRecButton_Click(sender As Object, e As EventArgs) Handles DeleteSlctdRecButton.Click
        Dim MesRes As DialogResult = MessageBox.Show("You can have a backup file of the records you selected before deleting it. Make a backup?", "Create Backup", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If MesRes = DialogResult.Yes Then
            MakebackupButton_Click(sender, e)
            DeleteSelectedRecords()
        Else
            DeleteSelectedRecords()
        End If
    End Sub
    Public Sub DeleteSelectedRecords()
        Try
            OpenConnection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Dim MesRes As DialogResult = MessageBox.Show("Click ok to continue deleting selected records.", "Confirm Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation)
        If MesRes = DialogResult.OK Then
            Try
                Query = "delete from todayssoldproducts where date >= '" & FromDateTimePicker.Text & "' and date <= '" & ToDateTimePicker.Text & "'"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                Reader.Close()
                Query = "delete from paidloanedproducts where datepaid >= '" & FromDateTimePicker.Text & "' and datepaid <= '" & ToDateTimePicker.Text & "'"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                Reader.Close()
                Try
                    Query = "delete from paymentsmade where date >= '" & FromDateTimePicker.Text & "' and date <= '" & ToDateTimePicker.Text & "'"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    Reader.Close()
                    Try
                        Query = "delete from todayssale where date >= '" & FromDateTimePicker.Text & "' and date <= '" & ToDateTimePicker.Text & "'"
                        Command = New MySqlCommand(Query, MysqlCon)
                        Reader = Command.ExecuteReader
                        Reader.Close()
                        Try
                            Query = "delete from todaysexpenses where date >= '" & FromDateTimePicker.Text & "' and date <= '" & ToDateTimePicker.Text & "'"
                            Command = New MySqlCommand(Query, MysqlCon)
                            Reader = Command.ExecuteReader
                            Reader.Close()
                            MessageBox.Show("Selected products are successfully deleted!", "", MessageBoxButtons.OK)
                        Catch ex As Exception
                            MessageBox.Show(ex.Message)
                        End Try
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
        MysqlCon.Close()
    End Sub
    Public Function LimitStrLen(str As String, len As Integer) As String
        If len >= str.Length Then
            Return str
        Else
            Return str.Substring(0, len) & "..."
        End If
    End Function

    Private Sub AllRecordsPrintDocument_PrintPage(sender As Object, e As PrintPageEventArgs) Handles AllRecordsPrintDocument.PrintPage
        Dim TitlePrintFont As New Font("Arial Narrow", 9, FontStyle.Bold)
        Dim PrintFontBold As New Font("Arial Narrow", 8, FontStyle.Bold)
        Dim PrintFont As New Font("Arial Narrow", 8)
        Dim lineheight As Single = PrintFontBold.GetHeight
        Dim HorizontalPrintLocation As Single = e.MarginBounds.Left
        Dim ROJPrintFont As New Font("Tahoma", 10, FontStyle.Bold)
        Dim ROJCenterPrintLocation As Single = Convert.ToSingle(e.PageBounds.Width / 2 - e.Graphics.MeasureString("ROJ MOTOR PARTS", ROJPrintFont).Width / 2)
        Dim RecordsDatesString As String = "Records from " & PrintFromDate.ToString("yyyy-MM-dd") & " to " & PrintToDate.ToString("yyyy-MM-dd")
        Dim RecordsDatesPrintLocation As Single = Convert.ToSingle(e.PageBounds.Width / 2 - e.Graphics.MeasureString(RecordsDatesString, TitlePrintFont).Width / 2)
        Dim NetSale As Decimal
        Try
            OpenConnection()
            If ROJPrinted = True Then

            Else
                e.Graphics.DrawString("ROJ MOTOR PARTS", ROJPrintFont, Brushes.Black, ROJCenterPrintLocation, 25)
                e.Graphics.DrawString(RecordsDatesString, TitlePrintFont, Brushes.Black, RecordsDatesPrintLocation, 40)
                ROJPrinted = True
                'get the dates of records to print
                Query = "select * from todayssale where date >= '" _
                & PrintFromDate.ToString("yyyy-MM-dd") & "' and date <= '" & PrintToDate.ToString("yyyy-MM-dd") & "'"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                While Reader.Read
                    Array.Resize(DatesToPrintArray, DatesToPrintArray.Length + 1)
                    DatesToPrintArray(DatesToPrintArray.Length - 1) = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                End While
                Reader.Close()
            End If
            'save data to hidden datagrid
            For i As Integer = StartAt To DatesToPrintArray.Length() - 1
                'save sold products to hidden datagrid
                HiddenSoldProductsDataGridView1.Rows.Clear()
                Try
                    Query = "select * from todayssoldproducts where date = '" & DatesToPrintArray(i) & "'"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    While Reader.Read
                        Dim a = Reader.GetString("productname")
                        Dim b = Reader.GetString("productbrand")
                        Dim c = Reader.GetString("productdescription")
                        Dim d = Reader.GetString("customername")
                        Dim f = Reader.GetDecimal("saleprice")
                        Dim g = Reader.GetDecimal("quantity")
                        Dim h = Reader.GetString("measuringunit")
                        Dim j = Reader.GetDecimal("totalprice")
                        Dim k = Reader.GetDecimal("discount")
                        Dim l = Reader.GetDecimal("discountedprice")
                        HiddenSoldProductsDataGridView1.Rows.Add(a, b, c, d, f, g, h, j, k, l)
                    End While
                    Reader.Close()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "1")
                End Try
                'save debts payments to hidden datagrid
                HiddenDebtPaymentsDataGridView.Rows.Clear()
                Try
                    Query = "select * from paymentsmade where date = '" & DatesToPrintArray(i) & "'"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    While Reader.Read
                        Dim a = Reader.GetString("customerfullname")
                        Dim b = Reader.GetDecimal("paidamount")
                        HiddenDebtPaymentsDataGridView.Rows.Add(a, b)
                    End While
                    Reader.Close()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "2")
                End Try
                'save expenses to hidden datagrid
                Try
                    HiddenExpensesDataGridView.Rows.Clear()
                    Query = "select * from todaysexpenses where date = '" & DatesToPrintArray(i) & "'"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    While Reader.Read
                        Dim a = Reader.GetString("name")
                        Dim b = Reader.GetString("description")
                        Dim c = Reader.GetDecimal("cost")
                        HiddenExpensesDataGridView.Rows.Add(a, b, c)
                    End While
                    Reader.Close()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "3")
                End Try
                'save todayssale to hidden datagrid
                Try
                    HiddenDailysalesDataGridView.Rows.Clear()
                    Query = "select * from todayssale where date = '" & DatesToPrintArray(i) & "'"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    While Reader.Read
                        Dim a = Reader.GetDecimal("grosssale")
                        Dim b = Reader.GetDecimal("totaldiscount")
                        Dim c = Reader.GetDecimal("totalexpenses")
                        Dim d As Decimal = a - b - c
                        HiddenDailysalesDataGridView.Rows.Add(a, b, c, d)
                        NetSale = d
                    End While
                    Reader.Close()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "4")
                End Try
                If NetSale = 0 Then

                Else
                    'Print date
                    If VerticalPrintLocation + lineheight >= 1008 Then
                        VerticalPrintLocation = e.MarginBounds.Top
                        e.HasMorePages = True
                        StartAt = i
                        Return
                    Else
                        If DatePrinted = False Then
                            e.Graphics.DrawString(DatesToPrintArray(i), PrintFontBold, Brushes.Red, HorizontalPrintLocation, VerticalPrintLocation)
                            VerticalPrintLocation += 15
                        Else

                        End If
                    End If
                    If PrintTotalSummary = False Then
                        For loop3 As Integer = StartForLoopAt To 3
                            Select Case DoNumber
                                Case 0
                                    'print sold products
                                    Dim ProductNameLocation As Single = e.MarginBounds.Left
                                    Dim ProductBrandLocation As Single = e.MarginBounds.Left + 100
                                    Dim ProductDescLocation As Single = e.MarginBounds.Left + 185
                                    Dim CustomerNameLocation As Single = e.MarginBounds.Left + 285
                                    Dim SalePriceLocation As Single = e.MarginBounds.Left + 385
                                    Dim QuantityLocation As Single = e.MarginBounds.Left + 455
                                    Dim MeasuringUnitLocation As Single = e.MarginBounds.Left + 520
                                    Dim TotalPriceLocation As Single = e.MarginBounds.Left + 590
                                    Dim DiscountLocation As Single = e.MarginBounds.Left + 660
                                    Dim DiscountedPriceLocation As Single = e.MarginBounds.Left + 730

                                    If VerticalPrintLocation + lineheight >= 1020 Then
                                        VerticalPrintLocation = e.MarginBounds.Top
                                        e.HasMorePages = True
                                        StartAt = i
                                        StartForLoopAt = loop3
                                        DatePrinted = True
                                        Return
                                    Else
                                        If SoldProdColPrinted = False Then
                                            e.Graphics.DrawString("SOLD PRODUCTS", PrintFontBold, Brushes.Blue, e.MarginBounds.Left, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                            e.Graphics.DrawString("PROD. NAME", PrintFontBold, Brushes.Black, ProductNameLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("BRAND", PrintFontBold, Brushes.Black, ProductBrandLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("DESCRIPTION", PrintFontBold, Brushes.Black, ProductDescLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("CUSTOMER NAME", PrintFontBold, Brushes.Black, CustomerNameLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("SALE PRICE", PrintFontBold, Brushes.Black, SalePriceLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("QUANTITY", PrintFontBold, Brushes.Black, QuantityLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("MEAS. UNIT", PrintFontBold, Brushes.Black, MeasuringUnitLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("TOT. PRICE", PrintFontBold, Brushes.Black, TotalPriceLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("DISCOUNT", PrintFontBold, Brushes.Black, DiscountLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("FINAL PRICE", PrintFontBold, Brushes.Black, DiscountedPriceLocation, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                        End If
                                    End If

                                    For row As Integer = StartAt2 To HiddenSoldProductsDataGridView1.RowCount() - 1
                                        If VerticalPrintLocation + lineheight >= 1060 Then
                                            e.Graphics.DrawString(LimitStrLen(HiddenSoldProductsDataGridView1.Rows(row).Cells(0).Value, 13), PrintFont, Brushes.Black, ProductNameLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(HiddenSoldProductsDataGridView1.Rows(row).Cells(1).Value, 10), PrintFont, Brushes.Black, ProductBrandLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(HiddenSoldProductsDataGridView1.Rows(row).Cells(2).Value, 13), PrintFont, Brushes.Black, ProductDescLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(HiddenSoldProductsDataGridView1.Rows(row).Cells(3).Value, 13), PrintFont, Brushes.Black, CustomerNameLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView1.Rows(row).Cells(4).Value, PrintFont, Brushes.Black, SalePriceLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView1.Rows(row).Cells(5).Value, PrintFont, Brushes.Black, QuantityLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView1.Rows(row).Cells(6).Value, PrintFont, Brushes.Black, MeasuringUnitLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView1.Rows(row).Cells(7).Value, PrintFont, Brushes.Black, TotalPriceLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView1.Rows(row).Cells(8).Value, PrintFont, Brushes.Black, DiscountLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView1.Rows(row).Cells(9).Value, PrintFont, Brushes.Black, DiscountedPriceLocation, VerticalPrintLocation)
                                            VerticalPrintLocation = e.MarginBounds.Top
                                            e.HasMorePages = True
                                            StartAt2 = row + 1
                                            StartAt = i
                                            StartForLoopAt = loop3
                                            DatePrinted = True
                                            SoldProdColPrinted = True
                                            Return
                                        Else
                                            e.Graphics.DrawString(LimitStrLen(HiddenSoldProductsDataGridView1.Rows(row).Cells(0).Value, 13), PrintFont, Brushes.Black, ProductNameLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(HiddenSoldProductsDataGridView1.Rows(row).Cells(1).Value, 10), PrintFont, Brushes.Black, ProductBrandLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(HiddenSoldProductsDataGridView1.Rows(row).Cells(2).Value, 13), PrintFont, Brushes.Black, ProductDescLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(HiddenSoldProductsDataGridView1.Rows(row).Cells(3).Value, 13), PrintFont, Brushes.Black, CustomerNameLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView1.Rows(row).Cells(4).Value, PrintFont, Brushes.Black, SalePriceLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView1.Rows(row).Cells(5).Value, PrintFont, Brushes.Black, QuantityLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView1.Rows(row).Cells(6).Value, PrintFont, Brushes.Black, MeasuringUnitLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView1.Rows(row).Cells(7).Value, PrintFont, Brushes.Black, TotalPriceLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView1.Rows(row).Cells(8).Value, PrintFont, Brushes.Black, DiscountLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView1.Rows(row).Cells(9).Value, PrintFont, Brushes.Black, DiscountedPriceLocation, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                        End If
                                    Next
                                    StartAt2 = 0
                                    DoNumber = 1
                                Case 1
                                    'print payments made
                                    Dim CustomerFullNamePrintLocation As Single = e.MarginBounds.Left
                                    Dim PaidAmountPrintLocation As Single = e.MarginBounds.Left + 200
                                    If VerticalPrintLocation + lineheight >= 1020 Then
                                        VerticalPrintLocation = e.MarginBounds.Top
                                        e.HasMorePages = True
                                        StartAt = i
                                        StartForLoopAt = loop3
                                        DatePrinted = True
                                        Return
                                    Else
                                        If DebtsPaymentsColPrinted = False Then
                                            e.Graphics.DrawString("DEBT'S PAYMENTS", PrintFontBold, Brushes.Blue, e.MarginBounds.Left, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                            e.Graphics.DrawString("CUSTOMER FULL NAME", PrintFontBold, Brushes.Black, CustomerFullNamePrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("PAID AMOUNT", PrintFontBold, Brushes.Black, PaidAmountPrintLocation, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                        End If
                                    End If
                                    For row As Integer = StartAt2 To HiddenDebtPaymentsDataGridView.RowCount() - 1
                                        If VerticalPrintLocation + lineheight >= 1060 Then
                                            e.Graphics.DrawString(HiddenDebtPaymentsDataGridView.Rows(row).Cells(0).Value, PrintFont, Brushes.Black, CustomerFullNamePrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenDebtPaymentsDataGridView.Rows(row).Cells(1).Value, PrintFont, Brushes.Black, PaidAmountPrintLocation, VerticalPrintLocation)
                                            VerticalPrintLocation = e.MarginBounds.Top
                                            e.HasMorePages = True
                                            StartAt2 = row + 1
                                            StartAt = i
                                            StartForLoopAt = loop3
                                            DatePrinted = True
                                            DebtsPaymentsColPrinted = True
                                            Return
                                        Else
                                            e.Graphics.DrawString(HiddenDebtPaymentsDataGridView.Rows(row).Cells(0).Value, PrintFont, Brushes.Black, CustomerFullNamePrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenDebtPaymentsDataGridView.Rows(row).Cells(1).Value, PrintFont, Brushes.Black, PaidAmountPrintLocation, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                        End If
                                    Next
                                    StartAt2 = 0
                                    DoNumber = 2
                                Case 2
                                    'print expenses
                                    Dim ExpNamePrintLocation As Single = e.MarginBounds.Left
                                    Dim ExpDescPrintLocaiton As Single = e.MarginBounds.Left + 200
                                    Dim ExpCostPrintLocation As Single = e.MarginBounds.Left + 400
                                    If VerticalPrintLocation + lineheight >= 1020 Then
                                        VerticalPrintLocation = e.MarginBounds.Top
                                        e.HasMorePages = True
                                        StartAt = i
                                        StartForLoopAt = loop3
                                        DatePrinted = True
                                        Return
                                    Else
                                        If ExpensesColPrinted = False Then
                                            e.Graphics.DrawString("EXPENSES", PrintFontBold, Brushes.Blue, e.MarginBounds.Left, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                            e.Graphics.DrawString("EXPENSES NAME", PrintFontBold, Brushes.Black, ExpNamePrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("EXPENSES DESCRIPTION", PrintFontBold, Brushes.Black, ExpDescPrintLocaiton, VerticalPrintLocation)
                                            e.Graphics.DrawString("EXPENSES COST", PrintFontBold, Brushes.Black, ExpCostPrintLocation, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                        End If
                                    End If
                                    For row As Integer = StartAt2 To HiddenExpensesDataGridView.RowCount() - 1
                                        If VerticalPrintLocation + lineheight >= 1060 Then
                                            e.Graphics.DrawString(HiddenExpensesDataGridView.Rows(row).Cells(0).Value, PrintFont, Brushes.Black, ExpNamePrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenExpensesDataGridView.Rows(row).Cells(1).Value, PrintFont, Brushes.Black, ExpDescPrintLocaiton, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenExpensesDataGridView.Rows(row).Cells(2).Value, PrintFont, Brushes.Black, ExpCostPrintLocation, VerticalPrintLocation)
                                            VerticalPrintLocation = e.MarginBounds.Top
                                            e.HasMorePages = True
                                            StartAt2 = row + 1
                                            StartAt = i
                                            StartForLoopAt = loop3
                                            DatePrinted = True
                                            ExpensesColPrinted = True
                                            Return
                                        Else
                                            e.Graphics.DrawString(HiddenExpensesDataGridView.Rows(row).Cells(0).Value, PrintFont, Brushes.Black, ExpNamePrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenExpensesDataGridView.Rows(row).Cells(1).Value, PrintFont, Brushes.Black, ExpDescPrintLocaiton, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenExpensesDataGridView.Rows(row).Cells(2).Value, PrintFont, Brushes.Black, ExpCostPrintLocation, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                        End If
                                    Next
                                    StartAt2 = 0
                                    DoNumber = 3
                                Case 3
                                    'print daily sales
                                    Dim grosssaleprintlocation As Single = e.MarginBounds.Left
                                    Dim totalexpensesprintlocation As Single = e.MarginBounds.Left + 100
                                    Dim totaldiscountprintlocation As Single = e.MarginBounds.Left + 200
                                    Dim Netsaleprintlocation As Single = e.MarginBounds.Left + 300
                                    If VerticalPrintLocation + lineheight >= 1020 Then
                                        VerticalPrintLocation = e.MarginBounds.Top
                                        e.HasMorePages = True
                                        StartAt = i
                                        StartForLoopAt = loop3
                                        DatePrinted = True
                                        Return
                                    Else
                                        If DailySalesColPrinted = False Then
                                            e.Graphics.DrawString("SALES SUMMARY", PrintFontBold, Brushes.Blue, e.MarginBounds.Left, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                            e.Graphics.DrawString("GROSS SALE", PrintFontBold, Brushes.Black, grosssaleprintlocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("TOTAL EXPENSES", PrintFontBold, Brushes.Black, totalexpensesprintlocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("TOTAL DISCOUNT", PrintFontBold, Brushes.Black, totaldiscountprintlocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("NET SALE", PrintFontBold, Brushes.Black, Netsaleprintlocation, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                        End If
                                    End If

                                    For row As Integer = StartAt2 To HiddenDailysalesDataGridView.RowCount() - 1
                                        If VerticalPrintLocation + lineheight >= 1060 Then
                                            VerticalPrintLocation = e.MarginBounds.Top
                                            e.HasMorePages = True
                                            StartAt2 = row
                                            StartAt = i
                                            StartForLoopAt = loop3
                                            DatePrinted = True
                                            DailySalesColPrinted = True
                                            Return
                                        Else
                                            e.Graphics.DrawString(HiddenDailysalesDataGridView.Rows(row).Cells(0).Value, PrintFont, Brushes.Black, grosssaleprintlocation, VerticalPrintLocation)
                                             e.Graphics.DrawString(HiddenDailysalesDataGridView.Rows(row).Cells(2).Value, PrintFont, Brushes.Black, totalexpensesprintlocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenDailysalesDataGridView.Rows(row).Cells(1).Value, PrintFont, Brushes.Black, totaldiscountprintlocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenDailysalesDataGridView.Rows(row).Cells(3).Value, PrintFont, Brushes.Black, Netsaleprintlocation, VerticalPrintLocation)
                                            VerticalPrintLocation += 20
                                        End If
                                        DatePrinted = False
                                        SoldProdColPrinted = False
                                        DebtsPaymentsColPrinted = False
                                        ExpensesColPrinted = False
                                        DailySalesColPrinted = False
                                    Next
                                    StartAt2 = 0
                                    DoNumber = 0
                                    StartForLoopAt = 0
                            End Select
                        Next
                    End If
                End If
            Next
            VerticalPrintLocation += lineheight * 2
            If VerticalPrintLocation + lineheight > 1060 - (TitlePrintFont.GetHeight + (4 * lineheight) + 80) Then
                VerticalPrintLocation = e.MarginBounds.Top
                e.HasMorePages = True
                PrintTotalSummary = True
                DatePrinted = True
            Else
                Dim SummaryTitle As String = "SUMMARY OF SALES RECORDS FROM " + RecordsForm.DateFromLabel.Text + " TO " + RecordsForm.DateToLabel.Text
                Dim TotalSummaryHorizontalPrintLocation As Single = e.MarginBounds.Left
                Dim LabelsHorizontalPrintLocation As Single = e.MarginBounds.Left
                Dim ValuesHorizontalPrintLocation As Single = e.MarginBounds.Left + 200
                e.Graphics.DrawString(SummaryTitle, TitlePrintFont, Brushes.Red, TotalSummaryHorizontalPrintLocation, VerticalPrintLocation)
                VerticalPrintLocation += 20
                e.Graphics.DrawString("GROSS SALE:", PrintFontBold, Brushes.Blue, LabelsHorizontalPrintLocation, VerticalPrintLocation)
                e.Graphics.DrawString(RecordsForm.totgrossTextBox.Text, PrintFont, Brushes.Black, ValuesHorizontalPrintLocation, VerticalPrintLocation)
                VerticalPrintLocation += 20
                e.Graphics.DrawString("EXPENSES:", PrintFontBold, Brushes.Blue, LabelsHorizontalPrintLocation, VerticalPrintLocation)
                e.Graphics.DrawString(RecordsForm.totexpTextBox.Text, PrintFont, Brushes.Black, ValuesHorizontalPrintLocation, VerticalPrintLocation)
                VerticalPrintLocation += 20
                e.Graphics.DrawString("DISCOUNT:", PrintFontBold, Brushes.Blue, LabelsHorizontalPrintLocation, VerticalPrintLocation)
                e.Graphics.DrawString(RecordsForm.totdiscountTextBox.Text, PrintFont, Brushes.Black, ValuesHorizontalPrintLocation, VerticalPrintLocation)
                VerticalPrintLocation += 20
                e.Graphics.DrawString("NET SALE:", PrintFontBold, Brushes.Blue, LabelsHorizontalPrintLocation, VerticalPrintLocation)
                e.Graphics.DrawString(RecordsForm.totnetsaleTextBox.Text, PrintFont, Brushes.Black, ValuesHorizontalPrintLocation, VerticalPrintLocation)
            End If
            e.HasMorePages = False
            Array.Resize(DatesToPrintArray, 0)
            StartAt = 0
            StartAt2 = 0
            DoNumber = 0
            StartForLoopAt = 0
            VerticalPrintLocation = 75
            PrintTotalSummary = False
            DatePrinted = False
            SoldProdColPrinted = False
            DebtsPaymentsColPrinted = False
            ExpensesColPrinted = False
            DailySalesColPrinted = False
            ROJPrinted = False
        Catch ex As Exception
            MessageBox.Show(ex.Message, "5")
        End Try
        MysqlCon.Close()
    End Sub

    Private Sub PrintPreviewDialog1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles PrintPreviewDialog1.FormClosing
        Array.Resize(DatesToPrintArray, 0)
        StartAt = 0
        StartAt2 = 0
        DoNumber = 0
        StartForLoopAt = 0
        VerticalPrintLocation = 75
        DatePrinted = False
        SoldProdColPrinted = False
        DebtsPaymentsColPrinted = False
        ExpensesColPrinted = False
        DailySalesColPrinted = False
        ROJPrinted = False
    End Sub
    Private Sub printPreview_PrintClick(sender As Object, e As EventArgs)
        Try
            PrintDialog1.Document = NewPrintDocument
            If PrintDialog1.ShowDialog() = DialogResult.OK Then
                NewPrintDocument.Print()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub PrintPreviewDialog1_Load(sender As Object, e As EventArgs) Handles PrintPreviewDialog1.Load
        Me.TopMost = True
    End Sub

    Friend PrintFromDate, PrintToDate As Date
    Private DatesToPrintArray() As String = {}
    Private StartAt As Integer = 0
    Private StartAt2 As Integer = 0
    Private DoNumber As Integer = 0
    Private StartForLoopAt As Integer = 0
    Private VerticalPrintLocation As Single = 75
    Private DatePrinted As Boolean = False
    Private SoldProdColPrinted As Boolean = False
    Private DebtsPaymentsColPrinted As Boolean = False
    Private ExpensesColPrinted As Boolean = False
    Private DailySalesColPrinted As Boolean = False
    Private ROJPrinted As Boolean = False
    Private PrintTotalSummary As Boolean = False
    Private Sub NewPrintDocument_PrintPage(sender As Object, e As PrintPageEventArgs) Handles NewPrintDocument.PrintPage
        Dim TitlePrintFont As New Font("Arial Narrow", 9, FontStyle.Bold)
        Dim ROJPrintFont As New Font("Tahoma", 10, FontStyle.Bold)
        Dim ROJCenterPrintLocation As Single = Convert.ToSingle(e.PageBounds.Width / 2 - e.Graphics.MeasureString("ROJ MOTOR PARTS", ROJPrintFont).Width / 2)
        Dim RecordsDatesString As String = "Records from " & PrintFromDate.ToString("yyyy-MM-dd") & " to " & PrintToDate.ToString("yyyy-MM-dd")
        Dim RecordsDatesPrintLocation As Single = Convert.ToSingle(e.PageBounds.Width / 2 - e.Graphics.MeasureString(RecordsDatesString, TitlePrintFont).Width / 2)
        Dim NetSale As Decimal

        Dim PrintFontBold As New Font("Arial Narrow", 8, FontStyle.Bold)
        Dim PrintFont As New Font("Arial Narrow", 8)
        Dim lineheight As Single = PrintFontBold.GetHeight
        Dim HorizontalPrintLocation As Single = e.MarginBounds.Left

        Try
            OpenConnection()

            If ROJPrinted = True Then

            Else
                e.Graphics.DrawString("ROJ MOTOR PARTS", ROJPrintFont, Brushes.Black, ROJCenterPrintLocation, 25)
                e.Graphics.DrawString(RecordsDatesString, TitlePrintFont, Brushes.Black, RecordsDatesPrintLocation, 45)
                ROJPrinted = True
                'get the dates of records to print
                Query = "SELECT * FROM todayssale WHERE date >= '" _
                & PrintFromDate.ToString("yyyy-MM-dd") & "' and date <= '" & PrintToDate.ToString("yyyy-MM-dd") & "'"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                While Reader.Read
                    Array.Resize(DatesToPrintArray, DatesToPrintArray.Length + 1)
                    DatesToPrintArray(DatesToPrintArray.Length - 1) = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                End While
                Reader.Close()
            End If
            For i As Integer = StartAt To DatesToPrintArray.Length() - 1
                'load Soldproducts to hidden soldproducts datagrid
                HiddenSoldProductsDataGridView.Rows.Clear()
                Try
                    Query = "SELECT * FROM todayssoldproducts WHERE date = '" & DatesToPrintArray(i) & "'"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    While Reader.Read
                        Dim datesold = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                        Dim OrderNo = Reader.GetString("orderno")
                        Dim customername = Reader.GetString("customername")
                        Dim productno = Reader.GetInt32("no")
                        Dim productname = Reader.GetString("productname")
                        Dim Productbrand = Reader.GetString("productbrand")
                        Dim productdescription = Reader.GetString("productdescription")
                        Dim PurchaseCost = Reader.GetDecimal("purchasecost")
                        Dim saleprice = Reader.GetDecimal("saleprice")
                        Dim quantity = Reader.GetDecimal("quantity")
                        Dim measuringunit = Reader.GetString("measuringunit")
                        Dim totalprice = Reader.GetDecimal("totalprice")
                        Dim discount = Reader.GetDecimal("discount")
                        Dim discountedprice = Reader.GetDecimal("discountedprice")
                        HiddenSoldProductsDataGridView.Rows.Add(datesold, OrderNo, customername, productno, productname, Productbrand, productdescription,
                                                                PurchaseCost, saleprice, quantity, measuringunit, totalprice, discount, discountedprice)

                    End While
                    Reader.Close()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Error adding Sold Products")
                End Try
                '-------------------------------------
                'load Loaned products to datagrid
                HiddenLoanedDataGridView.Rows.Clear()
                Try
                    Query = "SELECT * FROM loanedproducts WHERE date = '" & DatesToPrintArray(i) & "'"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    While Reader.Read
                        Dim datesold = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                        Dim OrderNo = Reader.GetString("orderno")
                        Dim customername = Reader.GetString("debtor")
                        Dim productno = Reader.GetInt32("no")
                        Dim productname = Reader.GetString("productname")
                        Dim Productbrand = Reader.GetString("productbrand")
                        Dim productdescription = Reader.GetString("productdescription")
                        Dim PurchaseCost = Reader.GetDecimal("purchasecost")
                        Dim saleprice = Reader.GetDecimal("saleprice")
                        Dim quantity = Reader.GetDecimal("quantity")
                        Dim measuringunit = Reader.GetString("measuringunit")
                        Dim totalprice = Reader.GetDecimal("totalprice")
                        Dim discount = Reader.GetDecimal("discount")
                        Dim discountedprice = Reader.GetDecimal("discountedprice")
                        HiddenLoanedDataGridView.Rows.Add(datesold, OrderNo, customername, productno, productname, Productbrand, productdescription,
                                                                PurchaseCost, saleprice, quantity, measuringunit, totalprice, discount, discountedprice)

                    End While
                    Reader.Close()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Error adding Loaned Products")
                End Try
                '-------------------------------------
                'load paymentsmade to datagrid
                DebtPaymentsDataGridView.Rows.Clear()
                Try
                    Query = "SELECT * FROM paymentsmade WHERE date = '" & DatesToPrintArray(i) & "'"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    While Reader.Read
                        Dim paymentdate = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                        Dim orderno = Reader.GetString("orderno")
                        Dim debtorid = Reader.GetString("customerid")
                        Dim debtorname = Reader.GetString("debtor")
                        Dim amountpaid = Reader.GetDecimal("paidamount")
                        DebtPaymentsDataGridView.Rows.Add(paymentdate, orderno, debtorid, debtorname, amountpaid)
                    End While
                    Reader.Close()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Error adding paymentsmade")
                End Try
                '-------------------------------------
                'load expenses to datagrid
                ExpensesDataGridView.Rows.Clear()
                Try
                    Query = "SELECT * FROM todaysexpenses WHERE date = '" & DatesToPrintArray(i) & "'"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    While Reader.Read
                        Dim expDate = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                        Dim expName = Reader.GetString("name")
                        Dim expDescription = Reader.GetString("description")
                        Dim expCost = Reader.GetString("cost")
                        ExpensesDataGridView.Rows.Add(expDate, expName, expDescription, expCost)
                    End While
                    Reader.Close()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Error adding expenses")
                End Try
                '-------------------------------------
                'load sale to datagrid
                HiddenDailysalesDataGridView.Rows.Clear()
                Try
                    Query = "SELECT * FROM todayssale WHERE date = '" & DatesToPrintArray(i) & "'"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    While Reader.Read
                        Dim salesdate = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                        Dim grosssale = Reader.GetDecimal("grosssale")
                        Dim totaldiscount = Reader.GetDecimal("totaldiscount")
                        Dim totalexpenses = Reader.GetDecimal("totalexpenses")
                        Dim todaysnetsale As Decimal = grosssale - totaldiscount - totalexpenses
                        HiddenDailysalesDataGridView.Rows.Add(salesdate, grosssale, totaldiscount, totalexpenses, todaysnetsale)
                        NetSale = todaysnetsale
                    End While
                    Reader.Close()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Error adding sale")
                End Try

                If NetSale = 0 And HiddenLoanedDataGridView.RowCount() = 0 Then

                Else
                    'Print date
                    If VerticalPrintLocation + lineheight >= 758 Then
                        VerticalPrintLocation = e.MarginBounds.Top
                        e.HasMorePages = True
                        StartAt = i
                        Return
                    Else
                        If DatePrinted = False Then
                            e.Graphics.DrawString(DatesToPrintArray(i), PrintFontBold, Brushes.Red, HorizontalPrintLocation, VerticalPrintLocation)
                            VerticalPrintLocation += 15
                        Else

                        End If
                    End If
                    If PrintTotalSummary = False Then
                        For loop3 As Integer = StartForLoopAt To 4
                            Select Case DoNumber
                                Case 0 'print sold products
                                    Dim OrderNoPrintLocation As Single = e.MarginBounds.Left
                                    Dim CustomerNameLocation As Single = OrderNoPrintLocation + 95
                                    Dim ProductNoPrintLocation As Single = CustomerNameLocation + 105
                                    Dim ProductNameLocation As Single = ProductNoPrintLocation + 95
                                    Dim ProductBrandLocation As Single = ProductNameLocation + 105
                                    Dim ProductDescLocation As Single = ProductBrandLocation + 85
                                    Dim purchasecostLocation As Single = ProductDescLocation + 85
                                    Dim SalePriceLocation As Single = purchasecostLocation + 70
                                    Dim QuantityLocation As Single = SalePriceLocation + 70
                                    Dim MeasuringUnitLocation As Single = QuantityLocation + 70
                                    Dim TotalPriceLocation As Single = MeasuringUnitLocation + 70
                                    Dim DiscountLocation As Single = TotalPriceLocation + 70
                                    Dim DiscountedPriceLocation As Single = DiscountLocation + 70

                                    If VerticalPrintLocation + lineheight >= 770 Then
                                        VerticalPrintLocation = e.MarginBounds.Top
                                        e.HasMorePages = True
                                        StartAt = i
                                        StartForLoopAt = loop3
                                        DatePrinted = True
                                        Return
                                    Else
                                        If SoldProdColPrinted = False Then
                                            e.Graphics.DrawString("SOLD PRODUCTS", PrintFontBold, Brushes.Blue, e.MarginBounds.Left, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                            e.Graphics.DrawString("ORDER NO", PrintFontBold, Brushes.Black, OrderNoPrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("CUSTOMER NAME", PrintFontBold, Brushes.Black, CustomerNameLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("PRODUCT NO", PrintFontBold, Brushes.Black, ProductNoPrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("PRODUCT NAME", PrintFontBold, Brushes.Black, ProductNameLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("BRAND", PrintFontBold, Brushes.Black, ProductBrandLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("DESCRIPTION", PrintFontBold, Brushes.Black, ProductDescLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("PUR. COST", PrintFontBold, Brushes.Black, purchasecostLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("SALE PRICE", PrintFontBold, Brushes.Black, SalePriceLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("QUANTITY", PrintFontBold, Brushes.Black, QuantityLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("MEAS. UNIT", PrintFontBold, Brushes.Black, MeasuringUnitLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("TOTAL PRICE", PrintFontBold, Brushes.Black, TotalPriceLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("DISCOUNT", PrintFontBold, Brushes.Black, DiscountLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("FINAL PRICE", PrintFontBold, Brushes.Black, DiscountedPriceLocation, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                        End If
                                    End If

                                    For row As Integer = StartAt2 To HiddenSoldProductsDataGridView.RowCount() - 1
                                        If VerticalPrintLocation + lineheight >= 810 Then
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView.Rows(row).Cells(1).Value, PrintFont, Brushes.Black, OrderNoPrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(HiddenSoldProductsDataGridView.Rows(row).Cells(2).Value, 18), PrintFont, Brushes.Black, CustomerNameLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView.Rows(row).Cells(3).Value, PrintFont, Brushes.Black, ProductNoPrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(HiddenSoldProductsDataGridView.Rows(row).Cells(4).Value, 18), PrintFont, Brushes.Black, ProductNameLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(HiddenSoldProductsDataGridView.Rows(row).Cells(5).Value, 12), PrintFont, Brushes.Black, ProductBrandLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(HiddenSoldProductsDataGridView.Rows(row).Cells(6).Value, 15), PrintFont, Brushes.Black, ProductDescLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView.Rows(row).Cells(7).Value, PrintFont, Brushes.Black, purchasecostLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView.Rows(row).Cells(8).Value, PrintFont, Brushes.Black, SalePriceLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView.Rows(row).Cells(9).Value, PrintFont, Brushes.Black, QuantityLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView.Rows(row).Cells(10).Value, PrintFont, Brushes.Black, MeasuringUnitLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView.Rows(row).Cells(11).Value, PrintFont, Brushes.Black, TotalPriceLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView.Rows(row).Cells(12).Value, PrintFont, Brushes.Black, DiscountLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView.Rows(row).Cells(13).Value, PrintFont, Brushes.Black, DiscountedPriceLocation, VerticalPrintLocation)
                                            VerticalPrintLocation = e.MarginBounds.Top
                                            e.HasMorePages = True
                                            StartAt2 = row + 1
                                            StartAt = i
                                            StartForLoopAt = loop3
                                            DatePrinted = True
                                            SoldProdColPrinted = True
                                            Return
                                        Else
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView.Rows(row).Cells(1).Value, PrintFont, Brushes.Black, OrderNoPrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(HiddenSoldProductsDataGridView.Rows(row).Cells(2).Value, 18), PrintFont, Brushes.Black, CustomerNameLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView.Rows(row).Cells(3).Value, PrintFont, Brushes.Black, ProductNoPrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(HiddenSoldProductsDataGridView.Rows(row).Cells(4).Value, 18), PrintFont, Brushes.Black, ProductNameLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(HiddenSoldProductsDataGridView.Rows(row).Cells(5).Value, 12), PrintFont, Brushes.Black, ProductBrandLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(HiddenSoldProductsDataGridView.Rows(row).Cells(6).Value, 15), PrintFont, Brushes.Black, ProductDescLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView.Rows(row).Cells(7).Value, PrintFont, Brushes.Black, purchasecostLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView.Rows(row).Cells(8).Value, PrintFont, Brushes.Black, SalePriceLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView.Rows(row).Cells(9).Value, PrintFont, Brushes.Black, QuantityLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView.Rows(row).Cells(10).Value, PrintFont, Brushes.Black, MeasuringUnitLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView.Rows(row).Cells(11).Value, PrintFont, Brushes.Black, TotalPriceLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView.Rows(row).Cells(12).Value, PrintFont, Brushes.Black, DiscountLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenSoldProductsDataGridView.Rows(row).Cells(13).Value, PrintFont, Brushes.Black, DiscountedPriceLocation, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                        End If
                                    Next
                                    StartAt2 = 0
                                    DoNumber = 1
                                Case 1 'print loaned products
                                    Dim OrderNoPrintLocation As Single = e.MarginBounds.Left
                                    Dim CustomerNameLocation As Single = OrderNoPrintLocation + 95
                                    Dim ProductNoPrintLocation As Single = CustomerNameLocation + 105
                                    Dim ProductNameLocation As Single = ProductNoPrintLocation + 95
                                    Dim ProductBrandLocation As Single = ProductNameLocation + 105
                                    Dim ProductDescLocation As Single = ProductBrandLocation + 85
                                    Dim purchasecostLocation As Single = ProductDescLocation + 85
                                    Dim SalePriceLocation As Single = purchasecostLocation + 70
                                    Dim QuantityLocation As Single = SalePriceLocation + 70
                                    Dim MeasuringUnitLocation As Single = QuantityLocation + 70
                                    Dim TotalPriceLocation As Single = MeasuringUnitLocation + 70
                                    Dim DiscountLocation As Single = TotalPriceLocation + 70
                                    Dim DiscountedPriceLocation As Single = DiscountLocation + 70

                                    If VerticalPrintLocation + lineheight >= 770 Then
                                        VerticalPrintLocation = e.MarginBounds.Top
                                        e.HasMorePages = True
                                        StartAt = i
                                        StartForLoopAt = loop3
                                        DatePrinted = True
                                        Return
                                    Else
                                        If SoldProdColPrinted = False Then
                                            e.Graphics.DrawString("LOANED PRODUCTS", PrintFontBold, Brushes.Blue, e.MarginBounds.Left, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                            e.Graphics.DrawString("ORDER NO", PrintFontBold, Brushes.Black, OrderNoPrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("CUSTOMER NAME", PrintFontBold, Brushes.Black, CustomerNameLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("PRODUCT NO", PrintFontBold, Brushes.Black, ProductNoPrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("PRODUCT NAME", PrintFontBold, Brushes.Black, ProductNameLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("BRAND", PrintFontBold, Brushes.Black, ProductBrandLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("DESCRIPTION", PrintFontBold, Brushes.Black, ProductDescLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("PUR. COST", PrintFontBold, Brushes.Black, purchasecostLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("SALE PRICE", PrintFontBold, Brushes.Black, SalePriceLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("QUANTITY", PrintFontBold, Brushes.Black, QuantityLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("MEAS. UNIT", PrintFontBold, Brushes.Black, MeasuringUnitLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("TOTAL PRICE", PrintFontBold, Brushes.Black, TotalPriceLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("DISCOUNT", PrintFontBold, Brushes.Black, DiscountLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("FINAL PRICE", PrintFontBold, Brushes.Black, DiscountedPriceLocation, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                        End If
                                    End If

                                    For row As Integer = StartAt2 To HiddenLoanedDataGridView.RowCount() - 1
                                        If VerticalPrintLocation + lineheight >= 810 Then
                                            e.Graphics.DrawString(HiddenLoanedDataGridView.Rows(row).Cells(1).Value, PrintFont, Brushes.Black, OrderNoPrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(HiddenLoanedDataGridView.Rows(row).Cells(2).Value, 18), PrintFont, Brushes.Black, CustomerNameLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenLoanedDataGridView.Rows(row).Cells(3).Value, PrintFont, Brushes.Black, ProductNoPrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(HiddenLoanedDataGridView.Rows(row).Cells(4).Value, 18), PrintFont, Brushes.Black, ProductNameLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(HiddenLoanedDataGridView.Rows(row).Cells(5).Value, 12), PrintFont, Brushes.Black, ProductBrandLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(HiddenLoanedDataGridView.Rows(row).Cells(6).Value, 15), PrintFont, Brushes.Black, ProductDescLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenLoanedDataGridView.Rows(row).Cells(7).Value, PrintFont, Brushes.Black, purchasecostLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenLoanedDataGridView.Rows(row).Cells(8).Value, PrintFont, Brushes.Black, SalePriceLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenLoanedDataGridView.Rows(row).Cells(9).Value, PrintFont, Brushes.Black, QuantityLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenLoanedDataGridView.Rows(row).Cells(10).Value, PrintFont, Brushes.Black, MeasuringUnitLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenLoanedDataGridView.Rows(row).Cells(11).Value, PrintFont, Brushes.Black, TotalPriceLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenLoanedDataGridView.Rows(row).Cells(12).Value, PrintFont, Brushes.Black, DiscountLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenLoanedDataGridView.Rows(row).Cells(13).Value, PrintFont, Brushes.Black, DiscountedPriceLocation, VerticalPrintLocation)
                                            VerticalPrintLocation = e.MarginBounds.Top
                                            e.HasMorePages = True
                                            StartAt2 = row + 1
                                            StartAt = i
                                            StartForLoopAt = loop3
                                            DatePrinted = True
                                            SoldProdColPrinted = True
                                            Return
                                        Else
                                            e.Graphics.DrawString(HiddenLoanedDataGridView.Rows(row).Cells(1).Value, PrintFont, Brushes.Black, OrderNoPrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(HiddenLoanedDataGridView.Rows(row).Cells(2).Value, 18), PrintFont, Brushes.Black, CustomerNameLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenLoanedDataGridView.Rows(row).Cells(3).Value, PrintFont, Brushes.Black, ProductNoPrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(HiddenLoanedDataGridView.Rows(row).Cells(4).Value, 18), PrintFont, Brushes.Black, ProductNameLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(HiddenLoanedDataGridView.Rows(row).Cells(5).Value, 10), PrintFont, Brushes.Black, ProductBrandLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(HiddenLoanedDataGridView.Rows(row).Cells(6).Value, 13), PrintFont, Brushes.Black, ProductDescLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenLoanedDataGridView.Rows(row).Cells(7).Value, PrintFont, Brushes.Black, purchasecostLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenLoanedDataGridView.Rows(row).Cells(8).Value, PrintFont, Brushes.Black, SalePriceLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenLoanedDataGridView.Rows(row).Cells(9).Value, PrintFont, Brushes.Black, QuantityLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenLoanedDataGridView.Rows(row).Cells(10).Value, PrintFont, Brushes.Black, MeasuringUnitLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenLoanedDataGridView.Rows(row).Cells(11).Value, PrintFont, Brushes.Black, TotalPriceLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenLoanedDataGridView.Rows(row).Cells(12).Value, PrintFont, Brushes.Black, DiscountLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenLoanedDataGridView.Rows(row).Cells(13).Value, PrintFont, Brushes.Black, DiscountedPriceLocation, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                        End If
                                    Next
                                    StartAt2 = 0
                                    DoNumber = 2
                                Case 2 'print payments made
                                    Dim OrderNOPrintLocation As Single = e.MarginBounds.Left
                                    Dim DebtorsIDprintLocation As Single = OrderNOPrintLocation + 100
                                    Dim DebtorsFullNamePrintLocation As Single = DebtorsIDprintLocation + 100
                                    Dim PaidAmountPrintLocation As Single = DebtorsFullNamePrintLocation + 150

                                    If VerticalPrintLocation + lineheight >= 770 Then
                                        VerticalPrintLocation = e.MarginBounds.Top
                                        e.HasMorePages = True
                                        StartAt = i
                                        StartForLoopAt = loop3
                                        DatePrinted = True
                                        Return
                                    Else
                                        If DebtsPaymentsColPrinted = False Then
                                            e.Graphics.DrawString("DEBT'S PAYMENTS", PrintFontBold, Brushes.Blue, e.MarginBounds.Left, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                            e.Graphics.DrawString("ORDER NO", PrintFontBold, Brushes.Black, OrderNOPrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("DEBTOR'S ID", PrintFontBold, Brushes.Black, DebtorsIDprintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("DEBTORS FULL NAME", PrintFontBold, Brushes.Black, DebtorsFullNamePrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("PAID AMOUNT", PrintFontBold, Brushes.Black, PaidAmountPrintLocation, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                        End If
                                    End If
                                    For row As Integer = StartAt2 To DebtPaymentsDataGridView.RowCount() - 1
                                        If VerticalPrintLocation + lineheight >= 810 Then
                                            e.Graphics.DrawString(DebtPaymentsDataGridView.Rows(row).Cells(1).Value, PrintFont, Brushes.Black, OrderNOPrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(DebtPaymentsDataGridView.Rows(row).Cells(2).Value, PrintFont, Brushes.Black, DebtorsIDprintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(DebtPaymentsDataGridView.Rows(row).Cells(3).Value, 24), PrintFont, Brushes.Black, DebtorsFullNamePrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(DebtPaymentsDataGridView.Rows(row).Cells(4).Value, PrintFont, Brushes.Black, PaidAmountPrintLocation, VerticalPrintLocation)
                                            VerticalPrintLocation = e.MarginBounds.Top
                                            e.HasMorePages = True
                                            StartAt2 = row + 1
                                            StartAt = i
                                            StartForLoopAt = loop3
                                            DatePrinted = True
                                            DebtsPaymentsColPrinted = True
                                            Return
                                        Else
                                            e.Graphics.DrawString(DebtPaymentsDataGridView.Rows(row).Cells(1).Value, PrintFont, Brushes.Black, OrderNOPrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(DebtPaymentsDataGridView.Rows(row).Cells(2).Value, PrintFont, Brushes.Black, DebtorsIDprintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(LimitStrLen(DebtPaymentsDataGridView.Rows(row).Cells(3).Value, 24), PrintFont, Brushes.Black, DebtorsFullNamePrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(DebtPaymentsDataGridView.Rows(row).Cells(4).Value, PrintFont, Brushes.Black, PaidAmountPrintLocation, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                        End If
                                    Next
                                    StartAt2 = 0
                                    DoNumber = 3
                                Case 3 'print expenses
                                    Dim ExpNamePrintLocation As Single = e.MarginBounds.Left
                                    Dim ExpDescPrintLocaiton As Single = ExpNamePrintLocation + 150
                                    Dim ExpCostPrintLocation As Single = ExpDescPrintLocaiton + 150
                                    If VerticalPrintLocation + lineheight >= 770 Then
                                        VerticalPrintLocation = e.MarginBounds.Top
                                        e.HasMorePages = True
                                        StartAt = i
                                        StartForLoopAt = loop3
                                        DatePrinted = True
                                        Return
                                    Else
                                        If ExpensesColPrinted = False Then
                                            e.Graphics.DrawString("EXPENSES", PrintFontBold, Brushes.Blue, e.MarginBounds.Left, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                            e.Graphics.DrawString("EXPENSE NAME", PrintFontBold, Brushes.Black, ExpNamePrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("EXPENSE DESCRIPTION", PrintFontBold, Brushes.Black, ExpDescPrintLocaiton, VerticalPrintLocation)
                                            e.Graphics.DrawString("EXPENSE COST", PrintFontBold, Brushes.Black, ExpCostPrintLocation, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                        End If
                                    End If
                                    For row As Integer = StartAt2 To ExpensesDataGridView.RowCount() - 1
                                        If VerticalPrintLocation + lineheight >= 810 Then
                                            e.Graphics.DrawString(ExpensesDataGridView.Rows(row).Cells(1).Value, PrintFont, Brushes.Black, ExpNamePrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(ExpensesDataGridView.Rows(row).Cells(2).Value, PrintFont, Brushes.Black, ExpDescPrintLocaiton, VerticalPrintLocation)
                                            e.Graphics.DrawString(ExpensesDataGridView.Rows(row).Cells(3).Value, PrintFont, Brushes.Black, ExpCostPrintLocation, VerticalPrintLocation)
                                            VerticalPrintLocation = e.MarginBounds.Top
                                            e.HasMorePages = True
                                            StartAt2 = row + 1
                                            StartAt = i
                                            StartForLoopAt = loop3
                                            DatePrinted = True
                                            ExpensesColPrinted = True
                                            Return
                                        Else
                                            e.Graphics.DrawString(ExpensesDataGridView.Rows(row).Cells(1).Value, PrintFont, Brushes.Black, ExpNamePrintLocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(ExpensesDataGridView.Rows(row).Cells(2).Value, PrintFont, Brushes.Black, ExpDescPrintLocaiton, VerticalPrintLocation)
                                            e.Graphics.DrawString(ExpensesDataGridView.Rows(row).Cells(3).Value, PrintFont, Brushes.Black, ExpCostPrintLocation, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                        End If
                                    Next
                                    StartAt2 = 0
                                    DoNumber = 4
                                Case 4
                                    'print daily sales
                                    Dim grosssaleprintlocation As Single = e.MarginBounds.Left
                                    Dim totaldiscountprintlocation As Single = e.MarginBounds.Left + 100
                                    Dim totalexpensesprintlocation As Single = e.MarginBounds.Left + 200
                                    Dim Netsaleprintlocation As Single = e.MarginBounds.Left + 300
                                    If VerticalPrintLocation + lineheight >= 770 Then
                                        VerticalPrintLocation = e.MarginBounds.Top
                                        e.HasMorePages = True
                                        StartAt = i
                                        StartForLoopAt = loop3
                                        DatePrinted = True
                                        Return
                                    Else
                                        If DailySalesColPrinted = False Then
                                            e.Graphics.DrawString("SALES SUMMARY", PrintFontBold, Brushes.Blue, e.MarginBounds.Left, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                            e.Graphics.DrawString("GROSS SALE", PrintFontBold, Brushes.Black, grosssaleprintlocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("TOTAL DISCOUNT", PrintFontBold, Brushes.Black, totalexpensesprintlocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("TOTAL EXPENSES", PrintFontBold, Brushes.Black, totaldiscountprintlocation, VerticalPrintLocation)
                                            e.Graphics.DrawString("NET SALE", PrintFontBold, Brushes.Black, Netsaleprintlocation, VerticalPrintLocation)
                                            VerticalPrintLocation += 15
                                        End If
                                    End If

                                    For row As Integer = StartAt2 To HiddenDailysalesDataGridView.RowCount() - 1
                                        If VerticalPrintLocation + lineheight >= 810 Then
                                            VerticalPrintLocation = e.MarginBounds.Top
                                            e.HasMorePages = True
                                            StartAt2 = row
                                            StartAt = i
                                            StartForLoopAt = loop3
                                            DatePrinted = True
                                            DailySalesColPrinted = True
                                            Return
                                        Else
                                            e.Graphics.DrawString(HiddenDailysalesDataGridView.Rows(row).Cells(1).Value, PrintFont, Brushes.Black, grosssaleprintlocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenDailysalesDataGridView.Rows(row).Cells(2).Value, PrintFont, Brushes.Black, totalexpensesprintlocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenDailysalesDataGridView.Rows(row).Cells(3).Value, PrintFont, Brushes.Black, totaldiscountprintlocation, VerticalPrintLocation)
                                            e.Graphics.DrawString(HiddenDailysalesDataGridView.Rows(row).Cells(4).Value, PrintFont, Brushes.Black, Netsaleprintlocation, VerticalPrintLocation)
                                            VerticalPrintLocation += 20
                                        End If
                                        DatePrinted = False
                                        SoldProdColPrinted = False
                                        DebtsPaymentsColPrinted = False
                                        ExpensesColPrinted = False
                                        DailySalesColPrinted = False
                                    Next
                                    StartAt2 = 0
                                    DoNumber = 0
                                    StartForLoopAt = 0
                            End Select
                        Next
                    End If
                End If
            Next
            VerticalPrintLocation += lineheight * 2
            If VerticalPrintLocation + lineheight > 825 - (TitlePrintFont.GetHeight + (4 * lineheight) + 80) Then
                VerticalPrintLocation = e.MarginBounds.Top
                e.HasMorePages = True
                PrintTotalSummary = True
                DatePrinted = True
                Return
            Else
                Dim SummaryTitle As String = "SUMMARY OF SALES RECORDS FROM " + RecordsForm.DateFromLabel.Text + " TO " + RecordsForm.DateToLabel.Text
                Dim TotalSummaryHorizontalPrintLocation As Single = e.MarginBounds.Left
                Dim LabelsHorizontalPrintLocation As Single = e.MarginBounds.Left
                Dim ValuesHorizontalPrintLocation As Single = e.MarginBounds.Left + 100
                e.Graphics.DrawString(SummaryTitle, TitlePrintFont, Brushes.Red, TotalSummaryHorizontalPrintLocation, VerticalPrintLocation)
                VerticalPrintLocation += 20
                e.Graphics.DrawString("GROSS SALE:", PrintFontBold, Brushes.Blue, LabelsHorizontalPrintLocation, VerticalPrintLocation)
                e.Graphics.DrawString(RecordsForm.totgrossTextBox.Text, PrintFont, Brushes.Black, ValuesHorizontalPrintLocation, VerticalPrintLocation)
                VerticalPrintLocation += 20
                e.Graphics.DrawString("EXPENSES:", PrintFontBold, Brushes.Blue, LabelsHorizontalPrintLocation, VerticalPrintLocation)
                e.Graphics.DrawString(RecordsForm.totexpTextBox.Text, PrintFont, Brushes.Black, ValuesHorizontalPrintLocation, VerticalPrintLocation)
                VerticalPrintLocation += 20
                e.Graphics.DrawString("DISCOUNT:", PrintFontBold, Brushes.Blue, LabelsHorizontalPrintLocation, VerticalPrintLocation)
                e.Graphics.DrawString(RecordsForm.totdiscountTextBox.Text, PrintFont, Brushes.Black, ValuesHorizontalPrintLocation, VerticalPrintLocation)
                VerticalPrintLocation += 20
                e.Graphics.DrawString("NET SALE:", PrintFontBold, Brushes.Blue, LabelsHorizontalPrintLocation, VerticalPrintLocation)
                e.Graphics.DrawString(RecordsForm.totnetsaleTextBox.Text, PrintFont, Brushes.Black, ValuesHorizontalPrintLocation, VerticalPrintLocation)
            End If
            e.HasMorePages = False
            Array.Resize(DatesToPrintArray, 0)
            StartAt = 0
            StartAt2 = 0
            DoNumber = 0
            StartForLoopAt = 0
            VerticalPrintLocation = 75
            PrintTotalSummary = False
            DatePrinted = False
            SoldProdColPrinted = False
            DebtsPaymentsColPrinted = False
            ExpensesColPrinted = False
            DailySalesColPrinted = False
            ROJPrinted = False
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        MysqlCon.Close()
    End Sub

    Private Sub PrintButton_Click(sender As Object, e As EventArgs) Handles PrintButton.Click
        Dim ShortBondPaper As New PaperSize("Short Bond Paper", 1100, 850)
        Dim SetMargin As New Margins(25, 25, 25, 25)
        With NewPrintDocument.DefaultPageSettings
            .PaperSize = ShortBondPaper
            .Margins = SetMargin
        End With
        PrintFromDate = FromDateTimePicker.Text
        PrintToDate = ToDateTimePicker.Text
        Dim b As New ToolStripButton
        b.Image = CType(PrintPreviewDialog1.Controls(1), ToolStrip).ImageList.Images(0)
        b.ToolTipText = "Print"
        b.DisplayStyle = ToolStripItemDisplayStyle.Image
        AddHandler b.Click, AddressOf printPreview_PrintClick
        CType(PrintPreviewDialog1.Controls(1), ToolStrip).Items.RemoveAt(0)
        CType(PrintPreviewDialog1.Controls(1), ToolStrip).Items.Insert(0, b)
        PrintPreviewDialog1.Document = NewPrintDocument
        PrintPreviewDialog1.ShowDialog()
    End Sub

    'Private Sub PrintSearchedRecordsButton_Click(sender As Object, e As EventArgs) Handles PrintSearchedRecordsButton.Click
    '    Dim ShortBondPaper As New PaperSize("Long Bond Paper", 850, 1100)
    '    Dim SetMargin As New Margins(25, 25, 25, 25)
    '    With AllRecordsPrintDocument.DefaultPageSettings
    '        .PaperSize = ShortBondPaper
    '        .Margins = SetMargin
    '    End With
    '    PrintFromDate = FromDateTimePicker.Text
    '    PrintToDate = ToDateTimePicker.Text
    '    Dim b As New ToolStripButton
    '    b.Image = CType(PrintPreviewDialog1.Controls(1), ToolStrip).ImageList.Images(0)
    '    b.ToolTipText = "Print"
    '    b.DisplayStyle = ToolStripItemDisplayStyle.Image
    '    AddHandler b.Click, AddressOf printPreview_PrintClick
    '    CType(PrintPreviewDialog1.Controls(1), ToolStrip).Items.RemoveAt(0)
    '    CType(PrintPreviewDialog1.Controls(1), ToolStrip).Items.Insert(0, b)
    '    PrintPreviewDialog1.Document = AllRecordsPrintDocument
    '    PrintPreviewDialog1.ShowDialog()
    'End Sub

    Friend BackUpORsearch, DeleteORBackup, PrintRecords As Boolean
    Private Sub SearchRecordsForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim ScreenResHeight, ScreenResWidth, yLocation, xLocation As Integer
        ScreenResHeight = Screen.PrimaryScreen.WorkingArea.Height
        ScreenResWidth = Screen.PrimaryScreen.WorkingArea.Width
        yLocation = (ScreenResHeight - 757) / 2 + 757 - 406
        xLocation = (ScreenResWidth - 1312) / 2 + 1312 - 236
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New Point(xLocation, yLocation)
        If BackUpORsearch = False And DeleteORBackup = False And PrintRecords = False Then
            MakebackupButton.Visible = False
            DeleteSlctdRecButton.Visible = False
            PrintSearchedRecordsButton.Visible = False
        Else
            If BackUpORsearch = True Then
                MakebackupButton.Visible = True
                DeleteSlctdRecButton.Visible = False
                PrintSearchedRecordsButton.Visible = False
            ElseIf PrintRecords = True Then
                DeleteSlctdRecButton.Visible = False
                MakebackupButton.Visible = False
                PrintButton.Visible = True
            Else
                DeleteSlctdRecButton.Visible = True
                MakebackupButton.Visible = False
                PrintSearchedRecordsButton.Visible = False
            End If
        End If
    End Sub

    Private Sub SearchButton_Click(sender As Object, e As EventArgs) Handles SearchButton.Click
        Dim flag As Integer
        Dim dateto, datefrom As String
        Try
            OpenConnection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        RecordsForm.SoldProductsDataGridView.Rows.Clear()
        RecordsForm.ExpensesDataGridView.Rows.Clear()
        RecordsForm.DailysalesDataGridView.Rows.Clear()
        RecordsForm.DebtPaymentsDataGridView.Rows.Clear()
        RecordsForm.DateFromLabel.Text = FromDateTimePicker.Text
        RecordsForm.DateToLabel.Text = ToDateTimePicker.Text

        TOTGROSSSALE = 0
        TOTDISCOUNT = 0
        TOTEXPENSES = 0
        TOTNETSALE = 0

        'sold products
        Try
            Dim No As Integer = 0
            Dim ColorAlternater As Boolean = False
            Query = "SELECT * FROM (SELECT date, orderno, customername, no, productname, productbrand, productdescription, purchasecost, saleprice, quantity, measuringunit, totalprice, discount, discountedprice, time, no as no2 
                     FROM todayssoldproducts 
                     UNION ALL
                     SELECT date, orderno, debtor, no, productname, productbrand, productdescription, purchasecost, saleprice, quantity, measuringunit, totalprice, discount, discountedprice, time, no + no as no2 
                     FROM loanedproducts) AS unionedsoldandloanedproducts
                     WHERE date >= '" & FromDateTimePicker.Text & "' AND date <= '" & ToDateTimePicker.Text & "'" _
                     & "ORDER BY date desc, time desc, productname asc"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim a = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                Dim OrderNo = Reader.GetString("orderno")
                Dim f = Reader.GetString("customername")
                Dim productno = Reader.GetInt32("no")
                Dim b = Reader.GetString("productname")
                Dim c = Reader.GetString("productbrand")
                Dim d = Reader.GetString("productdescription")
                Dim PurchaseCost = Reader.GetDecimal("purchasecost")
                Dim g = Reader.GetDecimal("saleprice")
                Dim h = Reader.GetDecimal("quantity")
                Dim i = Reader.GetString("measuringunit")
                Dim j = Reader.GetDecimal("totalprice")
                Dim k = Reader.GetDecimal("discount")
                Dim l = Reader.GetDecimal("discountedprice")
                Dim no2 = Reader.GetInt32("no2")
                Dim Loaned As Boolean = False
                If productno <> no2 Then
                    Loaned = True
                End If
                RecordsForm.SoldProductsDataGridView.Rows.Add(a, OrderNo, f, productno, b, c, d, PurchaseCost, g, h, i, j, k, l)
                If No > 0 Then
                    If OrderNo = RecordsForm.SoldProductsDataGridView(1, No - 1).Value() Then
                        RecordsForm.SoldProductsDataGridView.Rows(No).DefaultCellStyle.BackColor = RecordsForm.SoldProductsDataGridView.Rows(No - 1).DefaultCellStyle.BackColor
                    Else
                        If ColorAlternater = True Then
                            RecordsForm.SoldProductsDataGridView.Rows(No).DefaultCellStyle.BackColor = Color.White
                            ColorAlternater = False
                        Else
                            RecordsForm.SoldProductsDataGridView.Rows(No).DefaultCellStyle.BackColor = Color.Gray
                            ColorAlternater = True
                        End If
                    End If
                End If
                If Loaned = True Then
                    RecordsForm.SoldProductsDataGridView.Rows(No).Cells(13).Style.BackColor = Color.Red
                End If
                No += 1
            End While
            Reader.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        'daily expenses
        Try
            Query = "select * from todaysexpenses where date >= '" & FromDateTimePicker.Text _
            & "' and date <= '" & ToDateTimePicker.Text & "' order by date desc "
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim a = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                Dim b = Reader.GetString("name")
                Dim c = Reader.GetString("description")
                Dim d = Reader.GetString("cost")
                RecordsForm.ExpensesDataGridView.Rows.Add(a, b, c, d)
            End While
            Reader.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        'daily sales
        Try
            Query = "select * from todayssale where date >= '" & FromDateTimePicker.Text _
            & "' and date <= '" & ToDateTimePicker.Text & "' order by date desc "
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim a = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                Dim b = Reader.GetDecimal("grosssale")
                Dim c = Reader.GetDecimal("totaldiscount")
                Dim d = Reader.GetDecimal("totalexpenses")
                Dim netsale As Decimal = b - c - d
                RecordsForm.DailysalesDataGridView.Rows.Add(a, b, c, d, netsale)
                If flag = 0 Then
                    dateto = a.ToString()
                End If
                datefrom = a.ToString()
                flag = 1
                TOTGROSSSALE += b
                TOTDISCOUNT += c
                TOTEXPENSES += d
                TOTNETSALE += netsale
            End While
            RecordsForm.totgrossTextBox.Text = TOTGROSSSALE.ToString("n2")
            RecordsForm.totexpTextBox.Text = TOTEXPENSES.ToString("n2")
            RecordsForm.totdiscountTextBox.Text = TOTDISCOUNT.ToString("n2")
            RecordsForm.totnetsaleTextBox.Text = TOTNETSALE.ToString("n2")

            Reader.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        ''display debt payments
        'RecordsForm.DebtPaymentsDataGridView.Rows.Clear()
        'For i = 0 To RecordsForm.datetimeofunpaidcredits.Count - 1
        '    'Display debt payments 
        '    Query = "select * from paymentsmade where datetime = '" _
        '    & RecordsForm.datetimeofunpaidcredits(i).ToString("yyyy-MM-dd HH:mm:ss") _
        '    & "'" & "and date >= '" & FromDateTimePicker.Text & "'  and date <= '" _
        '    & ToDateTimePicker.Text & "' order by date desc"
        '    Command = New MySqlCommand(Query, MysqlCon)
        '    Reader = Command.ExecuteReader
        '    While Reader.Read
        '        Dim a = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
        '        Dim b = Reader.GetString("customerfullname")
        '        Dim c = Reader.GetDecimal("paidamount")
        '        Dim d = Reader.GetDateTime("datetime")
        '        RecordsForm.DebtPaymentsDataGridView.Rows.Add(a, b, c, d)
        '    End While
        '    Reader.Close()
        'Next
        Query = "SELECT * FROM paymentsmade 
                 WHERE date >= '" & FromDateTimePicker.Text & "' and date <= '" & ToDateTimePicker.Text & "' " _
                 & "ORDER BY datetime desc"
        Command = New MySqlCommand(Query, MysqlCon)
        Reader = Command.ExecuteReader
        While Reader.Read
            Dim a = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
            Dim b = Reader.GetString("orderno")
            Dim c = Reader.GetString("customerid")
            Dim d = Reader.GetString("debtor")
            Dim f = Reader.GetDecimal("paidamount")
            RecordsForm.DebtPaymentsDataGridView.Rows.Add(a, b, c, d, f)
        End While
        Reader.Close()

        RecordsForm.SoldProductsDataGridView.ClearSelection()
        RecordsForm.ExpensesDataGridView.ClearSelection()
        RecordsForm.DailysalesDataGridView.ClearSelection()
        RecordsForm.DebtPaymentsDataGridView.ClearSelection()

        MysqlCon.Close()
    End Sub
End Class