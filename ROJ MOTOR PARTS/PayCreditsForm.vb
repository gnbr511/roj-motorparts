﻿Imports MySql.Data.MySqlClient
Public Class PayCreditsForm
    Friend SelectedDebtor As String
    Private Sub PayCreditsForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            OpenConnection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Dim name As String
        Dim payment, amountpaid, rempayment, discount As Decimal
        Try
            Query = "select * from credits 
                    inner join (select sum(discount) as totaldiscount, 
                    datetime as datetime2 from loanedproducts group by datetime) A 
                    on credits.datetime = A.datetime2 where datetime = '" _
                    & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                name = Reader.GetString("customerfullname")
                payment = Reader.GetDecimal("payment")
                discount = Reader.GetDecimal("totaldiscount")
                amountpaid = Reader.GetDecimal("amountpaid")
                rempayment = payment - amountpaid
            End While
            Reader.Close()
            PayCreditsNameLabel.Text = name
            CreditsPaymentTextBox.Text = (discount + payment).ToString("n2")
            DiscountTextBox.Text = discount.ToString("n2")
            CreditsAmountPaidTextBox.Text = amountpaid.ToString("n2")
            CreditsRemPaymentTextBox.Text = rempayment.ToString("n2")

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        MysqlCon.Close()
    End Sub
    Private Sub ContinueToPayButton_Click(sender As Object, e As EventArgs) Handles ContinueToPayButton.Click
        Dim AmountToPay, RemPayment As Decimal
        Try
            AmountToPay = Decimal.Parse(CreditsAmountToPayTextBox.Text)
            RemPayment = CreditsRemPaymentTextBox.Text
            If AmountToPay > RemPayment Then
                MessageBox.Show("The amount to pay is greater than the remaining payment! Paying credits failed.", "", MessageBoxButtons.OK)
            ElseIf AmountToPay < 1 Then
                MessageBox.Show("No/Invalid amount to pay! Paying credits failed.", "", MessageBoxButtons.OK)
            Else
                Dim PCconfirmationMes As DialogResult
                PCconfirmationMes = MessageBox.Show("Click Yes to complete the transaction.", "Please confirm payment", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                If PCconfirmationMes = DialogResult.Yes Then
                    OpenConnection()
                    If AmountToPay = RemPayment Then
                        'add to paymentsmade
                        Try
                            Query = "insert into paymentsmade values('" _
                                & PayCreditsNameLabel.Text & "', " & AmountToPay _
                                & ", (select convert(sysdate(), date)), '" _
                                & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "')"
                            Command = New MySqlCommand(Query, MysqlCon)
                            Reader = Command.ExecuteReader
                            Reader.Close()
                            'update sales
                            Dim CountTodaysSale As Integer = 0
                            Dim gross As Decimal
                            Try
                                Query = "select * from todayssale where date = (select convert(sysdate(), date))"
                                Command = New MySqlCommand(Query, MysqlCon)
                                Reader = Command.ExecuteReader
                                While Reader.Read
                                    gross = Reader.GetDecimal("grosssale")
                                    CountTodaysSale = 1
                                End While
                                Reader.Close()
                                If CountTodaysSale > 0 Then
                                    Query = "update todayssale set grosssale = " & gross + AmountToPay & " where date = (select convert(sysdate(), date))"
                                    Command = New MySqlCommand(Query, MysqlCon)
                                    Reader = Command.ExecuteReader
                                    Reader.Close()
                                Else
                                    Query = "insert into todayssale (date, grosssale) values((select convert(sysdate(), date)), " & AmountToPay & ")"
                                    Command = New MySqlCommand(Query, MysqlCon)
                                    Reader = Command.ExecuteReader
                                    Reader.Close()
                                End If
                            Catch ex As Exception
                                MessageBox.Show(ex.Message, "3")
                            End Try
                            'delete
                            Try
                                'delete records from credits
                                Query = "delete from credits where datetime = '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                                Command = New MySqlCommand(Query, MysqlCon)
                                Reader = Command.ExecuteReader
                                Reader.Close()
                                'delete moreinfo
                                Query = "delete from moreinfo where datetime = '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                                Command = New MySqlCommand(Query, MysqlCon)
                                Reader = Command.ExecuteReader
                                Reader.Close()
                                'delete debtorsphotopath
                                Query = "delete from debtorsphotopath where datetime = '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                                Command = New MySqlCommand(Query, MysqlCon)
                                Reader = Command.ExecuteReader
                                Reader.Close()

                                'put loaned products to paidloanedproducts
                                Try
                                    'temporary containers of loanedproducts
                                    Dim AList, JList, KList, LList As New List(Of Integer)
                                    Dim BList, CList, DList, FList, IList, Mlist As New List(Of String)
                                    Dim GList, HList As New List(Of Decimal)
                                    Query = "select * from loanedproducts where datetime = '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                                    Command = New MySqlCommand(Query, MysqlCon)
                                    Reader = Command.ExecuteReader
                                    While Reader.Read
                                        Dim a = Reader.GetInt32("no")
                                        Dim b = Reader.GetString("productname")
                                        Dim c = Reader.GetString("productbrand")
                                        Dim d = Reader.GetString("productdescription")
                                        Dim f = Reader.GetString("customerfullname")
                                        Dim g = Reader.GetDecimal("saleprice")
                                        Dim h = Reader.GetDecimal("quantity")
                                        Dim i = Reader.GetString("measuringunit")
                                        Dim j = Reader.GetDecimal("totalprice")
                                        Dim k = Reader.GetDecimal("discount")
                                        Dim l = Reader.GetDecimal("discountedprice")
                                        Dim m = Reader.GetDateTime("datetime").ToString("yyyy-MM-dd HH:mm:ss")
                                        AList.Add(a)
                                        BList.Add(b)
                                        CList.Add(c)
                                        DList.Add(d)
                                        FList.Add(f)
                                        GList.Add(g)
                                        HList.Add(h)
                                        IList.Add(i)
                                        JList.Add(j)
                                        KList.Add(k)
                                        LList.Add(l)
                                        Mlist.Add(m)
                                    End While
                                    Reader.Close()

                                    'Transfer data from the list to paidloanedproducts
                                    For i = 0 To AList.Count - 1
                                        Query = "insert into paidloanedproducts values(" _
                                            & AList.Item(i) & ", '" & FList.Item(i) & "', '" & BList.Item(i) & "', '" & CList.Item(i) _
                                            & "', '" & DList.Item(i) & "', " & GList.Item(i) & ", " & HList.Item(i) & ", '" & IList.Item(i) _
                                            & "', " & JList.Item(i) & ", " & KList.Item(i) & ", " & LList.Item(i) & ", '" & Mlist.Item(i) _
                                            & "', (select convert(sysdate(), date)))"
                                        Command = New MySqlCommand(Query, MysqlCon)
                                        Reader = Command.ExecuteReader
                                        Reader.Close()
                                    Next

                                    'Delete loaned products
                                    Query = "delete from loanedproducts where datetime = '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                                    Command = New MySqlCommand(Query, MysqlCon)
                                    Reader = Command.ExecuteReader
                                    Reader.Close()

                                    StoreForm.StoreForm_Load(sender, e)
                                    Me.Close()
                                Catch ex As Exception
                                    MessageBox.Show(ex.Message)
                                End Try
                            Catch ex As Exception
                                MessageBox.Show(ex.Message)
                            End Try
                        Catch ex As Exception
                            MessageBox.Show(ex.Message)
                        End Try
                    ElseIf AmountToPay < RemPayment Then
                        'Add to paymentsmade
                        Try
                            Query = "insert into paymentsmade values('" & PayCreditsNameLabel.Text & "', " & AmountToPay _
                                & ", (select convert(sysdate(), date)), '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "')"
                            Command = New MySqlCommand(Query, MysqlCon)
                            Reader = Command.ExecuteReader
                            Reader.Close()
                            'Add amountpaid to credits
                            Try
                                Query = "update credits set amountpaid = amountpaid + " & AmountToPay _
                                    & " where datetime = '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                                Command = New MySqlCommand(Query, MysqlCon)
                                Reader = Command.ExecuteReader
                                Reader.Close()
                            Catch ex As Exception
                                MessageBox.Show(ex.Message, "2")
                            End Try
                            'update sales
                            Dim CountTodaysSale As Integer = 0
                            Dim gross As Decimal
                            Try
                                Query = "select * from todayssale where date = (select convert(sysdate(), date))"
                                Command = New MySqlCommand(Query, MysqlCon)
                                Reader = Command.ExecuteReader
                                While Reader.Read
                                    gross = Reader.GetDecimal("grosssale")
                                    CountTodaysSale = 1
                                End While
                                Reader.Close()
                                If CountTodaysSale > 0 Then
                                    Query = "update todayssale set grosssale = " & gross + AmountToPay & " where date = (select convert(sysdate(), date))"
                                    Command = New MySqlCommand(Query, MysqlCon)
                                    Reader = Command.ExecuteReader
                                    Reader.Close()
                                Else
                                    Query = "insert into todayssale (date, grosssale) values((select convert(sysdate(), date)), " & AmountToPay & ")"
                                    Command = New MySqlCommand(Query, MysqlCon)
                                    Reader = Command.ExecuteReader
                                    Reader.Close()
                                End If
                            Catch ex As Exception
                                MessageBox.Show(ex.Message, "3")
                            End Try
                            StoreForm.StoreForm_Load(sender, e)
                            Me.Close()
                        Catch ex As Exception
                            MessageBox.Show(ex.Message, "1")
                        End Try
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("Amount to pay must be numeric!", "Invalid Input")
            CreditsAmountToPayTextBox.Focus()
            CreditsAmountToPayTextBox.SelectAll()
        End Try

        MysqlCon.Close()
    End Sub

    Private Sub PayCreditsForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        CreditsAmountToPayTextBox.Clear()
        StoreForm.CreditsDataGridView.ClearSelection()
        StoreForm.PayCreditButton.Enabled = False
        StoreForm.MoreInfoButton.Enabled = False
        StoreForm.SearchDebtorTextBox.Clear()
    End Sub
End Class