﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PayCreditsForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PayCreditsForm))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PayCreditsNameLabel = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.CreditsRemPaymentTextBox = New System.Windows.Forms.TextBox()
        Me.CreditsAmountPaidTextBox = New System.Windows.Forms.TextBox()
        Me.CreditsPaymentTextBox = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.CreditsAmountToPayTextBox = New System.Windows.Forms.TextBox()
        Me.ContinueToPayButton = New System.Windows.Forms.Button()
        Me.DiscountTextBox = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(21, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 18)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Name:"
        '
        'PayCreditsNameLabel
        '
        Me.PayCreditsNameLabel.AutoSize = True
        Me.PayCreditsNameLabel.BackColor = System.Drawing.Color.Transparent
        Me.PayCreditsNameLabel.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PayCreditsNameLabel.ForeColor = System.Drawing.Color.Maroon
        Me.PayCreditsNameLabel.Location = New System.Drawing.Point(79, 22)
        Me.PayCreditsNameLabel.Name = "PayCreditsNameLabel"
        Me.PayCreditsNameLabel.Size = New System.Drawing.Size(96, 18)
        Me.PayCreditsNameLabel.TabIndex = 1
        Me.PayCreditsNameLabel.Text = "Debtor Name"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(21, 67)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 18)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Payment:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(21, 126)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(95, 18)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Amount Paid:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(21, 156)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(142, 18)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Remaining Payment:"
        '
        'CreditsRemPaymentTextBox
        '
        Me.CreditsRemPaymentTextBox.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CreditsRemPaymentTextBox.Location = New System.Drawing.Point(169, 148)
        Me.CreditsRemPaymentTextBox.Name = "CreditsRemPaymentTextBox"
        Me.CreditsRemPaymentTextBox.ReadOnly = True
        Me.CreditsRemPaymentTextBox.Size = New System.Drawing.Size(124, 26)
        Me.CreditsRemPaymentTextBox.TabIndex = 5
        Me.CreditsRemPaymentTextBox.TabStop = False
        Me.CreditsRemPaymentTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CreditsAmountPaidTextBox
        '
        Me.CreditsAmountPaidTextBox.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CreditsAmountPaidTextBox.Location = New System.Drawing.Point(169, 117)
        Me.CreditsAmountPaidTextBox.Name = "CreditsAmountPaidTextBox"
        Me.CreditsAmountPaidTextBox.ReadOnly = True
        Me.CreditsAmountPaidTextBox.Size = New System.Drawing.Size(124, 26)
        Me.CreditsAmountPaidTextBox.TabIndex = 6
        Me.CreditsAmountPaidTextBox.TabStop = False
        Me.CreditsAmountPaidTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CreditsPaymentTextBox
        '
        Me.CreditsPaymentTextBox.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CreditsPaymentTextBox.Location = New System.Drawing.Point(169, 59)
        Me.CreditsPaymentTextBox.Name = "CreditsPaymentTextBox"
        Me.CreditsPaymentTextBox.ReadOnly = True
        Me.CreditsPaymentTextBox.Size = New System.Drawing.Size(124, 26)
        Me.CreditsPaymentTextBox.TabIndex = 7
        Me.CreditsPaymentTextBox.TabStop = False
        Me.CreditsPaymentTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(21, 202)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(149, 18)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Enter amount to pay:"
        '
        'CreditsAmountToPayTextBox
        '
        Me.CreditsAmountToPayTextBox.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CreditsAmountToPayTextBox.Location = New System.Drawing.Point(169, 199)
        Me.CreditsAmountToPayTextBox.Name = "CreditsAmountToPayTextBox"
        Me.CreditsAmountToPayTextBox.Size = New System.Drawing.Size(124, 26)
        Me.CreditsAmountToPayTextBox.TabIndex = 9
        Me.CreditsAmountToPayTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ContinueToPayButton
        '
        Me.ContinueToPayButton.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContinueToPayButton.Location = New System.Drawing.Point(169, 241)
        Me.ContinueToPayButton.Name = "ContinueToPayButton"
        Me.ContinueToPayButton.Size = New System.Drawing.Size(124, 33)
        Me.ContinueToPayButton.TabIndex = 10
        Me.ContinueToPayButton.Text = "Continue to &Pay"
        Me.ContinueToPayButton.UseVisualStyleBackColor = True
        '
        'DiscountTextBox
        '
        Me.DiscountTextBox.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DiscountTextBox.Location = New System.Drawing.Point(169, 88)
        Me.DiscountTextBox.Name = "DiscountTextBox"
        Me.DiscountTextBox.ReadOnly = True
        Me.DiscountTextBox.Size = New System.Drawing.Size(124, 26)
        Me.DiscountTextBox.TabIndex = 12
        Me.DiscountTextBox.TabStop = False
        Me.DiscountTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(21, 96)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(68, 18)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Discount:"
        '
        'PayCreditsForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.ROJBackground
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(316, 294)
        Me.Controls.Add(Me.DiscountTextBox)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.ContinueToPayButton)
        Me.Controls.Add(Me.CreditsAmountToPayTextBox)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.CreditsPaymentTextBox)
        Me.Controls.Add(Me.CreditsAmountPaidTextBox)
        Me.Controls.Add(Me.CreditsRemPaymentTextBox)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PayCreditsNameLabel)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "PayCreditsForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Pay Credits"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents PayCreditsNameLabel As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents CreditsRemPaymentTextBox As TextBox
    Friend WithEvents CreditsAmountPaidTextBox As TextBox
    Friend WithEvents CreditsPaymentTextBox As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents CreditsAmountToPayTextBox As TextBox
    Friend WithEvents ContinueToPayButton As Button
    Friend WithEvents DiscountTextBox As TextBox
    Friend WithEvents Label6 As Label
End Class
