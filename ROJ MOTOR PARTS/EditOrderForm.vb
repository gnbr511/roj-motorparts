﻿Imports MySql.Data.MySqlClient
Public Class EditOrderForm
    Private discount, discountedprice As Decimal
    Friend MeasuringUnit As String
    Private Sub UpdateOrderButton_Click(sender As Object, e As EventArgs) Handles UpdateOrderButton.Click
        Try
            OpenConnection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        If QuantityTextBox.Text < 1 Then
            MessageBox.Show("Quantity of the order cannot be zero!", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            Dim orderquantity, stockavailable, f, subtotal, discount, grandtotal, totaldiscount, purchasecost As Decimal
            Dim a As Integer
            Dim b, c, d, g, p As String
            Dim enoughstock As Boolean = False
            If CustomerTextBox.Text <> "" Then
                If ProductNameTextBox.Text <> "" Then
                    'Check product availability
                    Try
                        orderquantity = Decimal.Parse(QuantityTextBox.Text)
                        Try
                            Query = "select * from products where no = " & HiddenProductNoTextBox.Text
                            Command = New MySqlCommand(Query, MysqlCon)
                            Reader = Command.ExecuteReader
                            While Reader.Read
                                a = Reader.GetInt32("no")
                                stockavailable = Reader.GetString("availablestock")
                            End While
                            Reader.Close()
                            ''''''''
                            If stockavailable >= orderquantity Then
                                enoughstock = True
                            Else
                                enoughstock = False
                            End If
                            ''''''''
                            If enoughstock = True Then
                                Try
                                    Query = "delete from orders where no = " & StoreForm.SelectedOrderNo
                                    Command = New MySqlCommand(Query, MysqlCon)
                                    Reader = Command.ExecuteReader
                                    Reader.Close()

                                    Query = "delete from orders where no = " & HiddenProductNoTextBox.Text
                                    Command = New MySqlCommand(Query, MysqlCon)
                                    Reader = Command.ExecuteReader
                                    Reader.Close()

                                    Try
                                        StoreForm.OrdersDataGridView.Rows.Clear()
                                        Query = "select * from products where no = " & HiddenProductNoTextBox.Text
                                        Command = New MySqlCommand(Query, MysqlCon)
                                        Reader = Command.ExecuteReader
                                        While Reader.Read
                                            a = Reader.GetInt32("no")
                                            b = Reader.GetString("productname")
                                            c = Reader.GetString("productbrand")
                                            d = Reader.GetString("productdescription")
                                            purchasecost = Reader.GetString("purchasecost")
                                            f = Reader.GetDecimal("saleprice")
                                            g = Reader.GetString("measuringunit")
                                        End While
                                        Reader.Close()
                                        'incase customer name is changed
                                        Try
                                            Query = "update orders set customername = '" & CustomerTextBox.Text & "'"
                                            Command = New MySqlCommand(Query, MysqlCon)
                                            Reader = Command.ExecuteReader
                                            Reader.Close()
                                        Catch ex As Exception
                                            MessageBox.Show(ex.Message, "customer name changed")
                                        End Try
                                        If DiscountTextBox.Text = "" Or IsNumeric(DiscountTextBox.Text) = False Then
                                            discount = 0
                                        Else
                                            discount = DiscountTextBox.Text
                                        End If
                                        Try
                                            Dim TotalPrice As Decimal = orderquantity * f
                                            Query = "insert into orders values(" & a & ",'" & CustomerTextBox.Text & "','" & b & "','" & c & "','" & d & "', '" & purchasecost & "'," & f & "," & orderquantity _
                                                            & ", '" & g & "'," & TotalPrice & ", " & discount & ", " & TotalPrice - discount & ", sysdate(), '" & StoreForm.OrderNoTextBox.Text & "')"
                                            Command = New MySqlCommand(Query, MysqlCon)
                                            Reader = Command.ExecuteReader
                                            Reader.Close()
                                            Try
                                                Query = "select * from orders order by time"
                                                Command = New MySqlCommand(Query, MysqlCon)
                                                Reader = Command.ExecuteReader
                                                While Reader.Read
                                                    Dim h = Reader.GetInt32("no")
                                                    p = Reader.GetString("customername")
                                                    Dim i = Reader.GetString("productname")
                                                    Dim j = Reader.GetString("productbrand")
                                                    Dim k = Reader.GetString("productdescription")
                                                    Dim pur_cost = Reader.GetString("purchasecost")
                                                    Dim l = Reader.GetDecimal("saleprice")
                                                    Dim m = Reader.GetDecimal("quantity")
                                                    Dim n = Reader.GetString("measuringunit")
                                                    Dim o = Reader.GetDecimal("totalprice")
                                                    Dim q = Reader.GetDecimal("discount")
                                                    Dim r = Reader.GetDecimal("discountedprice")
                                                    Dim OrderNO As Integer = StoreForm.OrdersDataGridView.RowCount() + 1
                                                    StoreForm.OrdersDataGridView.Rows.Add(h, OrderNO, i, j, k, purchasecost, l, m, n, o.ToString("n2"), q.ToString("n2"), r.ToString("n2"))
                                                    subtotal += o
                                                    totaldiscount += q
                                                End While
                                                Reader.Close()
                                                grandtotal = subtotal - totaldiscount

                                                StoreForm.CustomerNameTextBox.Text = p.ToString()
                                                StoreForm.SubTotalTextBox.Text = subtotal.ToString("n2")
                                                StoreForm.GrandTotalTextBox.Text = grandtotal.ToString("n2")
                                                StoreForm.DiscountTextBox.Text = totaldiscount.ToString("n2")
                                                StoreForm.AmountReceivedTextBox_TextChanged(sender, e)
                                                ProductNameTextBox.Clear()
                                                BrandTextBox.Clear()
                                                DescriptionTextBox.Clear()
                                                MeasuringUnitTextBox.Clear()
                                                PriceTextBox.Text = ""
                                                TotalPriceTextBox.Text = ""
                                                StoreForm.OrdersDataGridView.ClearSelection()
                                                Me.Close()
                                                With QuantityTextBox
                                                    .Clear()
                                                    .Focus()
                                                End With
                                            Catch ex As Exception
                                                MessageBox.Show(ex.Message)
                                            End Try
                                        Catch ex As Exception
                                            MessageBox.Show(ex.Message)
                                        End Try
                                    Catch ex As Exception
                                        MessageBox.Show(ex.Message)
                                    End Try

                                Catch ex As Exception
                                    MessageBox.Show(ex.Message)
                                End Try
                            Else
                                MessageBox.Show("Not enough stock for the order!", "Check product stock", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                With QuantityTextBox
                                    .Focus()
                                    .SelectAll()
                                End With
                            End If
                        Catch ex As Exception
                            MessageBox.Show(ex.Message)
                        End Try
                    Catch ex As Exception
                        MessageBox.Show("Quantity must be numeric!", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        With QuantityTextBox
                            .Focus()
                            .SelectAll()
                        End With
                    End Try
                Else
                    MessageBox.Show("No product selected!", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    ProductNameTextBox.Focus()
                End If
            Else
                MessageBox.Show("Customer name must not be empty!", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information)
                CustomerTextBox.Focus()
            End If
        End If
        MysqlCon.Close()
    End Sub

    Friend Sub QuantityTextBox_TextChanged(sender As Object, e As EventArgs) Handles QuantityTextBox.TextChanged
        'If PriceTextBox.Text = "" Then

        'Else
        '    Dim price As Decimal = PriceTextBox.Text
        '    If QuantityTextBox.Text = "" Or IsNumeric(QuantityTextBox.Text) = False Then
        '        TotalPriceTextBox.Clear()
        '        DiscountedPriceTextBox.Clear()
        '    Else
        '        Dim quantity As Decimal = QuantityTextBox.Text
        '        Dim totalprice As Decimal = price * quantity
        '        TotalPriceTextBox.Text = totalprice.ToString("n2")
        '    End If
        'End If
        Try
            If PriceTextBox.Text = "" Then

            Else
                If QuantityTextBox.Text <> "" Then
                    If MeasuringUnit.ToLower = "pcs" Or MeasuringUnit.ToLower = "pack" Or MeasuringUnit.ToLower = "pair" Then
                        Integer.Parse(QuantityTextBox.Text)
                    End If
                    Dim price As Decimal = PriceTextBox.Text
                    If QuantityTextBox.Text = "" Or IsNumeric(QuantityTextBox.Text) = False Or QuantityTextBox.Text = 0 Then
                        TotalPriceTextBox.Clear()
                        DiscountedPriceTextBox.Clear()
                        QuantityTextBox.Clear()
                    Else
                        Dim quantity As Decimal = QuantityTextBox.Text
                        Dim totalprice As Decimal = price * quantity
                        TotalPriceTextBox.Text = totalprice.ToString("n2")
                    End If
                Else
                    TotalPriceTextBox.Clear()
                    DiscountedPriceTextBox.Clear()
                End If
            End If
        Catch ex As Exception
            QuantityTextBox.Clear()
            If MeasuringUnit.ToLower = "pcs" Or MeasuringUnit.ToLower = "pack" Or MeasuringUnit.ToLower = "pair" Then
                MessageBox.Show("Quantity must be Integer!", "Invalid Input!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else
                MessageBox.Show("Quantity must be Numeric!", "Invalid Input!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End Try
    End Sub

    Private Sub SelectProductButton_Click(sender As Object, e As EventArgs) Handles SelectProductButton.Click
        SelectProductForm.EditOrAdd = "edit"
        SelectProductForm.ShowDialog()
    End Sub

    Private Sub EditOrderForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        StoreForm.OrdersDataGridView.ClearSelection()
        StoreForm.SelectedOrderNo = -1
        ProductNameTextBox.Clear()
        DescriptionTextBox.Clear()
        BrandTextBox.Clear()
        MeasuringUnitTextBox.Clear()
        PriceTextBox.Text = ""
        TotalPriceTextBox.Text = ""
        HiddenProductNoTextBox.Clear()
        QuantityTextBox.Clear()
    End Sub

    Private Sub TotalPriceTextBox_TextChanged(sender As Object, e As EventArgs) Handles TotalPriceTextBox.TextChanged
        DiscountTextBox_TextChanged(sender, e)
    End Sub

    Private Sub EditOrderForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim ScreenResHeight, ScreenResWidth, yLocation, xLocation As Integer
        ScreenResHeight = Screen.PrimaryScreen.WorkingArea.Height
        ScreenResWidth = Screen.PrimaryScreen.WorkingArea.Width
        yLocation = (ScreenResHeight - 757) / 2 + 56
        xLocation = (ScreenResWidth - 1369) / 2 + 726
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New Point(xLocation, yLocation)
    End Sub

    Private Sub DiscountTextBox_TextChanged(sender As Object, e As EventArgs) Handles DiscountTextBox.TextChanged
        If TotalPriceTextBox.Text = "" Then
            DiscountTextBox.Text = ""
        Else
            Dim totalprice As Decimal = TotalPriceTextBox.Text
            If DiscountTextBox.Text = "" Or IsNumeric(DiscountTextBox.Text) = False Then
                DiscountedPriceTextBox.Clear()
                discount = 0
            Else
                discount = DiscountTextBox.Text
                discountedprice = totalprice - discount
                DiscountedPriceTextBox.Text = discountedprice.ToString("n2")
            End If
        End If
    End Sub
End Class