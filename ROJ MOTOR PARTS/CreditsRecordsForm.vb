﻿Imports MySql.Data.MySqlClient
Imports System.Drawing.Printing
Public Class CreditsRecordsForm
    Private SelectedDebtor As String
    Private DiscountOfSelectedDebtor As Decimal
    Friend Sub CreditsRecordsForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            OpenConnection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        'Display Credits
        Try
            CreditsDataGridView.Rows.Clear()
            Query = "SELECT * FROM credits
                     INNER JOIN (SELECT SUM(discount) as totaldiscount, debtorsid
                                 FROM loanedproducts
                                 GROUP BY debtorsid) sumofdiscounts
                     ON credits.debtorsid = sumofdiscounts.debtorsid
                     ORDER BY debtor"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim id = Reader.GetString("debtorsid")
                Dim debtor = Reader.GetString("debtor")
                Dim payment = Reader.GetDecimal("payment")
                Dim discount = Reader.GetDecimal("totaldiscount")
                Dim amountpaid = Reader.GetDecimal("amountpaid")
                Dim remainingpayment = payment - amountpaid
                Dim contact = Reader.GetString("contact")
                CreditsDataGridView.Rows.Add(id, debtor, contact, payment + discount, discount, amountpaid, remainingpayment)
            End While
            Reader.Close()
            CreditsDataGridView.ClearSelection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Try
            Dim No As Int32
            Dim ColorAlternater As Boolean = False
            LoanedProductsDataGridView.Rows.Clear()
            Query = "SELECT * FROM Loanedproducts ORDER BY debtor"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim loaneddate = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                Dim orderno = Reader.GetString("orderno")
                Dim DebtorsID = Reader.GetString("debtorsid")
                Dim debtor = Reader.GetString("debtor")
                Dim prodno = Reader.GetInt32("no")
                Dim prodname = Reader.GetString("productname")
                Dim prodbrand = Reader.GetString("productbrand")
                Dim proddesc = Reader.GetString("productdescription")
                Dim purchasecost = Reader.GetDecimal("purchasecost")
                Dim saleprice = Reader.GetDecimal("saleprice")
                Dim quantity = Reader.GetDecimal("quantity")
                Dim measuringunit = Reader.GetString("measuringunit")
                Dim totalprice = Reader.GetDecimal("totalprice")
                Dim discount = Reader.GetDecimal("discount")
                Dim finalprice = Reader.GetDecimal("discountedprice")
                LoanedProductsDataGridView.Rows.Add(loaneddate, orderno, prodno, prodname, prodbrand, proddesc, purchasecost, saleprice, quantity, measuringunit, totalprice, discount, finalprice, DebtorsID, debtor)
                If No > 0 Then
                    If orderno = LoanedProductsDataGridView(1, No - 1).Value() Then
                        LoanedProductsDataGridView.Rows(No).DefaultCellStyle.BackColor = LoanedProductsDataGridView.Rows(No - 1).DefaultCellStyle.BackColor
                    Else
                        If ColorAlternater = True Then
                            LoanedProductsDataGridView.Rows(No).DefaultCellStyle.BackColor = Color.White
                            ColorAlternater = False
                        Else
                            LoanedProductsDataGridView.Rows(No).DefaultCellStyle.BackColor = Color.Gray
                            ColorAlternater = True
                        End If
                    End If
                End If
                No += 1
            End While
            Reader.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        MysqlCon.Close()
    End Sub

    Private Sub CreditsDataGridView_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles CreditsDataGridView.CellDoubleClick
        Try
            CreditsInformationForm.SelectedDebtor = CreditsDataGridView.CurrentRow.Cells(0).Value
            CreditsInformationForm.ShowDialog()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub MoreInfoButton_Click(sender As Object, e As EventArgs) Handles MoreInfoButton.Click
        CreditsInformationForm.SelectedDebtor = CreditsDataGridView.CurrentRow.Cells(0).Value
        CreditsInformationForm.ShowDialog()
    End Sub

    Private Sub CreditsRecordsForm_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        LoanedProductsDataGridView.ClearSelection()
        CreditsDataGridView.ClearSelection()
        MoreInfoButton.Enabled = False
        DeleteCreditsButton.Enabled = False
        PrintButton.Enabled = False
    End Sub
    Private Sub DeleteCreditsButton_Click(sender As Object, e As EventArgs) Handles DeleteCreditsButton.Click
        SelectedDebtor = CreditsDataGridView.CurrentRow.Cells(6).Value
        Dim amountpaid As Decimal = CreditsDataGridView.CurrentRow.Cells(4).Value
        Dim Mesres As DialogResult = MessageBox.Show("Deleting this record will also delete some credits information of this record such as loaned products and other information of the debtor! Click OK to continue.", "Confirm Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
        If Mesres = DialogResult.OK Then
            Try
                OpenConnection()
                Query = "delete from credits where datetime = '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                Reader.Close()
                Query = "delete from loanedproducts where datetime = '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                Reader.Close()
                Query = "delete from moreinfo where datetime = '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                Reader.Close()
                Query = "delete from debtorsphotopath where datetime = '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                Reader.Close()
                If amountpaid > 0 Then
                    Dim mesres2 As DialogResult = MessageBox.Show("Also delete the Payments Made by this debtor?", "Delete Payments Made", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                    If mesres2 = DialogResult.Yes Then
                        OpenConnection()
                        Query = "delete from paymentsmade where datetime = '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                        Command = New MySqlCommand(Query, MysqlCon)
                        Reader = Command.ExecuteReader
                        Reader.Close()
                    End If
                End If
                CreditsRecordsForm_Load(sender, e)
                RecordsForm.RefreshButton_Click(sender, e)
                MessageBox.Show("Deleted successfully!")
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
        MysqlCon.Close()
    End Sub
    Private Sub CreditsRecordsForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        RecordsForm.RefreshButton_Click(sender, e)
    End Sub
    Private SelectedCreditRecord As String
    Private Sub CreditsPrintDocument_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles CreditsPrintDocument.PrintPage
        Dim PrintFontBold As New Font("Arial Narrow", 10, FontStyle.Bold)
        Dim PrintFont As New Font("Arial Narrow", 10)
        Dim DebtorsInfoFieldPrintLocation As Single = e.MarginBounds.Left + 25
        Dim DebtorsInfoDataPrintLocation As Single = DebtorsInfoFieldPrintLocation + 150
        Dim CreditsSummaryFieldPrintLocation As Single = e.MarginBounds.Left + 550
        Dim CreditsSummaryDataPrintLocation As Single = CreditsSummaryFieldPrintLocation + 130
        Dim VerticalPrintLocation As Single = e.MarginBounds.Top + 25
        Dim PaymentDatePrintLocation As Single = e.MarginBounds.Left + 25
        Dim PaymentAmountprintlocation As Single = PaymentDatePrintLocation + 100
        Dim HeaderString As String = "ROJ MOTOR PARTS"
        Dim ROJFONT As New Font("elephant", 11)
        Dim ROJHorizontalPrintLocation As Single = Convert.ToSingle((e.PageBounds.Width / 2 - e.Graphics.MeasureString(HeaderString, ROJFONT).Width / 2) + 25)
        e.Graphics.DrawString(HeaderString, ROJFONT, Brushes.Black, ROJHorizontalPrintLocation, VerticalPrintLocation)
        VerticalPrintLocation += 15
        HeaderString = "Bintawan Rd, Solano, Nueva Vizcaya"
        e.Graphics.DrawString(HeaderString, PrintFont, Brushes.Black, ROJHorizontalPrintLocation, VerticalPrintLocation)
        VerticalPrintLocation += 15
        HeaderString = "Contact: 0916 169 6397"
        e.Graphics.DrawString(HeaderString, PrintFont, Brushes.Black, ROJHorizontalPrintLocation, VerticalPrintLocation)
        VerticalPrintLocation += 15
        HeaderString = "Facebook: www.facebook.com/jaysonrobin2019/"
        e.Graphics.DrawString(HeaderString, PrintFont, Brushes.Black, ROJHorizontalPrintLocation, VerticalPrintLocation)
        Dim customerphoto As Image = My.Resources.no_image_icon_13
        Dim rojlogo As Image = My.Resources.rojPrintlogo
        e.Graphics.DrawImage(rojlogo, ROJHorizontalPrintLocation - 100, 40, 100, 75)
        VerticalPrintLocation += 40
        e.Graphics.DrawString("DEBTOR'S INFORMATION", PrintFontBold, Brushes.Black, DebtorsInfoFieldPrintLocation, VerticalPrintLocation)
        e.Graphics.DrawString("CREDITS SUMMARY", PrintFontBold, Brushes.Black, CreditsSummaryFieldPrintLocation, VerticalPrintLocation)
        VerticalPrintLocation += 20
        Try
            OpenConnection()
            Query = "select credits.customerfullname, credits.contact, credits.payment, credits.amountpaid, debtorsphotopath.datetime, debtorsphotopath.debtorsphotopath
                     from credits
                     inner join debtorsphotopath
                     on credits.datetime = debtorsphotopath.datetime
                     where debtorsphotopath.datetime = '" & SelectedCreditRecord.ToString("yyyy-MM-dd HH:mm:ss") & "'"
            'Query = "select * from credits where datetime = '" & SelectedCreditRecord.ToString("yyyy-MM-dd HH:mm:ss") & "'"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                If System.IO.File.Exists(Reader.GetString("debtorsphotopath")) Then
                    customerphoto = Image.FromFile(Reader.GetString("debtorsphotopath"))
                End If
                e.Graphics.DrawImage(customerphoto, DebtorsInfoFieldPrintLocation, VerticalPrintLocation, 100, 75)
                VerticalPrintLocation += 85
                e.Graphics.DrawString("Customer's Full Name:", PrintFontBold, Brushes.Black, DebtorsInfoFieldPrintLocation, VerticalPrintLocation)
                e.Graphics.DrawString("Total Payment:", PrintFontBold, Brushes.Black, CreditsSummaryFieldPrintLocation, VerticalPrintLocation - 85)
                e.Graphics.DrawString(Reader.GetString("customerfullname"), PrintFont, Brushes.Black, DebtorsInfoDataPrintLocation, VerticalPrintLocation)
                e.Graphics.DrawString(Reader.GetDecimal("payment") + DiscountOfSelectedDebtor, PrintFont, Brushes.Black, CreditsSummaryDataPrintLocation, VerticalPrintLocation - 85)
                VerticalPrintLocation += 20
                e.Graphics.DrawString("Contact:", PrintFontBold, Brushes.Black, DebtorsInfoFieldPrintLocation, VerticalPrintLocation)
                e.Graphics.DrawString("Total Discount:", PrintFontBold, Brushes.Black, CreditsSummaryFieldPrintLocation, VerticalPrintLocation - 85)
                e.Graphics.DrawString(Reader.GetString("contact"), PrintFont, Brushes.Black, DebtorsInfoDataPrintLocation, VerticalPrintLocation)
                e.Graphics.DrawString(DiscountOfSelectedDebtor, PrintFont, Brushes.Black, CreditsSummaryDataPrintLocation, VerticalPrintLocation - 85)
                VerticalPrintLocation += 20
                e.Graphics.DrawString("Loan Date:", PrintFontBold, Brushes.Black, DebtorsInfoFieldPrintLocation, VerticalPrintLocation)
                e.Graphics.DrawString("Total Paid:", PrintFontBold, Brushes.Black, CreditsSummaryFieldPrintLocation, VerticalPrintLocation - 85)
                e.Graphics.DrawString(SelectedCreditRecord.ToString("dd-MM-yyyy"), PrintFont, Brushes.Black, DebtorsInfoDataPrintLocation, VerticalPrintLocation)
                e.Graphics.DrawString(Reader.GetDecimal("amountpaid"), PrintFont, Brushes.Black, CreditsSummaryDataPrintLocation, VerticalPrintLocation - 85)
                VerticalPrintLocation += 20
                e.Graphics.DrawString("More Information:", PrintFontBold, Brushes.Black, DebtorsInfoFieldPrintLocation, VerticalPrintLocation)
                e.Graphics.DrawString("Remaining Payment:", PrintFontBold, Brushes.Black, CreditsSummaryFieldPrintLocation, VerticalPrintLocation - 85)
                Dim remainingpayment As Decimal = Reader.GetDecimal("payment") - Reader.GetDecimal("amountpaid")
                e.Graphics.DrawString(remainingpayment, PrintFont, Brushes.Black, CreditsSummaryDataPrintLocation, VerticalPrintLocation - 85)
            End While
            Reader.Close()
            Try
                Query = "select * from moreinfo where datetime = '" & SelectedCreditRecord.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                While Reader.Read
                    e.Graphics.DrawString(Reader.GetString("info"), PrintFont, Brushes.Black, DebtorsInfoDataPrintLocation, VerticalPrintLocation)
                    VerticalPrintLocation += 15
                End While
                Reader.Close()
                Try
                    e.Graphics.DrawString("PAYMENT HISTORY", PrintFontBold, Brushes.Black, DebtorsInfoFieldPrintLocation, VerticalPrintLocation)
                    VerticalPrintLocation += 20
                    e.Graphics.DrawString("Date", PrintFontBold, Brushes.Black, PaymentDatePrintLocation, VerticalPrintLocation)
                    e.Graphics.DrawString("Amount Paid", PrintFontBold, Brushes.Black, PaymentAmountprintlocation, VerticalPrintLocation)
                    VerticalPrintLocation += 20
                    Query = "select * from paymentsmade where datetime = '" & SelectedCreditRecord.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    While Reader.Read
                        e.Graphics.DrawString(Reader.GetDateTime("date").ToString("dd-MM-yyyy"), PrintFont, Brushes.Black, PaymentDatePrintLocation, VerticalPrintLocation)
                        e.Graphics.DrawString(Reader.GetString("paidamount"), PrintFont, Brushes.Black, PaymentAmountprintlocation, VerticalPrintLocation)
                        VerticalPrintLocation += 15
                    End While
                    Reader.Close()
                    Try
                        Dim FieldPrintFont As New Font("Arial Narrow", 8, FontStyle.Bold)
                        Dim DataPrintFont As New Font("Arial Narrow", 8)
                        Dim ProductNameLocation As Single = e.MarginBounds.Left + 25
                        Dim ProductBrandLocation As Single = e.MarginBounds.Left + 140
                        Dim ProductDescLocation As Single = e.MarginBounds.Left + 210
                        Dim SalePriceLocation As Single = e.MarginBounds.Left + 325
                        Dim QuantityLocation As Single = e.MarginBounds.Left + 410
                        Dim MeasuringUnitLocation As Single = e.MarginBounds.Left + 475
                        Dim TotalPriceLocation As Single = e.MarginBounds.Left + 555
                        Dim DiscountLocation As Single = e.MarginBounds.Left + 635
                        Dim DiscountedPriceLocation As Single = e.MarginBounds.Left + 715
                        VerticalPrintLocation += 5
                        e.Graphics.DrawString("LOANED PRODUCTS", PrintFontBold, Brushes.Black, e.MarginBounds.Left + 25, VerticalPrintLocation)
                        VerticalPrintLocation += 15
                        e.Graphics.DrawString("PROD. NAME", FieldPrintFont, Brushes.Black, ProductNameLocation, VerticalPrintLocation)
                        e.Graphics.DrawString("BRAND", FieldPrintFont, Brushes.Black, ProductBrandLocation, VerticalPrintLocation)
                        e.Graphics.DrawString("DESCRIPTION", FieldPrintFont, Brushes.Black, ProductDescLocation, VerticalPrintLocation)
                        e.Graphics.DrawString("SALE PRICE", FieldPrintFont, Brushes.Black, SalePriceLocation, VerticalPrintLocation)
                        e.Graphics.DrawString("QUANTITY", FieldPrintFont, Brushes.Black, QuantityLocation, VerticalPrintLocation)
                        e.Graphics.DrawString("MEAS. UNIT", FieldPrintFont, Brushes.Black, MeasuringUnitLocation, VerticalPrintLocation)
                        e.Graphics.DrawString("TOT. PRICE", FieldPrintFont, Brushes.Black, TotalPriceLocation, VerticalPrintLocation)
                        e.Graphics.DrawString("DISCOUNT", FieldPrintFont, Brushes.Black, DiscountLocation, VerticalPrintLocation)
                        e.Graphics.DrawString("FINAL PRICE", FieldPrintFont, Brushes.Black, DiscountedPriceLocation, VerticalPrintLocation)
                        VerticalPrintLocation += 15
                        Query = "select * from loanedproducts where datetime = '" & SelectedCreditRecord.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                        Command = New MySqlCommand(Query, MysqlCon)
                        Reader = Command.ExecuteReader
                        While Reader.Read
                            e.Graphics.DrawString(SearchRecordsForm.LimitStrLen(Reader.GetString("productname"), 17), DataPrintFont, Brushes.Black, ProductNameLocation, VerticalPrintLocation)
                            e.Graphics.DrawString(SearchRecordsForm.LimitStrLen(Reader.GetString("productbrand"), 10), DataPrintFont, Brushes.Black, ProductBrandLocation, VerticalPrintLocation)
                            e.Graphics.DrawString(SearchRecordsForm.LimitStrLen(Reader.GetString("productdescription"), 17), DataPrintFont, Brushes.Black, ProductDescLocation, VerticalPrintLocation)
                            e.Graphics.DrawString(Reader.GetDecimal("saleprice"), DataPrintFont, Brushes.Black, SalePriceLocation, VerticalPrintLocation)
                            e.Graphics.DrawString(Reader.GetDecimal("quantity"), DataPrintFont, Brushes.Black, QuantityLocation, VerticalPrintLocation)
                            e.Graphics.DrawString(Reader.GetString("measuringunit"), DataPrintFont, Brushes.Black, MeasuringUnitLocation, VerticalPrintLocation)
                            e.Graphics.DrawString(Reader.GetDecimal("totalprice"), DataPrintFont, Brushes.Black, TotalPriceLocation, VerticalPrintLocation)
                            e.Graphics.DrawString(Reader.GetDecimal("discount"), DataPrintFont, Brushes.Black, DiscountLocation, VerticalPrintLocation)
                            e.Graphics.DrawString(Reader.GetDecimal("discountedprice"), DataPrintFont, Brushes.Black, DiscountedPriceLocation, VerticalPrintLocation)
                            VerticalPrintLocation += 15
                        End While
                        Reader.Close()
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        MysqlCon.Close()
    End Sub
    Private Sub printPreview_PrintClick(sender As Object, e As EventArgs)
        Try
            PrintDialog1.Document = CreditsPrintDocument
            If PrintDialog1.ShowDialog() = DialogResult.OK Then
                CreditsPrintDocument.Print()
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub PrintButton_Click(sender As Object, e As EventArgs) Handles PrintButton.Click
        Dim ShortBondPaper As New PaperSize("Short Bond Paper", 850, 1100)
        Dim SetMargin As New Margins(25, 25, 25, 25)
        With CreditsPrintDocument.DefaultPageSettings
            .PaperSize = ShortBondPaper
            .Margins = SetMargin
        End With
        Dim b As New ToolStripButton
        b.Image = CType(PrintPreviewDialog1.Controls(1), ToolStrip).ImageList.Images(0)
        b.ToolTipText = "Print"
        b.DisplayStyle = ToolStripItemDisplayStyle.Image
        AddHandler b.Click, AddressOf printPreview_PrintClick
        CType(PrintPreviewDialog1.Controls(1), ToolStrip).Items.RemoveAt(0)
        CType(PrintPreviewDialog1.Controls(1), ToolStrip).Items.Insert(0, b)
        PrintPreviewDialog1.Document = CreditsPrintDocument
        PrintPreviewDialog1.ShowDialog()
    End Sub

    Private Sub MoreInfoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MoreInfoToolStripMenuItem.Click
        If PrintButton.Enabled = False Then
            MessageBox.Show("No credit record has been selected!")
        Else
            MoreInfoButton_Click(sender, e)
        End If
    End Sub

    Private Sub DeleteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeleteToolStripMenuItem.Click
        If PrintButton.Enabled = False Then
            MessageBox.Show("No credit record has been selected!")
        Else
            DeleteCreditsButton_Click(sender, e)
        End If
    End Sub

    Private Sub PrintToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PrintToolStripMenuItem.Click
        If PrintButton.Enabled = False Then
            MessageBox.Show("No credit record has been selected!")
        Else
            PrintButton_Click(sender, e)
        End If
    End Sub

    Private Sub SearchCreditsTextBox_TextChanged(sender As Object, e As EventArgs) Handles SearchCreditsTextBox.TextChanged
        Try
            OpenConnection()
            CreditsDataGridView.Rows.Clear()
            Query = "SELECT * FROM (SELECT * FROM credits
                     INNER JOIN (SELECT SUM(discount) as totaldiscount, debtorsid as debtorsid2
                                 FROM loanedproducts
                                 GROUP BY debtorsid) sumofdiscounts
                     ON credits.debtorsid = sumofdiscounts.debtorsid2
                     ORDER BY debtor) credits 
                     WHERE debtor LIKE '%" & SearchCreditsTextBox.Text & "%' OR debtorsid LIKE '%" & SearchCreditsTextBox.Text & "%'
                     Order by debtor"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim id = Reader.GetString("debtorsid")
                Dim debtor = Reader.GetString("debtor")
                Dim payment = Reader.GetDecimal("payment")
                Dim discount = Reader.GetDecimal("totaldiscount")
                Dim amountpaid = Reader.GetDecimal("amountpaid")
                Dim remainingpayment = payment - amountpaid
                Dim contact = Reader.GetString("contact")
                CreditsDataGridView.Rows.Add(id, debtor, contact, payment + discount, discount, amountpaid, remainingpayment)
            End While
            Reader.Close()
            CreditsDataGridView.ClearSelection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        MysqlCon.Close()
    End Sub

    Private Sub LoanedProdSearchTextBox_TextChanged(sender As Object, e As EventArgs) Handles UnpaidLoanedProdSearchTextBox.TextChanged
        Try
            OpenConnection()
            Dim No As Int32
            Dim ColorAlternater As Boolean = False
            LoanedProductsDataGridView.Rows.Clear()
            Query = "select * from loanedproducts 
                 where debtor like '%" & UnpaidLoanedProdSearchTextBox.Text & "%'
                 or date like '%" & UnpaidLoanedProdSearchTextBox.Text & "%'
                 or orderno like '%" & UnpaidLoanedProdSearchTextBox.Text & "%'
                 or debtorsid like '%" & UnpaidLoanedProdSearchTextBox.Text & "%'
                 or productname like '%" & UnpaidLoanedProdSearchTextBox.Text & "%'
                 or productbrand like '%" & UnpaidLoanedProdSearchTextBox.Text & "%'
                 order by debtor"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim loaneddate = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                Dim orderno = Reader.GetString("orderno")
                Dim DebtorsID = Reader.GetString("debtorsid")
                Dim debtor = Reader.GetString("debtor")
                Dim prodno = Reader.GetInt32("no")
                Dim prodname = Reader.GetString("productname")
                Dim prodbrand = Reader.GetString("productbrand")
                Dim proddesc = Reader.GetString("productdescription")
                Dim purchasecost = Reader.GetDecimal("purchasecost")
                Dim saleprice = Reader.GetDecimal("saleprice")
                Dim quantity = Reader.GetDecimal("quantity")
                Dim measuringunit = Reader.GetString("measuringunit")
                Dim totalprice = Reader.GetDecimal("totalprice")
                Dim discount = Reader.GetDecimal("discount")
                Dim finalprice = Reader.GetDecimal("discountedprice")
                LoanedProductsDataGridView.Rows.Add(loaneddate, orderno, prodno, prodname, prodbrand, proddesc, purchasecost, saleprice, quantity, measuringunit, totalprice, discount, finalprice, DebtorsID, debtor)
                If No > 0 Then
                    If orderno = LoanedProductsDataGridView(1, No - 1).Value() Then
                        LoanedProductsDataGridView.Rows(No).DefaultCellStyle.BackColor = LoanedProductsDataGridView.Rows(No - 1).DefaultCellStyle.BackColor
                    Else
                        If ColorAlternater = True Then
                            LoanedProductsDataGridView.Rows(No).DefaultCellStyle.BackColor = Color.White
                            ColorAlternater = False
                        Else
                            LoanedProductsDataGridView.Rows(No).DefaultCellStyle.BackColor = Color.Gray
                            ColorAlternater = True
                        End If
                    End If
                End If
                No += 1
            End While
            Reader.Close()
            LoanedProductsDataGridView.ClearSelection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        MysqlCon.Close()
    End Sub

    Private Sub CreditsDataGridView_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles CreditsDataGridView.CellClick
        Try
            SelectedCreditRecord = CreditsDataGridView.CurrentRow.Cells(0).Value
            DiscountOfSelectedDebtor = CreditsDataGridView.CurrentRow.Cells(4).Value
            MoreInfoButton.Enabled = True
            'DeleteCreditsButton.Enabled = True
            PrintButton.Enabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class