﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class EditProductForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EditProductForm))
        Me.BrowsePicButton = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.ProductDescriptionTextBox = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ProductBrandTextBox = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CancelEditProductButton = New System.Windows.Forms.Button()
        Me.UpdateProductButton = New System.Windows.Forms.Button()
        Me.MeasuringUnitComboBox = New System.Windows.Forms.ComboBox()
        Me.SalePriceTextBox = New System.Windows.Forms.TextBox()
        Me.PurchaseCostTextBox = New System.Windows.Forms.TextBox()
        Me.MinStockLimitTextBox = New System.Windows.Forms.TextBox()
        Me.AvailStockTextBox = New System.Windows.Forms.TextBox()
        Me.ProductNameTextBox = New System.Windows.Forms.TextBox()
        Me.ProductPhotoPictureBox = New System.Windows.Forms.PictureBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.SupRetPriceTextBox = New System.Windows.Forms.TextBox()
        Me.ProductNoTextBox = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        CType(Me.ProductPhotoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BrowsePicButton
        '
        Me.BrowsePicButton.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BrowsePicButton.Location = New System.Drawing.Point(282, 107)
        Me.BrowsePicButton.Name = "BrowsePicButton"
        Me.BrowsePicButton.Size = New System.Drawing.Size(63, 25)
        Me.BrowsePicButton.TabIndex = 39
        Me.BrowsePicButton.Text = "Browse"
        Me.BrowsePicButton.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(36, 261)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(118, 16)
        Me.Label8.TabIndex = 38
        Me.Label8.Text = "Product Description"
        '
        'ProductDescriptionTextBox
        '
        Me.ProductDescriptionTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ProductDescriptionTextBox.Location = New System.Drawing.Point(160, 258)
        Me.ProductDescriptionTextBox.Name = "ProductDescriptionTextBox"
        Me.ProductDescriptionTextBox.Size = New System.Drawing.Size(185, 23)
        Me.ProductDescriptionTextBox.TabIndex = 23
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(76, 225)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 16)
        Me.Label7.TabIndex = 37
        Me.Label7.Text = "Brand Name"
        '
        'ProductBrandTextBox
        '
        Me.ProductBrandTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ProductBrandTextBox.Location = New System.Drawing.Point(160, 222)
        Me.ProductBrandTextBox.Name = "ProductBrandTextBox"
        Me.ProductBrandTextBox.Size = New System.Drawing.Size(185, 23)
        Me.ProductBrandTextBox.TabIndex = 22
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(89, 482)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(65, 16)
        Me.Label6.TabIndex = 36
        Me.Label6.Text = "Sale Price"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(65, 447)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(89, 16)
        Me.Label5.TabIndex = 35
        Me.Label5.Text = "Purchase Cost"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(61, 297)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(93, 16)
        Me.Label4.TabIndex = 34
        Me.Label4.Text = "Measuring Unit"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(28, 373)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(126, 16)
        Me.Label3.TabIndex = 33
        Me.Label3.Text = "Minimum Stock Limit"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(60, 336)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(94, 16)
        Me.Label2.TabIndex = 32
        Me.Label2.Text = "Available Stock"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(66, 189)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(88, 16)
        Me.Label1.TabIndex = 31
        Me.Label1.Text = "Product Name"
        '
        'CancelEditProductButton
        '
        Me.CancelEditProductButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CancelEditProductButton.Location = New System.Drawing.Point(246, 527)
        Me.CancelEditProductButton.Name = "CancelEditProductButton"
        Me.CancelEditProductButton.Size = New System.Drawing.Size(99, 32)
        Me.CancelEditProductButton.TabIndex = 31
        Me.CancelEditProductButton.Text = "Cancel"
        Me.CancelEditProductButton.UseVisualStyleBackColor = True
        '
        'UpdateProductButton
        '
        Me.UpdateProductButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UpdateProductButton.Location = New System.Drawing.Point(141, 527)
        Me.UpdateProductButton.Name = "UpdateProductButton"
        Me.UpdateProductButton.Size = New System.Drawing.Size(99, 32)
        Me.UpdateProductButton.TabIndex = 30
        Me.UpdateProductButton.Text = "Update"
        Me.UpdateProductButton.UseVisualStyleBackColor = True
        '
        'MeasuringUnitComboBox
        '
        Me.MeasuringUnitComboBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MeasuringUnitComboBox.FormattingEnabled = True
        Me.MeasuringUnitComboBox.Items.AddRange(New Object() {"Feet", "Kg", "Ltr", "Meter", "Pack", "Pcs"})
        Me.MeasuringUnitComboBox.Location = New System.Drawing.Point(160, 294)
        Me.MeasuringUnitComboBox.Name = "MeasuringUnitComboBox"
        Me.MeasuringUnitComboBox.Size = New System.Drawing.Size(185, 24)
        Me.MeasuringUnitComboBox.TabIndex = 24
        '
        'SalePriceTextBox
        '
        Me.SalePriceTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SalePriceTextBox.Location = New System.Drawing.Point(160, 479)
        Me.SalePriceTextBox.Name = "SalePriceTextBox"
        Me.SalePriceTextBox.Size = New System.Drawing.Size(185, 23)
        Me.SalePriceTextBox.TabIndex = 29
        '
        'PurchaseCostTextBox
        '
        Me.PurchaseCostTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PurchaseCostTextBox.Location = New System.Drawing.Point(160, 444)
        Me.PurchaseCostTextBox.Name = "PurchaseCostTextBox"
        Me.PurchaseCostTextBox.Size = New System.Drawing.Size(185, 23)
        Me.PurchaseCostTextBox.TabIndex = 28
        '
        'MinStockLimitTextBox
        '
        Me.MinStockLimitTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MinStockLimitTextBox.Location = New System.Drawing.Point(160, 370)
        Me.MinStockLimitTextBox.Name = "MinStockLimitTextBox"
        Me.MinStockLimitTextBox.Size = New System.Drawing.Size(185, 23)
        Me.MinStockLimitTextBox.TabIndex = 26
        '
        'AvailStockTextBox
        '
        Me.AvailStockTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AvailStockTextBox.Location = New System.Drawing.Point(160, 333)
        Me.AvailStockTextBox.Name = "AvailStockTextBox"
        Me.AvailStockTextBox.ReadOnly = True
        Me.AvailStockTextBox.Size = New System.Drawing.Size(185, 23)
        Me.AvailStockTextBox.TabIndex = 25
        '
        'ProductNameTextBox
        '
        Me.ProductNameTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ProductNameTextBox.Location = New System.Drawing.Point(160, 186)
        Me.ProductNameTextBox.Name = "ProductNameTextBox"
        Me.ProductNameTextBox.Size = New System.Drawing.Size(185, 23)
        Me.ProductNameTextBox.TabIndex = 21
        '
        'ProductPhotoPictureBox
        '
        Me.ProductPhotoPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ProductPhotoPictureBox.Image = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.uploadphoto
        Me.ProductPhotoPictureBox.Location = New System.Drawing.Point(116, 20)
        Me.ProductPhotoPictureBox.Name = "ProductPhotoPictureBox"
        Me.ProductPhotoPictureBox.Size = New System.Drawing.Size(160, 112)
        Me.ProductPhotoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.ProductPhotoPictureBox.TabIndex = 40
        Me.ProductPhotoPictureBox.TabStop = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(40, 411)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(114, 16)
        Me.Label9.TabIndex = 42
        Me.Label9.Text = "Sup. Retailer Price"
        '
        'SupRetPriceTextBox
        '
        Me.SupRetPriceTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SupRetPriceTextBox.Location = New System.Drawing.Point(160, 408)
        Me.SupRetPriceTextBox.Name = "SupRetPriceTextBox"
        Me.SupRetPriceTextBox.Size = New System.Drawing.Size(185, 23)
        Me.SupRetPriceTextBox.TabIndex = 27
        '
        'ProductNoTextBox
        '
        Me.ProductNoTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ProductNoTextBox.Location = New System.Drawing.Point(160, 150)
        Me.ProductNoTextBox.Name = "ProductNoTextBox"
        Me.ProductNoTextBox.ReadOnly = True
        Me.ProductNoTextBox.Size = New System.Drawing.Size(185, 23)
        Me.ProductNoTextBox.TabIndex = 43
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(84, 153)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(70, 16)
        Me.Label10.TabIndex = 44
        Me.Label10.Text = "Product No"
        '
        'EditProductForm
        '
        Me.AcceptButton = Me.UpdateProductButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.ROJBackground
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(383, 577)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.ProductNoTextBox)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.SupRetPriceTextBox)
        Me.Controls.Add(Me.ProductPhotoPictureBox)
        Me.Controls.Add(Me.BrowsePicButton)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.ProductDescriptionTextBox)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.ProductBrandTextBox)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CancelEditProductButton)
        Me.Controls.Add(Me.UpdateProductButton)
        Me.Controls.Add(Me.SalePriceTextBox)
        Me.Controls.Add(Me.PurchaseCostTextBox)
        Me.Controls.Add(Me.MinStockLimitTextBox)
        Me.Controls.Add(Me.AvailStockTextBox)
        Me.Controls.Add(Me.ProductNameTextBox)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.MeasuringUnitComboBox)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "EditProductForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Edit Product"
        CType(Me.ProductPhotoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ProductPhotoPictureBox As PictureBox
    Friend WithEvents BrowsePicButton As Button
    Friend WithEvents Label8 As Label
    Friend WithEvents ProductDescriptionTextBox As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents ProductBrandTextBox As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents CancelEditProductButton As Button
    Friend WithEvents UpdateProductButton As Button
    Friend WithEvents MeasuringUnitComboBox As ComboBox
    Friend WithEvents SalePriceTextBox As TextBox
    Friend WithEvents PurchaseCostTextBox As TextBox
    Friend WithEvents MinStockLimitTextBox As TextBox
    Friend WithEvents AvailStockTextBox As TextBox
    Friend WithEvents ProductNameTextBox As TextBox
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents Label9 As Label
    Friend WithEvents SupRetPriceTextBox As TextBox
    Friend WithEvents ProductNoTextBox As TextBox
    Friend WithEvents Label10 As Label
End Class
