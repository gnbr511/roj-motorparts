﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CreditsInformationForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CreditsInformationForm))
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ContactLabel = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.FullNameLabel = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.IDLabel = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.UpdateinfoButton = New System.Windows.Forms.Button()
        Me.DebtorsPhotoPictureBox = New System.Windows.Forms.PictureBox()
        Me.MoreInfoListBox = New System.Windows.Forms.ListBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.DiscountTextBox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.RemainingPaymentTextBox = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TotalPaidTextBox = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TotalPaymentTextBox = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.PaymentHistoryDataGridView = New System.Windows.Forms.DataGridView()
        Me.CHDateColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CHPaidAmountColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OrderNO01 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.PaymentHistoryTextBox = New System.Windows.Forms.TextBox()
        Me.LoanedProductsDataGridView = New System.Windows.Forms.DataGridView()
        Me.DateColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OrderNo2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductnameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductbrandColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductDescriptionColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PurchaseCost = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SalePriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QuantityColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MeasuringunitColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalPriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RdiscountColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RfinalPriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.SearchLoanedProductTextBox = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.SearchLoansTextBox = New System.Windows.Forms.TextBox()
        Me.LoansDataGridView = New System.Windows.Forms.DataGridView()
        Me.OrderDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OrderNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalPayment = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DiscountGiven = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AmountPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RemainingPayment = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PayToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintDialog1 = New System.Windows.Forms.PrintDialog()
        Me.CreditsPrintDocument = New System.Drawing.Printing.PrintDocument()
        Me.PrintPreviewDialog1 = New System.Windows.Forms.PrintPreviewDialog()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DebtorsPhotoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.PaymentHistoryDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.LoanedProductsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.LoansDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(210, 81)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(200, 18)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Customer's More Information"
        '
        'ContactLabel
        '
        Me.ContactLabel.AutoSize = True
        Me.ContactLabel.BackColor = System.Drawing.Color.Transparent
        Me.ContactLabel.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContactLabel.ForeColor = System.Drawing.Color.Maroon
        Me.ContactLabel.Location = New System.Drawing.Point(290, 62)
        Me.ContactLabel.Name = "ContactLabel"
        Me.ContactLabel.Size = New System.Drawing.Size(58, 18)
        Me.ContactLabel.TabIndex = 8
        Me.ContactLabel.Text = "Contact"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(210, 62)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 18)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Contact:"
        '
        'FullNameLabel
        '
        Me.FullNameLabel.AutoSize = True
        Me.FullNameLabel.BackColor = System.Drawing.Color.Transparent
        Me.FullNameLabel.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FullNameLabel.ForeColor = System.Drawing.Color.Maroon
        Me.FullNameLabel.Location = New System.Drawing.Point(290, 44)
        Me.FullNameLabel.Name = "FullNameLabel"
        Me.FullNameLabel.Size = New System.Drawing.Size(161, 18)
        Me.FullNameLabel.TabIndex = 6
        Me.FullNameLabel.Text = "Ginber Juanillo Candelrio"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(210, 44)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 18)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Full Name:"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.IDLabel)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.UpdateinfoButton)
        Me.GroupBox1.Controls.Add(Me.DebtorsPhotoPictureBox)
        Me.GroupBox1.Controls.Add(Me.FullNameLabel)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.ContactLabel)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.MoreInfoListBox)
        Me.GroupBox1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(14, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(621, 191)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Debtor's Personal Information"
        '
        'IDLabel
        '
        Me.IDLabel.AutoSize = True
        Me.IDLabel.BackColor = System.Drawing.Color.Transparent
        Me.IDLabel.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IDLabel.ForeColor = System.Drawing.Color.Maroon
        Me.IDLabel.Location = New System.Drawing.Point(290, 26)
        Me.IDLabel.Name = "IDLabel"
        Me.IDLabel.Size = New System.Drawing.Size(147, 18)
        Me.IDLabel.TabIndex = 19
        Me.IDLabel.Text = "Identification Number"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(210, 26)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(29, 18)
        Me.Label12.TabIndex = 18
        Me.Label12.Text = "ID:"
        '
        'UpdateinfoButton
        '
        Me.UpdateinfoButton.Location = New System.Drawing.Point(481, 102)
        Me.UpdateinfoButton.Name = "UpdateinfoButton"
        Me.UpdateinfoButton.Size = New System.Drawing.Size(127, 76)
        Me.UpdateinfoButton.TabIndex = 17
        Me.UpdateinfoButton.TabStop = False
        Me.UpdateinfoButton.Text = "&Update Information"
        Me.UpdateinfoButton.UseVisualStyleBackColor = True
        '
        'DebtorsPhotoPictureBox
        '
        Me.DebtorsPhotoPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DebtorsPhotoPictureBox.Image = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.no_image_icon_13
        Me.DebtorsPhotoPictureBox.Location = New System.Drawing.Point(13, 26)
        Me.DebtorsPhotoPictureBox.Name = "DebtorsPhotoPictureBox"
        Me.DebtorsPhotoPictureBox.Size = New System.Drawing.Size(181, 152)
        Me.DebtorsPhotoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.DebtorsPhotoPictureBox.TabIndex = 16
        Me.DebtorsPhotoPictureBox.TabStop = False
        '
        'MoreInfoListBox
        '
        Me.MoreInfoListBox.FormattingEnabled = True
        Me.MoreInfoListBox.ItemHeight = 18
        Me.MoreInfoListBox.Location = New System.Drawing.Point(213, 102)
        Me.MoreInfoListBox.Name = "MoreInfoListBox"
        Me.MoreInfoListBox.Size = New System.Drawing.Size(258, 76)
        Me.MoreInfoListBox.TabIndex = 12
        Me.MoreInfoListBox.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.DiscountTextBox)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.RemainingPaymentTextBox)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.TotalPaidTextBox)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.TotalPaymentTextBox)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(646, 8)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(264, 191)
        Me.GroupBox2.TabIndex = 11
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Credits Summary"
        '
        'DiscountTextBox
        '
        Me.DiscountTextBox.Location = New System.Drawing.Point(131, 69)
        Me.DiscountTextBox.Name = "DiscountTextBox"
        Me.DiscountTextBox.ReadOnly = True
        Me.DiscountTextBox.Size = New System.Drawing.Size(122, 26)
        Me.DiscountTextBox.TabIndex = 12
        Me.DiscountTextBox.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(14, 72)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 18)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Discount:"
        '
        'RemainingPaymentTextBox
        '
        Me.RemainingPaymentTextBox.Location = New System.Drawing.Point(131, 149)
        Me.RemainingPaymentTextBox.Name = "RemainingPaymentTextBox"
        Me.RemainingPaymentTextBox.ReadOnly = True
        Me.RemainingPaymentTextBox.Size = New System.Drawing.Size(122, 26)
        Me.RemainingPaymentTextBox.TabIndex = 10
        Me.RemainingPaymentTextBox.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(14, 153)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(111, 18)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Rem. Payment:"
        '
        'TotalPaidTextBox
        '
        Me.TotalPaidTextBox.Location = New System.Drawing.Point(131, 109)
        Me.TotalPaidTextBox.Name = "TotalPaidTextBox"
        Me.TotalPaidTextBox.ReadOnly = True
        Me.TotalPaidTextBox.Size = New System.Drawing.Size(122, 26)
        Me.TotalPaidTextBox.TabIndex = 8
        Me.TotalPaidTextBox.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(14, 112)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(77, 18)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Total Paid:"
        '
        'TotalPaymentTextBox
        '
        Me.TotalPaymentTextBox.Location = New System.Drawing.Point(131, 29)
        Me.TotalPaymentTextBox.Name = "TotalPaymentTextBox"
        Me.TotalPaymentTextBox.ReadOnly = True
        Me.TotalPaymentTextBox.Size = New System.Drawing.Size(122, 26)
        Me.TotalPaymentTextBox.TabIndex = 6
        Me.TotalPaymentTextBox.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(14, 32)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(109, 18)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Total Payment:"
        '
        'PaymentHistoryDataGridView
        '
        Me.PaymentHistoryDataGridView.AllowUserToAddRows = False
        Me.PaymentHistoryDataGridView.AllowUserToDeleteRows = False
        Me.PaymentHistoryDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.PaymentHistoryDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CHDateColumn, Me.CHPaidAmountColumn, Me.OrderNO01})
        Me.PaymentHistoryDataGridView.Location = New System.Drawing.Point(11, 52)
        Me.PaymentHistoryDataGridView.Name = "PaymentHistoryDataGridView"
        Me.PaymentHistoryDataGridView.ReadOnly = True
        Me.PaymentHistoryDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.PaymentHistoryDataGridView.Size = New System.Drawing.Size(243, 160)
        Me.PaymentHistoryDataGridView.TabIndex = 15
        '
        'CHDateColumn
        '
        Me.CHDateColumn.HeaderText = "Date Paid"
        Me.CHDateColumn.Name = "CHDateColumn"
        Me.CHDateColumn.ReadOnly = True
        Me.CHDateColumn.Width = 90
        '
        'CHPaidAmountColumn
        '
        Me.CHPaidAmountColumn.HeaderText = "Amount Paid"
        Me.CHPaidAmountColumn.Name = "CHPaidAmountColumn"
        Me.CHPaidAmountColumn.ReadOnly = True
        Me.CHPaidAmountColumn.Width = 110
        '
        'OrderNO01
        '
        Me.OrderNO01.HeaderText = "Order No"
        Me.OrderNO01.Name = "OrderNO01"
        Me.OrderNO01.ReadOnly = True
        Me.OrderNO01.Width = 130
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.PaymentHistoryTextBox)
        Me.GroupBox3.Controls.Add(Me.PaymentHistoryDataGridView)
        Me.GroupBox3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.ForeColor = System.Drawing.Color.Black
        Me.GroupBox3.Location = New System.Drawing.Point(646, 205)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(264, 226)
        Me.GroupBox3.TabIndex = 16
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Payment History"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(61, 23)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(52, 18)
        Me.Label11.TabIndex = 17
        Me.Label11.Text = "Search"
        '
        'PaymentHistoryTextBox
        '
        Me.PaymentHistoryTextBox.Location = New System.Drawing.Point(119, 20)
        Me.PaymentHistoryTextBox.Name = "PaymentHistoryTextBox"
        Me.PaymentHistoryTextBox.Size = New System.Drawing.Size(134, 26)
        Me.PaymentHistoryTextBox.TabIndex = 16
        Me.PaymentHistoryTextBox.TabStop = False
        '
        'LoanedProductsDataGridView
        '
        Me.LoanedProductsDataGridView.AllowUserToAddRows = False
        Me.LoanedProductsDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.White
        Me.LoanedProductsDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.LoanedProductsDataGridView.ColumnHeadersHeight = 40
        Me.LoanedProductsDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DateColumn, Me.OrderNo2, Me.ProductNo, Me.ProductnameColumn, Me.ProductbrandColumn, Me.ProductDescriptionColumn, Me.PurchaseCost, Me.SalePriceColumn, Me.QuantityColumn, Me.MeasuringunitColumn, Me.TotalPriceColumn, Me.RdiscountColumn, Me.RfinalPriceColumn})
        Me.LoanedProductsDataGridView.Location = New System.Drawing.Point(13, 47)
        Me.LoanedProductsDataGridView.Name = "LoanedProductsDataGridView"
        Me.LoanedProductsDataGridView.ReadOnly = True
        Me.LoanedProductsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.LoanedProductsDataGridView.Size = New System.Drawing.Size(873, 210)
        Me.LoanedProductsDataGridView.TabIndex = 17
        '
        'DateColumn
        '
        Me.DateColumn.HeaderText = "Date"
        Me.DateColumn.Name = "DateColumn"
        Me.DateColumn.ReadOnly = True
        Me.DateColumn.Width = 65
        '
        'OrderNo2
        '
        Me.OrderNo2.HeaderText = "Order No"
        Me.OrderNo2.Name = "OrderNo2"
        Me.OrderNo2.ReadOnly = True
        '
        'ProductNo
        '
        Me.ProductNo.HeaderText = "Product No"
        Me.ProductNo.Name = "ProductNo"
        Me.ProductNo.ReadOnly = True
        Me.ProductNo.Width = 90
        '
        'ProductnameColumn
        '
        Me.ProductnameColumn.HeaderText = "Product Name"
        Me.ProductnameColumn.Name = "ProductnameColumn"
        Me.ProductnameColumn.ReadOnly = True
        Me.ProductnameColumn.Width = 120
        '
        'ProductbrandColumn
        '
        Me.ProductbrandColumn.HeaderText = "Product Brand"
        Me.ProductbrandColumn.Name = "ProductbrandColumn"
        Me.ProductbrandColumn.ReadOnly = True
        Me.ProductbrandColumn.Width = 110
        '
        'ProductDescriptionColumn
        '
        Me.ProductDescriptionColumn.HeaderText = "Product Description"
        Me.ProductDescriptionColumn.Name = "ProductDescriptionColumn"
        Me.ProductDescriptionColumn.ReadOnly = True
        Me.ProductDescriptionColumn.Width = 110
        '
        'PurchaseCost
        '
        Me.PurchaseCost.HeaderText = "Purchase Cost"
        Me.PurchaseCost.Name = "PurchaseCost"
        Me.PurchaseCost.ReadOnly = True
        Me.PurchaseCost.Width = 70
        '
        'SalePriceColumn
        '
        Me.SalePriceColumn.HeaderText = "Sale Price"
        Me.SalePriceColumn.Name = "SalePriceColumn"
        Me.SalePriceColumn.ReadOnly = True
        Me.SalePriceColumn.Width = 70
        '
        'QuantityColumn
        '
        Me.QuantityColumn.HeaderText = "Quantity"
        Me.QuantityColumn.Name = "QuantityColumn"
        Me.QuantityColumn.ReadOnly = True
        Me.QuantityColumn.Width = 70
        '
        'MeasuringunitColumn
        '
        Me.MeasuringunitColumn.HeaderText = "Measuring Unit"
        Me.MeasuringunitColumn.Name = "MeasuringunitColumn"
        Me.MeasuringunitColumn.ReadOnly = True
        Me.MeasuringunitColumn.Width = 70
        '
        'TotalPriceColumn
        '
        Me.TotalPriceColumn.HeaderText = "Total Price"
        Me.TotalPriceColumn.Name = "TotalPriceColumn"
        Me.TotalPriceColumn.ReadOnly = True
        Me.TotalPriceColumn.Width = 70
        '
        'RdiscountColumn
        '
        Me.RdiscountColumn.HeaderText = "Discount"
        Me.RdiscountColumn.Name = "RdiscountColumn"
        Me.RdiscountColumn.ReadOnly = True
        Me.RdiscountColumn.Width = 70
        '
        'RfinalPriceColumn
        '
        Me.RfinalPriceColumn.HeaderText = "Final Price"
        Me.RfinalPriceColumn.Name = "RfinalPriceColumn"
        Me.RfinalPriceColumn.ReadOnly = True
        Me.RfinalPriceColumn.Width = 70
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox4.Controls.Add(Me.Label10)
        Me.GroupBox4.Controls.Add(Me.SearchLoanedProductTextBox)
        Me.GroupBox4.Controls.Add(Me.Label8)
        Me.GroupBox4.Controls.Add(Me.LoanedProductsDataGridView)
        Me.GroupBox4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(14, 437)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(899, 269)
        Me.GroupBox4.TabIndex = 18
        Me.GroupBox4.TabStop = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(385, 18)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(165, 18)
        Me.Label10.TabIndex = 13
        Me.Label10.Text = "Search Loaned Products"
        '
        'SearchLoanedProductTextBox
        '
        Me.SearchLoanedProductTextBox.Location = New System.Drawing.Point(556, 19)
        Me.SearchLoanedProductTextBox.Name = "SearchLoanedProductTextBox"
        Me.SearchLoanedProductTextBox.Size = New System.Drawing.Size(330, 21)
        Me.SearchLoanedProductTextBox.TabIndex = 20
        Me.SearchLoanedProductTextBox.TabStop = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(6, -3)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(116, 18)
        Me.Label8.TabIndex = 19
        Me.Label8.Text = "Loaned Products"
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox5.Controls.Add(Me.Label9)
        Me.GroupBox5.Controls.Add(Me.SearchLoansTextBox)
        Me.GroupBox5.Controls.Add(Me.LoansDataGridView)
        Me.GroupBox5.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.ForeColor = System.Drawing.Color.Black
        Me.GroupBox5.Location = New System.Drawing.Point(14, 205)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(621, 226)
        Me.GroupBox5.TabIndex = 17
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Loans"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(298, 23)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(95, 18)
        Me.Label9.TabIndex = 12
        Me.Label9.Text = "Search Loans"
        '
        'SearchLoansTextBox
        '
        Me.SearchLoansTextBox.Location = New System.Drawing.Point(399, 20)
        Me.SearchLoansTextBox.Name = "SearchLoansTextBox"
        Me.SearchLoansTextBox.Size = New System.Drawing.Size(209, 26)
        Me.SearchLoansTextBox.TabIndex = 11
        Me.SearchLoansTextBox.TabStop = False
        '
        'LoansDataGridView
        '
        Me.LoansDataGridView.AllowUserToAddRows = False
        Me.LoansDataGridView.AllowUserToDeleteRows = False
        Me.LoansDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.LoansDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.OrderDate, Me.OrderNo, Me.TotalPayment, Me.DiscountGiven, Me.AmountPaid, Me.RemainingPayment})
        Me.LoansDataGridView.Location = New System.Drawing.Point(13, 52)
        Me.LoansDataGridView.Name = "LoansDataGridView"
        Me.LoansDataGridView.ReadOnly = True
        Me.LoansDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.LoansDataGridView.Size = New System.Drawing.Size(595, 160)
        Me.LoansDataGridView.TabIndex = 0
        '
        'OrderDate
        '
        Me.OrderDate.HeaderText = "Date"
        Me.OrderDate.Name = "OrderDate"
        Me.OrderDate.ReadOnly = True
        Me.OrderDate.Width = 90
        '
        'OrderNo
        '
        Me.OrderNo.HeaderText = "Order No"
        Me.OrderNo.Name = "OrderNo"
        Me.OrderNo.ReadOnly = True
        Me.OrderNo.Width = 130
        '
        'TotalPayment
        '
        Me.TotalPayment.HeaderText = "Total Payment"
        Me.TotalPayment.Name = "TotalPayment"
        Me.TotalPayment.ReadOnly = True
        '
        'DiscountGiven
        '
        Me.DiscountGiven.HeaderText = "Discount Given"
        Me.DiscountGiven.Name = "DiscountGiven"
        Me.DiscountGiven.ReadOnly = True
        '
        'AmountPaid
        '
        Me.AmountPaid.HeaderText = "Amount Paid"
        Me.AmountPaid.Name = "AmountPaid"
        Me.AmountPaid.ReadOnly = True
        '
        'RemainingPayment
        '
        Me.RemainingPayment.HeaderText = "Remaining Payment"
        Me.RemainingPayment.Name = "RemainingPayment"
        Me.RemainingPayment.ReadOnly = True
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RefreshToolStripMenuItem, Me.PayToolStripMenuItem, Me.PrintToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(141, 70)
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.RefreshToolStripMenuItem.Text = "Refresh"
        '
        'PayToolStripMenuItem
        '
        Me.PayToolStripMenuItem.Enabled = False
        Me.PayToolStripMenuItem.Name = "PayToolStripMenuItem"
        Me.PayToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.PayToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.PayToolStripMenuItem.Text = "Pay"
        Me.PayToolStripMenuItem.Visible = False
        '
        'PrintToolStripMenuItem
        '
        Me.PrintToolStripMenuItem.Name = "PrintToolStripMenuItem"
        Me.PrintToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.PrintToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.PrintToolStripMenuItem.Text = "Print"
        '
        'PrintDialog1
        '
        Me.PrintDialog1.UseEXDialog = True
        '
        'CreditsPrintDocument
        '
        '
        'PrintPreviewDialog1
        '
        Me.PrintPreviewDialog1.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPreviewDialog1.Enabled = True
        Me.PrintPreviewDialog1.Icon = CType(resources.GetObject("PrintPreviewDialog1.Icon"), System.Drawing.Icon)
        Me.PrintPreviewDialog1.Name = "PrintPreviewDialog1"
        Me.PrintPreviewDialog1.Visible = False
        '
        'CreditsInformationForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.ROJBackground
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(925, 715)
        Me.ContextMenuStrip = Me.ContextMenuStrip1
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "CreditsInformationForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Credits Information"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DebtorsPhotoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.PaymentHistoryDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.LoanedProductsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.LoansDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Label3 As Label
    Friend WithEvents ContactLabel As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents FullNameLabel As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents MoreInfoListBox As ListBox
    Friend WithEvents DebtorsPhotoPictureBox As PictureBox
    Friend WithEvents UpdateinfoButton As Button
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents DiscountTextBox As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents RemainingPaymentTextBox As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents TotalPaidTextBox As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents TotalPaymentTextBox As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents PaymentHistoryDataGridView As DataGridView
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents LoanedProductsDataGridView As DataGridView
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents Label8 As Label
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents LoansDataGridView As DataGridView
    Friend WithEvents Label10 As Label
    Friend WithEvents SearchLoanedProductTextBox As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents SearchLoansTextBox As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents PaymentHistoryTextBox As TextBox
    Friend WithEvents IDLabel As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents OrderDate As DataGridViewTextBoxColumn
    Friend WithEvents OrderNo As DataGridViewTextBoxColumn
    Friend WithEvents TotalPayment As DataGridViewTextBoxColumn
    Friend WithEvents DiscountGiven As DataGridViewTextBoxColumn
    Friend WithEvents AmountPaid As DataGridViewTextBoxColumn
    Friend WithEvents RemainingPayment As DataGridViewTextBoxColumn
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents RefreshToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DateColumn As DataGridViewTextBoxColumn
    Friend WithEvents OrderNo2 As DataGridViewTextBoxColumn
    Friend WithEvents ProductNo As DataGridViewTextBoxColumn
    Friend WithEvents ProductnameColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductbrandColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductDescriptionColumn As DataGridViewTextBoxColumn
    Friend WithEvents PurchaseCost As DataGridViewTextBoxColumn
    Friend WithEvents SalePriceColumn As DataGridViewTextBoxColumn
    Friend WithEvents QuantityColumn As DataGridViewTextBoxColumn
    Friend WithEvents MeasuringunitColumn As DataGridViewTextBoxColumn
    Friend WithEvents TotalPriceColumn As DataGridViewTextBoxColumn
    Friend WithEvents RdiscountColumn As DataGridViewTextBoxColumn
    Friend WithEvents RfinalPriceColumn As DataGridViewTextBoxColumn
    Friend WithEvents CHDateColumn As DataGridViewTextBoxColumn
    Friend WithEvents CHPaidAmountColumn As DataGridViewTextBoxColumn
    Friend WithEvents OrderNO01 As DataGridViewTextBoxColumn
    Friend WithEvents PayToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrintToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrintDialog1 As PrintDialog
    Friend WithEvents CreditsPrintDocument As Printing.PrintDocument
    Friend WithEvents PrintPreviewDialog1 As PrintPreviewDialog
End Class
