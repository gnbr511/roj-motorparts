﻿Imports MySql.Data.MySqlClient
Public Class SelectProductForm

    Friend EditOrAdd As String 'use for the selectform to determine call is from add or edit form
    Friend customername As String
    Private Sub SelectProductForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim ScreenResHeight, ScreenResWidth, yLocation, xLocation As Integer
        ScreenResHeight = Screen.PrimaryScreen.WorkingArea.Height
        ScreenResWidth = Screen.PrimaryScreen.WorkingArea.Width
        yLocation = (ScreenResHeight - 757) / 2 + 56
        xLocation = (ScreenResWidth - 1369) / 2
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New Point(xLocation, yLocation)

        SelectProductsDataGridView.Rows.Clear()
        Try
            OpenConnection()
        Catch ex As Exception
        End Try

        Try
            Query = "select * from products order by productname limit 100"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim a = Reader.GetInt32("no")
                Dim b = Reader.GetString("productname")
                Dim c = Reader.GetString("productbrand")
                Dim d = Reader.GetString("productdescription")
                Dim h = Reader.GetDecimal("supretprice")
                Dim i = Reader.GetDecimal("purchasecost")
                Dim f = Reader.GetDecimal("saleprice")
                Dim g = Reader.GetDecimal("availablestock")
                If h = 0 Then
                    SelectProductsDataGridView.Rows.Add(a, b, c, d, "", i, f, g)
                Else
                    SelectProductsDataGridView.Rows.Add(a, b, c, d, h, i, f, g)
                End If
            End While
            Reader.Close()
            SelectProductsDataGridView.ClearSelection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        SelectSearchProductTextBox.Focus()
        MysqlCon.Close()
    End Sub

    Private Sub SelectSearchTextBox_TextChanged(sender As Object, e As EventArgs) Handles SelectSearchProductTextBox.TextChanged
        Try
            MysqlCon = New MySqlConnection
            MysqlCon.ConnectionString = "server = '" & Host & "'; " _
                & "database = '" & Database & "'; " _
                & "username = '" & User & "'; " _
                & "password = '" & Password & "'"
            MysqlCon.Open()
        Catch ex As Exception
        End Try

        SelectProductsDataGridView.Rows.Clear()
        Try
            Query = "select * from products where productname like '%" & SelectSearchProductTextBox.Text _
                & "%' or no like '%" & SelectSearchProductTextBox.Text & "%' order by productname"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim a = Reader.GetInt32("no")
                Dim b = Reader.GetString("productname")
                Dim c = Reader.GetString("productbrand")
                Dim d = Reader.GetString("productdescription")
                Dim h = Reader.GetDecimal("supretprice")
                Dim i = Reader.GetDecimal("purchasecost")
                Dim f = Reader.GetDecimal("saleprice")
                Dim g = Reader.GetDecimal("availablestock")
                If h = 0 Then
                    SelectProductsDataGridView.Rows.Add(a, b, c, d, "", i, f, g)
                Else
                    SelectProductsDataGridView.Rows.Add(a, b, c, d, h, i, f, g)
                End If
            End While
            SelectProductsDataGridView.ClearSelection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        MysqlCon.Close()
    End Sub

    Private Sub SelectProductsDataGridView_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles SelectProductsDataGridView.CellDoubleClick
        Dim SelectedProductNo As Integer = SelectProductsDataGridView.CurrentRow.Cells(0).Value
        Dim stockavailable As Integer = SelectProductsDataGridView.CurrentRow.Cells(7).Value
        If stockavailable <= 0 Then
            MessageBox.Show("This product is out-of-stock.", "Product not available!", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            Try
                MysqlCon = New MySqlConnection
                MysqlCon.ConnectionString = "server = '" & Host & "'; " _
                    & "database = '" & Database & "'; " _
                    & "username = '" & User & "'; " _
                    & "password = '" & Password & "'"
                MysqlCon.Open()
            Catch ex As Exception
            End Try
            Try
                Query = "Select * From products where no = " & SelectedProductNo
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                While Reader.Read
                    Dim a = Reader.GetInt32("no")
                    Dim b = Reader.GetString("Productname")
                    Dim c = Reader.GetString("productbrand")
                    Dim d = Reader.GetString("productdescription")
                    Dim f = Reader.GetDecimal("saleprice")
                    Dim g = Reader.GetString("measuringunit")
                    If EditOrAdd = "edit" Then
                        'check if item is already in the order.
                        Dim messagestring As String = "You can't change an item with an item that is already in the orders." _
                            & Environment.NewLine & Environment.NewLine & "Tip: If you want to increase the quantity of an ordered item, " _
                            & "you can directly edit/update it. You can also do it by clicking the 'ADD ORDERS' button, " _
                            & "select the item, enter the quantity to be added, and then click the 'ADD TO ORDERS' button."
                        Dim flag As Boolean = False
                        For i As Integer = 0 To StoreForm.OrdersDataGridView.RowCount() - 1
                            If StoreForm.OrdersDataGridView.Rows(i).Cells(0).Value() = SelectedProductNo Then
                                flag = True
                            End If
                        Next
                        If flag = True Then
                            MessageBox.Show(messagestring, "Invalid selection!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Else
                            EditOrderForm.HiddenProductNoTextBox.Text = a.ToString()
                            EditOrderForm.ProductNameTextBox.Text = b.ToString()
                            EditOrderForm.BrandTextBox.Text = c.ToString()
                            EditOrderForm.DescriptionTextBox.Text = d.ToString()
                            EditOrderForm.PriceTextBox.Text = f.ToString()
                            EditOrderForm.MeasuringUnit = g
                            EditOrderForm.MeasuringUnitTextBox.Text = g.ToString()
                            EditOrderForm.QuantityTextBox.Clear()
                        End If
                    Else
                        AddSalesForm.customername = customername
                        AddSalesForm.HiddenProductNoTextBox.Text = a.ToString()
                        AddSalesForm.ProductNameTextBox.Text = b.ToString()
                        AddSalesForm.BrandTextBox.Text = c.ToString()
                        AddSalesForm.DescriptionTextBox.Text = d.ToString()
                        AddSalesForm.PriceTextBox.Text = f.ToString()
                        AddSalesForm.MeasuringUnit = g
                        AddSalesForm.MeasuringUnitTextBox.Text = g.ToString()
                        AddSalesForm.QuantityTextBox.Clear()
                    End If
                End While
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
            MysqlCon.Close()
            SelectProductsDataGridView.Rows.Clear()
            Me.Close()
        End If
    End Sub

    Private Sub SelectProductForm_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        SelectProductsDataGridView.ClearSelection()
        SelectSearchProductTextBox.Focus()
    End Sub

    Private Sub SelectProductForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        SelectSearchProductTextBox.Clear()
        EditOrAdd = ""
    End Sub

End Class