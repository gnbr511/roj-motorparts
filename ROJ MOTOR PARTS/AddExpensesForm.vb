﻿Imports MySql.Data.MySqlClient
Public Class AddExpensesForm
    Private Sub AddExpenseButton_Click(sender As Object, e As EventArgs) Handles AddExpenseButton.Click
        Try
            OpenConnection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Dim ExpenseCost, TotExpenses As Decimal
        Dim ExpenseName, ExpenseDescription As String
        If ExpensesNameTextBox.Text = "" Then
            MessageBox.Show("Expense name must not be empty.", "Required Field", MessageBoxButtons.OK, MessageBoxIcon.Information)
            With ExpensesNameTextBox
                .Focus()
                .SelectAll()
            End With
        Else
            ExpenseName = ExpensesNameTextBox.Text
            ExpenseDescription = ExpensesDescriptionTextBox.Text
            Try
                ExpenseCost = Decimal.Parse(ExpensesCostTextBox.Text)
                Dim mesres As DialogResult = MessageBox.Show("Add expense?", "Please Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                If mesres = DialogResult.Yes Then
                    Try
                        Query = "insert into todaysexpenses values((select convert(sysdate(), date)), '" & ExpenseName & "', '" & ExpenseDescription _
                            & "', " & ExpenseCost & ")"
                        Command = New MySqlCommand(Query, MysqlCon)
                        Reader = Command.ExecuteReader
                        Reader.Close()
                        'Display Expenses
                        Try
                            StoreForm.ExpensesDataGridView.Rows.Clear()
                            Query = "select * from todaysexpenses where date = (select convert(sysdate(), date))"
                            Command = New MySqlCommand(Query, MysqlCon)
                            Reader = Command.ExecuteReader
                            While Reader.Read
                                Dim a As Integer = StoreForm.ExpensesDataGridView.RowCount() + 1
                                Dim b = Reader.GetString("name")
                                Dim c = Reader.GetString("description")
                                Dim d = Reader.GetString("cost")
                                StoreForm.ExpensesDataGridView.Rows.Add(a, b, c, d)
                                TotExpenses += d
                            End While
                            Reader.Close()
                            'Update or add todayssale from database
                            Dim counttodayssale As Integer = 0
                            Try
                                Query = "select * from todayssale where date = (select convert(sysdate(), date))"
                                Command = New MySqlCommand(Query, MysqlCon)
                                Reader = Command.ExecuteReader
                                While Reader.Read
                                    counttodayssale = 1
                                End While
                                Reader.Close()
                            Catch ex As Exception
                                MessageBox.Show(ex.Message)
                            End Try
                            Try
                                If counttodayssale < 1 Then
                                    Query = "insert into todayssale(date, totalexpenses) values((select convert(sysdate(), date)), " & TotExpenses & ")"
                                    Command = New MySqlCommand(Query, MysqlCon)
                                    Reader = Command.ExecuteReader
                                    Reader.Close()
                                Else
                                    Query = "update todayssale set totalexpenses = " & TotExpenses _
                                    & " where date = (select convert(sysdate(), date))"
                                    Command = New MySqlCommand(Query, MysqlCon)
                                    Reader = Command.ExecuteReader
                                    Reader.Close()
                                End If
                                'Display sales summary
                                Dim SummaryGrossSale, SummaryTotalDiscount, SummaryTotalExpenses, SummaryNetSale As Decimal
                                Try
                                    Query = "select * from todayssale where date = (select convert(sysdate(), date))"
                                    Command = New MySqlCommand(Query, MysqlCon)
                                    Reader = Command.ExecuteReader
                                    While Reader.Read
                                        SummaryGrossSale = Reader.GetDecimal("grosssale")
                                        SummaryTotalDiscount = Reader.GetDecimal("totaldiscount")
                                        SummaryTotalExpenses = Reader.GetDecimal("totalexpenses")
                                    End While
                                    Reader.Close()
                                    SummaryNetSale = SummaryGrossSale - (SummaryTotalDiscount + SummaryTotalExpenses)
                                    StoreForm.GrossSaleTextBox.Text = SummaryGrossSale.ToString("n2")
                                    StoreForm.TotalDiscountTextBox.Text = SummaryTotalDiscount.ToString("n2")
                                    StoreForm.TotalExpensesTextBox.Text = SummaryTotalExpenses.ToString("n2")
                                    StoreForm.NetSaleTextBox.Text = SummaryNetSale.ToString("n2")
                                    MessageBox.Show("Expenses added successfully.", "", MessageBoxButtons.OK, MessageBoxIcon.Information)

                                    'Clear
                                    TotExpenses = 0
                                    ExpensesNameTextBox.Clear()
                                    ExpensesDescriptionTextBox.Clear()
                                    ExpensesCostTextBox.Clear()
                                    ExpensesNameTextBox.Focus()
                                Catch ex As Exception
                                    MessageBox.Show(ex.Message, "Display sales summary error")
                                End Try
                            Catch ex As Exception
                                MessageBox.Show(ex.Message, "Update todayssale error")
                            End Try
                        Catch ex As Exception
                            MessageBox.Show(ex.Message, "Display exp error")
                        End Try
                    Catch ex As Exception
                        MessageBox.Show(ex.Message, "Add Expense Error")
                    End Try
                Else

                End If
            Catch ex As Exception
                MessageBox.Show("Expenses cost must be numeric.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Information)
                With ExpensesCostTextBox
                    .Focus()
                    .SelectAll()
                End With
            End Try
        End If
        MysqlCon.Close()
    End Sub

    Private Sub CancelExpenseButton_Click(sender As Object, e As EventArgs) Handles CancelExpenseButton.Click
        Me.Close()
    End Sub

    Private Sub AddExpensesForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        ExpensesCostTextBox.Clear()
        ExpensesNameTextBox.Clear()
        ExpensesDescriptionTextBox.Clear()
    End Sub

    Private Sub AddExpensesForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim ScreenResHeight, ScreenResWidth, yLocation, xLocation As Integer
        ScreenResHeight = Screen.PrimaryScreen.WorkingArea.Height
        ScreenResWidth = Screen.PrimaryScreen.WorkingArea.Width
        yLocation = (ScreenResHeight - 757) / 2 + 255
        xLocation = (ScreenResWidth - 1369) / 2 + 304
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New Point(xLocation, yLocation)
    End Sub

    Private Sub ExpensesCostTextBox_TextChanged(sender As Object, e As EventArgs) Handles ExpensesCostTextBox.TextChanged
        Try
            If Decimal.Parse(ExpensesCostTextBox.Text) = 0 Then
                ExpensesCostTextBox.Clear()
            End If
        Catch ex As Exception
            ExpensesCostTextBox.Clear()
        End Try
    End Sub
End Class