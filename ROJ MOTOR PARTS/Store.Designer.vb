﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class StoreForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(StoreForm))
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DateTimeLabel = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.totaldiscountLabel = New System.Windows.Forms.Label()
        Me.NetSaleTextBox = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TotalExpensesTextBox = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GrossSaleTextBox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TotalDiscountTextBox = New System.Windows.Forms.TextBox()
        Me.OrdersDataGridView = New System.Windows.Forms.DataGridView()
        Me.NoColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NumberColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductNameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductBrandColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductDescriptionColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QuantityColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MesuringUnitColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalPriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DiscountColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FinalPriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.time = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.purchasecost = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OrdersGroupBox = New System.Windows.Forms.GroupBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.OrderNoTextBox = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.DeleteOrderButton = New System.Windows.Forms.Button()
        Me.OrderCompleteButton = New System.Windows.Forms.Button()
        Me.CancelOrderButton = New System.Windows.Forms.Button()
        Me.EditOrderButton = New System.Windows.Forms.Button()
        Me.ChangeTextBox = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.AmountReceivedTextBox = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.GrandTotalTextBox = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.DiscountTextBox = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.SubTotalTextBox = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.CustomerNameTextBox = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.SearchDebtorTextBox = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.MoreInfoButton = New System.Windows.Forms.Button()
        Me.PayCreditButton = New System.Windows.Forms.Button()
        Me.CreditsDataGridView = New System.Windows.Forms.DataGridView()
        Me.DebtorID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CusNameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PaymentColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DscntColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AmountPaidColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RemainBalColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContactNoColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TodaySaleDataGridView = New System.Windows.Forms.DataGridView()
        Me.ProdNoColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustomerNameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MsrngUnitColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DscntTSPColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FinalTSDPriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OrderNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.ExpensesDataGridView = New System.Windows.Forms.DataGridView()
        Me.NumColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ExpensesNameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescriptionColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CostColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.AddSaleButton = New System.Windows.Forms.Button()
        Me.AddExpensesButton = New System.Windows.Forms.Button()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.TodaysPaidCreditsDataGridView = New System.Windows.Forms.DataGridView()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.AddSaleToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddExpensesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.InventoryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecordsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddSaleToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddExpensesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.InventoryToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecordsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.SettingsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TPCfullname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TPCPaidAmountColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DebtorsID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OrderNumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.OrdersDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.OrdersGroupBox.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        CType(Me.CreditsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TodaySaleDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        CType(Me.ExpensesDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        CType(Me.TodaysPaidCreditsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.DateTimeLabel)
        Me.GroupBox2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(1086, 42)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(256, 57)
        Me.GroupBox2.TabIndex = 8
        Me.GroupBox2.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(7, -2)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(126, 18)
        Me.Label1.TabIndex = 30
        Me.Label1.Text = "DATE AND TIME"
        '
        'DateTimeLabel
        '
        Me.DateTimeLabel.BackColor = System.Drawing.Color.Transparent
        Me.DateTimeLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DateTimeLabel.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimeLabel.ForeColor = System.Drawing.Color.Black
        Me.DateTimeLabel.Location = New System.Drawing.Point(12, 18)
        Me.DateTimeLabel.Name = "DateTimeLabel"
        Me.DateTimeLabel.Size = New System.Drawing.Size(232, 31)
        Me.DateTimeLabel.TabIndex = 9
        Me.DateTimeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Timer1
        '
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.totaldiscountLabel)
        Me.GroupBox3.Controls.Add(Me.NetSaleTextBox)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.TotalExpensesTextBox)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.GrossSaleTextBox)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.TotalDiscountTextBox)
        Me.GroupBox3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(1086, 106)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(256, 160)
        Me.GroupBox3.TabIndex = 10
        Me.GroupBox3.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, -2)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(139, 18)
        Me.Label2.TabIndex = 31
        Me.Label2.Text = "SALES SUMMARY"
        '
        'totaldiscountLabel
        '
        Me.totaldiscountLabel.AutoSize = True
        Me.totaldiscountLabel.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.totaldiscountLabel.Location = New System.Drawing.Point(22, 59)
        Me.totaldiscountLabel.Name = "totaldiscountLabel"
        Me.totaldiscountLabel.Size = New System.Drawing.Size(112, 16)
        Me.totaldiscountLabel.TabIndex = 6
        Me.totaldiscountLabel.Text = "TOTAL DISCOUNT"
        '
        'NetSaleTextBox
        '
        Me.NetSaleTextBox.Location = New System.Drawing.Point(140, 122)
        Me.NetSaleTextBox.Name = "NetSaleTextBox"
        Me.NetSaleTextBox.ReadOnly = True
        Me.NetSaleTextBox.Size = New System.Drawing.Size(98, 23)
        Me.NetSaleTextBox.TabIndex = 5
        Me.NetSaleTextBox.TabStop = False
        Me.NetSaleTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(22, 125)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(64, 16)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "NET SALE"
        '
        'TotalExpensesTextBox
        '
        Me.TotalExpensesTextBox.Location = New System.Drawing.Point(140, 90)
        Me.TotalExpensesTextBox.Name = "TotalExpensesTextBox"
        Me.TotalExpensesTextBox.ReadOnly = True
        Me.TotalExpensesTextBox.Size = New System.Drawing.Size(98, 23)
        Me.TotalExpensesTextBox.TabIndex = 3
        Me.TotalExpensesTextBox.TabStop = False
        Me.TotalExpensesTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(22, 93)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(111, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "TOTAL EXPENSES"
        '
        'GrossSaleTextBox
        '
        Me.GrossSaleTextBox.Location = New System.Drawing.Point(140, 23)
        Me.GrossSaleTextBox.Name = "GrossSaleTextBox"
        Me.GrossSaleTextBox.ReadOnly = True
        Me.GrossSaleTextBox.Size = New System.Drawing.Size(98, 23)
        Me.GrossSaleTextBox.TabIndex = 1
        Me.GrossSaleTextBox.TabStop = False
        Me.GrossSaleTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(22, 25)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(82, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "GROSS SALE"
        '
        'TotalDiscountTextBox
        '
        Me.TotalDiscountTextBox.Location = New System.Drawing.Point(140, 56)
        Me.TotalDiscountTextBox.Name = "TotalDiscountTextBox"
        Me.TotalDiscountTextBox.ReadOnly = True
        Me.TotalDiscountTextBox.Size = New System.Drawing.Size(98, 23)
        Me.TotalDiscountTextBox.TabIndex = 7
        Me.TotalDiscountTextBox.TabStop = False
        Me.TotalDiscountTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'OrdersDataGridView
        '
        Me.OrdersDataGridView.AllowUserToAddRows = False
        Me.OrdersDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro
        Me.OrdersDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.OrdersDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.OrdersDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NoColumn, Me.NumberColumn, Me.ProductNameColumn, Me.ProductBrandColumn, Me.ProductDescriptionColumn, Me.PriceColumn, Me.QuantityColumn, Me.MesuringUnitColumn, Me.TotalPriceColumn, Me.DiscountColumn, Me.FinalPriceColumn, Me.time, Me.purchasecost})
        Me.OrdersDataGridView.Location = New System.Drawing.Point(10, 82)
        Me.OrdersDataGridView.MultiSelect = False
        Me.OrdersDataGridView.Name = "OrdersDataGridView"
        Me.OrdersDataGridView.ReadOnly = True
        Me.OrdersDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.OrdersDataGridView.Size = New System.Drawing.Size(694, 356)
        Me.OrdersDataGridView.TabIndex = 11
        '
        'NoColumn
        '
        Me.NoColumn.HeaderText = "No"
        Me.NoColumn.Name = "NoColumn"
        Me.NoColumn.ReadOnly = True
        Me.NoColumn.Width = 59
        '
        'NumberColumn
        '
        Me.NumberColumn.HeaderText = "#"
        Me.NumberColumn.Name = "NumberColumn"
        Me.NumberColumn.ReadOnly = True
        Me.NumberColumn.Visible = False
        Me.NumberColumn.Width = 29
        '
        'ProductNameColumn
        '
        Me.ProductNameColumn.HeaderText = "Product Name"
        Me.ProductNameColumn.Name = "ProductNameColumn"
        Me.ProductNameColumn.ReadOnly = True
        Me.ProductNameColumn.Width = 94
        '
        'ProductBrandColumn
        '
        Me.ProductBrandColumn.HeaderText = "Brand"
        Me.ProductBrandColumn.Name = "ProductBrandColumn"
        Me.ProductBrandColumn.ReadOnly = True
        Me.ProductBrandColumn.Width = 70
        '
        'ProductDescriptionColumn
        '
        Me.ProductDescriptionColumn.HeaderText = "Description"
        Me.ProductDescriptionColumn.Name = "ProductDescriptionColumn"
        Me.ProductDescriptionColumn.ReadOnly = True
        Me.ProductDescriptionColumn.Width = 75
        '
        'PriceColumn
        '
        Me.PriceColumn.HeaderText = "Price"
        Me.PriceColumn.Name = "PriceColumn"
        Me.PriceColumn.ReadOnly = True
        Me.PriceColumn.Width = 60
        '
        'QuantityColumn
        '
        Me.QuantityColumn.HeaderText = "Qty"
        Me.QuantityColumn.Name = "QuantityColumn"
        Me.QuantityColumn.ReadOnly = True
        Me.QuantityColumn.Width = 48
        '
        'MesuringUnitColumn
        '
        Me.MesuringUnitColumn.HeaderText = "Msrng Unit"
        Me.MesuringUnitColumn.Name = "MesuringUnitColumn"
        Me.MesuringUnitColumn.ReadOnly = True
        Me.MesuringUnitColumn.Width = 48
        '
        'TotalPriceColumn
        '
        Me.TotalPriceColumn.HeaderText = "Total Price"
        Me.TotalPriceColumn.Name = "TotalPriceColumn"
        Me.TotalPriceColumn.ReadOnly = True
        Me.TotalPriceColumn.Width = 60
        '
        'DiscountColumn
        '
        Me.DiscountColumn.HeaderText = "Discount"
        Me.DiscountColumn.Name = "DiscountColumn"
        Me.DiscountColumn.ReadOnly = True
        Me.DiscountColumn.Width = 65
        '
        'FinalPriceColumn
        '
        Me.FinalPriceColumn.HeaderText = "Final Price"
        Me.FinalPriceColumn.Name = "FinalPriceColumn"
        Me.FinalPriceColumn.ReadOnly = True
        Me.FinalPriceColumn.Width = 71
        '
        'time
        '
        Me.time.HeaderText = "Time"
        Me.time.Name = "time"
        Me.time.ReadOnly = True
        Me.time.Visible = False
        '
        'purchasecost
        '
        Me.purchasecost.HeaderText = "PurchaseCost"
        Me.purchasecost.Name = "purchasecost"
        Me.purchasecost.ReadOnly = True
        Me.purchasecost.Visible = False
        '
        'OrdersGroupBox
        '
        Me.OrdersGroupBox.BackColor = System.Drawing.Color.Transparent
        Me.OrdersGroupBox.Controls.Add(Me.Label18)
        Me.OrdersGroupBox.Controls.Add(Me.OrderNoTextBox)
        Me.OrdersGroupBox.Controls.Add(Me.Label13)
        Me.OrdersGroupBox.Controls.Add(Me.DeleteOrderButton)
        Me.OrdersGroupBox.Controls.Add(Me.OrderCompleteButton)
        Me.OrdersGroupBox.Controls.Add(Me.CancelOrderButton)
        Me.OrdersGroupBox.Controls.Add(Me.EditOrderButton)
        Me.OrdersGroupBox.Controls.Add(Me.ChangeTextBox)
        Me.OrdersGroupBox.Controls.Add(Me.Label12)
        Me.OrdersGroupBox.Controls.Add(Me.AmountReceivedTextBox)
        Me.OrdersGroupBox.Controls.Add(Me.Label11)
        Me.OrdersGroupBox.Controls.Add(Me.GrandTotalTextBox)
        Me.OrdersGroupBox.Controls.Add(Me.Label10)
        Me.OrdersGroupBox.Controls.Add(Me.DiscountTextBox)
        Me.OrdersGroupBox.Controls.Add(Me.Label9)
        Me.OrdersGroupBox.Controls.Add(Me.SubTotalTextBox)
        Me.OrdersGroupBox.Controls.Add(Me.Label8)
        Me.OrdersGroupBox.Controls.Add(Me.CustomerNameTextBox)
        Me.OrdersGroupBox.Controls.Add(Me.Label7)
        Me.OrdersGroupBox.Controls.Add(Me.OrdersDataGridView)
        Me.OrdersGroupBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OrdersGroupBox.Location = New System.Drawing.Point(9, 42)
        Me.OrdersGroupBox.Name = "OrdersGroupBox"
        Me.OrdersGroupBox.Size = New System.Drawing.Size(711, 659)
        Me.OrdersGroupBox.TabIndex = 11
        Me.OrdersGroupBox.TabStop = False
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(8, 37)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(80, 19)
        Me.Label18.TabIndex = 30
        Me.Label18.Text = "Order No."
        '
        'OrderNoTextBox
        '
        Me.OrderNoTextBox.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.OrderNoTextBox.Location = New System.Drawing.Point(94, 34)
        Me.OrderNoTextBox.Name = "OrderNoTextBox"
        Me.OrderNoTextBox.ReadOnly = True
        Me.OrderNoTextBox.Size = New System.Drawing.Size(171, 27)
        Me.OrderNoTextBox.TabIndex = 29
        Me.OrderNoTextBox.TabStop = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(9, -2)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(72, 18)
        Me.Label13.TabIndex = 28
        Me.Label13.Text = "ORDERS"
        '
        'DeleteOrderButton
        '
        Me.DeleteOrderButton.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DeleteOrderButton.Location = New System.Drawing.Point(17, 593)
        Me.DeleteOrderButton.Name = "DeleteOrderButton"
        Me.DeleteOrderButton.Size = New System.Drawing.Size(166, 43)
        Me.DeleteOrderButton.TabIndex = 27
        Me.DeleteOrderButton.Text = "&DELETE ORDER"
        Me.DeleteOrderButton.UseVisualStyleBackColor = True
        '
        'OrderCompleteButton
        '
        Me.OrderCompleteButton.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OrderCompleteButton.Location = New System.Drawing.Point(527, 593)
        Me.OrderCompleteButton.Name = "OrderCompleteButton"
        Me.OrderCompleteButton.Size = New System.Drawing.Size(166, 43)
        Me.OrderCompleteButton.TabIndex = 26
        Me.OrderCompleteButton.Text = "&ORDER COMPLETE"
        Me.OrderCompleteButton.UseVisualStyleBackColor = True
        '
        'CancelOrderButton
        '
        Me.CancelOrderButton.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CancelOrderButton.Location = New System.Drawing.Point(187, 593)
        Me.CancelOrderButton.Name = "CancelOrderButton"
        Me.CancelOrderButton.Size = New System.Drawing.Size(166, 43)
        Me.CancelOrderButton.TabIndex = 25
        Me.CancelOrderButton.Text = "&CANCEL ORDERS"
        Me.CancelOrderButton.UseVisualStyleBackColor = True
        '
        'EditOrderButton
        '
        Me.EditOrderButton.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EditOrderButton.Location = New System.Drawing.Point(357, 593)
        Me.EditOrderButton.Name = "EditOrderButton"
        Me.EditOrderButton.Size = New System.Drawing.Size(166, 43)
        Me.EditOrderButton.TabIndex = 24
        Me.EditOrderButton.Text = "&EDIT ORDER"
        Me.EditOrderButton.UseVisualStyleBackColor = True
        '
        'ChangeTextBox
        '
        Me.ChangeTextBox.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChangeTextBox.Location = New System.Drawing.Point(556, 531)
        Me.ChangeTextBox.Name = "ChangeTextBox"
        Me.ChangeTextBox.ReadOnly = True
        Me.ChangeTextBox.Size = New System.Drawing.Size(137, 27)
        Me.ChangeTextBox.TabIndex = 23
        Me.ChangeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(400, 539)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(62, 19)
        Me.Label12.TabIndex = 22
        Me.Label12.Text = "Change"
        '
        'AmountReceivedTextBox
        '
        Me.AmountReceivedTextBox.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AmountReceivedTextBox.Location = New System.Drawing.Point(556, 498)
        Me.AmountReceivedTextBox.Name = "AmountReceivedTextBox"
        Me.AmountReceivedTextBox.Size = New System.Drawing.Size(137, 27)
        Me.AmountReceivedTextBox.TabIndex = 21
        Me.AmountReceivedTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(400, 506)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(133, 19)
        Me.Label11.TabIndex = 20
        Me.Label11.Text = "Amount Received"
        '
        'GrandTotalTextBox
        '
        Me.GrandTotalTextBox.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.GrandTotalTextBox.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GrandTotalTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GrandTotalTextBox.Location = New System.Drawing.Point(556, 465)
        Me.GrandTotalTextBox.Name = "GrandTotalTextBox"
        Me.GrandTotalTextBox.ReadOnly = True
        Me.GrandTotalTextBox.Size = New System.Drawing.Size(137, 27)
        Me.GrandTotalTextBox.TabIndex = 19
        Me.GrandTotalTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(400, 473)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(93, 19)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Grand Total"
        '
        'DiscountTextBox
        '
        Me.DiscountTextBox.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DiscountTextBox.Location = New System.Drawing.Point(174, 498)
        Me.DiscountTextBox.Name = "DiscountTextBox"
        Me.DiscountTextBox.ReadOnly = True
        Me.DiscountTextBox.Size = New System.Drawing.Size(137, 27)
        Me.DiscountTextBox.TabIndex = 17
        Me.DiscountTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(18, 506)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(111, 19)
        Me.Label9.TabIndex = 16
        Me.Label9.Text = "Total Discount"
        '
        'SubTotalTextBox
        '
        Me.SubTotalTextBox.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SubTotalTextBox.Location = New System.Drawing.Point(174, 465)
        Me.SubTotalTextBox.Name = "SubTotalTextBox"
        Me.SubTotalTextBox.ReadOnly = True
        Me.SubTotalTextBox.Size = New System.Drawing.Size(137, 27)
        Me.SubTotalTextBox.TabIndex = 15
        Me.SubTotalTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(18, 473)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(84, 19)
        Me.Label8.TabIndex = 14
        Me.Label8.Text = "Total Price"
        '
        'CustomerNameTextBox
        '
        Me.CustomerNameTextBox.Enabled = False
        Me.CustomerNameTextBox.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CustomerNameTextBox.Location = New System.Drawing.Point(450, 34)
        Me.CustomerNameTextBox.Name = "CustomerNameTextBox"
        Me.CustomerNameTextBox.Size = New System.Drawing.Size(254, 27)
        Me.CustomerNameTextBox.TabIndex = 13
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(325, 37)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(123, 19)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Customer Name"
        '
        'GroupBox7
        '
        Me.GroupBox7.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox7.Controls.Add(Me.Label17)
        Me.GroupBox7.Controls.Add(Me.SearchDebtorTextBox)
        Me.GroupBox7.Controls.Add(Me.Label3)
        Me.GroupBox7.Controls.Add(Me.MoreInfoButton)
        Me.GroupBox7.Controls.Add(Me.PayCreditButton)
        Me.GroupBox7.Controls.Add(Me.CreditsDataGridView)
        Me.GroupBox7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox7.Location = New System.Drawing.Point(893, 447)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(449, 254)
        Me.GroupBox7.TabIndex = 29
        Me.GroupBox7.TabStop = False
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.Lavender
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(187, 22)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(56, 19)
        Me.Label17.TabIndex = 34
        Me.Label17.Text = "Search"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'SearchDebtorTextBox
        '
        Me.SearchDebtorTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SearchDebtorTextBox.Location = New System.Drawing.Point(251, 20)
        Me.SearchDebtorTextBox.Name = "SearchDebtorTextBox"
        Me.SearchDebtorTextBox.Size = New System.Drawing.Size(188, 23)
        Me.SearchDebtorTextBox.TabIndex = 33
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, -3)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(75, 18)
        Me.Label3.TabIndex = 32
        Me.Label3.Text = "CREDITS"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MoreInfoButton
        '
        Me.MoreInfoButton.Enabled = False
        Me.MoreInfoButton.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MoreInfoButton.Location = New System.Drawing.Point(15, 213)
        Me.MoreInfoButton.Name = "MoreInfoButton"
        Me.MoreInfoButton.Size = New System.Drawing.Size(90, 31)
        Me.MoreInfoButton.TabIndex = 2
        Me.MoreInfoButton.Text = "&More Info"
        Me.MoreInfoButton.UseVisualStyleBackColor = True
        '
        'PayCreditButton
        '
        Me.PayCreditButton.Enabled = False
        Me.PayCreditButton.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PayCreditButton.Location = New System.Drawing.Point(111, 213)
        Me.PayCreditButton.Name = "PayCreditButton"
        Me.PayCreditButton.Size = New System.Drawing.Size(90, 31)
        Me.PayCreditButton.TabIndex = 1
        Me.PayCreditButton.Text = "&Pay"
        Me.PayCreditButton.UseVisualStyleBackColor = True
        Me.PayCreditButton.Visible = False
        '
        'CreditsDataGridView
        '
        Me.CreditsDataGridView.AllowUserToAddRows = False
        Me.CreditsDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Gainsboro
        Me.CreditsDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.CreditsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.CreditsDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DebtorID, Me.CusNameColumn, Me.PaymentColumn, Me.DscntColumn, Me.AmountPaidColumn, Me.RemainBalColumn, Me.ContactNoColumn})
        Me.CreditsDataGridView.Location = New System.Drawing.Point(14, 48)
        Me.CreditsDataGridView.MultiSelect = False
        Me.CreditsDataGridView.Name = "CreditsDataGridView"
        Me.CreditsDataGridView.ReadOnly = True
        Me.CreditsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.CreditsDataGridView.Size = New System.Drawing.Size(425, 159)
        Me.CreditsDataGridView.TabIndex = 0
        '
        'DebtorID
        '
        Me.DebtorID.HeaderText = "Debtor's ID"
        Me.DebtorID.Name = "DebtorID"
        Me.DebtorID.ReadOnly = True
        '
        'CusNameColumn
        '
        Me.CusNameColumn.HeaderText = "Debtor"
        Me.CusNameColumn.Name = "CusNameColumn"
        Me.CusNameColumn.ReadOnly = True
        Me.CusNameColumn.Width = 120
        '
        'PaymentColumn
        '
        Me.PaymentColumn.HeaderText = "Payment"
        Me.PaymentColumn.Name = "PaymentColumn"
        Me.PaymentColumn.ReadOnly = True
        Me.PaymentColumn.Width = 70
        '
        'DscntColumn
        '
        Me.DscntColumn.HeaderText = "Discount"
        Me.DscntColumn.Name = "DscntColumn"
        Me.DscntColumn.ReadOnly = True
        Me.DscntColumn.Width = 70
        '
        'AmountPaidColumn
        '
        Me.AmountPaidColumn.HeaderText = "Amount Paid"
        Me.AmountPaidColumn.Name = "AmountPaidColumn"
        Me.AmountPaidColumn.ReadOnly = True
        Me.AmountPaidColumn.Width = 70
        '
        'RemainBalColumn
        '
        Me.RemainBalColumn.HeaderText = "Remaining Payment"
        Me.RemainBalColumn.Name = "RemainBalColumn"
        Me.RemainBalColumn.ReadOnly = True
        Me.RemainBalColumn.Width = 70
        '
        'ContactNoColumn
        '
        Me.ContactNoColumn.HeaderText = "Contact No"
        Me.ContactNoColumn.Name = "ContactNoColumn"
        Me.ContactNoColumn.ReadOnly = True
        Me.ContactNoColumn.Width = 78
        '
        'TodaySaleDataGridView
        '
        Me.TodaySaleDataGridView.AllowUserToAddRows = False
        Me.TodaySaleDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.White
        Me.TodaySaleDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle3
        Me.TodaySaleDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.TodaySaleDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ProdNoColumn, Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.CustomerNameColumn, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.MsrngUnitColumn, Me.DataGridViewTextBoxColumn7, Me.DscntTSPColumn, Me.FinalTSDPriceColumn, Me.OrderNo})
        Me.TodaySaleDataGridView.Location = New System.Drawing.Point(6, 27)
        Me.TodaySaleDataGridView.Name = "TodaySaleDataGridView"
        Me.TodaySaleDataGridView.ReadOnly = True
        Me.TodaySaleDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.TodaySaleDataGridView.Size = New System.Drawing.Size(340, 185)
        Me.TodaySaleDataGridView.TabIndex = 12
        '
        'ProdNoColumn
        '
        Me.ProdNoColumn.HeaderText = "No"
        Me.ProdNoColumn.Name = "ProdNoColumn"
        Me.ProdNoColumn.ReadOnly = True
        Me.ProdNoColumn.Width = 59
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "#"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        Me.DataGridViewTextBoxColumn1.Width = 30
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Product Name"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 130
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Brand"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 80
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Description"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 80
        '
        'CustomerNameColumn
        '
        Me.CustomerNameColumn.HeaderText = "Customer Name"
        Me.CustomerNameColumn.Name = "CustomerNameColumn"
        Me.CustomerNameColumn.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Price"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 70
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Qty"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 50
        '
        'MsrngUnitColumn
        '
        Me.MsrngUnitColumn.HeaderText = "Msrng Unit"
        Me.MsrngUnitColumn.Name = "MsrngUnitColumn"
        Me.MsrngUnitColumn.ReadOnly = True
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Total Price"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 70
        '
        'DscntTSPColumn
        '
        Me.DscntTSPColumn.HeaderText = "Discount"
        Me.DscntTSPColumn.Name = "DscntTSPColumn"
        Me.DscntTSPColumn.ReadOnly = True
        Me.DscntTSPColumn.Width = 65
        '
        'FinalTSDPriceColumn
        '
        Me.FinalTSDPriceColumn.HeaderText = "Final Price"
        Me.FinalTSDPriceColumn.Name = "FinalTSDPriceColumn"
        Me.FinalTSDPriceColumn.ReadOnly = True
        Me.FinalTSDPriceColumn.Width = 65
        '
        'OrderNo
        '
        Me.OrderNo.HeaderText = "Order Number"
        Me.OrderNo.Name = "OrderNo"
        Me.OrderNo.ReadOnly = True
        Me.OrderNo.Width = 125
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox5.Controls.Add(Me.Label14)
        Me.GroupBox5.Controls.Add(Me.TodaySaleDataGridView)
        Me.GroupBox5.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.Location = New System.Drawing.Point(726, 43)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(354, 223)
        Me.GroupBox5.TabIndex = 13
        Me.GroupBox5.TabStop = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(13, -2)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(209, 18)
        Me.Label14.TabIndex = 29
        Me.Label14.Text = "TODAY'S SOLD PRODUCTS"
        '
        'ExpensesDataGridView
        '
        Me.ExpensesDataGridView.AllowUserToAddRows = False
        Me.ExpensesDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.Gainsboro
        Me.ExpensesDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle4
        Me.ExpensesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ExpensesDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NumColumn, Me.ExpensesNameColumn, Me.DescriptionColumn, Me.CostColumn})
        Me.ExpensesDataGridView.Location = New System.Drawing.Point(7, 23)
        Me.ExpensesDataGridView.Name = "ExpensesDataGridView"
        Me.ExpensesDataGridView.ReadOnly = True
        Me.ExpensesDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ExpensesDataGridView.Size = New System.Drawing.Size(339, 137)
        Me.ExpensesDataGridView.TabIndex = 14
        '
        'NumColumn
        '
        Me.NumColumn.HeaderText = "#"
        Me.NumColumn.Name = "NumColumn"
        Me.NumColumn.ReadOnly = True
        Me.NumColumn.Width = 30
        '
        'ExpensesNameColumn
        '
        Me.ExpensesNameColumn.HeaderText = "Expenses Name"
        Me.ExpensesNameColumn.Name = "ExpensesNameColumn"
        Me.ExpensesNameColumn.ReadOnly = True
        Me.ExpensesNameColumn.Width = 121
        '
        'DescriptionColumn
        '
        Me.DescriptionColumn.HeaderText = "Description"
        Me.DescriptionColumn.Name = "DescriptionColumn"
        Me.DescriptionColumn.ReadOnly = True
        Me.DescriptionColumn.Width = 121
        '
        'CostColumn
        '
        Me.CostColumn.HeaderText = "Cost"
        Me.CostColumn.Name = "CostColumn"
        Me.CostColumn.ReadOnly = True
        Me.CostColumn.Width = 70
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox6.Controls.Add(Me.ExpensesDataGridView)
        Me.GroupBox6.Controls.Add(Me.Label15)
        Me.GroupBox6.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox6.Location = New System.Drawing.Point(726, 272)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(354, 169)
        Me.GroupBox6.TabIndex = 29
        Me.GroupBox6.TabStop = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(13, -3)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(158, 18)
        Me.Label15.TabIndex = 29
        Me.Label15.Text = "TODAY'S EXPENSES"
        '
        'AddSaleButton
        '
        Me.AddSaleButton.BackColor = System.Drawing.Color.Transparent
        Me.AddSaleButton.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AddSaleButton.Location = New System.Drawing.Point(730, 574)
        Me.AddSaleButton.Name = "AddSaleButton"
        Me.AddSaleButton.Size = New System.Drawing.Size(153, 46)
        Me.AddSaleButton.TabIndex = 30
        Me.AddSaleButton.Text = "&ADD ORDERS"
        Me.AddSaleButton.UseVisualStyleBackColor = False
        '
        'AddExpensesButton
        '
        Me.AddExpensesButton.BackColor = System.Drawing.Color.Transparent
        Me.AddExpensesButton.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AddExpensesButton.Location = New System.Drawing.Point(730, 632)
        Me.AddExpensesButton.Name = "AddExpensesButton"
        Me.AddExpensesButton.Size = New System.Drawing.Size(153, 46)
        Me.AddExpensesButton.TabIndex = 31
        Me.AddExpensesButton.Text = "ADD E&XPENSES"
        Me.AddExpensesButton.UseVisualStyleBackColor = False
        '
        'GroupBox8
        '
        Me.GroupBox8.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox8.Controls.Add(Me.TodaysPaidCreditsDataGridView)
        Me.GroupBox8.Controls.Add(Me.Label16)
        Me.GroupBox8.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox8.Location = New System.Drawing.Point(1085, 273)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(256, 168)
        Me.GroupBox8.TabIndex = 32
        Me.GroupBox8.TabStop = False
        '
        'TodaysPaidCreditsDataGridView
        '
        Me.TodaysPaidCreditsDataGridView.AllowUserToAddRows = False
        Me.TodaysPaidCreditsDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.Gainsboro
        Me.TodaysPaidCreditsDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle5
        Me.TodaysPaidCreditsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.TodaysPaidCreditsDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.TPCfullname, Me.TPCPaidAmountColumn, Me.DebtorsID, Me.OrderNumber})
        Me.TodaysPaidCreditsDataGridView.Location = New System.Drawing.Point(9, 22)
        Me.TodaysPaidCreditsDataGridView.Name = "TodaysPaidCreditsDataGridView"
        Me.TodaysPaidCreditsDataGridView.ReadOnly = True
        Me.TodaysPaidCreditsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.TodaysPaidCreditsDataGridView.Size = New System.Drawing.Size(238, 137)
        Me.TodaysPaidCreditsDataGridView.TabIndex = 31
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(8, -2)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(238, 18)
        Me.Label16.TabIndex = 30
        Me.Label16.Text = "TODAY'S CREDIT'S PAYMENTS"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddSaleToolStripMenuItem, Me.AddExpensesToolStripMenuItem, Me.ToolStripSeparator3, Me.InventoryToolStripMenuItem, Me.RecordsToolStripMenuItem, Me.SettingsToolStripMenuItem, Me.ToolStripSeparator4, Me.RefreshToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(185, 148)
        '
        'AddSaleToolStripMenuItem
        '
        Me.AddSaleToolStripMenuItem.Name = "AddSaleToolStripMenuItem"
        Me.AddSaleToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.AddSaleToolStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me.AddSaleToolStripMenuItem.Text = "Add Order"
        '
        'AddExpensesToolStripMenuItem
        '
        Me.AddExpensesToolStripMenuItem.Name = "AddExpensesToolStripMenuItem"
        Me.AddExpensesToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.AddExpensesToolStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me.AddExpensesToolStripMenuItem.Text = "Add Expenses"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(181, 6)
        '
        'InventoryToolStripMenuItem
        '
        Me.InventoryToolStripMenuItem.Name = "InventoryToolStripMenuItem"
        Me.InventoryToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.I), System.Windows.Forms.Keys)
        Me.InventoryToolStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me.InventoryToolStripMenuItem.Text = "Inventory"
        '
        'RecordsToolStripMenuItem
        '
        Me.RecordsToolStripMenuItem.Name = "RecordsToolStripMenuItem"
        Me.RecordsToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.R), System.Windows.Forms.Keys)
        Me.RecordsToolStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me.RecordsToolStripMenuItem.Text = "Records"
        '
        'SettingsToolStripMenuItem
        '
        Me.SettingsToolStripMenuItem.Name = "SettingsToolStripMenuItem"
        Me.SettingsToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.SettingsToolStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me.SettingsToolStripMenuItem.Text = "Settings"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(181, 6)
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me.RefreshToolStripMenuItem.Text = "Refresh"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.MenuStrip1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1353, 27)
        Me.MenuStrip1.TabIndex = 33
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddSaleToolStripMenuItem1, Me.AddExpensesToolStripMenuItem1, Me.ToolStripSeparator1, Me.InventoryToolStripMenuItem1, Me.RecordsToolStripMenuItem1, Me.SettingsToolStripMenuItem1, Me.ToolStripSeparator2, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.F), System.Windows.Forms.Keys)
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(45, 23)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'AddSaleToolStripMenuItem1
        '
        Me.AddSaleToolStripMenuItem1.Name = "AddSaleToolStripMenuItem1"
        Me.AddSaleToolStripMenuItem1.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.AddSaleToolStripMenuItem1.Size = New System.Drawing.Size(227, 24)
        Me.AddSaleToolStripMenuItem1.Text = "&Add Order"
        '
        'AddExpensesToolStripMenuItem1
        '
        Me.AddExpensesToolStripMenuItem1.Name = "AddExpensesToolStripMenuItem1"
        Me.AddExpensesToolStripMenuItem1.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.AddExpensesToolStripMenuItem1.Size = New System.Drawing.Size(227, 24)
        Me.AddExpensesToolStripMenuItem1.Text = "Add E&xpenses"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(224, 6)
        '
        'InventoryToolStripMenuItem1
        '
        Me.InventoryToolStripMenuItem1.Name = "InventoryToolStripMenuItem1"
        Me.InventoryToolStripMenuItem1.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.I), System.Windows.Forms.Keys)
        Me.InventoryToolStripMenuItem1.Size = New System.Drawing.Size(227, 24)
        Me.InventoryToolStripMenuItem1.Text = "&Inventory"
        '
        'RecordsToolStripMenuItem1
        '
        Me.RecordsToolStripMenuItem1.Name = "RecordsToolStripMenuItem1"
        Me.RecordsToolStripMenuItem1.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.R), System.Windows.Forms.Keys)
        Me.RecordsToolStripMenuItem1.Size = New System.Drawing.Size(227, 24)
        Me.RecordsToolStripMenuItem1.Text = "&Records"
        '
        'SettingsToolStripMenuItem1
        '
        Me.SettingsToolStripMenuItem1.Name = "SettingsToolStripMenuItem1"
        Me.SettingsToolStripMenuItem1.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.SettingsToolStripMenuItem1.Size = New System.Drawing.Size(227, 24)
        Me.SettingsToolStripMenuItem1.Text = "&Settings"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(224, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.F4), System.Windows.Forms.Keys)
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(227, 24)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'TPCfullname
        '
        Me.TPCfullname.HeaderText = "Debtor"
        Me.TPCfullname.Name = "TPCfullname"
        Me.TPCfullname.ReadOnly = True
        Me.TPCfullname.Width = 130
        '
        'TPCPaidAmountColumn
        '
        Me.TPCPaidAmountColumn.HeaderText = "Amount Paid"
        Me.TPCPaidAmountColumn.Name = "TPCPaidAmountColumn"
        Me.TPCPaidAmountColumn.ReadOnly = True
        Me.TPCPaidAmountColumn.Width = 80
        '
        'DebtorsID
        '
        Me.DebtorsID.HeaderText = "Debtor's ID"
        Me.DebtorsID.Name = "DebtorsID"
        Me.DebtorsID.ReadOnly = True
        Me.DebtorsID.Width = 120
        '
        'OrderNumber
        '
        Me.OrderNumber.HeaderText = "Order No"
        Me.OrderNumber.Name = "OrderNumber"
        Me.OrderNumber.ReadOnly = True
        Me.OrderNumber.Width = 120
        '
        'StoreForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Gray
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1353, 718)
        Me.ContextMenuStrip = Me.ContextMenuStrip1
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.AddExpensesButton)
        Me.Controls.Add(Me.GroupBox8)
        Me.Controls.Add(Me.AddSaleButton)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.OrdersGroupBox)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "StoreForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ROJ Motor Parts Shop"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.OrdersDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.OrdersGroupBox.ResumeLayout(False)
        Me.OrdersGroupBox.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        CType(Me.CreditsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TodaySaleDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.ExpensesDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        CType(Me.TodaysPaidCreditsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents DateTimeLabel As Label
    Friend WithEvents Timer1 As Timer
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents NetSaleTextBox As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents TotalExpensesTextBox As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents GrossSaleTextBox As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents OrdersDataGridView As DataGridView
    Friend WithEvents OrdersGroupBox As GroupBox
    Friend WithEvents ChangeTextBox As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents AmountReceivedTextBox As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents GrandTotalTextBox As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents DiscountTextBox As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents SubTotalTextBox As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents CustomerNameTextBox As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents DeleteOrderButton As Button
    Friend WithEvents OrderCompleteButton As Button
    Friend WithEvents CancelOrderButton As Button
    Friend WithEvents EditOrderButton As Button
    Friend WithEvents TodaySaleDataGridView As DataGridView
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents Label14 As Label
    Friend WithEvents ExpensesDataGridView As DataGridView
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents Label15 As Label
    Friend WithEvents TotalDiscountTextBox As TextBox
    Friend WithEvents totaldiscountLabel As Label
    Friend WithEvents AddSaleButton As Button
    Friend WithEvents AddExpensesButton As Button
    Friend WithEvents NumColumn As DataGridViewTextBoxColumn
    Friend WithEvents ExpensesNameColumn As DataGridViewTextBoxColumn
    Friend WithEvents DescriptionColumn As DataGridViewTextBoxColumn
    Friend WithEvents CostColumn As DataGridViewTextBoxColumn
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents CreditsDataGridView As DataGridView
    Friend WithEvents PayCreditButton As Button
    Friend WithEvents MoreInfoButton As Button
    Friend WithEvents GroupBox8 As GroupBox
    Friend WithEvents TodaysPaidCreditsDataGridView As DataGridView
    Friend WithEvents Label16 As Label
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents RefreshToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AddSaleToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AddExpensesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents InventoryToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RecordsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SettingsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents InventoryToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents RecordsToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents SettingsToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents Label13 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents SearchDebtorTextBox As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents AddSaleToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents AddExpensesToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As ToolStripSeparator
    Friend WithEvents ToolStripSeparator4 As ToolStripSeparator
    Friend WithEvents Label18 As Label
    Friend WithEvents OrderNoTextBox As TextBox
    Friend WithEvents ProdNoColumn As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents CustomerNameColumn As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents MsrngUnitColumn As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents DscntTSPColumn As DataGridViewTextBoxColumn
    Friend WithEvents FinalTSDPriceColumn As DataGridViewTextBoxColumn
    Friend WithEvents OrderNo As DataGridViewTextBoxColumn
    Friend WithEvents NoColumn As DataGridViewTextBoxColumn
    Friend WithEvents NumberColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductNameColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductBrandColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductDescriptionColumn As DataGridViewTextBoxColumn
    Friend WithEvents PriceColumn As DataGridViewTextBoxColumn
    Friend WithEvents QuantityColumn As DataGridViewTextBoxColumn
    Friend WithEvents MesuringUnitColumn As DataGridViewTextBoxColumn
    Friend WithEvents TotalPriceColumn As DataGridViewTextBoxColumn
    Friend WithEvents DiscountColumn As DataGridViewTextBoxColumn
    Friend WithEvents FinalPriceColumn As DataGridViewTextBoxColumn
    Friend WithEvents time As DataGridViewTextBoxColumn
    Friend WithEvents purchasecost As DataGridViewTextBoxColumn
    Friend WithEvents DebtorID As DataGridViewTextBoxColumn
    Friend WithEvents CusNameColumn As DataGridViewTextBoxColumn
    Friend WithEvents PaymentColumn As DataGridViewTextBoxColumn
    Friend WithEvents DscntColumn As DataGridViewTextBoxColumn
    Friend WithEvents AmountPaidColumn As DataGridViewTextBoxColumn
    Friend WithEvents RemainBalColumn As DataGridViewTextBoxColumn
    Friend WithEvents ContactNoColumn As DataGridViewTextBoxColumn
    Friend WithEvents TPCfullname As DataGridViewTextBoxColumn
    Friend WithEvents TPCPaidAmountColumn As DataGridViewTextBoxColumn
    Friend WithEvents DebtorsID As DataGridViewTextBoxColumn
    Friend WithEvents OrderNumber As DataGridViewTextBoxColumn
End Class
