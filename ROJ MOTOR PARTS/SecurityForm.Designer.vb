﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SecurityForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SecurityForm))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SecEnterButton = New System.Windows.Forms.Button()
        Me.SecurityusernameTextBox = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SecuritypasswordTextBox = New System.Windows.Forms.TextBox()
        Me.SecCancelButton = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(55, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(365, 55)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Enter Administrator Username and Password To "
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'SecEnterButton
        '
        Me.SecEnterButton.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SecEnterButton.Location = New System.Drawing.Point(170, 172)
        Me.SecEnterButton.Name = "SecEnterButton"
        Me.SecEnterButton.Size = New System.Drawing.Size(103, 31)
        Me.SecEnterButton.TabIndex = 2
        Me.SecEnterButton.Tag = ""
        Me.SecEnterButton.Text = "Enter"
        Me.SecEnterButton.UseVisualStyleBackColor = True
        '
        'SecurityusernameTextBox
        '
        Me.SecurityusernameTextBox.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SecurityusernameTextBox.Location = New System.Drawing.Point(170, 85)
        Me.SecurityusernameTextBox.Name = "SecurityusernameTextBox"
        Me.SecurityusernameTextBox.Size = New System.Drawing.Size(218, 27)
        Me.SecurityusernameTextBox.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(84, 88)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 19)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Username"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(88, 129)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 19)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Password"
        '
        'SecuritypasswordTextBox
        '
        Me.SecuritypasswordTextBox.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SecuritypasswordTextBox.Location = New System.Drawing.Point(170, 126)
        Me.SecuritypasswordTextBox.Name = "SecuritypasswordTextBox"
        Me.SecuritypasswordTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.SecuritypasswordTextBox.Size = New System.Drawing.Size(218, 27)
        Me.SecuritypasswordTextBox.TabIndex = 1
        '
        'SecCancelButton
        '
        Me.SecCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.SecCancelButton.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SecCancelButton.Location = New System.Drawing.Point(285, 172)
        Me.SecCancelButton.Name = "SecCancelButton"
        Me.SecCancelButton.Size = New System.Drawing.Size(103, 31)
        Me.SecCancelButton.TabIndex = 6
        Me.SecCancelButton.Text = "Cancel"
        Me.SecCancelButton.UseVisualStyleBackColor = True
        '
        'SecurityForm
        '
        Me.AcceptButton = Me.SecEnterButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.ROJBackground
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.CancelButton = Me.SecCancelButton
        Me.ClientSize = New System.Drawing.Size(477, 226)
        Me.Controls.Add(Me.SecCancelButton)
        Me.Controls.Add(Me.SecuritypasswordTextBox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.SecurityusernameTextBox)
        Me.Controls.Add(Me.SecEnterButton)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "SecurityForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Security"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents SecEnterButton As Button
    Friend WithEvents SecurityusernameTextBox As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents SecuritypasswordTextBox As TextBox
    Friend WithEvents SecCancelButton As Button
End Class
