﻿Imports MySql.Data.MySqlClient
Public Class PayLoanForm
    Private Sub PayLoanForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            OrdernoTextBox.Text = CreditsInformationForm.LoansDataGridView.CurrentRow.Cells(1).Value()
            PaymentTextBox.Text = CreditsInformationForm.LoansDataGridView.CurrentRow.Cells(2).Value()
            DiscountTextBox.Text = CreditsInformationForm.LoansDataGridView.CurrentRow.Cells(3).Value()
            AmountpaidtTextBox.Text = CreditsInformationForm.LoansDataGridView.CurrentRow.Cells(4).Value()
            RemainingpaymentTextBox.Text = CreditsInformationForm.LoansDataGridView.CurrentRow.Cells(5).Value()
            AmountToPayTextBox.Clear()
            AmountToPayTextBox.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub AmountToPayTextBox_TextChanged(sender As Object, e As EventArgs) Handles AmountToPayTextBox.TextChanged

    End Sub

    Private Sub ContinueToPayButton_Click(sender As Object, e As EventArgs) Handles ContinueToPayButton.Click
        Dim AmountTopay As Decimal
        Try
            OpenConnection()
            Try
                AmountTopay = Decimal.Parse(AmountToPayTextBox.Text)
                Try
                    If AmountTopay > RemainingpaymentTextBox.Text Then
                        MessageBox.Show("Payment amount should not be greater than the remaining payment!", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        With AmountpaidtTextBox
                            .Focus()
                            .SelectAll()
                        End With
                    ElseIf AmountTopay > 0 Then
                        Dim mesres As DialogResult = MessageBox.Show("Click OK to confirm the payment.", "Confirm Payment", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
                        If mesres = DialogResult.OK Then
                            Query = "INSERT INTO paymentsmade
                             VALUES('" & OrdernoTextBox.Text & "', '" & CreditsInformationForm.IDLabel.Text & "', '" & CreditsInformationForm.FullNameLabel.Text & "', " _
                             & AmountTopay & ", '" & DateTime.Now().ToString("yyyy-MM-dd") & "', '" & DateTime.Now().ToString("yyyy-MM-dd hh:mm:ss") & "')"
                            Command = New MySqlCommand(Query, MysqlCon)
                            Reader = Command.ExecuteReader
                            Reader.Close()

                            Query = "UPDATE credits SET amountpaid = amountpaid + " & AmountTopay & " WHERE debtorsid = '" & CreditsInformationForm.IDLabel.Text & "'"
                            Command = New MySqlCommand(Query, MysqlCon)
                            Reader = Command.ExecuteReader
                            Reader.Close()

                            MessageBox.Show("Payment Recorded Successfully!", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information)

                            Me.Close()
                        End If
                    Else
                        MessageBox.Show("Payment should be greater than 0", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        With AmountpaidtTextBox
                            .Focus()
                            .SelectAll()
                        End With
                    End If
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            Catch ex As FormatException
                MessageBox.Show("Amount to pay should be numeric!", "Invalid Input")
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        MysqlCon.Close()
    End Sub

    Private Sub PayLoanForm_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        PayLoanForm_Load(sender, e)
    End Sub
End Class