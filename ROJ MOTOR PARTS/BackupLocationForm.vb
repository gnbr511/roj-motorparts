﻿Imports MySql.Data.MySqlClient
Public Class BackupLocationForm
    Private Autobackupfilepath As String
    Private Sub BrowseButton_Click(sender As Object, e As EventArgs) Handles BrowseButton.Click
        If FolderBrowserDialog1.ShowDialog() = DialogResult.OK Then
            Autobackupfilepath = FolderBrowserDialog1.SelectedPath
            AutoBackupLocationTextBox.Text = Autobackupfilepath
            SaveButton.Enabled = True
        End If
    End Sub
    Private Sub SaveButton_Click(sender As Object, e As EventArgs) Handles SaveButton.Click
        Dim ConfirmMessage As DialogResult = MessageBox.Show("Click 'Ok' to save automatic back up location.", "Confirm", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
        If ConfirmMessage = DialogResult.OK Then
            Try
                OpenConnection()
                Query = "delete from autobackuplocation"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                Reader.Close()

                Query = "insert into autobackuplocation values(1, '" & Autobackupfilepath.Replace("\", "\\") & "')"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                Reader.Close()
                SaveButton.Enabled = False
                BackupLocationForm_Load(sender, e)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
        MysqlCon.Close()
    End Sub
    Private Sub BackupLocationForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        AutoBackupLocationTextBox.Clear()
        SaveButton.Enabled = False
        BrowseButton.Focus()
    End Sub

    Private Sub BackupLocationForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim autobackuplocation As String
        Try
            OpenConnection()
            Query = "select * from autobackuplocation"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                autobackuplocation = Reader.GetString("autobackuplocation")
            End While
            Reader.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        If autobackuplocation = "" Then
            DisableAutoBackupButton.Enabled = False
            AutoBackupLocationTextBox.Text = "Auto back up is disabled."
        Else
            DisableAutoBackupButton.Enabled = True
            AutoBackupLocationTextBox.Text = autobackuplocation
        End If
        MysqlCon.Close()
    End Sub
    Private Sub DisableAutoBackupButton_Click(sender As Object, e As EventArgs) Handles DisableAutoBackupButton.Click
        Dim message As DialogResult = MessageBox.Show("Disabling auto back up will also clear the setted auto back up location!", "Please Confirm", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation)
        If message = DialogResult.OK Then
            Try
                OpenConnection()
                Query = "delete from autobackuplocation"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                Reader.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
            DisableAutoBackupButton.Enabled = False
            AutoBackupLocationTextBox.Text = "Auto back up is disabled."
        End If
    End Sub

    Private Sub BackupLocationForm_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        BackupLocationForm_Load(sender, e)
        MysqlCon.Close()
    End Sub
End Class