﻿Public Class SettingsForm
    Private Sub UserSettingsPictureBox_Click(sender As Object, e As EventArgs) Handles UserSettingsPictureBox.Click
        SecurityForm.GINBAHoption = "GinbahUsersSettings"
        SecurityForm.GINBAHoption2 = "Access Users Settings"
        SecurityForm.ShowDialog()
    End Sub

    Private Sub AboutPictureBox_Click(sender As Object, e As EventArgs) Handles AboutPictureBox.Click
        AboutForm.ShowDialog()
    End Sub

    Private Sub BackupAllDataToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BackupAllDataToolStripMenuItem.Click
        SecurityForm.GINBAHoption = "GinbahBackup"
        SecurityForm.GINBAHoption2 = "Back Up All Data"
        SecurityForm.ShowDialog()
    End Sub

    Private Sub RestoreToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RestoreToolStripMenuItem.Click
        SecurityForm.GINBAHoption = "GinbahRestore"
        SecurityForm.GINBAHoption2 = "Restore or Load Data"
        SecurityForm.ShowDialog()
    End Sub

    Private Sub UserSettingsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UserSettingsToolStripMenuItem.Click
        UserSettingsPictureBox_Click(sender, e)
    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        AboutPictureBox_Click(sender, e)
    End Sub

    Private Sub SetAutoBackupLocationToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SetAutoBackupLocationToolStripMenuItem.Click
        SecurityForm.GINBAHoption = "GinbahSetAutoBackup"
        SecurityForm.GINBAHoption2 = "Set Automatic Backup Location"
        SecurityForm.ShowDialog()
    End Sub
End Class