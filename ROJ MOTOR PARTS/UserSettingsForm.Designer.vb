﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserSettingsForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UserSettingsForm))
        Me.UsersDataGridView = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.DeleteUserButton = New System.Windows.Forms.Button()
        Me.EditButton = New System.Windows.Forms.Button()
        Me.AddUserButton = New System.Windows.Forms.Button()
        Me.NumColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.no = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UsersNameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UsernameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PasswordColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UserTypeColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.UsersDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'UsersDataGridView
        '
        Me.UsersDataGridView.AllowUserToAddRows = False
        Me.UsersDataGridView.AllowUserToDeleteRows = False
        Me.UsersDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.UsersDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NumColumn, Me.no, Me.UsersNameColumn, Me.UsernameColumn, Me.PasswordColumn, Me.UserTypeColumn})
        Me.UsersDataGridView.Location = New System.Drawing.Point(12, 25)
        Me.UsersDataGridView.MultiSelect = False
        Me.UsersDataGridView.Name = "UsersDataGridView"
        Me.UsersDataGridView.ReadOnly = True
        Me.UsersDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.UsersDataGridView.Size = New System.Drawing.Size(655, 228)
        Me.UsersDataGridView.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.DeleteUserButton)
        Me.GroupBox1.Controls.Add(Me.EditButton)
        Me.GroupBox1.Controls.Add(Me.AddUserButton)
        Me.GroupBox1.Controls.Add(Me.UsersDataGridView)
        Me.GroupBox1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(11, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(678, 342)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Users"
        '
        'DeleteUserButton
        '
        Me.DeleteUserButton.Location = New System.Drawing.Point(441, 269)
        Me.DeleteUserButton.Name = "DeleteUserButton"
        Me.DeleteUserButton.Size = New System.Drawing.Size(160, 56)
        Me.DeleteUserButton.TabIndex = 4
        Me.DeleteUserButton.Text = "&DELETE"
        Me.DeleteUserButton.UseVisualStyleBackColor = True
        '
        'EditButton
        '
        Me.EditButton.Location = New System.Drawing.Point(267, 269)
        Me.EditButton.Name = "EditButton"
        Me.EditButton.Size = New System.Drawing.Size(160, 56)
        Me.EditButton.TabIndex = 3
        Me.EditButton.Text = "&EDIT"
        Me.EditButton.UseVisualStyleBackColor = True
        '
        'AddUserButton
        '
        Me.AddUserButton.Location = New System.Drawing.Point(93, 269)
        Me.AddUserButton.Name = "AddUserButton"
        Me.AddUserButton.Size = New System.Drawing.Size(160, 56)
        Me.AddUserButton.TabIndex = 2
        Me.AddUserButton.Text = "&ADD"
        Me.AddUserButton.UseVisualStyleBackColor = True
        '
        'NumColumn
        '
        Me.NumColumn.FillWeight = 125.0!
        Me.NumColumn.HeaderText = "#"
        Me.NumColumn.Name = "NumColumn"
        Me.NumColumn.ReadOnly = True
        Me.NumColumn.Width = 50
        '
        'no
        '
        Me.no.HeaderText = "no"
        Me.no.Name = "no"
        Me.no.ReadOnly = True
        Me.no.Visible = False
        '
        'UsersNameColumn
        '
        Me.UsersNameColumn.HeaderText = "Name"
        Me.UsersNameColumn.Name = "UsersNameColumn"
        Me.UsersNameColumn.ReadOnly = True
        Me.UsersNameColumn.Width = 210
        '
        'UsernameColumn
        '
        Me.UsernameColumn.HeaderText = "Username"
        Me.UsernameColumn.Name = "UsernameColumn"
        Me.UsernameColumn.ReadOnly = True
        Me.UsernameColumn.Width = 210
        '
        'PasswordColumn
        '
        Me.PasswordColumn.HeaderText = "Password"
        Me.PasswordColumn.Name = "PasswordColumn"
        Me.PasswordColumn.ReadOnly = True
        Me.PasswordColumn.Visible = False
        Me.PasswordColumn.Width = 150
        '
        'UserTypeColumn
        '
        Me.UserTypeColumn.HeaderText = "Type"
        Me.UserTypeColumn.Name = "UserTypeColumn"
        Me.UserTypeColumn.ReadOnly = True
        Me.UserTypeColumn.Width = 150
        '
        'UserSettingsForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.ROJBackground
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(701, 360)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "UserSettingsForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "User Settings"
        CType(Me.UsersDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents UsersDataGridView As DataGridView
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents DeleteUserButton As Button
    Friend WithEvents EditButton As Button
    Friend WithEvents AddUserButton As Button
    Friend WithEvents NumColumn As DataGridViewTextBoxColumn
    Friend WithEvents no As DataGridViewTextBoxColumn
    Friend WithEvents UsersNameColumn As DataGridViewTextBoxColumn
    Friend WithEvents UsernameColumn As DataGridViewTextBoxColumn
    Friend WithEvents PasswordColumn As DataGridViewTextBoxColumn
    Friend WithEvents UserTypeColumn As DataGridViewTextBoxColumn
End Class
