﻿Imports MySql.Data.MySqlClient
Imports System.Drawing.Printing
Public Class CreditsInformationForm

    Friend SelectedDebtor As String
    Private Sub CreditsInformationForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            OpenConnection()
            Try
                'Display debtor's photo
                Query = "SELECT * FROM debtorsphotopath
                         WHERE debtorsid = '" & SelectedDebtor & "'"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                While Reader.Read
                    If System.IO.File.Exists(Reader.GetString("debtorsphotopath")) Then
                        DebtorsPhotoPictureBox.Image = Image.FromFile(Reader.GetString("debtorsphotopath"))
                    Else
                        DebtorsPhotoPictureBox.Image = My.Resources.no_image_icon_13
                    End If
                End While
                Reader.Close()
                'Display debtor's name, contact, credit's summary
                Query = "SELECT * FROM (SELECT credits.*, tabletotaldiscount.totaldiscount 
                                        FROM credits                                       
                                        INNER JOIN (SELECT SUM(discount) as totaldiscount, debtorsid
                                                    FROM loanedproducts
                                                    GROUP BY debtorsid) as tabletotaldiscount
                                        ON credits.debtorsid = tabletotaldiscount.debtorsid) creditsinfoandsummary
                         WHERE debtorsid = '" & SelectedDebtor & "'"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                While Reader.Read
                    IDLabel.Text = Reader.GetString("debtorsid")
                    FullNameLabel.Text = Reader.GetString("debtor")
                    ContactLabel.Text = Reader.GetString("contact")
                    TotalPaymentTextBox.Text = Reader.GetDecimal("payment") + Reader.GetDecimal("totaldiscount")
                    DiscountTextBox.Text = Reader.GetDecimal("totaldiscount")
                    TotalPaidTextBox.Text = Reader.GetDecimal("amountpaid")
                    RemainingPaymentTextBox.Text = Reader.GetDecimal("payment") - Reader.GetDecimal("amountpaid")
                End While
                Reader.Close()
                'Display more information
                MoreInfoListBox.Items.Clear()
                Query = "SELECT * FROM moreinfo where debtorsid = '" & SelectedDebtor & "'"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                While Reader.Read
                    MoreInfoListBox.Items.Add(Reader.GetString("info"))
                End While
                Reader.Close()
                'display payment history
                PaymentHistoryDataGridView.Rows.Clear()
                Query = "SELECT * FROM paymentsmade where customerid = '" & SelectedDebtor & "'"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                While Reader.Read
                    PaymentHistoryDataGridView.Rows.Add(Reader.GetDateTime("date").ToString("yyyy-MM-dd"), Reader.GetDecimal("paidamount"), Reader.GetString("orderno"))
                End While
                Reader.Close()
                'display loans
                LoansDataGridView.Rows.Clear()
                Query = "SELECT * FROM (SELECT loanedproducts.date, loanedproducts.orderno, SUM(loanedproducts.discountedprice) totalpayment, SUM(loanedproducts.discount) totaldiscount, paymentsmade.paidamount, loanedproducts.debtorsid
                                        FROM loanedproducts
                                        INNER JOIN (SELECT orderno, SUM(paidamount) paidamount  
	                                                FROM paymentsmade GROUP BY orderno) paymentsmade
                                        ON paymentsmade.orderno = loanedproducts.orderno
                                        GROUP BY ORDERNO) loans
                         WHERE debtorsid = '" & SelectedDebtor & "'"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                While Reader.Read
                    Dim orderdate = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                    Dim orderno = Reader.GetString("orderno")
                    Dim totalpayment = Reader.GetDecimal("totalpayment")
                    Dim totaldiscount = Reader.GetDecimal("totaldiscount")
                    Dim amountpaid = Reader.GetDecimal("paidamount")
                    Dim remainingpayment = totalpayment - amountpaid
                    LoansDataGridView.Rows.Add(orderdate, orderno, totalpayment + totaldiscount, totaldiscount, amountpaid, remainingpayment)
                End While
                Reader.Close()
                Query = "select * 
                         from (select date, orderno, sum(discountedprice) totalpayment, sum(discount) totaldiscount, debtorsid 
                               from loanedproducts where not exists (select * 
                                                         from paymentsmade 
                                                         where loanedproducts.orderno = paymentsmade.orderno) 
                               group by orderno) loans
                         where debtorsid = '" & SelectedDebtor & "'"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                While Reader.Read
                    Dim orderdate = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                    Dim orderno = Reader.GetString("orderno")
                    Dim totalpayment = Reader.GetDecimal("totalpayment")
                    Dim totaldiscount = Reader.GetDecimal("totaldiscount")
                    Dim amountpaid As Decimal = 0.00
                    Dim remainingpayment = totalpayment - amountpaid
                    LoansDataGridView.Rows.Add(orderdate, orderno, totalpayment, totaldiscount, amountpaid.ToString("N2"), remainingpayment)
                End While
                Reader.Close()
                'display loanedproducts
                LoanedProductsDataGridView.Rows.Clear()
                Query = "SELECT * FROM Loanedproducts WHERE debtorsid = '" & SelectedDebtor & "'"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                While Reader.Read
                    Dim loaneddate = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                    Dim orderno = Reader.GetString("orderno")
                    Dim prodno = Reader.GetInt32("no")
                    Dim prodname = Reader.GetString("productname")
                    Dim prodbrand = Reader.GetString("productbrand")
                    Dim proddesc = Reader.GetString("productdescription")
                    Dim purchasecost = Reader.GetDecimal("purchasecost")
                    Dim saleprice = Reader.GetDecimal("saleprice")
                    Dim quantity = Reader.GetDecimal("quantity")
                    Dim measuringunit = Reader.GetString("measuringunit")
                    Dim totalprice = Reader.GetDecimal("totalprice")
                    Dim discount = Reader.GetDecimal("discount")
                    Dim finalprice = Reader.GetDecimal("discountedprice")
                    LoanedProductsDataGridView.Rows.Add(loaneddate, orderno, prodno, prodname, prodbrand, proddesc, purchasecost, saleprice, quantity, measuringunit, totalprice, discount, finalprice)
                End While
                Reader.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Displaying Information Problem")
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Datebase Connection Problem")
        End Try
        LoansDataGridView.ClearSelection()
        LoanedProductsDataGridView.ClearSelection()
        PaymentHistoryDataGridView.ClearSelection()
        With PayToolStripMenuItem
            .Enabled = False
            .Visible = False
        End With
        MysqlCon.Close()
    End Sub
    Private Sub RefreshToolStripMenuItem_Click_1(sender As Object, e As EventArgs) Handles RefreshToolStripMenuItem.Click
        CreditsInformationForm_Load(sender, e)
    End Sub
    Private Sub LoansDataGridView_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles LoansDataGridView.CellClick
        Try
            OpenConnection()
            Try
                LoanedProductsDataGridView.Rows.Clear()
                Query = "SELECT * FROM loanedproducts WHERE orderno = '" & LoansDataGridView.CurrentRow.Cells(1).Value & "'"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                While Reader.Read
                    Dim loaneddate = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                    Dim orderno = Reader.GetString("orderno")
                    Dim prodno = Reader.GetInt32("no")
                    Dim prodname = Reader.GetString("productname")
                    Dim prodbrand = Reader.GetString("productbrand")
                    Dim proddesc = Reader.GetString("productdescription")
                    Dim purchasecost = Reader.GetDecimal("purchasecost")
                    Dim saleprice = Reader.GetDecimal("saleprice")
                    Dim quantity = Reader.GetDecimal("quantity")
                    Dim measuringunit = Reader.GetString("measuringunit")
                    Dim totalprice = Reader.GetDecimal("totalprice")
                    Dim discount = Reader.GetDecimal("discount")
                    Dim finalprice = Reader.GetDecimal("discountedprice")
                    LoanedProductsDataGridView.Rows.Add(loaneddate, orderno, prodno, prodname, prodbrand, proddesc, purchasecost, saleprice, quantity, measuringunit, totalprice, discount, finalprice)
                End While
                Reader.Close()
                Try
                    PaymentHistoryDataGridView.Rows.Clear()
                    Query = "SELECT * FROM paymentsmade WHERE orderno = '" & LoansDataGridView.CurrentRow.Cells(1).Value & "'"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    While Reader.Read
                        Dim datepaid = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                        Dim paidamount = Reader.GetDecimal("paidamount")
                        Dim orderno = Reader.GetString("orderno")
                        PaymentHistoryDataGridView.Rows.Add(datepaid, paidamount, orderno)
                    End While
                    Reader.Close()
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
                PaymentHistoryDataGridView.ClearSelection()
                LoanedProductsDataGridView.ClearSelection()
                With PayToolStripMenuItem
                    .Enabled = True
                    .Visible = True
                End With
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error")
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        MysqlCon.Close()
    End Sub
    Private Sub SearchLoansTextBox_TextChanged(sender As Object, e As EventArgs) Handles SearchLoansTextBox.TextChanged
        Try
            OpenConnection()
            LoansDataGridView.Rows.Clear()
            Query = "SELECT * FROM (SELECT * FROM (SELECT loanedproducts.date, loanedproducts.orderno, SUM(loanedproducts.discountedprice) totalpayment, SUM(loanedproducts.discount) totaldiscount, paymentsmade.paidamount, loanedproducts.debtorsid
                                        FROM loanedproducts
                                        INNER JOIN (SELECT orderno, paidamount 
	                                                FROM paymentsmade) paymentsmade
                                        ON paymentsmade.orderno = loanedproducts.orderno
                                        GROUP BY ORDERNO) loans
                         WHERE debtorsid = '" & SelectedDebtor & "') loans 
                     WHERE date like '%" & SearchLoansTextBox.Text & "%'
                     OR orderno like '%" & SearchLoansTextBox.Text & "%'"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim orderdate = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                Dim orderno = Reader.GetString("orderno")
                Dim totalpayment = Reader.GetDecimal("totalpayment")
                Dim totaldiscount = Reader.GetDecimal("totaldiscount")
                Dim amountpaid = Reader.GetDecimal("paidamount")
                Dim remainingpayment = totalpayment - amountpaid
                LoansDataGridView.Rows.Add(orderdate, orderno, totalpayment + totaldiscount, totaldiscount, amountpaid, remainingpayment)
            End While
            Reader.Close()
            Query = "SELECT * FROM (select * 
                         from (select date, orderno, sum(discountedprice) totalpayment, sum(discount) totaldiscount, debtorsid 
                               from loanedproducts where not exists (select * 
                                                         from paymentsmade 
                                                         where loanedproducts.orderno = paymentsmade.orderno) 
                               group by orderno) loans
                         where debtorsid = '" & SelectedDebtor & "') loans
                    WHERE date like '%" & SearchLoansTextBox.Text & "%' 
                    OR orderno like '%" & SearchLoansTextBox.Text & "%'"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim orderdate = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                Dim orderno = Reader.GetString("orderno")
                Dim totalpayment = Reader.GetDecimal("totalpayment")
                Dim totaldiscount = Reader.GetDecimal("totaldiscount")
                Dim amountpaid As Decimal = 0.00
                Dim remainingpayment = totalpayment - amountpaid
                LoansDataGridView.Rows.Add(orderdate, orderno, totalpayment, totaldiscount, amountpaid, remainingpayment)
            End While
            Reader.Close()
            LoansDataGridView.ClearSelection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        MysqlCon.Close()
    End Sub
    Private Sub SearchLoanedProductTextBox_TextChanged(sender As Object, e As EventArgs) Handles SearchLoanedProductTextBox.TextChanged
        Try
            OpenConnection()
            LoanedProductsDataGridView.Rows.Clear()
            Query = "SELECT * FROM (SELECT * FROM Loanedproducts WHERE debtorsid = '" & SelectedDebtor & "') as selectedloanedproducts
                     WHERE orderno LIKE '%" & SearchLoanedProductTextBox.Text & "%' 
                     OR date LIKE '%" & SearchLoanedProductTextBox.Text & "%'
                     OR no LIKE '%" & SearchLoanedProductTextBox.Text & "%'
                     OR productname LIKE '%" & SearchLoanedProductTextBox.Text & "%'"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim loaneddate = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                Dim orderno = Reader.GetString("orderno")
                Dim prodno = Reader.GetInt32("no")
                Dim prodname = Reader.GetString("productname")
                Dim prodbrand = Reader.GetString("productbrand")
                Dim proddesc = Reader.GetString("productdescription")
                Dim purchasecost = Reader.GetDecimal("purchasecost")
                Dim saleprice = Reader.GetDecimal("saleprice")
                Dim quantity = Reader.GetDecimal("quantity")
                Dim measuringunit = Reader.GetString("measuringunit")
                Dim totalprice = Reader.GetDecimal("totalprice")
                Dim discount = Reader.GetDecimal("discount")
                Dim finalprice = Reader.GetDecimal("discountedprice")
                LoanedProductsDataGridView.Rows.Add(loaneddate, orderno, prodno, prodname, prodbrand, proddesc, purchasecost, saleprice, quantity, measuringunit, totalprice, discount, finalprice)
            End While
            Reader.Close()
            LoanedProductsDataGridView.ClearSelection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        MysqlCon.Close()
    End Sub
    Private Sub PaymentHistoryTextBox_TextChanged(sender As Object, e As EventArgs) Handles PaymentHistoryTextBox.TextChanged
        Try
            OpenConnection()
            PaymentHistoryDataGridView.Rows.Clear()
            Query = "SELECT * FROM paymentsmade WHERE date LIKE '%" _
                    & PaymentHistoryTextBox.Text & "%' OR orderno LIKE '%" & PaymentHistoryTextBox.Text _
                    & "%' AND customerid = '" & SelectedDebtor & "'"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim datepaid = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                Dim paidamount = Reader.GetDecimal("paidamount")
                Dim orderno = Reader.GetString("orderno")
                PaymentHistoryDataGridView.Rows.Add(datepaid, paidamount, orderno)
            End While
            Reader.Close()
            PaymentHistoryDataGridView.ClearSelection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        MysqlCon.Close()
    End Sub
    Private Sub PayToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PayToolStripMenuItem.Click
        PayLoanForm.ShowDialog()
    End Sub

    Private Sub UpdateinfoButton_Click(sender As Object, e As EventArgs) Handles UpdateinfoButton.Click
        UpdateInfoForm.ShowDialog()
    End Sub

    Private Sub CreditsInformationForm_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        CreditsInformationForm_Load(sender, e)
    End Sub
    Private Sub printPreview_PrintClick(sender As Object, e As EventArgs)
        Try
            PrintDialog1.Document = CreditsPrintDocument
            If PrintDialog1.ShowDialog() = DialogResult.OK Then
                CreditsPrintDocument.Print()
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub PrintToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PrintToolStripMenuItem.Click
        CreditsInformationForm_Load(sender, e)
        Dim ShortBondPaper As New PaperSize("Short Bond Paper", 1100, 850)
        Dim SetMargin As New Margins(25, 25, 25, 25)
        With CreditsPrintDocument.DefaultPageSettings
            .PaperSize = ShortBondPaper
            .Margins = SetMargin
        End With
        Dim b As New ToolStripButton
        b.Image = CType(PrintPreviewDialog1.Controls(1), ToolStrip).ImageList.Images(0)
        b.ToolTipText = "Print"
        b.DisplayStyle = ToolStripItemDisplayStyle.Image
        AddHandler b.Click, AddressOf printPreview_PrintClick
        CType(PrintPreviewDialog1.Controls(1), ToolStrip).Items.RemoveAt(0)
        CType(PrintPreviewDialog1.Controls(1), ToolStrip).Items.Insert(0, b)
        PrintPreviewDialog1.Document = CreditsPrintDocument
        PrintPreviewDialog1.ShowDialog()
    End Sub
    Private HeadersPrintedBool As Boolean = False
    Private VerticalPrintLocation As Single = 50
    Private LoansIndex As Integer = 0
    Private PaymentHistoryIndex As Integer = 0
    Private LoanedProducsIndex As Integer = 0
    Private CaseNumber As Integer = 1
    Private StartGinberAt As Integer = 0
    Private LoanedProductsColumnNamePrinted As Boolean = False

    Private Sub PrintPreviewDialog1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles PrintPreviewDialog1.FormClosing
        HeadersPrintedBool = False
        VerticalPrintLocation = 50
        LoansIndex = 0
        PaymentHistoryIndex = 0
        LoanedProducsIndex = 0
        CaseNumber = 1
        StartGinberAt = 0
        LoanedProductsColumnNamePrinted = False
    End Sub
    Private Sub CreditsPrintDocument_PrintPage(sender As Object, e As PrintPageEventArgs) Handles CreditsPrintDocument.PrintPage
        'Dim VerticalPrintLocation As Single = e.MarginBounds.Top + 25
        Dim PrintFontBold As New Font("Arial Narrow", 10, FontStyle.Bold)
        Dim PrintFont As New Font("Arial Narrow", 10)
        Dim DebtorsInfoFieldPrintFont As New Font("Arial Narrow", 12, FontStyle.Bold)
        Dim DebtorsInfoDataPrintFont As New Font("Arial Narrow", 12)
        Dim LoansDataBoldPrintFont As New Font("Arial Narrow", 10, FontStyle.Bold)
        Dim LoansDataPrintFont As New Font("Arial Narrow", 10)
        Dim Lineheight As Single = LoansDataBoldPrintFont.GetHeight
        Dim DebtorsPhotoPrintLocation As Single = e.MarginBounds.Left
        Dim DebtorsInfoFieldPrintLocation As Single = DebtorsPhotoPrintLocation + 160
        Dim DebtorsInfoDataPrintLocation As Single = DebtorsInfoFieldPrintLocation + 75
        Dim CreditsSummaryFieldPrintLocation As Single = e.MarginBounds.Left + 650
        Dim CreditsSummaryDataPrintLocation As Single = CreditsSummaryFieldPrintLocation + 145
        Dim PaymentDatePrintLocation As Single = e.MarginBounds.Left + 25
        Dim PaymentAmountprintlocation As Single = PaymentDatePrintLocation + 100
        Dim LoansPrintLocation As Single = e.MarginBounds.Left
        Dim PaymentHistoryPrintLocation As Single = e.MarginBounds.Left + 650

        If HeadersPrintedBool = False Then
            Dim HeaderString As String = "ROJ MOTOR PARTS"
            Dim ROJFONT As New Font("elephant", 11)
            Dim ROJHorizontalPrintLocation As Single = Convert.ToSingle((e.PageBounds.Width / 2 - e.Graphics.MeasureString(HeaderString, ROJFONT).Width / 2) + 25)

            e.Graphics.DrawString(HeaderString, ROJFONT, Brushes.Black, ROJHorizontalPrintLocation, VerticalPrintLocation)
            VerticalPrintLocation += 15
            HeaderString = "Bintawan Rd, Solano, Nueva Vizcaya"
            e.Graphics.DrawString(HeaderString, PrintFont, Brushes.Black, ROJHorizontalPrintLocation, VerticalPrintLocation)
            VerticalPrintLocation += 15
            HeaderString = "Contact: 0916 169 6397"
            e.Graphics.DrawString(HeaderString, PrintFont, Brushes.Black, ROJHorizontalPrintLocation, VerticalPrintLocation)
            VerticalPrintLocation += 15
            HeaderString = "Facebook: www.facebook.com/jaysonrobin2019/"
            e.Graphics.DrawString(HeaderString, PrintFont, Brushes.Black, ROJHorizontalPrintLocation, VerticalPrintLocation)
            Dim rojlogo As Image = My.Resources.rojPrintlogo
            e.Graphics.DrawImage(rojlogo, ROJHorizontalPrintLocation - 100, 40, 100, 75)
            VerticalPrintLocation += 40

            e.Graphics.DrawString("DEBTOR'S INFORMATION", DebtorsInfoFieldPrintFont, Brushes.Black, DebtorsPhotoPrintLocation, VerticalPrintLocation)
            e.Graphics.DrawString("CREDITS SUMMARY", DebtorsInfoFieldPrintFont, Brushes.Black, CreditsSummaryFieldPrintLocation, VerticalPrintLocation)
            VerticalPrintLocation += 25

            e.Graphics.DrawImage(DebtorsPhotoPictureBox.Image, DebtorsPhotoPrintLocation, VerticalPrintLocation, 150, 150)
            e.Graphics.DrawString("ID:", DebtorsInfoFieldPrintFont, Brushes.Black, DebtorsInfoFieldPrintLocation, VerticalPrintLocation)
            e.Graphics.DrawString(IDLabel.Text, DebtorsInfoDataPrintFont, Brushes.Black, DebtorsInfoDataPrintLocation, VerticalPrintLocation)

            e.Graphics.DrawString("Total Payment:", DebtorsInfoFieldPrintFont, Brushes.Black, CreditsSummaryFieldPrintLocation, VerticalPrintLocation)
            e.Graphics.DrawString(TotalPaymentTextBox.Text, DebtorsInfoDataPrintFont, Brushes.Black, CreditsSummaryDataPrintLocation, VerticalPrintLocation)
            VerticalPrintLocation += 20

            e.Graphics.DrawString("Name:", DebtorsInfoFieldPrintFont, Brushes.Black, DebtorsInfoFieldPrintLocation, VerticalPrintLocation)
            e.Graphics.DrawString(FullNameLabel.Text, DebtorsInfoDataPrintFont, Brushes.Black, DebtorsInfoDataPrintLocation, VerticalPrintLocation)

            e.Graphics.DrawString("Total Discount:", DebtorsInfoFieldPrintFont, Brushes.Black, CreditsSummaryFieldPrintLocation, VerticalPrintLocation)
            e.Graphics.DrawString(DiscountTextBox.Text, DebtorsInfoDataPrintFont, Brushes.Black, CreditsSummaryDataPrintLocation, VerticalPrintLocation)

            VerticalPrintLocation += 20
            e.Graphics.DrawString("Contact:", DebtorsInfoFieldPrintFont, Brushes.Black, DebtorsInfoFieldPrintLocation, VerticalPrintLocation)
            e.Graphics.DrawString(ContactLabel.Text, DebtorsInfoDataPrintFont, Brushes.Black, DebtorsInfoDataPrintLocation, VerticalPrintLocation)

            e.Graphics.DrawString("Total Paid:", DebtorsInfoFieldPrintFont, Brushes.Black, CreditsSummaryFieldPrintLocation, VerticalPrintLocation)
            e.Graphics.DrawString(TotalPaidTextBox.Text, DebtorsInfoDataPrintFont, Brushes.Black, CreditsSummaryDataPrintLocation, VerticalPrintLocation)

            VerticalPrintLocation += 20
            e.Graphics.DrawString("More Info:", DebtorsInfoFieldPrintFont, Brushes.Black, DebtorsInfoFieldPrintLocation, VerticalPrintLocation)

            e.Graphics.DrawString("Remaining Payment:", DebtorsInfoFieldPrintFont, Brushes.Black, CreditsSummaryFieldPrintLocation, VerticalPrintLocation)
            e.Graphics.DrawString(RemainingPaymentTextBox.Text, DebtorsInfoDataPrintFont, Brushes.Black, CreditsSummaryDataPrintLocation, VerticalPrintLocation)
            Try
                For i As Int32 = 0 To 5
                    e.Graphics.DrawString(MoreInfoListBox.Items(i), DebtorsInfoDataPrintFont, Brushes.Black, DebtorsInfoDataPrintLocation, VerticalPrintLocation)
                    VerticalPrintLocation += 15
                Next
            Catch ex As Exception

            End Try
            VerticalPrintLocation = 320

            e.Graphics.DrawString("LOANS", LoansDataBoldPrintFont, Brushes.Black, LoansPrintLocation, VerticalPrintLocation)
            e.Graphics.DrawString("PAYMENT HISTORY", LoansDataBoldPrintFont, Brushes.Black, PaymentHistoryPrintLocation, VerticalPrintLocation)
            VerticalPrintLocation += 20
            e.Graphics.DrawString("Date Loaned", LoansDataBoldPrintFont, Brushes.Black, LoansPrintLocation, VerticalPrintLocation)
            e.Graphics.DrawString("Order No", LoansDataBoldPrintFont, Brushes.Black, LoansPrintLocation + 80, VerticalPrintLocation)
            e.Graphics.DrawString("Total Payment", LoansDataBoldPrintFont, Brushes.Black, LoansPrintLocation + 180, VerticalPrintLocation)
            e.Graphics.DrawString("Total Discount", LoansDataBoldPrintFont, Brushes.Black, LoansPrintLocation + 280, VerticalPrintLocation)
            e.Graphics.DrawString("Amount Paid", LoansDataBoldPrintFont, Brushes.Black, LoansPrintLocation + 380, VerticalPrintLocation)
            e.Graphics.DrawString("Remaining Payment", LoansDataBoldPrintFont, Brushes.Black, LoansPrintLocation + 480, VerticalPrintLocation)

            e.Graphics.DrawString("Date Paid", LoansDataBoldPrintFont, Brushes.Black, PaymentHistoryPrintLocation, VerticalPrintLocation)
            e.Graphics.DrawString("Amount Paid", LoansDataBoldPrintFont, Brushes.Black, PaymentHistoryPrintLocation + 80, VerticalPrintLocation)
            e.Graphics.DrawString("Order No", LoansDataBoldPrintFont, Brushes.Black, PaymentHistoryPrintLocation + 180, VerticalPrintLocation)
            VerticalPrintLocation += 15
            HeadersPrintedBool = True
        End If

        Try
            For Ginber As Integer = StartGinberAt To 2
                Select Case CaseNumber
                    Case 1
                        For i As Integer = LoansIndex To LoansDataGridView.RowCount() - 1
                            If VerticalPrintLocation + Lineheight <= 810 Then
                                e.Graphics.DrawString(LoansDataGridView.Rows(i).Cells(0).Value, LoansDataPrintFont, Brushes.Black, LoansPrintLocation, VerticalPrintLocation)
                                e.Graphics.DrawString(LoansDataGridView.Rows(i).Cells(1).Value, LoansDataPrintFont, Brushes.Black, LoansPrintLocation + 80, VerticalPrintLocation)
                                e.Graphics.DrawString(LoansDataGridView.Rows(i).Cells(2).Value, LoansDataPrintFont, Brushes.Black, LoansPrintLocation + 180, VerticalPrintLocation)
                                e.Graphics.DrawString(LoansDataGridView.Rows(i).Cells(3).Value, LoansDataPrintFont, Brushes.Black, LoansPrintLocation + 280, VerticalPrintLocation)
                                e.Graphics.DrawString(LoansDataGridView.Rows(i).Cells(4).Value, LoansDataPrintFont, Brushes.Black, LoansPrintLocation + 380, VerticalPrintLocation)
                                e.Graphics.DrawString(LoansDataGridView.Rows(i).Cells(5).Value, LoansDataPrintFont, Brushes.Black, LoansPrintLocation + 480, VerticalPrintLocation)
                                If PaymentHistoryDataGridView.RowCount > i Then
                                    e.Graphics.DrawString(PaymentHistoryDataGridView.Rows(i).Cells(0).Value, LoansDataPrintFont, Brushes.Black, PaymentHistoryPrintLocation, VerticalPrintLocation)
                                    e.Graphics.DrawString(PaymentHistoryDataGridView.Rows(i).Cells(1).Value, LoansDataPrintFont, Brushes.Black, PaymentHistoryPrintLocation + 80, VerticalPrintLocation)
                                    e.Graphics.DrawString(PaymentHistoryDataGridView.Rows(i).Cells(2).Value, LoansDataPrintFont, Brushes.Black, PaymentHistoryPrintLocation + 180, VerticalPrintLocation)
                                    PaymentHistoryIndex += 1
                                End If
                                VerticalPrintLocation += 15
                            Else
                                VerticalPrintLocation = e.MarginBounds.Top
                                e.HasMorePages = True
                                LoansIndex = i
                                StartGinberAt = Ginber
                                Return
                            End If
                        Next
                        CaseNumber = 2
                    Case 2
                        For i As Integer = PaymentHistoryIndex To PaymentHistoryDataGridView.RowCount() - 1
                            If VerticalPrintLocation + Lineheight <= 810 Then
                                e.Graphics.DrawString(PaymentHistoryDataGridView.Rows(i).Cells(0).Value, LoansDataPrintFont, Brushes.Black, PaymentHistoryPrintLocation, VerticalPrintLocation)
                                e.Graphics.DrawString(PaymentHistoryDataGridView.Rows(i).Cells(1).Value, LoansDataPrintFont, Brushes.Black, PaymentHistoryPrintLocation + 80, VerticalPrintLocation)
                                e.Graphics.DrawString(PaymentHistoryDataGridView.Rows(i).Cells(2).Value, LoansDataPrintFont, Brushes.Black, PaymentHistoryPrintLocation + 180, VerticalPrintLocation)
                                VerticalPrintLocation += 15
                            Else
                                VerticalPrintLocation = e.MarginBounds.Top
                                e.HasMorePages = True
                                PaymentHistoryIndex = i
                                StartGinberAt = Ginber
                                Return
                            End If
                        Next
                        CaseNumber = 3
                        VerticalPrintLocation += 10
                    Case 3
                        Dim DateLoanedLocation As Single = e.MarginBounds.Left
                        Dim OrderNoPrintLocation As Single = e.MarginBounds.Left + 80
                        Dim ProductNoPrintLocation As Single = OrderNoPrintLocation + 105
                        Dim ProductNameLocation As Single = ProductNoPrintLocation + 95
                        Dim ProductBrandLocation As Single = ProductNameLocation + 105
                        Dim ProductDescLocation As Single = ProductBrandLocation + 85
                        Dim purchasecostLocation As Single = ProductDescLocation + 85
                        Dim SalePriceLocation As Single = purchasecostLocation + 70
                        Dim QuantityLocation As Single = SalePriceLocation + 70
                        Dim MeasuringUnitLocation As Single = QuantityLocation + 70
                        Dim TotalPriceLocation As Single = MeasuringUnitLocation + 70
                        Dim DiscountLocation As Single = TotalPriceLocation + 70
                        Dim DiscountedPriceLocation As Single = DiscountLocation + 70
                        If LoanedProductsColumnNamePrinted = False Then
                            If VerticalPrintLocation + Lineheight * 3 <= 810 Then
                                e.Graphics.DrawString("LOANED PRODUCTS", LoansDataBoldPrintFont, Brushes.Black, DateLoanedLocation, VerticalPrintLocation)
                                VerticalPrintLocation += 20
                                e.Graphics.DrawString("Date Loaned", LoansDataBoldPrintFont, Brushes.Black, DateLoanedLocation, VerticalPrintLocation)
                                e.Graphics.DrawString("Order No", LoansDataBoldPrintFont, Brushes.Black, OrderNoPrintLocation, VerticalPrintLocation)
                                e.Graphics.DrawString("Product No", LoansDataBoldPrintFont, Brushes.Black, ProductNoPrintLocation, VerticalPrintLocation)
                                e.Graphics.DrawString("Product Name", LoansDataBoldPrintFont, Brushes.Black, ProductNameLocation, VerticalPrintLocation)
                                e.Graphics.DrawString("Brand", LoansDataBoldPrintFont, Brushes.Black, ProductBrandLocation, VerticalPrintLocation)
                                e.Graphics.DrawString("Description", LoansDataBoldPrintFont, Brushes.Black, ProductDescLocation, VerticalPrintLocation)
                                e.Graphics.DrawString("Pur. Cost", LoansDataBoldPrintFont, Brushes.Black, purchasecostLocation, VerticalPrintLocation)
                                e.Graphics.DrawString("Sale Price", LoansDataBoldPrintFont, Brushes.Black, SalePriceLocation, VerticalPrintLocation)
                                e.Graphics.DrawString("Quantity", LoansDataBoldPrintFont, Brushes.Black, QuantityLocation, VerticalPrintLocation)
                                e.Graphics.DrawString("Meas. Unit", LoansDataBoldPrintFont, Brushes.Black, MeasuringUnitLocation, VerticalPrintLocation)
                                e.Graphics.DrawString("Total Price", LoansDataBoldPrintFont, Brushes.Black, TotalPriceLocation, VerticalPrintLocation)
                                e.Graphics.DrawString("Discount", LoansDataBoldPrintFont, Brushes.Black, DiscountLocation, VerticalPrintLocation)
                                e.Graphics.DrawString("Final Price", LoansDataBoldPrintFont, Brushes.Black, DiscountedPriceLocation, VerticalPrintLocation)
                                VerticalPrintLocation += 15
                                LoanedProductsColumnNamePrinted = True
                            Else
                                VerticalPrintLocation = e.MarginBounds.Top
                                e.HasMorePages = True
                                StartGinberAt = Ginber
                                Return
                            End If
                        End If
                        For row As Integer = LoanedProducsIndex To LoanedProductsDataGridView.RowCount() - 1
                            If VerticalPrintLocation + Lineheight <= 810 Then
                                e.Graphics.DrawString(LoanedProductsDataGridView.Rows(row).Cells(0).Value, PrintFont, Brushes.Black, DateLoanedLocation, VerticalPrintLocation)
                                e.Graphics.DrawString(LoanedProductsDataGridView.Rows(row).Cells(1).Value, PrintFont, Brushes.Black, OrderNoPrintLocation, VerticalPrintLocation)
                                e.Graphics.DrawString(LoanedProductsDataGridView.Rows(row).Cells(2).Value, PrintFont, Brushes.Black, ProductNoPrintLocation, VerticalPrintLocation)
                                e.Graphics.DrawString(SearchRecordsForm.LimitStrLen(LoanedProductsDataGridView.Rows(row).Cells(3).Value, 13), PrintFont, Brushes.Black, ProductNameLocation, VerticalPrintLocation)
                                e.Graphics.DrawString(SearchRecordsForm.LimitStrLen(LoanedProductsDataGridView.Rows(row).Cells(4).Value, 10), PrintFont, Brushes.Black, ProductBrandLocation, VerticalPrintLocation)
                                e.Graphics.DrawString(SearchRecordsForm.LimitStrLen(LoanedProductsDataGridView.Rows(row).Cells(5).Value, 10), PrintFont, Brushes.Black, ProductDescLocation, VerticalPrintLocation)
                                e.Graphics.DrawString(LoanedProductsDataGridView.Rows(row).Cells(6).Value, PrintFont, Brushes.Black, purchasecostLocation, VerticalPrintLocation)
                                e.Graphics.DrawString(LoanedProductsDataGridView.Rows(row).Cells(7).Value, PrintFont, Brushes.Black, SalePriceLocation, VerticalPrintLocation)
                                e.Graphics.DrawString(LoanedProductsDataGridView.Rows(row).Cells(8).Value, PrintFont, Brushes.Black, QuantityLocation, VerticalPrintLocation)
                                e.Graphics.DrawString(LoanedProductsDataGridView.Rows(row).Cells(9).Value, PrintFont, Brushes.Black, MeasuringUnitLocation, VerticalPrintLocation)
                                e.Graphics.DrawString(LoanedProductsDataGridView.Rows(row).Cells(10).Value, PrintFont, Brushes.Black, TotalPriceLocation, VerticalPrintLocation)
                                e.Graphics.DrawString(LoanedProductsDataGridView.Rows(row).Cells(11).Value, PrintFont, Brushes.Black, DiscountLocation, VerticalPrintLocation)
                                e.Graphics.DrawString(LoanedProductsDataGridView.Rows(row).Cells(12).Value, PrintFont, Brushes.Black, DiscountedPriceLocation, VerticalPrintLocation)
                                VerticalPrintLocation += 15
                            Else
                                VerticalPrintLocation = e.MarginBounds.Top
                                e.HasMorePages = True
                                LoanedProducsIndex = row
                                StartGinberAt = Ginber
                                Return
                            End If
                        Next
                End Select
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class