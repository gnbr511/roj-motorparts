﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SearchRecordsForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SearchRecordsForm))
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.FromDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.ToDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SearchButton = New System.Windows.Forms.Button()
        Me.MakebackupButton = New System.Windows.Forms.Button()
        Me.DeleteSlctdRecButton = New System.Windows.Forms.Button()
        Me.PrintSearchedRecordsButton = New System.Windows.Forms.Button()
        Me.PrintPreviewDialog1 = New System.Windows.Forms.PrintPreviewDialog()
        Me.AllRecordsPrintDocument = New System.Drawing.Printing.PrintDocument()
        Me.HiddenSoldProductsDataGridView1 = New System.Windows.Forms.DataGridView()
        Me.ProductnameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductbrandColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductDescriptionColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustomernameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SalePriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QuantityColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MeasuringunitColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalPriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RdiscountColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RfinalPriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HiddenDebtPaymentsDataGridView = New System.Windows.Forms.DataGridView()
        Me.DebtPaymentsCusFullNameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DebtPaymentsPaidAmountColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HiddenExpensesDataGridView = New System.Windows.Forms.DataGridView()
        Me.ExpNameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ExpDescriptionColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ExpCostColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HiddenDailysalesDataGridView = New System.Windows.Forms.DataGridView()
        Me.Date3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GrossSaleColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.totdiscountColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.totExpensesColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NetSaleColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrintDialog1 = New System.Windows.Forms.PrintDialog()
        Me.PrintButton = New System.Windows.Forms.Button()
        Me.NewPrintDocument = New System.Drawing.Printing.PrintDocument()
        Me.HiddenSoldProductsDataGridView = New System.Windows.Forms.DataGridView()
        Me.DateColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OrderNumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PurchaseCost = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HiddenLoanedDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DebtPaymentsDataGridView = New System.Windows.Forms.DataGridView()
        Me.DebtpaymentDateColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OrderNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DebtorsIDNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.datetime = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ExpensesDataGridView = New System.Windows.Forms.DataGridView()
        Me.DateexpColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn27 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn28 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn29 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.HiddenSoldProductsDataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HiddenDebtPaymentsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HiddenExpensesDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HiddenDailysalesDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HiddenSoldProductsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HiddenLoanedDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DebtPaymentsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ExpensesDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'FromDateTimePicker
        '
        Me.FromDateTimePicker.CalendarFont = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FromDateTimePicker.CustomFormat = "yyyy-MM-dd"
        Me.FromDateTimePicker.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FromDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FromDateTimePicker.Location = New System.Drawing.Point(48, 79)
        Me.FromDateTimePicker.Name = "FromDateTimePicker"
        Me.FromDateTimePicker.Size = New System.Drawing.Size(116, 23)
        Me.FromDateTimePicker.TabIndex = 0
        '
        'ToDateTimePicker
        '
        Me.ToDateTimePicker.CalendarFont = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToDateTimePicker.CustomFormat = "yyyy-MM-dd"
        Me.ToDateTimePicker.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.ToDateTimePicker.Location = New System.Drawing.Point(48, 138)
        Me.ToDateTimePicker.Name = "ToDateTimePicker"
        Me.ToDateTimePicker.Size = New System.Drawing.Size(116, 23)
        Me.ToDateTimePicker.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(45, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(121, 16)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Year - Month - Date"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(45, 60)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "From:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(45, 119)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(28, 16)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "To:"
        '
        'SearchButton
        '
        Me.SearchButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SearchButton.Location = New System.Drawing.Point(44, 198)
        Me.SearchButton.Name = "SearchButton"
        Me.SearchButton.Size = New System.Drawing.Size(124, 32)
        Me.SearchButton.TabIndex = 5
        Me.SearchButton.Text = "SEARCH RECORDS"
        Me.SearchButton.UseVisualStyleBackColor = True
        '
        'MakebackupButton
        '
        Me.MakebackupButton.Enabled = False
        Me.MakebackupButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MakebackupButton.Location = New System.Drawing.Point(44, 198)
        Me.MakebackupButton.Name = "MakebackupButton"
        Me.MakebackupButton.Size = New System.Drawing.Size(124, 32)
        Me.MakebackupButton.TabIndex = 6
        Me.MakebackupButton.Text = "MAKE BACKUP"
        Me.MakebackupButton.UseVisualStyleBackColor = True
        '
        'DeleteSlctdRecButton
        '
        Me.DeleteSlctdRecButton.Enabled = False
        Me.DeleteSlctdRecButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DeleteSlctdRecButton.Location = New System.Drawing.Point(44, 246)
        Me.DeleteSlctdRecButton.Name = "DeleteSlctdRecButton"
        Me.DeleteSlctdRecButton.Size = New System.Drawing.Size(124, 43)
        Me.DeleteSlctdRecButton.TabIndex = 7
        Me.DeleteSlctdRecButton.Text = "DELETE SELECTED RECORDS"
        Me.DeleteSlctdRecButton.UseVisualStyleBackColor = True
        '
        'PrintSearchedRecordsButton
        '
        Me.PrintSearchedRecordsButton.Enabled = False
        Me.PrintSearchedRecordsButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PrintSearchedRecordsButton.Location = New System.Drawing.Point(44, 246)
        Me.PrintSearchedRecordsButton.Name = "PrintSearchedRecordsButton"
        Me.PrintSearchedRecordsButton.Size = New System.Drawing.Size(124, 43)
        Me.PrintSearchedRecordsButton.TabIndex = 8
        Me.PrintSearchedRecordsButton.Text = "PRINT SEARCHED RECORDS"
        Me.PrintSearchedRecordsButton.UseVisualStyleBackColor = True
        Me.PrintSearchedRecordsButton.Visible = False
        '
        'PrintPreviewDialog1
        '
        Me.PrintPreviewDialog1.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPreviewDialog1.Enabled = True
        Me.PrintPreviewDialog1.Icon = CType(resources.GetObject("PrintPreviewDialog1.Icon"), System.Drawing.Icon)
        Me.PrintPreviewDialog1.Name = "PrintPreviewDialog1"
        Me.PrintPreviewDialog1.Visible = False
        '
        'AllRecordsPrintDocument
        '
        '
        'HiddenSoldProductsDataGridView1
        '
        Me.HiddenSoldProductsDataGridView1.AllowUserToAddRows = False
        Me.HiddenSoldProductsDataGridView1.AllowUserToDeleteRows = False
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.Gainsboro
        Me.HiddenSoldProductsDataGridView1.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle9
        Me.HiddenSoldProductsDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.HiddenSoldProductsDataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ProductnameColumn, Me.ProductbrandColumn, Me.ProductDescriptionColumn, Me.CustomernameColumn, Me.SalePriceColumn, Me.QuantityColumn, Me.MeasuringunitColumn, Me.TotalPriceColumn, Me.RdiscountColumn, Me.RfinalPriceColumn})
        Me.HiddenSoldProductsDataGridView1.Location = New System.Drawing.Point(12, 578)
        Me.HiddenSoldProductsDataGridView1.Name = "HiddenSoldProductsDataGridView1"
        Me.HiddenSoldProductsDataGridView1.ReadOnly = True
        Me.HiddenSoldProductsDataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.HiddenSoldProductsDataGridView1.Size = New System.Drawing.Size(776, 92)
        Me.HiddenSoldProductsDataGridView1.TabIndex = 9
        '
        'ProductnameColumn
        '
        Me.ProductnameColumn.HeaderText = "Product Name"
        Me.ProductnameColumn.Name = "ProductnameColumn"
        Me.ProductnameColumn.ReadOnly = True
        Me.ProductnameColumn.Width = 110
        '
        'ProductbrandColumn
        '
        Me.ProductbrandColumn.HeaderText = "Product Brand"
        Me.ProductbrandColumn.Name = "ProductbrandColumn"
        Me.ProductbrandColumn.ReadOnly = True
        Me.ProductbrandColumn.Width = 90
        '
        'ProductDescriptionColumn
        '
        Me.ProductDescriptionColumn.HeaderText = "Product Description"
        Me.ProductDescriptionColumn.Name = "ProductDescriptionColumn"
        Me.ProductDescriptionColumn.ReadOnly = True
        Me.ProductDescriptionColumn.Width = 90
        '
        'CustomernameColumn
        '
        Me.CustomernameColumn.HeaderText = "Customer Name"
        Me.CustomernameColumn.Name = "CustomernameColumn"
        Me.CustomernameColumn.ReadOnly = True
        Me.CustomernameColumn.Width = 90
        '
        'SalePriceColumn
        '
        Me.SalePriceColumn.HeaderText = "Sale Price"
        Me.SalePriceColumn.Name = "SalePriceColumn"
        Me.SalePriceColumn.ReadOnly = True
        Me.SalePriceColumn.Width = 70
        '
        'QuantityColumn
        '
        Me.QuantityColumn.HeaderText = "Quantity"
        Me.QuantityColumn.Name = "QuantityColumn"
        Me.QuantityColumn.ReadOnly = True
        Me.QuantityColumn.Width = 70
        '
        'MeasuringunitColumn
        '
        Me.MeasuringunitColumn.HeaderText = "Measuring Unit"
        Me.MeasuringunitColumn.Name = "MeasuringunitColumn"
        Me.MeasuringunitColumn.ReadOnly = True
        '
        'TotalPriceColumn
        '
        Me.TotalPriceColumn.HeaderText = "Total Price"
        Me.TotalPriceColumn.Name = "TotalPriceColumn"
        Me.TotalPriceColumn.ReadOnly = True
        Me.TotalPriceColumn.Width = 70
        '
        'RdiscountColumn
        '
        Me.RdiscountColumn.HeaderText = "Discount"
        Me.RdiscountColumn.Name = "RdiscountColumn"
        Me.RdiscountColumn.ReadOnly = True
        Me.RdiscountColumn.Width = 70
        '
        'RfinalPriceColumn
        '
        Me.RfinalPriceColumn.HeaderText = "Final Price"
        Me.RfinalPriceColumn.Name = "RfinalPriceColumn"
        Me.RfinalPriceColumn.ReadOnly = True
        Me.RfinalPriceColumn.Width = 70
        '
        'HiddenDebtPaymentsDataGridView
        '
        Me.HiddenDebtPaymentsDataGridView.AllowUserToAddRows = False
        Me.HiddenDebtPaymentsDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.Gainsboro
        Me.HiddenDebtPaymentsDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle10
        Me.HiddenDebtPaymentsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.HiddenDebtPaymentsDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DebtPaymentsCusFullNameColumn, Me.DebtPaymentsPaidAmountColumn})
        Me.HiddenDebtPaymentsDataGridView.Location = New System.Drawing.Point(1037, 578)
        Me.HiddenDebtPaymentsDataGridView.Name = "HiddenDebtPaymentsDataGridView"
        Me.HiddenDebtPaymentsDataGridView.ReadOnly = True
        Me.HiddenDebtPaymentsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.HiddenDebtPaymentsDataGridView.Size = New System.Drawing.Size(225, 75)
        Me.HiddenDebtPaymentsDataGridView.TabIndex = 10
        '
        'DebtPaymentsCusFullNameColumn
        '
        Me.DebtPaymentsCusFullNameColumn.HeaderText = "Debtor's Full Name"
        Me.DebtPaymentsCusFullNameColumn.Name = "DebtPaymentsCusFullNameColumn"
        Me.DebtPaymentsCusFullNameColumn.ReadOnly = True
        Me.DebtPaymentsCusFullNameColumn.Width = 110
        '
        'DebtPaymentsPaidAmountColumn
        '
        Me.DebtPaymentsPaidAmountColumn.HeaderText = "Paid Amount"
        Me.DebtPaymentsPaidAmountColumn.Name = "DebtPaymentsPaidAmountColumn"
        Me.DebtPaymentsPaidAmountColumn.ReadOnly = True
        Me.DebtPaymentsPaidAmountColumn.Width = 70
        '
        'HiddenExpensesDataGridView
        '
        Me.HiddenExpensesDataGridView.AllowUserToAddRows = False
        Me.HiddenExpensesDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.Gainsboro
        Me.HiddenExpensesDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle11
        Me.HiddenExpensesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.HiddenExpensesDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ExpNameColumn, Me.ExpDescriptionColumn, Me.ExpCostColumn})
        Me.HiddenExpensesDataGridView.Location = New System.Drawing.Point(794, 578)
        Me.HiddenExpensesDataGridView.Name = "HiddenExpensesDataGridView"
        Me.HiddenExpensesDataGridView.ReadOnly = True
        Me.HiddenExpensesDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.HiddenExpensesDataGridView.Size = New System.Drawing.Size(237, 72)
        Me.HiddenExpensesDataGridView.TabIndex = 11
        '
        'ExpNameColumn
        '
        Me.ExpNameColumn.HeaderText = "Expenses Name"
        Me.ExpNameColumn.Name = "ExpNameColumn"
        Me.ExpNameColumn.ReadOnly = True
        Me.ExpNameColumn.Width = 130
        '
        'ExpDescriptionColumn
        '
        Me.ExpDescriptionColumn.HeaderText = "Description"
        Me.ExpDescriptionColumn.Name = "ExpDescriptionColumn"
        Me.ExpDescriptionColumn.ReadOnly = True
        Me.ExpDescriptionColumn.Width = 130
        '
        'ExpCostColumn
        '
        Me.ExpCostColumn.HeaderText = "Cost"
        Me.ExpCostColumn.Name = "ExpCostColumn"
        Me.ExpCostColumn.ReadOnly = True
        Me.ExpCostColumn.Width = 70
        '
        'HiddenDailysalesDataGridView
        '
        Me.HiddenDailysalesDataGridView.AllowUserToAddRows = False
        Me.HiddenDailysalesDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.Gainsboro
        Me.HiddenDailysalesDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle12
        Me.HiddenDailysalesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.HiddenDailysalesDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Date3, Me.GrossSaleColumn, Me.totdiscountColumn, Me.totExpensesColumn, Me.NetSaleColumn})
        Me.HiddenDailysalesDataGridView.Location = New System.Drawing.Point(292, 232)
        Me.HiddenDailysalesDataGridView.Name = "HiddenDailysalesDataGridView"
        Me.HiddenDailysalesDataGridView.ReadOnly = True
        Me.HiddenDailysalesDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.HiddenDailysalesDataGridView.Size = New System.Drawing.Size(545, 110)
        Me.HiddenDailysalesDataGridView.TabIndex = 12
        '
        'Date3
        '
        Me.Date3.HeaderText = "Date"
        Me.Date3.Name = "Date3"
        Me.Date3.ReadOnly = True
        '
        'GrossSaleColumn
        '
        Me.GrossSaleColumn.HeaderText = "Gross Sale"
        Me.GrossSaleColumn.Name = "GrossSaleColumn"
        Me.GrossSaleColumn.ReadOnly = True
        '
        'totdiscountColumn
        '
        Me.totdiscountColumn.HeaderText = "Total Discount"
        Me.totdiscountColumn.Name = "totdiscountColumn"
        Me.totdiscountColumn.ReadOnly = True
        '
        'totExpensesColumn
        '
        Me.totExpensesColumn.HeaderText = "Total Expenses"
        Me.totExpensesColumn.Name = "totExpensesColumn"
        Me.totExpensesColumn.ReadOnly = True
        '
        'NetSaleColumn
        '
        Me.NetSaleColumn.HeaderText = "Net Sale"
        Me.NetSaleColumn.Name = "NetSaleColumn"
        Me.NetSaleColumn.ReadOnly = True
        '
        'PrintDialog1
        '
        Me.PrintDialog1.UseEXDialog = True
        '
        'PrintButton
        '
        Me.PrintButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PrintButton.Location = New System.Drawing.Point(42, 246)
        Me.PrintButton.Name = "PrintButton"
        Me.PrintButton.Size = New System.Drawing.Size(124, 43)
        Me.PrintButton.TabIndex = 13
        Me.PrintButton.Text = "PRINT SEARCHED RECORDS"
        Me.PrintButton.UseVisualStyleBackColor = True
        Me.PrintButton.Visible = False
        '
        'NewPrintDocument
        '
        '
        'HiddenSoldProductsDataGridView
        '
        Me.HiddenSoldProductsDataGridView.AllowUserToAddRows = False
        Me.HiddenSoldProductsDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.White
        Me.HiddenSoldProductsDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle13
        Me.HiddenSoldProductsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.HiddenSoldProductsDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DateColumn, Me.OrderNumber, Me.DataGridViewTextBoxColumn1, Me.ProductNo, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.PurchaseCost, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10})
        Me.HiddenSoldProductsDataGridView.Location = New System.Drawing.Point(12, 357)
        Me.HiddenSoldProductsDataGridView.Name = "HiddenSoldProductsDataGridView"
        Me.HiddenSoldProductsDataGridView.ReadOnly = True
        Me.HiddenSoldProductsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.HiddenSoldProductsDataGridView.Size = New System.Drawing.Size(1257, 85)
        Me.HiddenSoldProductsDataGridView.TabIndex = 14
        '
        'DateColumn
        '
        Me.DateColumn.HeaderText = "Date"
        Me.DateColumn.Name = "DateColumn"
        Me.DateColumn.ReadOnly = True
        Me.DateColumn.Width = 70
        '
        'OrderNumber
        '
        Me.OrderNumber.HeaderText = "Order No"
        Me.OrderNumber.Name = "OrderNumber"
        Me.OrderNumber.ReadOnly = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Customer Name"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 120
        '
        'ProductNo
        '
        Me.ProductNo.HeaderText = "Product No"
        Me.ProductNo.Name = "ProductNo"
        Me.ProductNo.ReadOnly = True
        Me.ProductNo.Width = 90
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Product Name"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 120
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Product Brand"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 110
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Product Description"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 110
        '
        'PurchaseCost
        '
        Me.PurchaseCost.HeaderText = "Purchase Cost"
        Me.PurchaseCost.Name = "PurchaseCost"
        Me.PurchaseCost.ReadOnly = True
        Me.PurchaseCost.Width = 70
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Sale Price"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 70
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Quantity"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 70
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Measuring Unit"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 70
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Total Price"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Width = 70
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Discount"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 70
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Final Price"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Width = 70
        '
        'HiddenLoanedDataGridView
        '
        Me.HiddenLoanedDataGridView.AllowUserToAddRows = False
        Me.HiddenLoanedDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle14.BackColor = System.Drawing.Color.White
        Me.HiddenLoanedDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle14
        Me.HiddenLoanedDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.HiddenLoanedDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn14, Me.DataGridViewTextBoxColumn15, Me.DataGridViewTextBoxColumn16, Me.DataGridViewTextBoxColumn17, Me.DataGridViewTextBoxColumn18, Me.DataGridViewTextBoxColumn19, Me.DataGridViewTextBoxColumn20, Me.DataGridViewTextBoxColumn21, Me.DataGridViewTextBoxColumn22, Me.DataGridViewTextBoxColumn23, Me.DataGridViewTextBoxColumn24})
        Me.HiddenLoanedDataGridView.Location = New System.Drawing.Point(12, 448)
        Me.HiddenLoanedDataGridView.Name = "HiddenLoanedDataGridView"
        Me.HiddenLoanedDataGridView.ReadOnly = True
        Me.HiddenLoanedDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.HiddenLoanedDataGridView.Size = New System.Drawing.Size(1257, 85)
        Me.HiddenLoanedDataGridView.TabIndex = 15
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Date"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Width = 70
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Order No"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "Customer Name"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.Width = 120
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "Product No"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.Width = 90
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "Product Name"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.Width = 120
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "Product Brand"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.Width = 110
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "Product Description"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        Me.DataGridViewTextBoxColumn17.Width = 110
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "Purchase Cost"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = True
        Me.DataGridViewTextBoxColumn18.Width = 70
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.HeaderText = "Sale Price"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.ReadOnly = True
        Me.DataGridViewTextBoxColumn19.Width = 70
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.HeaderText = "Quantity"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.ReadOnly = True
        Me.DataGridViewTextBoxColumn20.Width = 70
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.HeaderText = "Measuring Unit"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.ReadOnly = True
        Me.DataGridViewTextBoxColumn21.Width = 70
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.HeaderText = "Total Price"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        Me.DataGridViewTextBoxColumn22.ReadOnly = True
        Me.DataGridViewTextBoxColumn22.Width = 70
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.HeaderText = "Discount"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        Me.DataGridViewTextBoxColumn23.ReadOnly = True
        Me.DataGridViewTextBoxColumn23.Width = 70
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.HeaderText = "Final Price"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        Me.DataGridViewTextBoxColumn24.ReadOnly = True
        Me.DataGridViewTextBoxColumn24.Width = 70
        '
        'DebtPaymentsDataGridView
        '
        Me.DebtPaymentsDataGridView.AllowUserToAddRows = False
        Me.DebtPaymentsDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle15.BackColor = System.Drawing.Color.Gainsboro
        Me.DebtPaymentsDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle15
        Me.DebtPaymentsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DebtPaymentsDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DebtpaymentDateColumn, Me.OrderNo, Me.DebtorsIDNo, Me.DataGridViewTextBoxColumn25, Me.DataGridViewTextBoxColumn26, Me.datetime})
        Me.DebtPaymentsDataGridView.Location = New System.Drawing.Point(292, 138)
        Me.DebtPaymentsDataGridView.Name = "DebtPaymentsDataGridView"
        Me.DebtPaymentsDataGridView.ReadOnly = True
        Me.DebtPaymentsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DebtPaymentsDataGridView.Size = New System.Drawing.Size(496, 80)
        Me.DebtPaymentsDataGridView.TabIndex = 16
        '
        'DebtpaymentDateColumn
        '
        Me.DebtpaymentDateColumn.HeaderText = "Date"
        Me.DebtpaymentDateColumn.Name = "DebtpaymentDateColumn"
        Me.DebtpaymentDateColumn.ReadOnly = True
        Me.DebtpaymentDateColumn.Width = 70
        '
        'OrderNo
        '
        Me.OrderNo.HeaderText = "Order No"
        Me.OrderNo.Name = "OrderNo"
        Me.OrderNo.ReadOnly = True
        '
        'DebtorsIDNo
        '
        Me.DebtorsIDNo.HeaderText = "Debtors ID"
        Me.DebtorsIDNo.Name = "DebtorsIDNo"
        Me.DebtorsIDNo.ReadOnly = True
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.HeaderText = "Debtor's Full Name"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        Me.DataGridViewTextBoxColumn25.ReadOnly = True
        Me.DataGridViewTextBoxColumn25.Width = 110
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.HeaderText = "Paid Amount"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        Me.DataGridViewTextBoxColumn26.ReadOnly = True
        Me.DataGridViewTextBoxColumn26.Width = 70
        '
        'datetime
        '
        Me.datetime.HeaderText = "datetime"
        Me.datetime.Name = "datetime"
        Me.datetime.ReadOnly = True
        Me.datetime.Visible = False
        '
        'ExpensesDataGridView
        '
        Me.ExpensesDataGridView.AllowUserToAddRows = False
        Me.ExpensesDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle16.BackColor = System.Drawing.Color.Gainsboro
        Me.ExpensesDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle16
        Me.ExpensesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ExpensesDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DateexpColumn, Me.DataGridViewTextBoxColumn27, Me.DataGridViewTextBoxColumn28, Me.DataGridViewTextBoxColumn29})
        Me.ExpensesDataGridView.Location = New System.Drawing.Point(292, 12)
        Me.ExpensesDataGridView.Name = "ExpensesDataGridView"
        Me.ExpensesDataGridView.ReadOnly = True
        Me.ExpensesDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ExpensesDataGridView.Size = New System.Drawing.Size(496, 113)
        Me.ExpensesDataGridView.TabIndex = 17
        '
        'DateexpColumn
        '
        Me.DateexpColumn.HeaderText = "Date"
        Me.DateexpColumn.Name = "DateexpColumn"
        Me.DateexpColumn.ReadOnly = True
        Me.DateexpColumn.Width = 70
        '
        'DataGridViewTextBoxColumn27
        '
        Me.DataGridViewTextBoxColumn27.HeaderText = "Expenses Name"
        Me.DataGridViewTextBoxColumn27.Name = "DataGridViewTextBoxColumn27"
        Me.DataGridViewTextBoxColumn27.ReadOnly = True
        Me.DataGridViewTextBoxColumn27.Width = 150
        '
        'DataGridViewTextBoxColumn28
        '
        Me.DataGridViewTextBoxColumn28.HeaderText = "Description"
        Me.DataGridViewTextBoxColumn28.Name = "DataGridViewTextBoxColumn28"
        Me.DataGridViewTextBoxColumn28.ReadOnly = True
        Me.DataGridViewTextBoxColumn28.Width = 150
        '
        'DataGridViewTextBoxColumn29
        '
        Me.DataGridViewTextBoxColumn29.HeaderText = "Cost"
        Me.DataGridViewTextBoxColumn29.Name = "DataGridViewTextBoxColumn29"
        Me.DataGridViewTextBoxColumn29.ReadOnly = True
        Me.DataGridViewTextBoxColumn29.Width = 80
        '
        'SearchRecordsForm
        '
        Me.AcceptButton = Me.SearchButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.ROJBackground
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(212, 351)
        Me.Controls.Add(Me.ExpensesDataGridView)
        Me.Controls.Add(Me.DebtPaymentsDataGridView)
        Me.Controls.Add(Me.HiddenLoanedDataGridView)
        Me.Controls.Add(Me.HiddenSoldProductsDataGridView)
        Me.Controls.Add(Me.PrintButton)
        Me.Controls.Add(Me.HiddenDailysalesDataGridView)
        Me.Controls.Add(Me.HiddenExpensesDataGridView)
        Me.Controls.Add(Me.HiddenDebtPaymentsDataGridView)
        Me.Controls.Add(Me.HiddenSoldProductsDataGridView1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ToDateTimePicker)
        Me.Controls.Add(Me.FromDateTimePicker)
        Me.Controls.Add(Me.SearchButton)
        Me.Controls.Add(Me.PrintSearchedRecordsButton)
        Me.Controls.Add(Me.MakebackupButton)
        Me.Controls.Add(Me.DeleteSlctdRecButton)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(1076, 319)
        Me.Name = "SearchRecordsForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Search Records"
        Me.TopMost = True
        CType(Me.HiddenSoldProductsDataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HiddenDebtPaymentsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HiddenExpensesDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HiddenDailysalesDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HiddenSoldProductsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HiddenLoanedDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DebtPaymentsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ExpensesDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents FromDateTimePicker As DateTimePicker
    Friend WithEvents ToDateTimePicker As DateTimePicker
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents SearchButton As Button
    Friend WithEvents MakebackupButton As Button
    Friend WithEvents DeleteSlctdRecButton As Button
    Friend WithEvents PrintSearchedRecordsButton As Button
    Friend WithEvents PrintPreviewDialog1 As PrintPreviewDialog
    Friend WithEvents AllRecordsPrintDocument As Printing.PrintDocument
    Friend WithEvents HiddenSoldProductsDataGridView1 As DataGridView
    Friend WithEvents ProductnameColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductbrandColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductDescriptionColumn As DataGridViewTextBoxColumn
    Friend WithEvents CustomernameColumn As DataGridViewTextBoxColumn
    Friend WithEvents SalePriceColumn As DataGridViewTextBoxColumn
    Friend WithEvents QuantityColumn As DataGridViewTextBoxColumn
    Friend WithEvents MeasuringunitColumn As DataGridViewTextBoxColumn
    Friend WithEvents TotalPriceColumn As DataGridViewTextBoxColumn
    Friend WithEvents RdiscountColumn As DataGridViewTextBoxColumn
    Friend WithEvents RfinalPriceColumn As DataGridViewTextBoxColumn
    Friend WithEvents HiddenDebtPaymentsDataGridView As DataGridView
    Friend WithEvents DebtPaymentsCusFullNameColumn As DataGridViewTextBoxColumn
    Friend WithEvents DebtPaymentsPaidAmountColumn As DataGridViewTextBoxColumn
    Friend WithEvents HiddenExpensesDataGridView As DataGridView
    Friend WithEvents ExpNameColumn As DataGridViewTextBoxColumn
    Friend WithEvents ExpDescriptionColumn As DataGridViewTextBoxColumn
    Friend WithEvents ExpCostColumn As DataGridViewTextBoxColumn
    Friend WithEvents HiddenDailysalesDataGridView As DataGridView
    Friend WithEvents PrintDialog1 As PrintDialog
    Friend WithEvents PrintButton As Button
    Friend WithEvents NewPrintDocument As Printing.PrintDocument
    Friend WithEvents HiddenSoldProductsDataGridView As DataGridView
    Friend WithEvents DateColumn As DataGridViewTextBoxColumn
    Friend WithEvents OrderNumber As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents ProductNo As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents PurchaseCost As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As DataGridViewTextBoxColumn
    Friend WithEvents HiddenLoanedDataGridView As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn11 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn24 As DataGridViewTextBoxColumn
    Friend WithEvents Date3 As DataGridViewTextBoxColumn
    Friend WithEvents GrossSaleColumn As DataGridViewTextBoxColumn
    Friend WithEvents totdiscountColumn As DataGridViewTextBoxColumn
    Friend WithEvents totExpensesColumn As DataGridViewTextBoxColumn
    Friend WithEvents NetSaleColumn As DataGridViewTextBoxColumn
    Friend WithEvents DebtPaymentsDataGridView As DataGridView
    Friend WithEvents DebtpaymentDateColumn As DataGridViewTextBoxColumn
    Friend WithEvents OrderNo As DataGridViewTextBoxColumn
    Friend WithEvents DebtorsIDNo As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn25 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn26 As DataGridViewTextBoxColumn
    Friend WithEvents datetime As DataGridViewTextBoxColumn
    Friend WithEvents ExpensesDataGridView As DataGridView
    Friend WithEvents DateexpColumn As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn27 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn28 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn29 As DataGridViewTextBoxColumn
End Class
