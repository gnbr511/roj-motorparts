﻿Imports MySql.Data.MySqlClient
Module Module1
    Public MysqlCon As MySqlConnection
    Public Query As String

    Public Command As MySqlCommand
    Public Reader As MySqlDataReader

    Public Host As String = "localhost"
    Public Database As String = "rojmotorparts"
    Public User As String = "root"
    Public Password As String = ""

    Public Sub OpenConnection()
        Try
            MysqlCon = New MySqlConnection
            MysqlCon.ConnectionString = "server = '" & Host & "';" _
                & "database = '" & Database & "';" _
                & "username = '" & User & "';" _
                & "password = '" & Password & "'"
            MysqlCon.Open()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Public Sub GinberBackUp()
        Try
            Dim backupstring As String = "/c c:\xampp\mysql\bin\mysqldump --user=root --password= --result-file=" & BackUpForm.savepath & " rojmotorparts"
            Process.Start("cmd", backupstring)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Public Sub GinberRestore()
        Try
            Dim restorestring As String = "/c c:\xampp\mysql\bin\mysql --user=root --password= rojmotorparts < " & RestoreDataForm.filepath
            Process.Start("cmd", restorestring)
            MessageBox.Show("Data loaded successfully! The system needs to be restarted!", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Public Sub GinberBackUpSpecificData()
        Try
            Dim backupspecString As String = "/c c:\xampp\mysql\bin\mysqldump --user=root --password=" _
            & " --skip-add-drop-table --no-create-info --skip-comments" _
            & " rojmotorparts paymentsmade todaysexpenses todayssale todayssoldproducts" _
            & " --where=" & """date>='" & BackUpForm.FromDate & "' and date<='" & BackUpForm.ToDate & "'"">" & BackUpForm.savepath
            Process.Start("cmd", backupspecString)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Module