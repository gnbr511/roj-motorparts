﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BackUpForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BackUpForm))
        Me.BrowseButton = New System.Windows.Forms.Button()
        Me.LocationTextBox = New System.Windows.Forms.TextBox()
        Me.SaveButton = New System.Windows.Forms.Button()
        Me.BackupCancelButton = New System.Windows.Forms.Button()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BackupFileNameTextBox = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'BrowseButton
        '
        Me.BrowseButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BrowseButton.Location = New System.Drawing.Point(4, 56)
        Me.BrowseButton.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.BrowseButton.Name = "BrowseButton"
        Me.BrowseButton.Size = New System.Drawing.Size(71, 27)
        Me.BrowseButton.TabIndex = 0
        Me.BrowseButton.Text = "&Browse"
        Me.BrowseButton.UseVisualStyleBackColor = True
        '
        'LocationTextBox
        '
        Me.LocationTextBox.Location = New System.Drawing.Point(79, 58)
        Me.LocationTextBox.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.LocationTextBox.Name = "LocationTextBox"
        Me.LocationTextBox.ReadOnly = True
        Me.LocationTextBox.Size = New System.Drawing.Size(246, 23)
        Me.LocationTextBox.TabIndex = 1
        '
        'SaveButton
        '
        Me.SaveButton.Enabled = False
        Me.SaveButton.Location = New System.Drawing.Point(168, 130)
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(75, 27)
        Me.SaveButton.TabIndex = 2
        Me.SaveButton.Text = "Save"
        Me.SaveButton.UseVisualStyleBackColor = True
        '
        'BackupCancelButton
        '
        Me.BackupCancelButton.Location = New System.Drawing.Point(249, 130)
        Me.BackupCancelButton.Name = "BackupCancelButton"
        Me.BackupCancelButton.Size = New System.Drawing.Size(75, 27)
        Me.BackupCancelButton.TabIndex = 3
        Me.BackupCancelButton.Text = "Cancel"
        Me.BackupCancelButton.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(55, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(236, 18)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Browse storage location for backup"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Location = New System.Drawing.Point(8, 88)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 16)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "File Name"
        '
        'BackupFileNameTextBox
        '
        Me.BackupFileNameTextBox.Location = New System.Drawing.Point(79, 85)
        Me.BackupFileNameTextBox.Name = "BackupFileNameTextBox"
        Me.BackupFileNameTextBox.ReadOnly = True
        Me.BackupFileNameTextBox.Size = New System.Drawing.Size(172, 23)
        Me.BackupFileNameTextBox.TabIndex = 6
        '
        'BackUpForm
        '
        Me.AcceptButton = Me.SaveButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.ROJBackground
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(336, 180)
        Me.Controls.Add(Me.BackupFileNameTextBox)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BackupCancelButton)
        Me.Controls.Add(Me.SaveButton)
        Me.Controls.Add(Me.LocationTextBox)
        Me.Controls.Add(Me.BrowseButton)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "BackUpForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Back Up"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BrowseButton As Button
    Friend WithEvents LocationTextBox As TextBox
    Friend WithEvents SaveButton As Button
    Friend WithEvents BackupCancelButton As Button
    Friend WithEvents FolderBrowserDialog1 As FolderBrowserDialog
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents BackupFileNameTextBox As TextBox
End Class
