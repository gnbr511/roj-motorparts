﻿Public Class BackUpForm
    Public savepath As String
    Friend Backupfilename, FromDate, ToDate As String
    Private Sub BackUpForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub BrowseButton_Click(sender As Object, e As EventArgs) Handles BrowseButton.Click
        If FolderBrowserDialog1.ShowDialog() = DialogResult.OK Then
            LocationTextBox.Text = FolderBrowserDialog1.SelectedPath
            With BackupFileNameTextBox
                .Text = "rojmotorparts" & DateTime.Now().ToString("yyyyMMddHHmmss")
                .SelectAll()
                .Focus()
            End With
            SaveButton.Enabled = True
        End If
    End Sub
    Private Sub SaveButton_Click(sender As Object, e As EventArgs) Handles SaveButton.Click
        If LocationTextBox.Text = "" Then
            MessageBox.Show("Must browse for location!", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            If BackupFileNameTextBox.Text = "" Then
                MessageBox.Show("Must enter backup file name!", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else
                savepath = """" & LocationTextBox.Text & "\" & BackupFileNameTextBox.Text & ".sql"""
                GinberBackUp()
                MessageBox.Show("Backup saved successfully!", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.Close()
            End If
        End If
    End Sub

    Private Sub BackUpForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        BackupFileNameTextBox.Clear()
        LocationTextBox.Clear()
        SaveButton.Enabled = False
        BrowseButton.Focus()
        savepath = ""
    End Sub

    Private Sub BackupCancelButton_Click(sender As Object, e As EventArgs) Handles BackupCancelButton.Click
        Me.Close()
    End Sub
End Class