﻿Imports MySql.Data.MySqlClient
Public Class UpdateInfoForm
    Private DebtorsPhotoPath As String
    Private Sub UpdateInfoForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            OpenConnection()
            MoreInfoListBox.Items.Clear()
            MoreInfoTextBox.Clear()
            Try
                Query = "SELECT debtorsphotopath FROM debtorsphotopath WHERE debtorsid = '" _
                        & CreditsInformationForm.SelectedDebtor & "'"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                While Reader.Read
                    DebtorsPhotoPath = Reader.GetString("debtorsphotopath")
                    If System.IO.File.Exists(DebtorsPhotoPath) Then
                        DebtorsPhotoPictureBox.Image = Image.FromFile(DebtorsPhotoPath)
                    Else
                        DebtorsPhotoPictureBox.Image = My.Resources.no_image_icon_13
                    End If
                End While
                Reader.Close()
                Try
                    Query = "SELECT debtor, contact FROM credits WHERE debtorsid = '" _
                        & CreditsInformationForm.SelectedDebtor & "'"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    While Reader.Read
                        CustomerFullNameTextBox.Text = Reader.GetString("debtor")
                        CustomerContactTextBox.Text = Reader.GetString("contact")
                    End While
                    Reader.Close()
                    Try
                        Query = "SELECT info FROM moreinfo WHERE debtorsid = '" _
                        & CreditsInformationForm.SelectedDebtor & "'"
                        Command = New MySqlCommand(Query, MysqlCon)
                        Reader = Command.ExecuteReader
                        While Reader.Read
                            MoreInfoListBox.Items.Add(Reader.GetString("info"))
                        End While
                        Reader.Close()
                    Catch ex As Exception
                        MessageBox.Show(ex.Message, "More Info Error")
                    End Try
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Name & Contact Error")
                End Try
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Image Error")
            End Try
            MysqlCon.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub AddInfoButton_Click(sender As Object, e As EventArgs) Handles AddInfoButton.Click
        If MoreInfoTextBox.Text = "" Then
        Else
            MoreInfoListBox.Items.Add(MoreInfoTextBox.Text)
            MoreInfoTextBox.Clear()
            MoreInfoTextBox.Focus()
        End If
    End Sub

    Private Sub RemoveInfoButton_Click(sender As Object, e As EventArgs) Handles RemoveInfoButton.Click
        If MoreInfoListBox.SelectedIndex < 0 Then
        Else
            MoreInfoListBox.Items.RemoveAt(MoreInfoListBox.SelectedIndex)
        End If
    End Sub

    Private Sub BrowseDebtorsPhotoButton_Click(sender As Object, e As EventArgs) Handles BrowseDebtorsPhotoButton.Click
        Try
            OpenFileDialog1.Filter = "JPEG|*.jpg|PNG|*.png|ALL FILES|*.*"
            If OpenFileDialog1.ShowDialog = DialogResult.OK Then
                DebtorsPhotoPictureBox.Image = Image.FromFile(OpenFileDialog1.FileName)
                DebtorsPhotoPath = OpenFileDialog1.FileName.Replace("\", "\\")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub UpdateButton_Click(sender As Object, e As EventArgs) Handles UpdateButton.Click
        Try
            OpenConnection()
            If CustomerFullNameTextBox.Text <> "" Then
                If CustomerContactTextBox.Text <> "" Then
                    Dim MessageResponse As DialogResult = MessageBox.Show("Update debtors information?", "Please Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                    If MessageResponse = DialogResult.Yes Then
                        Try
                            'update debtors photo
                            Query = "UPDATE debtorsphotopath SET debtorsphotopath = '" _
                                & DebtorsPhotoPath.Replace("\", "\\") & "' WHERE debtorsid = '" _
                                & CreditsInformationForm.SelectedDebtor & "'"
                            Command = New MySqlCommand(Query, MysqlCon)
                            Reader = Command.ExecuteReader
                            Reader.Close()
                            Try
                                'Update customer name and contact
                                Query = "UPDATE credits SET debtor = '" _
                                    & CustomerFullNameTextBox.Text _
                                    & "', contact = '" & CustomerContactTextBox.Text _
                                    & "' WHERE debtorsid = '" & CreditsInformationForm.SelectedDebtor & "'"
                                Command = New MySqlCommand(Query, MysqlCon)
                                Reader = Command.ExecuteReader
                                Reader.Close()
                                Try
                                    'Delete more info and insert new info
                                    'Delete
                                    Query = "DELETE FROM moreinfo WHERE debtorsid = '" _
                                             & CreditsInformationForm.SelectedDebtor & "'"
                                    Command = New MySqlCommand(Query, MysqlCon)
                                    Reader = Command.ExecuteReader
                                    Reader.Close()
                                    'Insert
                                    If MoreInfoListBox.Items.Count < 1 Then
                                    Else
                                        For Gin2 As Integer = 0 To MoreInfoListBox.Items.Count() - 1
                                            Try
                                                Query = "insert into moreinfo values('" _
                                                    & CreditsInformationForm.SelectedDebtor _
                                                    & "', '" & MoreInfoListBox.Items(Gin2).ToString() & "')"
                                                Command = New MySqlCommand(Query, MysqlCon)
                                                Reader = Command.ExecuteReader
                                                Reader.Close()
                                            Catch ex As Exception
                                                MessageBox.Show(ex.Message)
                                            End Try
                                        Next
                                    End If
                                    MessageBox.Show("Debtors Information Updated Successfully.", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                    Me.Close()
                                Catch ex As Exception
                                    MessageBox.Show(ex.Message, "Updating More Info Error")
                                End Try
                            Catch ex As Exception
                                MessageBox.Show(ex.Message, "Updating Name/Contact Error")
                            End Try
                        Catch ex As Exception
                            MessageBox.Show(ex.Message, "Updating Image Error")
                        End Try
                    End If
                Else
                    MessageBox.Show("Customer contact is required!", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Else
                MessageBox.Show("Customer full name is required!", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
            MysqlCon.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Database Connection Error")
        End Try
    End Sub
End Class