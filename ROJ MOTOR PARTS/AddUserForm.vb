﻿'Electronic Point of Sale, Inventory, and Records Management System for ROJ Motor Parts
'Programmed By: Ginber J. Candelario
Imports MySql.Data.MySqlClient
Public Class AddUserForm
    Private Sub AddUserForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        NameOfUserTextBox.Focus()
    End Sub
    Private Sub AddUserForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        NameOfUserTextBox.Clear()
        UsernameTextBox.Clear()
        PasswordTextBox.Clear()
    End Sub
    Private Sub AddUserForm_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        NameOfUserTextBox.Focus()
    End Sub

    Private Sub CancelButton_Click(sender As Object, e As EventArgs) Handles aCancelButton.Click
        Me.Close()
    End Sub

    Private Sub AddUserButton_Click(sender As Object, e As EventArgs) Handles AddUserButton.Click
        Try
            OpenConnection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Dim name, username, userpassword, type As String

        If NameOfUserTextBox.Text = "" Then
            MessageBox.Show("Name of the user should not be empty!", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information)
            NameOfUserTextBox.Focus()
        Else
            If UsernameTextBox.Text = "" Then
                MessageBox.Show("Username should not be empty!", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information)
                UsernameTextBox.Focus()
            Else
                If PasswordTextBox.Text = "" Then
                    MessageBox.Show("Password should not be empty!", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    PasswordTextBox.Focus()
                Else
                    If UserTypeComboBox.Text = "" Then
                        MessageBox.Show("Please select type of user!", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        UserTypeComboBox.Focus()
                    Else
                        name = NameOfUserTextBox.Text
                        username = UsernameTextBox.Text
                        userpassword = PasswordTextBox.Text
                        type = UserTypeComboBox.Text
                        Dim usernameExist As Boolean = False
                        Dim userno As Integer
                        Try
                            Query = "select * from users"
                            Command = New MySqlCommand(Query, MysqlCon)
                            Reader = Command.ExecuteReader
                            While Reader.Read
                                Dim b = Reader.GetString("username")
                                userno = Reader.GetInt32("no")
                                If b = username Then
                                    usernameExist = True
                                End If
                            End While
                            Reader.Close()
                        Catch ex As Exception
                            MessageBox.Show(ex.Message)
                        End Try
                        If usernameExist = True Then
                            MessageBox.Show("The username you entered is already taken!", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            UsernameTextBox.Focus()
                            UsernameTextBox.SelectAll()
                        Else
                            Dim mesres As DialogResult
                            mesres = MessageBox.Show("Add user?", "Please Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                            If mesres = DialogResult.Yes Then
                                Try
                                    Query = "insert into users values( " & userno + 1 & ", '" & name & "', '" _
                                    & username & "', '" & userpassword & "', '" & type & "')"
                                    Command = New MySqlCommand(Query, MysqlCon)
                                    Reader = Command.ExecuteReader
                                    Reader.Close()
                                    UserSettingsForm.UsersDataGridView.Rows.Clear()
                                    UserSettingsForm.UserSettingsForm_Load(sender, e)
                                    UserSettingsForm.UserNumber = UserSettingsForm.UsersDataGridView.CurrentRow.Cells(1).Value
                                    UserSettingsForm.NameSelecteduser = UserSettingsForm.UsersDataGridView.CurrentRow.Cells(2).Value
                                    UserSettingsForm.SelectedUser = UserSettingsForm.UsersDataGridView.CurrentRow.Cells(3).Value
                                    UserSettingsForm.UserType = UserSettingsForm.UsersDataGridView.CurrentRow.Cells(5).Value
                                    NameOfUserTextBox.Clear()
                                    UsernameTextBox.Clear()
                                    PasswordTextBox.Clear()
                                    MessageBox.Show("New user has been successfully added!", "", MessageBoxButtons.OK)
                                Catch ex As Exception
                                    MessageBox.Show(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If
        MysqlCon.Close()
    End Sub
End Class