﻿Imports MySql.Data.MySqlClient
Public Class EditProductForm
    Private ProductPhotoPath As String
    Private ProductPhotoPath2 As String
    Private ProductPhotoPath3 As String
    Private Sub CancelEditProductButton_Click(sender As Object, e As EventArgs) Handles CancelEditProductButton.Click
        Me.Close()
    End Sub

    Private Sub UpdateProductButton_Click(sender As Object, e As EventArgs) Handles UpdateProductButton.Click
        Try
            OpenConnection()
        Catch ex As Exception
        End Try

        Dim MessageResult As String = MessageBox.Show("Update this product?", "Please Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If MessageResult = DialogResult.Yes Then
            Dim ProdName, MeasureUnit, ProductBrand, ProductDescription As String
            Dim AvailStock, MinStockLimit, PurchaseCost, SalePrice, supretprice As Decimal

            If ProductNameTextBox.Text <> "" Then
                Try
                    AvailStock = Decimal.Parse(AvailStockTextBox.Text)
                    Try
                        MinStockLimit = Decimal.Parse(MinStockLimitTextBox.Text)
                        If MeasuringUnitComboBox.Text <> "" Then
                            Try
                                PurchaseCost = Decimal.Parse(PurchaseCostTextBox.Text)
                                Try
                                    SalePrice = Decimal.Parse(SalePriceTextBox.Text)

                                    ProdName = ProductNameTextBox.Text()
                                    MeasureUnit = MeasuringUnitComboBox.Text()
                                    ProductBrand = ProductBrandTextBox.Text()
                                    ProductDescription = ProductDescriptionTextBox.Text()
                                    If SupRetPriceTextBox.Text = "" Or IsNumeric(SupRetPriceTextBox.Text) = False Then
                                        supretprice = 0
                                    Else
                                        supretprice = SupRetPriceTextBox.Text()
                                    End If

                                    If ProductPhotoPath2 = "" Then
                                        ProductPhotoPath3 = ProductPhotoPath.Replace("\", "\\")
                                    Else
                                        ProductPhotoPath3 = ProductPhotoPath2
                                    End If

                                    'UPDATE THE PRODUCT FROM THE DATABASE
                                    Try
                                        OpenConnection()
                                        Query = "update products set productname = '" & ProdName & "', productbrand = '" & ProductBrand & "', " _
                                            & "productdescription = '" & ProductDescription & "', availablestock = " & AvailStock & ", " _
                                            & "minstocklimit = " & MinStockLimit & ", measuringunit = '" & MeasureUnit & "', supretprice = " & supretprice & ", " _
                                            & "purchasecost = " & PurchaseCost & ", saleprice = " & SalePrice & ", " _
                                            & "photopath = '" & ProductPhotoPath3 & "' where no = " & InventoryForm.ProductNo
                                        Command = New MySqlCommand(Query, MysqlCon)
                                        Reader = Command.ExecuteReader
                                        Reader.Close()
                                        MessageBox.Show("Product updated successfully.", "Product Updated", MessageBoxButtons.OK, MessageBoxIcon.Information)

                                        'DISPLAY UPDATED PRODUCT ON INVENTORY
                                        Try
                                            InventoryForm.ProductsDataGridView.Rows.Clear()

                                            Query = "select * from products order by productname"
                                            Command = New MySqlCommand(Query, MysqlCon)
                                            Reader = Command.ExecuteReader

                                            While Reader.Read
                                                Dim a = Reader.GetString("productname")
                                                Dim i = Reader.GetString("productbrand")
                                                Dim j = Reader.GetString("productdescription")
                                                Dim b = Reader.GetDecimal("availablestock")
                                                Dim c = Reader.GetDecimal("minstocklimit")
                                                Dim d = Reader.GetString("measuringunit")
                                                Dim k = Reader.GetDecimal("supretprice")
                                                Dim f = Reader.GetDecimal("purchasecost")
                                                Dim g = Reader.GetDecimal("saleprice")
                                                Dim h = Reader.GetInt32("no")
                                                If k = 0 Then
                                                    InventoryForm.ProductsDataGridView.Rows.Add(h, a, i, j, b, c, d, "", f, g)
                                                Else
                                                    InventoryForm.ProductsDataGridView.Rows.Add(h, a, i, j, b, c, d, k, f, g)
                                                End If
                                            End While
                                            Reader.Close()
                                            ProductPhotoPath2 = ""
                                            Me.Close()
                                        Catch sqlex As MySqlException
                                            MessageBox.Show(sqlex.Message)
                                        End Try

                                    Catch mysqlex As MySqlException
                                        MessageBox.Show(mysqlex.Message)
                                    End Try

                                Catch SalePriceex As Exception
                                    MessageBox.Show("Sale Price must be numeric!", "Invalid Input",
                                                    MessageBoxButtons.OK, MessageBoxIcon.Information)
                                    With SalePriceTextBox
                                        .Focus()
                                        .SelectAll()
                                    End With
                                End Try
                            Catch PurchaseCostex As Exception
                                MessageBox.Show("Purchase Cost must be numeric!", "Ivalid Input",
                                                MessageBoxButtons.OK, MessageBoxIcon.Information)
                                With PurchaseCostTextBox
                                    .Focus()
                                    .SelectAll()
                                End With
                            End Try
                        Else
                            MessageBox.Show("Please select or input measuring unit!", "Invalid Input",
                                            MessageBoxButtons.OK, MessageBoxIcon.Information)
                            With MeasuringUnitComboBox
                                .Focus()
                            End With
                        End If
                    Catch MinStockLimitex As Exception
                        MessageBox.Show("Minimum stock limit must be numeric!", "Invalid Entry",
                                        MessageBoxButtons.OK, MessageBoxIcon.Information)
                        With MinStockLimitTextBox
                            .Focus()
                            .SelectAll()
                        End With
                    End Try
                Catch AvailStockex As FormatException
                    MessageBox.Show("Available Stock must be numeric!", "Invalid Entry",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information)
                    With AvailStockTextBox
                        .Focus()
                        .SelectAll()
                    End With
                End Try
            Else
                MessageBox.Show("Product name should not be empty!", "Invalid Entry",
                                MessageBoxButtons.OK, MessageBoxIcon.Information)
                ProductNameTextBox.Focus()
            End If
        Else
        End If
        MysqlCon.Close()
        For i As Integer = 0 To InventoryForm.ProductsDataGridView.Rows.Count - 1
            If InventoryForm.ProductsDataGridView.Rows(i).Cells(4).Value <= InventoryForm.ProductsDataGridView.Rows(i).Cells(5).Value Then
                InventoryForm.ProductsDataGridView.Rows(i).Cells(4).Style.BackColor = Color.Red
            End If
        Next
    End Sub

    Private Sub EditProductForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            MysqlCon = New MySqlConnection
            MysqlCon.ConnectionString = "server = '" & Host & "'; " _
                & "database = '" & Database & "'; " _
                & "username = '" & User & "'; " _
                & "password = '" & Password & "'"
            MysqlCon.Open()
        Catch ex As Exception
        End Try
        Try
            Query = "select * from products where no = " & InventoryForm.ProductNo
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim a = Reader.GetInt32("no")
                Dim b = Reader.GetString("productname")
                Dim c = Reader.GetString("productbrand")
                Dim d = Reader.GetString("productdescription")
                Dim f = Reader.GetDecimal("availablestock")
                Dim g = Reader.GetDecimal("minstocklimit")
                Dim h = Reader.GetString("measuringunit")
                Dim l = Reader.GetDecimal("supretprice")
                Dim i = Reader.GetDecimal("purchasecost")
                Dim j = Reader.GetDecimal("saleprice")
                Dim k = Reader.GetString("photopath")
                ProductPhotoPath = k
                If k = "" Then
                    ProductPhotoPictureBox.Image = My.Resources.no_image_icon_13
                ElseIf System.IO.File.Exists(k) Then
                    ProductPhotoPictureBox.Image = Image.FromFile(k)
                Else
                End If
                ProductNoTextBox.Text = a
                ProductNameTextBox.Text = b
                ProductBrandTextBox.Text = c
                ProductDescriptionTextBox.Text = d
                MeasuringUnitComboBox.Text = h
                If MeasuringUnitComboBox.Text.ToLower = "pcs" Or MeasuringUnitComboBox.Text.ToLower = "pack" Or MeasuringUnitComboBox.Text.ToLower = "pair" Then
                    AvailStockTextBox.Text = CInt(f)
                    MinStockLimitTextBox.Text = CInt(g)
                Else
                    AvailStockTextBox.Text = f
                    MinStockLimitTextBox.Text = g
                End If
                If l = 0 Then
                    SupRetPriceTextBox.Text = ""
                Else
                    SupRetPriceTextBox.Text = l
                End If
                PurchaseCostTextBox.Text = i
                SalePriceTextBox.Text = j
            End While
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        MysqlCon.Close()
    End Sub

    Private Sub BrowsePicButton_Click(sender As Object, e As EventArgs) Handles BrowsePicButton.Click
        OpenFileDialog1.Filter = "JPEG|*.jpg|PNG|*.png|ALL FILES|*.*"
        If OpenFileDialog1.ShowDialog = DialogResult.OK Then
            ProductPhotoPictureBox.Image = Image.FromFile(OpenFileDialog1.FileName)
            ProductPhotoPath2 = OpenFileDialog1.FileName.Replace("\", "\\")
        End If
    End Sub

    Private Sub AvailStockTextBox_TextChanged(sender As Object, e As EventArgs) Handles AvailStockTextBox.TextChanged
        Try
            If AvailStockTextBox.Text = "" Then
            ElseIf MeasuringUnitComboBox.Text.ToLower = "pcs" Or MeasuringUnitComboBox.Text.ToLower = "pack" Or MeasuringUnitComboBox.Text.ToLower = "pair" Then
                Integer.Parse(AvailStockTextBox.Text)
            End If
            If IsNumeric(AvailStockTextBox.Text) = True Or AvailStockTextBox.Text = "." Then

            Else
                AvailStockTextBox.Clear()
                AvailStockTextBox.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show("The measuring unit is " & MeasuringUnitComboBox.Text & ", your available stock should be a whole number!", "Check Inputs")
            AvailStockTextBox.Clear()
            AvailStockTextBox.Focus()
        End Try
    End Sub

    Private Sub EditProductForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        ProductPhotoPath = ""
        ProductNameTextBox.Clear()
        MeasuringUnitComboBox.SelectedIndex = -1
        MeasuringUnitComboBox.Text = ""
        ProductBrandTextBox.Clear()
        ProductDescriptionTextBox.Clear()
        AvailStockTextBox.Clear()
        MinStockLimitTextBox.Clear()
        SupRetPriceTextBox.Clear()
        PurchaseCostTextBox.Clear()
        SalePriceTextBox.Clear()
        ProductPhotoPictureBox.Image = My.Resources.uploadphoto
        ProductNameTextBox.Focus()
    End Sub

    Private Sub MinStockLimitTextBox_TextChanged(sender As Object, e As EventArgs) Handles MinStockLimitTextBox.TextChanged
        Try
            If MinStockLimitTextBox.Text = "" Then
            ElseIf MeasuringUnitComboBox.Text.ToLower = "pcs" Or MeasuringUnitComboBox.Text.ToLower = "pack" Or MeasuringUnitComboBox.Text.ToLower = "pair" Then
                Integer.Parse(MinStockLimitTextBox.Text)
            End If
            If IsNumeric(MinStockLimitTextBox.Text) = True Or MinStockLimitTextBox.Text = "." Then

            Else
                MinStockLimitTextBox.Clear()
                MinStockLimitTextBox.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show("The measuring unit is " & MeasuringUnitComboBox.Text & ", your available stock should be a whole number!", "Check Inputs")
            MinStockLimitTextBox.Clear()
            MinStockLimitTextBox.Focus()
        End Try
    End Sub

    Private Sub SupRetPriceTextBox_TextChanged(sender As Object, e As EventArgs) Handles SupRetPriceTextBox.TextChanged
        Try
            If SupRetPriceTextBox.Text = "" Then
            Else
                If IsNumeric(SupRetPriceTextBox.Text) = False Then
                    If SupRetPriceTextBox.Text <> "." Then
                        SupRetPriceTextBox.Clear()
                        SupRetPriceTextBox.Focus()
                    End If
                ElseIf SupRetPriceTextBox.Text <= 0 Then
                    SupRetPriceTextBox.Clear()
                    SupRetPriceTextBox.Focus()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub PurchaseCostTextBox_TextChanged(sender As Object, e As EventArgs) Handles PurchaseCostTextBox.TextChanged
        Try
            If PurchaseCostTextBox.Text = "" Then
            Else
                If IsNumeric(PurchaseCostTextBox.Text) = False Then
                    If PurchaseCostTextBox.Text <> "." Then
                        PurchaseCostTextBox.Clear()
                        PurchaseCostTextBox.Focus()
                    End If
                ElseIf PurchaseCostTextBox.Text <= 0 Then
                    PurchaseCostTextBox.Clear()
                    PurchaseCostTextBox.Focus()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SalePriceTextBox_TextChanged(sender As Object, e As EventArgs) Handles SalePriceTextBox.TextChanged
        Try
            If SalePriceTextBox.Text = "" Then
            Else
                If IsNumeric(SalePriceTextBox.Text) = False Then
                    If SalePriceTextBox.Text <> "." Then
                        SalePriceTextBox.Clear()
                        SalePriceTextBox.Focus()
                    End If
                ElseIf SalePriceTextBox.Text <= 0 Then
                    SalePriceTextBox.Clear()
                    SalePriceTextBox.Focus()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub MeasuringUnitComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles MeasuringUnitComboBox.SelectedIndexChanged
        AvailStockTextBox_TextChanged(sender, e)
        MinStockLimitTextBox_TextChanged(sender, e)
    End Sub
End Class