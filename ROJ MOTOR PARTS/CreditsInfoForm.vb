﻿Imports MySql.Data.MySqlClient
Public Class CreditsInfoForm
    Friend SelectedDebtor As String
    Friend FromStore As Boolean
    Private Sub CreditsInfoForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            OpenConnection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Dim CustomerFullName, Contact, DebtorsPhotoPath As String
        Dim Payment, AmountPaid, RemainingPayment, Discount As Decimal
        Dim CreditDatetime As DateTime
        Try
            Query = "select * from credits 
                    inner join (select sum(discount) as totaldiscount, datetime as datetime2 from loanedproducts group by datetime) A 
                    on credits.datetime = A.datetime2 where datetime = '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                CustomerFullName = Reader.GetString("customerfullname")
                Contact = Reader.GetString("contact")
                Payment = Reader.GetDecimal("payment")
                Discount = Reader.GetDecimal("totaldiscount")
                AmountPaid = Reader.GetDecimal("amountpaid")
                RemainingPayment = Payment - AmountPaid
                CreditDatetime = Reader.GetDateTime("datetime")
            End While
            Reader.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        FullNameLabel.Text = CustomerFullName
        ContactLabel.Text = Contact
        LoanDateLabel.Text = CreditDatetime.ToString("dd-MM-yyyy")
        TotalPaymentTextBox.Text = (Discount + Payment).ToString("n2")
        DiscountTextBox.Text = Discount.ToString("n2")
        TotalPaidTextBox.Text = AmountPaid.ToString("n2")
        RemainingPaymentTextBox.Text = RemainingPayment.ToString("n2")
        Try
            Query = "select debtorsphotopath from debtorsphotopath where datetime = '" _
                & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                DebtorsPhotoPath = Reader.GetString("debtorsphotopath")
                If System.IO.File.Exists(DebtorsPhotoPath) Then
                    DebtorsPhotoPictureBox.Image = Image.FromFile(DebtorsPhotoPath)
                Else
                    DebtorsPhotoPictureBox.Image = My.Resources.no_image_icon_13
                End If
            End While
            Reader.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Try
            MoreInfoListBox.Items.Clear()
            Query = "select * from moreinfo where datetime = '" _
                & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim a = Reader.GetString("info")
                MoreInfoListBox.Items.Add(a)
            End While
            Reader.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Dim num As Integer = 0
        Try
            LoanedProductsDataGridView.Rows.Clear()
            Query = "select * from loanedproducts where datetime = '" _
                & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                num += 1
                Dim a = Reader.GetString("productname")
                Dim b = Reader.GetDecimal("saleprice")
                Dim c = Reader.GetDecimal("quantity")
                Dim d = Reader.GetDecimal("totalprice")
                Dim f = Reader.GetDecimal("discount")
                Dim g = Reader.GetDecimal("discountedprice")
                Dim h = Reader.GetString("measuringunit")
                Dim i = Reader.GetString("productbrand")
                Dim j = Reader.GetString("productdescription")
                Dim k = Reader.GetInt32("no")
                LoanedProductsDataGridView.Rows.Add(num, a, b, c, d, f, g, h, i, j, k)
                LoanedProductsDataGridView.Rows(num - 1).Cells(6).Style.BackColor = Color.Aquamarine
            End While
            Reader.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        LoanedProductsDataGridView.ClearSelection()
        Try
            PaymentHistoryDataGridView.Rows.Clear()
            Query = "select * from paymentsmade where datetime = '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "' order by date desc"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim a = Reader.GetDateTime("date").ToString("dd-MM-yyyy")
                Dim b = Reader.GetDecimal("paidamount")
                Dim c = Reader.GetDateTime("datetime").ToString("yyyy-MM-dd HH:mm:ss")
                PaymentHistoryDataGridView.Rows.Add(a, b, c)
            End While
            Reader.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        LoanedProductsDataGridView.ClearSelection()
        PaymentHistoryDataGridView.ClearSelection()
        MysqlCon.Close()
    End Sub

    Private Sub CreditsInfoForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        FullNameLabel.ResetText()
        ContactLabel.ResetText()
        MoreInfoListBox.Items.Clear()
        TotalPaymentTextBox.Clear()
        TotalPaidTextBox.Clear()
        RemainingPaymentTextBox.Clear()
        LoanedProductsDataGridView.Rows.Clear()
        LoanedProductsDataGridView.Visible = True
        PaymentHistoryDataGridView.Visible = False
        PaymentHistoryDataGridView.Rows.Clear()
        StoreForm.CreditsDataGridView.ClearSelection()
        StoreForm.PayCreditButton.Enabled = False
        StoreForm.MoreInfoButton.Enabled = False
        StoreForm.SearchDebtorTextBox.Clear()
        CreditsRecordsForm.MoreInfoButton.Enabled = False
        CreditsRecordsForm.CreditsDataGridView.ClearSelection()
        DebtorsPhotoPictureBox.Image = My.Resources.no_image_icon_13
    End Sub

    Private Sub CreditsInfoForm_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        LoanedProductsDataGridView.ClearSelection()
        PaymentHistoryDataGridView.ClearSelection()
        LoanedProdContextMenuStrip1.Enabled = False
        LoanedProductsLabel.ForeColor = Color.Maroon
        PaymentHistoryLabel.ForeColor = Color.Black
        FullNameLabel.ResetText()
        ContactLabel.ResetText()
        MoreInfoListBox.Items.Clear()
        TotalPaymentTextBox.Clear()
        TotalPaidTextBox.Clear()
        RemainingPaymentTextBox.Clear()
        LoanedProductsDataGridView.Rows.Clear()
        LoanedProductsDataGridView.Visible = True
        PaymentHistoryDataGridView.Visible = False
        PaymentHistoryDataGridView.Rows.Clear()
        StoreForm.CreditsDataGridView.ClearSelection()
        StoreForm.PayCreditButton.Enabled = False
        StoreForm.MoreInfoButton.Enabled = False
        CreditsRecordsForm.MoreInfoButton.Enabled = False
        CreditsRecordsForm.CreditsDataGridView.ClearSelection()
        DebtorsPhotoPictureBox.Image = My.Resources.no_image_icon_13
        CreditsInfoForm_Load(sender, e)
    End Sub

    Private Sub PaymentHistoryLabel_Click(sender As Object, e As EventArgs) Handles PaymentHistoryLabel.Click
        LoanedProductsDataGridView.Visible = False
        PaymentHistoryDataGridView.Visible = True
        LoanedProductsLabel.ForeColor = Color.Black
        PaymentHistoryLabel.ForeColor = Color.Maroon
    End Sub

    Private Sub LoanedProductsLabel_Click(sender As Object, e As EventArgs) Handles LoanedProductsLabel.Click
        LoanedProductsDataGridView.Visible = True
        PaymentHistoryDataGridView.Visible = False
        LoanedProductsLabel.ForeColor = Color.Maroon
        PaymentHistoryLabel.ForeColor = Color.Black
    End Sub

    Private Sub PaymentHisToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles PaymentHisToolStripMenuItem2.Click
        Dim paydate As Date = PaymentHistoryDataGridView.CurrentRow.Cells(0).Value
        Dim paidamount As Decimal = PaymentHistoryDataGridView.CurrentRow.Cells(1).Value
        Dim customerfullname As String
        Dim count As Integer
        Dim mesres As DialogResult = MessageBox.Show("Click OK to continue deleting this record.", "Confirm Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
        If mesres = DialogResult.OK Then
            Try
                OpenConnection()
                Query = "select * from paymentsmade where paidamount = " & paidamount & " and date = '" & paydate.ToString("yyyy-MM-dd") & "' and datetime = '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                While Reader.Read
                    customerfullname = Reader.GetString("customerfullname")
                    count += 1
                End While
                Reader.Close()
                Try
                    Query = "delete from paymentsmade where customerfullname = '" & customerfullname _
                        & "' and date = '" & paydate.ToString("yyyy-MM-dd") & "' and paidamount = " & paidamount & " and datetime = '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    Reader.Close()
                    Try
                        Dim i As Integer = 1
                        While count > i
                            Query = "insert into paymentsmade values('" & customerfullname & "', " & paidamount _
                                & ", '" & paydate.ToString("yyyy-MM-dd") & "', '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "')"
                            Command = New MySqlCommand(Query, MysqlCon)
                            Reader = Command.ExecuteReader
                            Reader.Close()
                            i += 1
                        End While
                        Try
                            Query = "update credits set amountpaid = amountpaid - " & paidamount & "where datetime = '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                            Command = New MySqlCommand(Query, MysqlCon)
                            Reader = Command.ExecuteReader
                            Reader.Close()
                            CreditsInfoForm_Load(sender, e)
                            CreditsRecordsForm.CreditsRecordsForm_Load(sender, e)
                            StoreForm.StoreForm_Load(sender, e)
                            LoanedProductsDataGridView.Visible = False
                            PaymentHistoryDataGridView.Visible = True
                            LoanedProductsLabel.ForeColor = Color.Black
                            PaymentHistoryLabel.ForeColor = Color.Maroon
                        Catch ex As Exception
                            MessageBox.Show(ex.Message)
                        End Try
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
        MysqlCon.Close()
    End Sub
    'loanedprod delete
    Private No As Integer
    Private Sub DELETEToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DELETEToolStripMenuItem.Click
        Dim mesres As DialogResult = MessageBox.Show("Click OK to continue deleting this record.", "Confirm Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
        Dim totalprice As Decimal
        Dim count As Integer
        If mesres = DialogResult.OK Then
            Try
                Try
                    Try
                        OpenConnection()
                        Query = "select * from loanedproducts where no = " & No & " and datetime = '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                        Command = New MySqlCommand(Query, MysqlCon)
                        Reader = Command.ExecuteReader
                        While Reader.Read
                            totalprice = Reader.GetDecimal("discountedprice")
                        End While
                        Reader.Close()
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try
                    Query = "update credits set payment = payment -" & totalprice & "where datetime = '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    Reader.Close()
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
                Query = "delete from loanedproducts where no = " & No & " and datetime = '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                Reader.Close()
                Try
                    Query = "select count(*) count from loanedproducts where datetime = '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    While Reader.Read
                        count = Reader.GetInt32("count")
                    End While
                    Reader.Close()
                    If count < 1 Then
                        Query = "delete from credits where datetime = '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                        Command = New MySqlCommand(Query, MysqlCon)
                        Reader = Command.ExecuteReader
                        Reader.Close()
                        Query = "delete from moreinfo where datetime = '" & SelectedDebtor.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                        Command = New MySqlCommand(Query, MysqlCon)
                        Reader = Command.ExecuteReader
                        Reader.Close()
                        Me.Close()
                    End If
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
                MessageBox.Show("Record deleted Successfully!")
                CreditsInfoForm_Load(sender, e)
                CreditsRecordsForm.CreditsRecordsForm_Load(sender, e)
                RecordsForm.RefreshButton_Click(sender, e)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
        MysqlCon.Close()
    End Sub

    Private Sub LoanedProductsDataGridView_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles LoanedProductsDataGridView.CellClick
        No = LoanedProductsDataGridView.CurrentRow.Cells(10).Value
        If FromStore = False Then
            LoanedProdContextMenuStrip1.Enabled = True
            paymenthistoryContextMenuStrip2.Enabled = True
        End If
    End Sub

    Private Sub UpdateinfoButton_Click(sender As Object, e As EventArgs) Handles UpdateinfoButton.Click
        UpdateInfoForm.ShowDialog()
    End Sub
End Class