﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RestoreDataForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RestoreDataForm))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BackupCancelButton = New System.Windows.Forms.Button()
        Me.ContinueButton = New System.Windows.Forms.Button()
        Me.FilePathTextBox = New System.Windows.Forms.TextBox()
        Me.BrowseButton = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(72, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(201, 18)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Select a data file to be loaded"
        '
        'BackupCancelButton
        '
        Me.BackupCancelButton.Location = New System.Drawing.Point(249, 131)
        Me.BackupCancelButton.Name = "BackupCancelButton"
        Me.BackupCancelButton.Size = New System.Drawing.Size(75, 27)
        Me.BackupCancelButton.TabIndex = 10
        Me.BackupCancelButton.Text = "Cancel"
        Me.BackupCancelButton.UseVisualStyleBackColor = True
        '
        'ContinueButton
        '
        Me.ContinueButton.Enabled = False
        Me.ContinueButton.Location = New System.Drawing.Point(166, 131)
        Me.ContinueButton.Name = "ContinueButton"
        Me.ContinueButton.Size = New System.Drawing.Size(75, 27)
        Me.ContinueButton.TabIndex = 9
        Me.ContinueButton.Text = "&Continue"
        Me.ContinueButton.UseVisualStyleBackColor = True
        '
        'FilePathTextBox
        '
        Me.FilePathTextBox.Location = New System.Drawing.Point(11, 64)
        Me.FilePathTextBox.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.FilePathTextBox.Name = "FilePathTextBox"
        Me.FilePathTextBox.ReadOnly = True
        Me.FilePathTextBox.Size = New System.Drawing.Size(314, 23)
        Me.FilePathTextBox.TabIndex = 8
        '
        'BrowseButton
        '
        Me.BrowseButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BrowseButton.Location = New System.Drawing.Point(10, 91)
        Me.BrowseButton.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.BrowseButton.Name = "BrowseButton"
        Me.BrowseButton.Size = New System.Drawing.Size(76, 27)
        Me.BrowseButton.TabIndex = 7
        Me.BrowseButton.Text = "&Select"
        Me.BrowseButton.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'RestoreDataForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.ROJBackground
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(337, 180)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BackupCancelButton)
        Me.Controls.Add(Me.ContinueButton)
        Me.Controls.Add(Me.FilePathTextBox)
        Me.Controls.Add(Me.BrowseButton)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "RestoreDataForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Restore/Load Data"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As Label
    Friend WithEvents BackupCancelButton As Button
    Friend WithEvents ContinueButton As Button
    Friend WithEvents FilePathTextBox As TextBox
    Friend WithEvents BrowseButton As Button
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
End Class
