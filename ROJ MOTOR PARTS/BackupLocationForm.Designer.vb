﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class BackupLocationForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BackupLocationForm))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.AutoBackupLocationCancelButton = New System.Windows.Forms.Button()
        Me.SaveButton = New System.Windows.Forms.Button()
        Me.AutoBackupLocationTextBox = New System.Windows.Forms.TextBox()
        Me.BrowseButton = New System.Windows.Forms.Button()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.DisableAutoBackupButton = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(37, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(305, 18)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Browse storage location for automatic backup"
        '
        'AutoBackupLocationCancelButton
        '
        Me.AutoBackupLocationCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.AutoBackupLocationCancelButton.Location = New System.Drawing.Point(272, 125)
        Me.AutoBackupLocationCancelButton.Name = "AutoBackupLocationCancelButton"
        Me.AutoBackupLocationCancelButton.Size = New System.Drawing.Size(75, 27)
        Me.AutoBackupLocationCancelButton.TabIndex = 8
        Me.AutoBackupLocationCancelButton.Text = "Cancel"
        Me.AutoBackupLocationCancelButton.UseVisualStyleBackColor = True
        '
        'SaveButton
        '
        Me.SaveButton.Enabled = False
        Me.SaveButton.Location = New System.Drawing.Point(191, 125)
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(75, 27)
        Me.SaveButton.TabIndex = 7
        Me.SaveButton.Text = "Save"
        Me.SaveButton.UseVisualStyleBackColor = True
        '
        'AutoBackupLocationTextBox
        '
        Me.AutoBackupLocationTextBox.Location = New System.Drawing.Point(100, 73)
        Me.AutoBackupLocationTextBox.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.AutoBackupLocationTextBox.Name = "AutoBackupLocationTextBox"
        Me.AutoBackupLocationTextBox.ReadOnly = True
        Me.AutoBackupLocationTextBox.Size = New System.Drawing.Size(246, 21)
        Me.AutoBackupLocationTextBox.TabIndex = 6
        '
        'BrowseButton
        '
        Me.BrowseButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BrowseButton.Location = New System.Drawing.Point(25, 71)
        Me.BrowseButton.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.BrowseButton.Name = "BrowseButton"
        Me.BrowseButton.Size = New System.Drawing.Size(71, 27)
        Me.BrowseButton.TabIndex = 5
        Me.BrowseButton.Text = "&Browse"
        Me.BrowseButton.UseVisualStyleBackColor = True
        '
        'DisableAutoBackupButton
        '
        Me.DisableAutoBackupButton.Enabled = False
        Me.DisableAutoBackupButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.DisableAutoBackupButton.Location = New System.Drawing.Point(25, 104)
        Me.DisableAutoBackupButton.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.DisableAutoBackupButton.Name = "DisableAutoBackupButton"
        Me.DisableAutoBackupButton.Size = New System.Drawing.Size(71, 27)
        Me.DisableAutoBackupButton.TabIndex = 10
        Me.DisableAutoBackupButton.Text = "Disable"
        Me.DisableAutoBackupButton.UseVisualStyleBackColor = True
        '
        'BackupLocationForm
        '
        Me.AcceptButton = Me.BrowseButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.ROJBackground
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.CancelButton = Me.AutoBackupLocationCancelButton
        Me.ClientSize = New System.Drawing.Size(381, 169)
        Me.Controls.Add(Me.DisableAutoBackupButton)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.AutoBackupLocationCancelButton)
        Me.Controls.Add(Me.SaveButton)
        Me.Controls.Add(Me.AutoBackupLocationTextBox)
        Me.Controls.Add(Me.BrowseButton)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "BackupLocationForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Set Auto Backup Location"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents AutoBackupLocationCancelButton As Button
    Friend WithEvents SaveButton As Button
    Friend WithEvents AutoBackupLocationTextBox As TextBox
    Friend WithEvents BrowseButton As Button
    Friend WithEvents FolderBrowserDialog1 As FolderBrowserDialog
    Friend WithEvents DisableAutoBackupButton As Button
End Class
