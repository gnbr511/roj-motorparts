﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddSalesForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AddSalesForm))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CustomerTextBox = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ProductNameTextBox = New System.Windows.Forms.TextBox()
        Me.BrandTextBox = New System.Windows.Forms.TextBox()
        Me.DescriptionTextBox = New System.Windows.Forms.TextBox()
        Me.SelectProductButton = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.QuantityTextBox = New System.Windows.Forms.TextBox()
        Me.AddToOrdersButton = New System.Windows.Forms.Button()
        Me.HiddenProductNoTextBox = New System.Windows.Forms.TextBox()
        Me.TotalPriceTextBox = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.PriceTextBox = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.DiscountTextBox = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.DiscountedPriceTextBox = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.MeasuringUnitTextBox = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(33, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 18)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Customer"
        '
        'CustomerTextBox
        '
        Me.CustomerTextBox.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CustomerTextBox.Location = New System.Drawing.Point(163, 18)
        Me.CustomerTextBox.Name = "CustomerTextBox"
        Me.CustomerTextBox.Size = New System.Drawing.Size(204, 26)
        Me.CustomerTextBox.TabIndex = 0
        Me.CustomerTextBox.Text = "Walk-in"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(33, 58)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(104, 18)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Product Name"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(33, 90)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 18)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Brand"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(33, 122)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(83, 18)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Description"
        '
        'ProductNameTextBox
        '
        Me.ProductNameTextBox.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ProductNameTextBox.Location = New System.Drawing.Point(163, 55)
        Me.ProductNameTextBox.Name = "ProductNameTextBox"
        Me.ProductNameTextBox.ReadOnly = True
        Me.ProductNameTextBox.Size = New System.Drawing.Size(204, 26)
        Me.ProductNameTextBox.TabIndex = 1
        '
        'BrandTextBox
        '
        Me.BrandTextBox.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BrandTextBox.Location = New System.Drawing.Point(163, 87)
        Me.BrandTextBox.Name = "BrandTextBox"
        Me.BrandTextBox.ReadOnly = True
        Me.BrandTextBox.Size = New System.Drawing.Size(204, 26)
        Me.BrandTextBox.TabIndex = 6
        Me.BrandTextBox.TabStop = False
        '
        'DescriptionTextBox
        '
        Me.DescriptionTextBox.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescriptionTextBox.Location = New System.Drawing.Point(163, 119)
        Me.DescriptionTextBox.Name = "DescriptionTextBox"
        Me.DescriptionTextBox.ReadOnly = True
        Me.DescriptionTextBox.Size = New System.Drawing.Size(204, 26)
        Me.DescriptionTextBox.TabIndex = 7
        Me.DescriptionTextBox.TabStop = False
        '
        'SelectProductButton
        '
        Me.SelectProductButton.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SelectProductButton.Location = New System.Drawing.Point(163, 221)
        Me.SelectProductButton.Name = "SelectProductButton"
        Me.SelectProductButton.Size = New System.Drawing.Size(115, 33)
        Me.SelectProductButton.TabIndex = 2
        Me.SelectProductButton.Text = "&Select Product"
        Me.SelectProductButton.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(33, 274)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(62, 18)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Quantity"
        '
        'QuantityTextBox
        '
        Me.QuantityTextBox.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.QuantityTextBox.Location = New System.Drawing.Point(163, 271)
        Me.QuantityTextBox.Name = "QuantityTextBox"
        Me.QuantityTextBox.Size = New System.Drawing.Size(115, 26)
        Me.QuantityTextBox.TabIndex = 3
        Me.QuantityTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AddToOrdersButton
        '
        Me.AddToOrdersButton.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AddToOrdersButton.Location = New System.Drawing.Point(163, 405)
        Me.AddToOrdersButton.Name = "AddToOrdersButton"
        Me.AddToOrdersButton.Size = New System.Drawing.Size(115, 48)
        Me.AddToOrdersButton.TabIndex = 17
        Me.AddToOrdersButton.Text = "&ADD TO ORDERS"
        Me.AddToOrdersButton.UseVisualStyleBackColor = True
        '
        'HiddenProductNoTextBox
        '
        Me.HiddenProductNoTextBox.Location = New System.Drawing.Point(36, 229)
        Me.HiddenProductNoTextBox.Name = "HiddenProductNoTextBox"
        Me.HiddenProductNoTextBox.Size = New System.Drawing.Size(61, 20)
        Me.HiddenProductNoTextBox.TabIndex = 12
        Me.HiddenProductNoTextBox.Visible = False
        '
        'TotalPriceTextBox
        '
        Me.TotalPriceTextBox.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TotalPriceTextBox.Location = New System.Drawing.Point(163, 303)
        Me.TotalPriceTextBox.Name = "TotalPriceTextBox"
        Me.TotalPriceTextBox.ReadOnly = True
        Me.TotalPriceTextBox.Size = New System.Drawing.Size(115, 26)
        Me.TotalPriceTextBox.TabIndex = 13
        Me.TotalPriceTextBox.TabStop = False
        Me.TotalPriceTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(33, 306)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(79, 18)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Total Price"
        '
        'PriceTextBox
        '
        Me.PriceTextBox.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PriceTextBox.Location = New System.Drawing.Point(163, 183)
        Me.PriceTextBox.Name = "PriceTextBox"
        Me.PriceTextBox.ReadOnly = True
        Me.PriceTextBox.Size = New System.Drawing.Size(115, 26)
        Me.PriceTextBox.TabIndex = 15
        Me.PriceTextBox.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(33, 186)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(42, 18)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Price"
        '
        'DiscountTextBox
        '
        Me.DiscountTextBox.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DiscountTextBox.Location = New System.Drawing.Point(163, 335)
        Me.DiscountTextBox.Name = "DiscountTextBox"
        Me.DiscountTextBox.Size = New System.Drawing.Size(115, 26)
        Me.DiscountTextBox.TabIndex = 4
        Me.DiscountTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(33, 338)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(67, 18)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "Discount"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(33, 370)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(121, 18)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "Discounted Price"
        '
        'DiscountedPriceTextBox
        '
        Me.DiscountedPriceTextBox.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DiscountedPriceTextBox.Location = New System.Drawing.Point(163, 367)
        Me.DiscountedPriceTextBox.Name = "DiscountedPriceTextBox"
        Me.DiscountedPriceTextBox.ReadOnly = True
        Me.DiscountedPriceTextBox.Size = New System.Drawing.Size(115, 26)
        Me.DiscountedPriceTextBox.TabIndex = 19
        Me.DiscountedPriceTextBox.TabStop = False
        Me.DiscountedPriceTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(33, 154)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(107, 18)
        Me.Label10.TabIndex = 22
        Me.Label10.Text = "Measuring Unit"
        '
        'MeasuringUnitTextBox
        '
        Me.MeasuringUnitTextBox.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MeasuringUnitTextBox.Location = New System.Drawing.Point(163, 151)
        Me.MeasuringUnitTextBox.Name = "MeasuringUnitTextBox"
        Me.MeasuringUnitTextBox.ReadOnly = True
        Me.MeasuringUnitTextBox.Size = New System.Drawing.Size(115, 26)
        Me.MeasuringUnitTextBox.TabIndex = 21
        Me.MeasuringUnitTextBox.TabStop = False
        '
        'AddSalesForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.ROJBackground
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(399, 464)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.MeasuringUnitTextBox)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.DiscountedPriceTextBox)
        Me.Controls.Add(Me.DiscountTextBox)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.PriceTextBox)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.TotalPriceTextBox)
        Me.Controls.Add(Me.HiddenProductNoTextBox)
        Me.Controls.Add(Me.AddToOrdersButton)
        Me.Controls.Add(Me.QuantityTextBox)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.SelectProductButton)
        Me.Controls.Add(Me.DescriptionTextBox)
        Me.Controls.Add(Me.BrandTextBox)
        Me.Controls.Add(Me.ProductNameTextBox)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.CustomerTextBox)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(725, 125)
        Me.Name = "AddSalesForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add Order"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents CustomerTextBox As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents ProductNameTextBox As TextBox
    Friend WithEvents BrandTextBox As TextBox
    Friend WithEvents DescriptionTextBox As TextBox
    Friend WithEvents SelectProductButton As Button
    Friend WithEvents Label5 As Label
    Friend WithEvents QuantityTextBox As TextBox
    Friend WithEvents AddToOrdersButton As Button
    Friend WithEvents HiddenProductNoTextBox As TextBox
    Friend WithEvents TotalPriceTextBox As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents PriceTextBox As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents DiscountTextBox As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents DiscountedPriceTextBox As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents MeasuringUnitTextBox As TextBox
End Class
