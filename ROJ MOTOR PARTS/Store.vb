﻿'Program Name: ePOSIRMS (Electronic Point of Sale, Inventory, and Record Management System)
'Program Description: This program is a thesis project of Ginber Candelario.
'Programmer: GINBER J. CANDELARIO, BSIT

Imports MySql.Data.MySqlClient
Public Class StoreForm
    Friend SelectedOrderNo As Integer = -1
    Private discount As Decimal
    Private Sub StoreForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Try
            Dim Backuplocation As String
            Dim BackupFileName As String = "rojmotorparts" & DateTime.Now().ToString("yyyyMMddHHmmss")
            OpenConnection()
            Query = "select * from autobackuplocation"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Backuplocation = Reader.GetString("autobackuplocation")
            End While
            Reader.Close()
            If Backuplocation = "" Then
            Else
                'If System.IO.File.Exists(Backuplocation + "\rojmotorparts.sql") Then
                '    System.IO.File.Delete(Backuplocation + "\rojmotorparts.sql")
                'End If
                BackUpForm.savepath = """" & Backuplocation & "\" & BackupFilename & ".sql"""
                GinberBackUp()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        MysqlCon.Close()
        LoginForm.Close()
    End Sub
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim a As String
        a = System.DateTime.Now.Date.ToString("dd-MM-yyyy")
        DateTimeLabel.Text = a & "  " & String.Format("{0:hh:mm:ss tt}", System.DateTime.Now)
    End Sub

    Friend Sub StoreForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Timer1.Enabled = True
        Dim subtotal, grandtotal, totaldiscount As Decimal
        Dim p As String
        Try
            OpenConnection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        'Display orders
        OrdersDataGridView.Rows.Clear()
        Try
            Query = "select * from orders order by time"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim h = Reader.GetInt32("no")
                p = Reader.GetString("customername")
                Dim i = Reader.GetString("productname")
                Dim j = Reader.GetString("productbrand")
                Dim k = Reader.GetString("productdescription")
                Dim purchasecost = Reader.GetDecimal("purchasecost")
                Dim l = Reader.GetDecimal("saleprice")
                Dim m = Reader.GetDecimal("quantity")
                Dim n = Reader.GetString("measuringunit")
                Dim o = Reader.GetDecimal("totalprice")
                Dim q = Reader.GetDecimal("discount")
                Dim r = Reader.GetDecimal("discountedprice")
                Dim s = Reader.GetString("time")
                OrderNoTextBox.Text = Reader.GetString("OrderNo")
                Dim OrderNO As Integer = OrdersDataGridView.RowCount() + 1
                OrdersDataGridView.Rows.Add(h, OrderNO, i, j, k, l, m, n, o, q, r, s, purchasecost)
                subtotal += o
                totaldiscount += q
                CustomerNameTextBox.Text = p.ToString()
            End While
            Reader.Close()
            OrdersDataGridView.ClearSelection()
            SelectedOrderNo = -1

            grandtotal = subtotal - totaldiscount

            SubTotalTextBox.Text = subtotal.ToString("n2")
            DiscountTextBox.Text = totaldiscount.ToString("n2")
            GrandTotalTextBox.Text = grandtotal.ToString("n2")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        'get grosssale and totaldiscount
        Dim grosssale As Decimal = 0
        Dim totdiscount As Decimal = 0
        'Display today's sold products
        TodaySaleDataGridView.Rows.Clear()
        Try
            'Query = "select no, productname, productbrand, productdescription,customerfullname as customername, saleprice, quantity, 
            '         measuringunit, totalprice, discount, concat('(', discountedprice, ')') as discountedprice, cast(datetime as date) as date, orderno from paidloanedproducts
            '         where (select convert(datetime, date)) = (select convert(sysdate(), date))                     
            '         union all select * from todayssoldproducts where date = (select convert(sysdate(), date)) order by date desc"
            'Query = "select * from todayssoldproducts where date = (select convert(sysdate(), date)) order by time desc"
            Query = "SELECT * FROM (SELECT no, productname, productbrand, productdescription, customername, saleprice, quantity, measuringunit, 
                        totalprice, discount, discountedprice, orderno, date, time, no as no2
                     FROM todayssoldproducts UNION ALL 
                     SELECT no, productname, productbrand, productdescription, debtor, saleprice, quantity, measuringunit,
                        totalprice, discount, discountedprice, orderno, date, time, no + no as no2
                     FROM loanedproducts) AS loanedandsoldproducts
                     WHERE date = (SELECT CONVERT(sysdate(), date))
                     ORDER BY date DESC, time DESC, productname ASC"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            Dim No As Integer = 0
            Dim ColorAlternater As Boolean = False
            While Reader.Read
                Dim a = Reader.GetInt32("no")
                Dim b As Integer = TodaySaleDataGridView.RowCount() + 1
                Dim c = Reader.GetString("productname")
                Dim d = Reader.GetString("productbrand")
                Dim f = Reader.GetString("productdescription")
                Dim g = Reader.GetString("customername")
                Dim h = Reader.GetDecimal("saleprice")
                Dim i = Reader.GetDecimal("quantity")
                Dim j = Reader.GetString("measuringunit")
                Dim k = Reader.GetDecimal("totalprice")
                Dim l = Reader.GetDecimal("discount")
                Dim n = Reader.GetString("orderno")
                Dim m = Reader.GetDecimal("discountedprice")
                Dim no2 = Reader.GetInt32("no2")
                TodaySaleDataGridView.Rows.Add(a, b, c, d, f, g, h, i, j, k, l, m, n)
                If No > 0 Then
                    If n = TodaySaleDataGridView(12, No - 1).Value Then
                        TodaySaleDataGridView.Rows(No).DefaultCellStyle.BackColor = TodaySaleDataGridView.Rows(No - 1).DefaultCellStyle.BackColor
                    Else
                        If ColorAlternater = True Then
                            TodaySaleDataGridView.Rows(No).DefaultCellStyle.BackColor = Color.White
                            ColorAlternater = False
                        Else
                            TodaySaleDataGridView.Rows(No).DefaultCellStyle.BackColor = Color.Gray
                            ColorAlternater = True
                        End If
                    End If
                End If
                If a <> no2 Then
                    TodaySaleDataGridView.Rows(No).Cells(11).Style.BackColor = Color.Red
                Else
                    grosssale += k
                    totdiscount += l
                End If
                No += 1
            End While
            TodaySaleDataGridView.ClearSelection()
            Reader.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        ''Display today's debts payments
        Dim sumpaymentsmade As Decimal = 0
        TodaysPaidCreditsDataGridView.Rows.Clear()
        Try
            Query = "select * from paymentsmade
                     inner join (select debtor as c_debtor, debtorsid from credits) credits
                     on paymentsmade.customerid = credits.debtorsid
                     where date = (select convert(sysdate(), date))"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim a = Reader.GetString("customerid")
                Dim b = Reader.GetString("c_debtor")
                Dim c = Reader.GetDecimal("paidamount")
                Dim d = Reader.GetString("orderno")
                TodaysPaidCreditsDataGridView.Rows.Add(b, c, a, d)
                sumpaymentsmade += c
            End While
            Reader.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        'Display expenses
        Dim TotExpenses As Decimal = 0
        Try
            ExpensesDataGridView.Rows.Clear()
            Query = "select * from todaysexpenses where date = (select convert(sysdate(), date))"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim a As Integer = ExpensesDataGridView.RowCount() + 1
                Dim b = Reader.GetString("name")
                Dim c = Reader.GetString("description")
                Dim d = Reader.GetString("cost")
                ExpensesDataGridView.Rows.Add(a, b, c, d)
                TotExpenses += d
            End While
            Reader.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        'Add or update todays sale
        Dim CountTodaysSale As Integer = 0
        Dim SumGrossCreditsPayments As Decimal = SumPaymentsMade + grosssale
        Try
            Query = "select * from todayssale where date = (select convert(sysdate(), date))"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                CountTodaysSale = 1
            End While
            Reader.Close()
            If CountTodaysSale > 0 Then
                Try
                    Query = "update todayssale set grosssale = " & SumGrossCreditsPayments & ", totaldiscount = " & totdiscount _
                                            & ", totalexpenses = " & TotExpenses & " where date = (select convert(sysdate(), date))"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    Reader.Close()
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            Else
                Try
                    Query = "insert into todayssale(date, grosssale, totaldiscount, totalexpenses) 
                             values((select convert(sysdate(), date)), " _
                             & SumGrossCreditsPayments & ", " & totdiscount & ", " & TotExpenses & ")"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    Reader.Close()
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            End If
            'Display sales
            Dim SummaryGrossSale, SummaryTotalDiscount, SummaryTotalExpenses, SummaryNetSale As Decimal
            Try
                Query = "select * from todayssale where date = (select convert(sysdate(), date))"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                While Reader.Read
                    SummaryGrossSale = Reader.GetDecimal("grosssale")
                    SummaryTotalDiscount = Reader.GetDecimal("totaldiscount")
                    SummaryTotalExpenses = Reader.GetDecimal("totalexpenses")
                End While
                Reader.Close()
                SummaryNetSale = SummaryGrossSale - (SummaryTotalDiscount + SummaryTotalExpenses)
                GrossSaleTextBox.Text = SummaryGrossSale.ToString("n2")
                TotalDiscountTextBox.Text = SummaryTotalDiscount.ToString("n2")
                TotalExpensesTextBox.Text = SummaryTotalExpenses.ToString("n2")
                NetSaleTextBox.Text = SummaryNetSale.ToString("n2")
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        'Display Credits
        'Try
        '    CreditsDataGridView.Rows.Clear()
        '    Query = "select * from credits 
        '            inner join (select sum(discount) as totaldiscount, datetime as datetime2 
        '            from loanedproducts group by datetime) A    
        '            on credits.datetime = A.datetime2 order by debtor"
        '    Command = New MySqlCommand(Query, MysqlCon)
        '    Reader = Command.ExecuteReader
        '    While Reader.Read
        '        Dim a = Reader.GetString("debtor")
        '        Dim b = Reader.GetString("contact")
        '        Dim c = Reader.GetDecimal("payment")
        '        Dim Dscnt = Reader.GetDecimal("totaldiscount")
        '        Dim d = Reader.GetDecimal("amountpaid")
        '        Dim rempayment As Decimal = c - d
        '        Dim g = Reader.GetString("datetime")
        '        CreditsDataGridView.Rows.Add(a, b, c + Dscnt, Dscnt, d, rempayment, g)
        '    End While
        '    Reader.Close()
        '    CreditsDataGridView.ClearSelection()
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message)
        'End Try
        Try
            CreditsDataGridView.Rows.Clear()
            Query = "SELECT * FROM credits
                     INNER JOIN (SELECT SUM(discount) as totaldiscount, debtorsid
                                 FROM loanedproducts
                                 GROUP BY debtorsid) sumofdiscounts
                     ON credits.debtorsid = sumofdiscounts.debtorsid
                     ORDER BY debtor"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim id = Reader.GetString("debtorsid")
                Dim debtor = Reader.GetString("debtor")
                Dim payment = Reader.GetDecimal("payment")
                Dim discount = Reader.GetDecimal("totaldiscount")
                Dim amountpaid = Reader.GetDecimal("amountpaid")
                Dim remainingpayment = payment - amountpaid
                Dim contact = Reader.GetString("contact")
                CreditsDataGridView.Rows.Add(id, debtor, payment + discount, discount, amountpaid, remainingpayment, contact)
            End While
            CreditsDataGridView.ClearSelection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        If OrdersDataGridView.RowCount = 0 Then
            DeleteOrderButton.Enabled = False
            CancelOrderButton.Enabled = False
            EditOrderButton.Enabled = False
            OrderCompleteButton.Enabled = False
            CustomerNameTextBox.Clear()
        Else
            DeleteOrderButton.Enabled = True
            CancelOrderButton.Enabled = True
            EditOrderButton.Enabled = True
            OrderCompleteButton.Enabled = True
        End If

        MysqlCon.Close()
    End Sub

    Private Sub AddSaleButton_Click(sender As Object, e As EventArgs) Handles AddSaleButton.Click
        OrdersGroupBox.Visible = True
        AddSalesForm.ShowDialog()
    End Sub

    Private Sub AddExpensesButton_Click(sender As Object, e As EventArgs) Handles AddExpensesButton.Click
        AddExpensesForm.ShowDialog()
    End Sub

    Private Sub DiscountTextBox_TextChanged(sender As Object, e As EventArgs) Handles DiscountTextBox.TextChanged
        Dim subtotal, grandtotal As Decimal
        If DiscountTextBox.Text = "" Or IsNumeric(DiscountTextBox.Text) = False Then
            discount = 0
        Else
            discount = DiscountTextBox.Text
        End If
        If SubTotalTextBox.Text = "" Then
            subtotal = 0
        Else
            subtotal = SubTotalTextBox.Text
        End If
        grandtotal = subtotal - discount
        GrandTotalTextBox.Text = grandtotal.ToString("n2")
        AmountReceivedTextBox_TextChanged(sender, e)
    End Sub

    Friend Sub AmountReceivedTextBox_TextChanged(sender As Object, e As EventArgs) Handles AmountReceivedTextBox.TextChanged
        Try
            Dim grandtotal, change, amountreceived As Decimal
            If GrandTotalTextBox.Text = "" Then
                grandtotal = 0
            Else
                grandtotal = GrandTotalTextBox.Text
            End If
            If AmountReceivedTextBox.Text = "" Or IsNumeric(AmountReceivedTextBox.Text) = False Then
                AmountReceivedTextBox.Text = ""
            Else
                amountreceived = AmountReceivedTextBox.Text
                If amountreceived < 0 Then
                    AmountReceivedTextBox.Text = ""
                End If
            End If
            If AmountReceivedTextBox.Text = "" Then
                ChangeTextBox.Clear()
            Else
                change = amountreceived - grandtotal
                ChangeTextBox.Text = change.ToString("n2")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub OrderCompleteButton_Click(sender As Object, e As EventArgs) Handles OrderCompleteButton.Click
        Dim totaldiscount As Decimal
        Dim NoAmountReceive As String = "The amount received is zero (0)! Save all the orders as credits?"
        Dim NotEnoughAmount As String = "The amount received is not enough for all the orders! Save remaining payment as credits?"
        Try
            OpenConnection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Dim grosssale As Integer
        If OrdersDataGridView.Rows.Count() > 0 Then
            Dim MesRes As DialogResult
            If AmountReceivedTextBox.Text = "" Or AmountReceivedTextBox.Text = "0" Or IsNumeric(AmountReceivedTextBox.Text) = False Then
                'save as credits
                Dim mesres2 As DialogResult
                mesres2 = MessageBox.Show(NoAmountReceive, "No amount received!", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                If mesres2 = DialogResult.Yes Then
                    AddInfoForm.CustomerFullNameTextBox.Text = CustomerNameTextBox.Text
                    AddInfoForm.ShowDialog()
                End If
            ElseIf ChangeTextBox.Text < 0 Then
                'save remaining payment as credits
                Dim mesres3 As DialogResult
                mesres3 = MessageBox.Show(NotEnoughAmount, "Not enough amount", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                If mesres3 = DialogResult.Yes Then
                    AddInfoForm.CustomerFullNameTextBox.Text = CustomerNameTextBox.Text
                    AddInfoForm.ShowDialog()
                End If
            Else
                MesRes = MessageBox.Show("Click OK to continue.", "Please Confirm", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
                If MesRes = DialogResult.OK Then
                    Try
                        OpenConnection()
                        For SelectedRow As Integer = 0 To OrdersDataGridView.RowCount() - 1

                            Dim prodno As Integer = OrdersDataGridView(0, SelectedRow).Value()
                            Dim productname As String = OrdersDataGridView(2, SelectedRow).Value()
                            Dim productbrand As String = OrdersDataGridView(3, SelectedRow).Value()
                            Dim productdescription As String = OrdersDataGridView(4, SelectedRow).Value()
                            Dim customername As String = CustomerNameTextBox.Text
                            Dim saleprice As Decimal = OrdersDataGridView(5, SelectedRow).Value()
                            Dim quantity As Decimal = OrdersDataGridView(6, SelectedRow).Value()
                            Dim measuringunit As String = OrdersDataGridView(7, SelectedRow).Value()
                            Dim totalprice As Decimal = OrdersDataGridView(8, SelectedRow).Value()
                            Dim discount As Decimal = OrdersDataGridView(9, SelectedRow).Value()
                            Dim finalprice As Decimal = OrdersDataGridView(10, SelectedRow).Value()
                            Dim time As String = OrdersDataGridView(11, SelectedRow).Value()
                            Dim purchasecost As Decimal = OrdersDataGridView(12, SelectedRow).Value()
                            Try
                                Query = "insert into todayssoldproducts values (" & prodno & ", '" & productname & "', '" & productbrand & "', '" & productdescription & "', '" _
                                    & customername & "', " & purchasecost & ", " & saleprice & ", " & quantity & ", '" & measuringunit & "', " & totalprice & ", " & discount & ", " & finalprice _
                                    & ", (select convert(sysdate(), date)), '" & time & "', '" & OrderNoTextBox.Text & "')"
                                Command = New MySqlCommand(Query, MysqlCon)
                                Reader = Command.ExecuteReader
                                Reader.Close()
                            Catch ex As Exception
                                MessageBox.Show(ex.Message)
                            End Try
                            'Subtact order quantity to stock
                            Try
                                Query = "update products set availablestock = availablestock - " & quantity & " where no = " & prodno
                                Command = New MySqlCommand(Query, MysqlCon)
                                Reader = Command.ExecuteReader
                                Reader.Close()
                            Catch ex As Exception
                                MessageBox.Show(ex.Message)
                            End Try

                        Next

                        AmountReceivedTextBox.Clear()
                        OrderNoTextBox.Clear()
                        Try
                            Query = "delete from orders"
                            Command = New MySqlCommand(Query, MysqlCon)
                            Reader = Command.ExecuteReader
                            Reader.Close()
                        Catch ex As Exception
                            MessageBox.Show(ex.Message)
                        End Try
                        StoreForm_Load(sender, e)
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try
                End If
            End If
        Else
            MessageBox.Show("No order to be completed!", "No Order", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
        MysqlCon.Close()
    End Sub

    Private Sub DeleteOrderButton_Click(sender As Object, e As EventArgs) Handles DeleteOrderButton.Click
        If SelectedOrderNo < 0 Then
            MessageBox.Show("Select order to be deleted!", "No Order Selected", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            Dim SelectedOrderNo2 = SelectedOrderNo
            Dim MesRes As DialogResult
            MesRes = MessageBox.Show("Delete the selected order?", "Please Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If MesRes = DialogResult.Yes Then
                Try
                    OpenConnection()
                    Query = "delete from orders where no = " & SelectedOrderNo2
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    Reader.Close()
                    StoreForm_Load(sender, e)
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
                If OrdersDataGridView.RowCount = 0 Then
                    OrderNoTextBox.Clear()
                End If
            End If
        End If
        MysqlCon.Close()
    End Sub

    Private Sub OrdersDataGridView_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles OrdersDataGridView.CellClick
        Try
            SelectedOrderNo = OrdersDataGridView.CurrentRow.Cells(0).Value
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CancelOrderButton_Click(sender As Object, e As EventArgs) Handles CancelOrderButton.Click
        Dim mesres As DialogResult = DialogResult.No
        mesres = MessageBox.Show("This will clear all inputted orders! Please confirm.", "Cancel Orders", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation)
        If mesres = DialogResult.OK Then
            Try
                OpenConnection()
                Query = "delete from orders"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                Reader.Close()
                CustomerNameTextBox.Clear()
                SubTotalTextBox.Text = "0.00"
                DiscountTextBox.Clear()
                GrandTotalTextBox.Text = "0.00"
                AmountReceivedTextBox.Clear()
                ChangeTextBox.Clear()
                OrderNoTextBox.Clear()
                With OrdersDataGridView
                    .ClearSelection()
                    .Rows.Clear()
                End With
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Else

        End If
        RefreshToolStripMenuItem_Click(sender, e)
        MysqlCon.Close()
    End Sub

    Private Sub EditOrderButton_Click(sender As Object, e As EventArgs) Handles EditOrderButton.Click
        Try
            OpenConnection()
            Try
                If SelectedOrderNo < 0 Then
                    MessageBox.Show("Select order to be updated!", "No Order Selected", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    Query = "select * from orders where no = " & SelectedOrderNo
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    While Reader.Read
                        Dim l = Reader.GetString("measuringunit")
                        Dim a = Reader.GetInt32("no")
                        Dim b = Reader.GetString("customername")
                        Dim c = Reader.GetString("productname")
                        Dim d = Reader.GetString("productbrand")
                        Dim f = Reader.GetString("productdescription")
                        Dim g = Reader.GetDecimal("saleprice")
                        Dim h = Reader.GetDecimal("quantity")
                        Dim i = Reader.GetDecimal("totalprice")
                        Dim j = Reader.GetDecimal("discount")
                        Dim k = Reader.GetDecimal("discountedprice")
                        EditOrderForm.MeasuringUnit = l
                        EditOrderForm.HiddenProductNoTextBox.Text = a.ToString()
                        EditOrderForm.CustomerTextBox.Text = b.ToString()
                        EditOrderForm.ProductNameTextBox.Text = c.ToString()
                        EditOrderForm.BrandTextBox.Text = d.ToString()
                        EditOrderForm.DescriptionTextBox.Text = f.ToString()
                        EditOrderForm.MeasuringUnitTextBox.Text = l.ToString()
                        EditOrderForm.PriceTextBox.Text = g.ToString("n2")
                        If l.ToLower = "pcs" Or l.ToLower = "pack" Or l.ToLower = "pair" Then
                            EditOrderForm.QuantityTextBox.Text = CInt(h)
                        Else
                            EditOrderForm.QuantityTextBox.Text = h.ToString()
                        End If
                        EditOrderForm.TotalPriceTextBox.Text = i.ToString("n2")
                        EditOrderForm.DiscountTextBox.Text = j.ToString("n2")
                        EditOrderForm.DiscountedPriceTextBox.Text = k.ToString("n2")
                    End While
                    Reader.Close()
                    EditOrderForm.ShowDialog()
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        MysqlCon.Close()
    End Sub

    Private Sub CreditsDataGridView_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles CreditsDataGridView.CellDoubleClick
        CreditsInformationForm.SelectedDebtor = CreditsDataGridView.CurrentRow.Cells(0).Value
        CreditsInformationForm.ShowDialog()
    End Sub

    Private Sub CreditsDataGridView_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles CreditsDataGridView.CellClick
        PayCreditsForm.SelectedDebtor = CreditsDataGridView.CurrentRow.Cells(0).Value
        PayCreditButton.Enabled = True
        MoreInfoButton.Enabled = True
    End Sub

    Private Sub PayCreditButton_Click(sender As Object, e As EventArgs) Handles PayCreditButton.Click
        PayCreditsForm.ShowDialog()
    End Sub

    Private Sub MoreInfoButton_Click(sender As Object, e As EventArgs) Handles MoreInfoButton.Click
        'CreditsInfoForm.FromStore = True
        'CreditsInfoForm.LoanedProdContextMenuStrip1.Enabled = False
        'CreditsInfoForm.paymenthistoryContextMenuStrip2.Enabled = False
        'CreditsInfoForm.SelectedDebtor = CreditsDataGridView.CurrentRow.Cells(6).Value
        'CreditsInfoForm.ShowDialog()
        CreditsInformationForm.SelectedDebtor = CreditsDataGridView.CurrentRow.Cells(0).Value
        CreditsInformationForm.ShowDialog()
    End Sub

    Private Sub RefreshToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RefreshToolStripMenuItem.Click
        StoreForm_Load(sender, e)
    End Sub

    Private Sub AddSaleToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AddSaleToolStripMenuItem.Click
        AddSaleButton_Click(sender, e)
    End Sub

    Private Sub AddExpensesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AddExpensesToolStripMenuItem.Click
        AddExpensesButton_Click(sender, e)
    End Sub

    Private Sub InventoryToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InventoryToolStripMenuItem.Click
        InventoryToolStripMenuItem1_Click(sender, e)
    End Sub

    Private Sub RecordsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RecordsToolStripMenuItem.Click
        RecordsToolStripMenuItem1_Click(sender, e)
    End Sub

    Private Sub SettingsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SettingsToolStripMenuItem.Click
        SettingsToolStripMenuItem1_Click(sender, e)
    End Sub

    Private Sub InventoryToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles InventoryToolStripMenuItem1.Click
        SecurityForm.GINBAHoption = "GinbahInventory"
        SecurityForm.GINBAHoption2 = "Access the Inventory"
        SecurityForm.ShowDialog()
    End Sub

    Private Sub RecordsToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles RecordsToolStripMenuItem1.Click
        SecurityForm.GINBAHoption = "GinbahRecords"
        SecurityForm.GINBAHoption2 = "Access the Records"
        SecurityForm.ShowDialog()
    End Sub

    Private Sub SettingsToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles SettingsToolStripMenuItem1.Click
        'settings
        SettingsForm.ShowDialog()
    End Sub

    Private Sub SearchDebtorTextBox_TextChanged(sender As Object, e As EventArgs) Handles SearchDebtorTextBox.TextChanged
        'CreditsDataGridView.Rows.Clear()
        'Try
        '    OpenConnection()
        '    Query = "select * from credits where customerfullname like '%" & SearchDebtorTextBox.Text & "%' order by customerfullname"
        '    Command = New MySqlCommand(Query, MysqlCon)
        '    Reader = Command.ExecuteReader
        '    While Reader.Read
        '        Dim a = Reader.GetString("customerfullname")
        '        Dim b = Reader.GetString("contact")
        '        Dim c = Reader.GetDecimal("payment")
        '        Dim d = Reader.GetDecimal("amountpaid")
        '        Dim rempayment As Decimal = c - d
        '        Dim g = Reader.GetString("datetime")
        '        CreditsDataGridView.Rows.Add(a, b, c, d, rempayment, g)
        '    End While
        '    Reader.Close()
        '    CreditsDataGridView.ClearSelection()
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message)
        'End Try
        'MysqlCon.Close()
        Try
            OpenConnection()
            CreditsDataGridView.Rows.Clear()
            Query = "select * from (select * from credits                  
                    inner join (select sum(discount) as totaldiscount, datetime as datetime2 
                    from loanedproducts group by datetime) A    
                    on credits.datetime = A.datetime2) B 
                    where B.customerfullname like '%" & SearchDebtorTextBox.Text & "%'
                    or cast(B.datetime as date)like '%" & SearchDebtorTextBox.Text & "%' order by customerfullname"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim a = Reader.GetString("customerfullname")
                Dim b = Reader.GetString("contact")
                Dim c = Reader.GetDecimal("payment")
                Dim dscnt = Reader.GetDecimal("totaldiscount")
                Dim d = Reader.GetDecimal("amountpaid")
                Dim rempayment As Decimal = c - d
                Dim g = Reader.GetString("datetime")
                CreditsDataGridView.Rows.Add(a, b, c + dscnt, dscnt, d, rempayment, g)
            End While
            Reader.Close()
            CreditsDataGridView.ClearSelection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        MysqlCon.Close()
    End Sub
    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub
    Private Sub AddSaleToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles AddSaleToolStripMenuItem1.Click
        AddSaleButton_Click(sender, e)
    End Sub
    Private Sub AddExpensesToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles AddExpensesToolStripMenuItem1.Click
        AddExpensesButton_Click(sender, e)
    End Sub
    Private Sub StoreForm_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        StoreForm_Load(sender, e)
    End Sub

    Private Sub CreditsDataGridView_MouseHover(sender As Object, e As EventArgs) Handles CreditsDataGridView.MouseHover
        ContextMenuStrip1.Enabled = False
    End Sub

    Private Sub StoreForm_MouseHover(sender As Object, e As EventArgs) Handles MyBase.MouseHover
        ContextMenuStrip1.Enabled = True
    End Sub

    Private Sub OrdersDataGridView_MouseHover(sender As Object, e As EventArgs) Handles OrdersDataGridView.MouseHover
        ContextMenuStrip1.Enabled = False
    End Sub

End Class