﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SelectProductForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SelectProductForm))
        Me.SelectProductsDataGridView = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SelectSearchProductTextBox = New System.Windows.Forms.TextBox()
        Me.noColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductNameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BrandColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescriptionColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SupRetPriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PurchaseCostColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SalePriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StockColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.SelectProductsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SelectProductsDataGridView
        '
        Me.SelectProductsDataGridView.AllowUserToAddRows = False
        Me.SelectProductsDataGridView.AllowUserToDeleteRows = False
        Me.SelectProductsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.SelectProductsDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.noColumn, Me.ProductNameColumn, Me.BrandColumn, Me.DescriptionColumn, Me.SupRetPriceColumn, Me.PurchaseCostColumn, Me.SalePriceColumn, Me.StockColumn})
        Me.SelectProductsDataGridView.Location = New System.Drawing.Point(3, 37)
        Me.SelectProductsDataGridView.Name = "SelectProductsDataGridView"
        Me.SelectProductsDataGridView.ReadOnly = True
        Me.SelectProductsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.SelectProductsDataGridView.Size = New System.Drawing.Size(719, 216)
        Me.SelectProductsDataGridView.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(11, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(95, 16)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Search Product"
        '
        'SelectSearchProductTextBox
        '
        Me.SelectSearchProductTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SelectSearchProductTextBox.Location = New System.Drawing.Point(112, 8)
        Me.SelectSearchProductTextBox.Name = "SelectSearchProductTextBox"
        Me.SelectSearchProductTextBox.Size = New System.Drawing.Size(238, 23)
        Me.SelectSearchProductTextBox.TabIndex = 0
        '
        'noColumn
        '
        Me.noColumn.HeaderText = "no"
        Me.noColumn.Name = "noColumn"
        Me.noColumn.ReadOnly = True
        Me.noColumn.Width = 50
        '
        'ProductNameColumn
        '
        Me.ProductNameColumn.HeaderText = "Product Name"
        Me.ProductNameColumn.Name = "ProductNameColumn"
        Me.ProductNameColumn.ReadOnly = True
        Me.ProductNameColumn.Width = 140
        '
        'BrandColumn
        '
        Me.BrandColumn.HeaderText = "Brand"
        Me.BrandColumn.Name = "BrandColumn"
        Me.BrandColumn.ReadOnly = True
        Me.BrandColumn.Width = 120
        '
        'DescriptionColumn
        '
        Me.DescriptionColumn.HeaderText = "Description"
        Me.DescriptionColumn.Name = "DescriptionColumn"
        Me.DescriptionColumn.ReadOnly = True
        Me.DescriptionColumn.Width = 120
        '
        'SupRetPriceColumn
        '
        Me.SupRetPriceColumn.HeaderText = "Sup. Retailer Price"
        Me.SupRetPriceColumn.Name = "SupRetPriceColumn"
        Me.SupRetPriceColumn.ReadOnly = True
        Me.SupRetPriceColumn.Width = 63
        '
        'PurchaseCostColumn
        '
        Me.PurchaseCostColumn.HeaderText = "Purchase Cost"
        Me.PurchaseCostColumn.Name = "PurchaseCostColumn"
        Me.PurchaseCostColumn.ReadOnly = True
        Me.PurchaseCostColumn.Width = 63
        '
        'SalePriceColumn
        '
        Me.SalePriceColumn.HeaderText = "Sale Price"
        Me.SalePriceColumn.Name = "SalePriceColumn"
        Me.SalePriceColumn.ReadOnly = True
        Me.SalePriceColumn.Width = 63
        '
        'StockColumn
        '
        Me.StockColumn.HeaderText = "Stock"
        Me.StockColumn.Name = "StockColumn"
        Me.StockColumn.ReadOnly = True
        Me.StockColumn.Width = 56
        '
        'SelectProductForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.BackgroundImage = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.ROJBackground
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(726, 253)
        Me.Controls.Add(Me.SelectSearchProductTextBox)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.SelectProductsDataGridView)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(600, 150)
        Me.Name = "SelectProductForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Select Product"
        CType(Me.SelectProductsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents SelectProductsDataGridView As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents SelectSearchProductTextBox As TextBox
    Friend WithEvents noColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductNameColumn As DataGridViewTextBoxColumn
    Friend WithEvents BrandColumn As DataGridViewTextBoxColumn
    Friend WithEvents DescriptionColumn As DataGridViewTextBoxColumn
    Friend WithEvents SupRetPriceColumn As DataGridViewTextBoxColumn
    Friend WithEvents PurchaseCostColumn As DataGridViewTextBoxColumn
    Friend WithEvents SalePriceColumn As DataGridViewTextBoxColumn
    Friend WithEvents StockColumn As DataGridViewTextBoxColumn
End Class
