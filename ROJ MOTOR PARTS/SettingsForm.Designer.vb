﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SettingsForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SettingsForm))
        Me.UserSettingsPictureBox = New System.Windows.Forms.PictureBox()
        Me.AboutPictureBox = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BackupAllDataToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RestoreToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UserSettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SetAutoBackupLocationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        CType(Me.UserSettingsPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AboutPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'UserSettingsPictureBox
        '
        Me.UserSettingsPictureBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.UserSettingsPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.UserSettingsPictureBox.Image = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.user_settings_icon
        Me.UserSettingsPictureBox.Location = New System.Drawing.Point(25, 63)
        Me.UserSettingsPictureBox.Name = "UserSettingsPictureBox"
        Me.UserSettingsPictureBox.Size = New System.Drawing.Size(235, 228)
        Me.UserSettingsPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.UserSettingsPictureBox.TabIndex = 0
        Me.UserSettingsPictureBox.TabStop = False
        '
        'AboutPictureBox
        '
        Me.AboutPictureBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.AboutPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AboutPictureBox.Image = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.about_icon
        Me.AboutPictureBox.Location = New System.Drawing.Point(289, 63)
        Me.AboutPictureBox.Name = "AboutPictureBox"
        Me.AboutPictureBox.Size = New System.Drawing.Size(235, 228)
        Me.AboutPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.AboutPictureBox.TabIndex = 1
        Me.AboutPictureBox.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Indigo
        Me.Label1.Location = New System.Drawing.Point(46, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(195, 25)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "USERS SETTINGS"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Indigo
        Me.Label2.Location = New System.Drawing.Point(365, 35)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(85, 25)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "ABOUT"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(549, 24)
        Me.MenuStrip1.TabIndex = 4
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BackupAllDataToolStripMenuItem, Me.RestoreToolStripMenuItem, Me.SetAutoBackupLocationToolStripMenuItem, Me.UserSettingsToolStripMenuItem, Me.AboutToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'BackupAllDataToolStripMenuItem
        '
        Me.BackupAllDataToolStripMenuItem.Name = "BackupAllDataToolStripMenuItem"
        Me.BackupAllDataToolStripMenuItem.ShortcutKeyDisplayString = "Alt + B"
        Me.BackupAllDataToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.B), System.Windows.Forms.Keys)
        Me.BackupAllDataToolStripMenuItem.Size = New System.Drawing.Size(246, 22)
        Me.BackupAllDataToolStripMenuItem.Text = "&Back up All Data"
        Me.BackupAllDataToolStripMenuItem.ToolTipText = "This will create a backup file of all data from the system. (Inventories, users, " &
    "and all records data)"
        '
        'RestoreToolStripMenuItem
        '
        Me.RestoreToolStripMenuItem.Name = "RestoreToolStripMenuItem"
        Me.RestoreToolStripMenuItem.ShortcutKeyDisplayString = "Alt + R"
        Me.RestoreToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.R), System.Windows.Forms.Keys)
        Me.RestoreToolStripMenuItem.Size = New System.Drawing.Size(246, 22)
        Me.RestoreToolStripMenuItem.Text = "&Restore/Load Data"
        Me.RestoreToolStripMenuItem.ToolTipText = "This will insert the data that you will select and might delete all current data " &
    "in the system depending on the data you selected."
        '
        'UserSettingsToolStripMenuItem
        '
        Me.UserSettingsToolStripMenuItem.Name = "UserSettingsToolStripMenuItem"
        Me.UserSettingsToolStripMenuItem.ShortcutKeyDisplayString = "Alt + U"
        Me.UserSettingsToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.U), System.Windows.Forms.Keys)
        Me.UserSettingsToolStripMenuItem.Size = New System.Drawing.Size(246, 22)
        Me.UserSettingsToolStripMenuItem.Text = "&User Settings"
        '
        'SetAutoBackupLocationToolStripMenuItem
        '
        Me.SetAutoBackupLocationToolStripMenuItem.Name = "SetAutoBackupLocationToolStripMenuItem"
        Me.SetAutoBackupLocationToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.SetAutoBackupLocationToolStripMenuItem.Size = New System.Drawing.Size(246, 22)
        Me.SetAutoBackupLocationToolStripMenuItem.Text = "Set Auto Backup Location"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.ShortcutKeyDisplayString = "Alt + A"
        Me.AboutToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(246, 22)
        Me.AboutToolStripMenuItem.Text = "&About"
        '
        'SettingsForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.ROJBackground
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(549, 306)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.AboutPictureBox)
        Me.Controls.Add(Me.UserSettingsPictureBox)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "SettingsForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Settings"
        CType(Me.UserSettingsPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AboutPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents UserSettingsPictureBox As PictureBox
    Friend WithEvents AboutPictureBox As PictureBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BackupAllDataToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RestoreToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents UserSettingsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SetAutoBackupLocationToolStripMenuItem As ToolStripMenuItem
End Class
