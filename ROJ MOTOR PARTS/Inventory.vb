﻿Imports MySql.Data.MySqlClient
Imports System.Drawing.Printing
Public Class InventoryForm
    Friend ProductNo As Integer = -1
    Private Sub AddProductButton_Click(sender As Object, e As EventArgs) Handles AddProductButton.Click
        AddProductForm.ShowDialog()
    End Sub
    Private Sub InventoryForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ProductsDataGridView.Rows.Clear()
        Try
            OpenConnection()
        Catch ex As Exception
        End Try
        Try
            Query = "select * from products order by productname limit 100"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim a = Reader.GetString("productname")
                Dim i = Reader.GetString("productbrand")
                Dim j = Reader.GetString("productdescription")
                Dim b = Reader.GetDecimal("availablestock")
                Dim c = Reader.GetDecimal("minstocklimit")
                Dim d = Reader.GetString("measuringunit")
                Dim k = Reader.GetDecimal("supretprice")
                Dim f = Reader.GetDecimal("purchasecost")
                Dim g = Reader.GetDecimal("saleprice")
                Dim h = Reader.GetInt32("no")
                If k = 0 Then
                    ProductsDataGridView.Rows.Add(h, a, i, j, b, c, d, "", f, g)
                Else
                    ProductsDataGridView.Rows.Add(h, a, i, j, b, c, d, k, f, g)
                End If
            End While
            Reader.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        End Try
        ProductsDataGridView.ClearSelection()
        For i As Integer = 0 To ProductsDataGridView.Rows.Count - 1
            If ProductsDataGridView.Rows(i).Cells(4).Value <= ProductsDataGridView.Rows(i).Cells(5).Value Then
                ProductsDataGridView.Rows(i).Cells(4).Style.BackColor = Color.Red
            End If
            Dim ifExistInt As Integer = 0
            Try
                Query = "select * from stockshistory where no = " & ProductsDataGridView.Rows(i).Cells(0).Value
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                While Reader.Read
                    ifExistInt = 1
                End While
                Reader.Close()
                If ifExistInt = 0 Then
                    Query = "insert into stockshistory values(" & ProductsDataGridView.Rows(i).Cells(0).Value & ", (select sysdate()), " _
                             & ProductsDataGridView.Rows(i).Cells(4).Value & ", " & ProductsDataGridView.Rows(i).Cells(4).Value & ")"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    Reader.Close()
                End If
            Catch ex As Exception
                MessageBox.Show(Query)
                MessageBox.Show(ex.Message)
            End Try
        Next
        MysqlCon.Close()
    End Sub

    Private Sub AddStockButton_Click(sender As Object, e As EventArgs) Handles AddStockButton.Click
        If ProductNo < 0 Then
            MessageBox.Show("Select a product to add stock.", "No product selected!", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            AddStockForm.ShowDialog()
        End If
    End Sub

    Friend Sub InventoryForm_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        ProductPictureBox.Image = My.Resources.no_image_icon_13
        ProductNameLabel.Text = "Product name"
        ProductBrandLabel.Text = "Product brand"
        ProductDescriptionLabel.Text = "Product Description"
        ProductNo = -1
        SearchProductTextBox.Clear()
        SearchProductTextBox.Focus()
        InventoryForm_Load(sender, e)
    End Sub

    Private Sub ProductsDataGridView_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles ProductsDataGridView.CellClick
        Try
            OpenConnection()
        Catch ex As Exception
        End Try
        ProductNo = ProductsDataGridView.CurrentRow.Cells(0).Value
        Dim prodname, prodbrand, proddes As String
        Dim prodphotopath As String = ""
        Try
            Query = "select * from products where no = " & ProductNo
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                prodname = Reader.GetString("productname")
                prodbrand = Reader.GetString("productbrand")
                proddes = Reader.GetString("productdescription")
                prodphotopath = Reader.GetString("photopath")
                Try
                    ProductNameLabel.Text = prodname.ToString()
                    ProductBrandLabel.Text = prodbrand.ToString()
                    ProductDescriptionLabel.Text = proddes.ToString()
                    If System.IO.File.Exists(prodphotopath) Then
                        ProductPictureBox.Image = Image.FromFile(prodphotopath)
                    Else
                        ProductPictureBox.Image = My.Resources.no_image_icon_13
                    End If
                Catch ex As Exception

                End Try
            End While
            Reader.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        End Try
        MysqlCon.Close()
    End Sub

    Private Sub EditProductDetailsButton_Click(sender As Object, e As EventArgs) Handles EditProductDetailsButton.Click
        If ProductNo < 0 Then
            MessageBox.Show("Select a product to update.", "No product selected!", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            EditProductForm.ShowDialog()
        End If
    End Sub

    Private Sub DeleteProductButton_Click(sender As Object, e As EventArgs) Handles DeleteProductButton.Click
        Try
            OpenConnection()
        Catch ex As Exception
        End Try
        Dim ProductNoToDelete As Int32 = ProductNo
        If ProductNo < 0 Then
            MessageBox.Show("Select a product to delete.", "No product selected!", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            Dim existOnRecords As Boolean = False
            Try
                Query = "select no from todayssoldproducts where no = " & ProductNo & " union select no from loanedproducts where no = " & ProductNo
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                While Reader.Read
                    existOnRecords = True
                End While
                Reader.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try

            If existOnRecords = False Then
                Dim MessageResult As String = MessageBox.Show("Are you sure to delete this product?", "Confirm Action", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                If MessageResult = DialogResult.Yes Then
                    If MysqlCon.State = ConnectionState.Open Then
                    Else
                        Try
                            OpenConnection()
                        Catch ex As Exception
                        End Try
                    End If
                    Try
                        Query = "delete from products where no = " & ProductNoToDelete
                        Command = New MySqlCommand(Query, MysqlCon)
                        Reader = Command.ExecuteReader
                        Reader.Close()
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try
                    InventoryForm_Load(sender, e)
                End If
            Else
                MessageBox.Show("Cannot delete this item because there is a record of this product being sold/loaned.", "Item Cannot be Deleted!", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
        MysqlCon.Close()
    End Sub

    Private Sub ProductsDataGridView_KeyDown(sender As Object, e As KeyEventArgs) Handles ProductsDataGridView.KeyDown
        Try
            OpenConnection()
        Catch ex As Exception
        End Try
        ProductNo = ProductsDataGridView.CurrentRow.Cells(0).Value
        Dim prodname, prodbrand, proddes As String
        Dim prodphotopath As String = ""
        Try
            Query = "select * from products where no = " & ProductNo
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                prodname = Reader.GetString("productname")
                prodbrand = Reader.GetString("productbrand")
                proddes = Reader.GetString("productdescription")
                prodphotopath = Reader.GetString("photopath")
                Try
                    ProductNameLabel.Text = prodname.ToString()
                    ProductBrandLabel.Text = prodbrand.ToString()
                    ProductDescriptionLabel.Text = proddes.ToString()
                    If System.IO.File.Exists(prodphotopath) Then
                        ProductPictureBox.Image = Image.FromFile(prodphotopath)
                    Else
                        ProductPictureBox.Image = My.Resources.no_image_icon_13
                    End If
                Catch ex As Exception

                End Try
            End While
            Reader.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        MysqlCon.Close()
    End Sub

    Private Sub ProductsDataGridView_KeyUp(sender As Object, e As KeyEventArgs) Handles ProductsDataGridView.KeyUp
        ProductsDataGridView_KeyDown(sender, e)
    End Sub

    Private Sub SearchProductTextBox_TextChanged(sender As Object, e As EventArgs) Handles SearchProductTextBox.TextChanged
        Try
            OpenConnection()
        Catch ex As Exception
        End Try

        ProductsDataGridView.Rows.Clear()
        If SearchProductTextBox.Text = "" Then
            RefreshToolStripMenuItem_Click(sender, e)
        Else
            Try
                Query = "select * from products where no like '%" & SearchProductTextBox.Text & "%' OR productname like '%" & SearchProductTextBox.Text & "%' " _
                    & "OR productbrand like '%" & SearchProductTextBox.Text & "%' OR productdescription like '%" _
                    & SearchProductTextBox.Text & "%' order by productname"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                While Reader.Read
                    Dim a = Reader.GetString("productname")
                    Dim i = Reader.GetString("productbrand")
                    Dim j = Reader.GetString("productdescription")
                    Dim b = Reader.GetDecimal("availablestock")
                    Dim c = Reader.GetDecimal("minstocklimit")
                    Dim d = Reader.GetString("measuringunit")
                    Dim k = Reader.GetDecimal("supretprice")
                    Dim f = Reader.GetDecimal("purchasecost")
                    Dim g = Reader.GetDecimal("saleprice")
                    Dim h = Reader.GetInt32("no")
                    If k = 0 Then
                        ProductsDataGridView.Rows.Add(h, a, i, j, b, c, d, "", f, g)
                    Else
                        ProductsDataGridView.Rows.Add(h, a, i, j, b, c, d, k, f, g)
                    End If
                End While
                Reader.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
        ProductsDataGridView.ClearSelection()
        ProductNo = -1
        ProductPictureBox.Image = My.Resources.no_image_icon_13
        ProductNameLabel.Text = "Product name"
        ProductBrandLabel.Text = "Product brand"
        ProductDescriptionLabel.Text = "Product Description"
        For i As Integer = 0 To ProductsDataGridView.Rows.Count - 1
            If ProductsDataGridView.Rows(i).Cells(4).Value <= ProductsDataGridView.Rows(i).Cells(5).Value Then
                ProductsDataGridView.Rows(i).Cells(4).Style.BackColor = Color.Red
            End If
        Next
        MysqlCon.Close()
    End Sub

    Private Sub AddProductToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AddProductToolStripMenuItem.Click
        AddProductButton_Click(sender, e)
    End Sub

    Private Sub AddStockToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AddStockToolStripMenuItem.Click
        AddStockButton_Click(sender, e)
    End Sub

    Private Sub EditProductDetailsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EditProductDetailsToolStripMenuItem.Click
        EditProductDetailsButton_Click(sender, e)
    End Sub

    Private Sub DeleteProductToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeleteProductToolStripMenuItem.Click
        DeleteProductButton_Click(sender, e)
    End Sub
    Private Sub StocksHistoryToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles StocksHistoryToolStripMenuItem.Click
        If ProductNo < 0 Then
            MessageBox.Show("Select a product to view stocks history.", "No product selected!", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            StocksHistoryForm.ShowDialog()
        End If
    End Sub
    Private Sub PrintDisplayedProductsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PrintDisplayedProductsToolStripMenuItem.Click
        Dim ShortBondPaper As New PaperSize("Short Bond Paper", 850, 1100)
        Dim SetMargin As New Margins(25, 25, 25, 25)
        With ProductsPrintDocument.DefaultPageSettings
            .PaperSize = ShortBondPaper
            .Margins = SetMargin
        End With
        Dim b As New ToolStripButton
        b.Image = CType(PrintPreviewDialog1.Controls(1), ToolStrip).ImageList.Images(0)
        b.ToolTipText = "Print"
        b.DisplayStyle = ToolStripItemDisplayStyle.Image
        AddHandler b.Click, AddressOf printPreview_PrintClick
        CType(PrintPreviewDialog1.Controls(1), ToolStrip).Items.RemoveAt(0)
        CType(PrintPreviewDialog1.Controls(1), ToolStrip).Items.Insert(0, b)
        PrintPreviewDialog1.Document = ProductsPrintDocument
        PrintPreviewDialog1.ShowDialog()
    End Sub
    Private Sub printPreview_PrintClick(sender As Object, e As EventArgs)
        Try
            PrintDialog1.Document = ProductsPrintDocument
            If PrintDialog1.ShowDialog() = DialogResult.OK Then
                ProductsPrintDocument.Print()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private i As Integer = 0
    Private TitlePrinted As Boolean = False
    Private VerticalPrintLocation As Single = 30
    Private Sub ProductsPrintDocument_PrintPage(sender As Object, e As PrintPageEventArgs) Handles ProductsPrintDocument.PrintPage
        Dim TitlePrintFont As New Font("Arial Narrow", 9, FontStyle.Bold)
        Dim PrintFont As New Font("Arial Narrow", 8)
        Dim PrintFontBold As New Font("Arial Narrow", 8, FontStyle.Bold)
        Dim LineHeight As Single = PrintFont.GetHeight

        Dim TitlePrintLocation As Single = Convert.ToSingle(e.PageBounds.Width / 2 - e.Graphics.MeasureString("PRODUCTS", TitlePrintFont).Width / 2)
        Dim ProductNoLocation As Single = e.MarginBounds.Left
        Dim ProductNameLocation As Single = e.MarginBounds.Left + 60
        Dim ProductBrandLocation As Single = e.MarginBounds.Left + 190
        Dim ProductDescLocation As Single = e.MarginBounds.Left + 270
        Dim AvailableStockLocation As Single = e.MarginBounds.Left + 375
        Dim MinimumStockLimitLocation As Single = e.MarginBounds.Left + 445
        Dim MeasuringUnitLocation As Single = e.MarginBounds.Left + 532
        Dim SupplierRetailerPriceLocation As Single = e.MarginBounds.Left + 590
        Dim PurchaseCostLocation As Single = e.MarginBounds.Left + 665
        Dim SalePriceLocation As Single = e.MarginBounds.Left + 755

        If TitlePrinted = False Then
            e.Graphics.DrawString("PRODUCTS", TitlePrintFont, Brushes.Black, TitlePrintLocation, VerticalPrintLocation)
            VerticalPrintLocation += 20
            TitlePrinted = True
        End If

        e.Graphics.DrawString("No", PrintFontBold, Brushes.Black, ProductNoLocation, VerticalPrintLocation)
        e.Graphics.DrawString("Product Name", PrintFontBold, Brushes.Black, ProductNameLocation, VerticalPrintLocation)
        e.Graphics.DrawString("Product brand", PrintFontBold, Brushes.Black, ProductBrandLocation, VerticalPrintLocation)
        e.Graphics.DrawString("Product Description", PrintFontBold, Brushes.Black, ProductDescLocation, VerticalPrintLocation)
        e.Graphics.DrawString("Stocks", PrintFontBold, Brushes.Black, AvailableStockLocation, VerticalPrintLocation)
        e.Graphics.DrawString("Min.Stock Limit", PrintFontBold, Brushes.Black, MinimumStockLimitLocation, VerticalPrintLocation)
        e.Graphics.DrawString("Meas. Unit", PrintFontBold, Brushes.Black, MeasuringUnitLocation, VerticalPrintLocation)
        e.Graphics.DrawString("Sup.Ret.Price", PrintFontBold, Brushes.Black, SupplierRetailerPriceLocation, VerticalPrintLocation)
        e.Graphics.DrawString("Purchase Cost", PrintFontBold, Brushes.Black, PurchaseCostLocation, VerticalPrintLocation)
        e.Graphics.DrawString("Sale Price", PrintFontBold, Brushes.Black, SalePriceLocation, VerticalPrintLocation)
        VerticalPrintLocation += 15

        For row As Integer = i To ProductsDataGridView.RowCount() - 1
            If VerticalPrintLocation + LineHeight >= 1060 Then
                e.Graphics.DrawString(ProductsDataGridView.Rows(row).Cells(0).Value, PrintFont, Brushes.Black, ProductNoLocation, VerticalPrintLocation)
                e.Graphics.DrawString(SearchRecordsForm.LimitStrLen(ProductsDataGridView.Rows(row).Cells(1).Value, 20), PrintFont, Brushes.Black, ProductNameLocation, VerticalPrintLocation)
                e.Graphics.DrawString(SearchRecordsForm.LimitStrLen(ProductsDataGridView.Rows(row).Cells(2).Value, 12), PrintFont, Brushes.Black, ProductBrandLocation, VerticalPrintLocation)
                e.Graphics.DrawString(SearchRecordsForm.LimitStrLen(ProductsDataGridView.Rows(row).Cells(3).Value, 12), PrintFont, Brushes.Black, ProductDescLocation, VerticalPrintLocation)
                e.Graphics.DrawString(ProductsDataGridView.Rows(row).Cells(4).Value, PrintFont, Brushes.Black, AvailableStockLocation, VerticalPrintLocation)
                e.Graphics.DrawString(ProductsDataGridView.Rows(row).Cells(5).Value, PrintFont, Brushes.Black, MinimumStockLimitLocation, VerticalPrintLocation)
                e.Graphics.DrawString(ProductsDataGridView.Rows(row).Cells(6).Value, PrintFont, Brushes.Black, MeasuringUnitLocation, VerticalPrintLocation)
                e.Graphics.DrawString(ProductsDataGridView.Rows(row).Cells(7).Value, PrintFont, Brushes.Black, SupplierRetailerPriceLocation, VerticalPrintLocation)
                e.Graphics.DrawString(ProductsDataGridView.Rows(row).Cells(8).Value, PrintFont, Brushes.Black, PurchaseCostLocation, VerticalPrintLocation)
                e.Graphics.DrawString(ProductsDataGridView.Rows(row).Cells(9).Value, PrintFont, Brushes.Black, SalePriceLocation, VerticalPrintLocation)
                VerticalPrintLocation = 30
                e.HasMorePages = True
                i = row + 1
                Return
            Else
                e.Graphics.DrawString(ProductsDataGridView.Rows(row).Cells(0).Value, PrintFont, Brushes.Black, ProductNoLocation, VerticalPrintLocation)
                e.Graphics.DrawString(SearchRecordsForm.LimitStrLen(ProductsDataGridView.Rows(row).Cells(1).Value, 20), PrintFont, Brushes.Black, ProductNameLocation, VerticalPrintLocation)
                e.Graphics.DrawString(SearchRecordsForm.LimitStrLen(ProductsDataGridView.Rows(row).Cells(2).Value, 12), PrintFont, Brushes.Black, ProductBrandLocation, VerticalPrintLocation)
                e.Graphics.DrawString(SearchRecordsForm.LimitStrLen(ProductsDataGridView.Rows(row).Cells(3).Value, 12), PrintFont, Brushes.Black, ProductDescLocation, VerticalPrintLocation)
                e.Graphics.DrawString(ProductsDataGridView.Rows(row).Cells(4).Value, PrintFont, Brushes.Black, AvailableStockLocation, VerticalPrintLocation)
                e.Graphics.DrawString(ProductsDataGridView.Rows(row).Cells(5).Value, PrintFont, Brushes.Black, MinimumStockLimitLocation, VerticalPrintLocation)
                e.Graphics.DrawString(ProductsDataGridView.Rows(row).Cells(6).Value, PrintFont, Brushes.Black, MeasuringUnitLocation, VerticalPrintLocation)
                e.Graphics.DrawString(ProductsDataGridView.Rows(row).Cells(7).Value, PrintFont, Brushes.Black, SupplierRetailerPriceLocation, VerticalPrintLocation)
                e.Graphics.DrawString(ProductsDataGridView.Rows(row).Cells(8).Value, PrintFont, Brushes.Black, PurchaseCostLocation, VerticalPrintLocation)
                e.Graphics.DrawString(ProductsDataGridView.Rows(row).Cells(9).Value, PrintFont, Brushes.Black, SalePriceLocation, VerticalPrintLocation)
                VerticalPrintLocation += 15
            End If
        Next
        e.HasMorePages = False
        i = 0
        TitlePrinted = False
        VerticalPrintLocation = 30
    End Sub

    Private Sub RefreshToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RefreshToolStripMenuItem.Click
        InventoryForm_Activated(sender, e)
    End Sub

    Private Sub ShowAllProductsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ShowAllProductsToolStripMenuItem.Click
        ProductsDataGridView.Rows.Clear()
        Try
            OpenConnection()
        Catch ex As Exception
        End Try
        Try
            Query = "select * from products order by productname"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim a = Reader.GetString("productname")
                Dim i = Reader.GetString("productbrand")
                Dim j = Reader.GetString("productdescription")
                Dim b = Reader.GetDecimal("availablestock")
                Dim c = Reader.GetDecimal("minstocklimit")
                Dim d = Reader.GetString("measuringunit")
                Dim k = Reader.GetDecimal("supretprice")
                Dim f = Reader.GetDecimal("purchasecost")
                Dim g = Reader.GetDecimal("saleprice")
                Dim h = Reader.GetInt32("no")
                If k = 0 Then
                    ProductsDataGridView.Rows.Add(h, a, i, j, b, c, d, "", f, g)
                Else
                    ProductsDataGridView.Rows.Add(h, a, i, j, b, c, d, k, f, g)
                End If
            End While
            Reader.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        End Try
        ProductsDataGridView.ClearSelection()
        ProductNo = -1
        For i As Integer = 0 To ProductsDataGridView.Rows.Count - 1
            If ProductsDataGridView.Rows(i).Cells(4).Value <= ProductsDataGridView.Rows(i).Cells(5).Value Then
                ProductsDataGridView.Rows(i).Cells(4).Style.BackColor = Color.Red
            End If
        Next
        ProductPictureBox.Image = My.Resources.no_image_icon_13
        ProductNameLabel.Text = "Product name"
        ProductBrandLabel.Text = "Product brand"
        ProductDescriptionLabel.Text = "Product Description"
        MysqlCon.Close()
    End Sub

    Private Sub ShowLowStockProductsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ShowLowStockProductsToolStripMenuItem.Click
        ProductsDataGridView.Rows.Clear()
        Try
            OpenConnection()
        Catch ex As Exception
        End Try
        Try
            Query = "select * from products where availablestock <= minstocklimit order by productname"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim a = Reader.GetString("productname")
                Dim i = Reader.GetString("productbrand")
                Dim j = Reader.GetString("productdescription")
                Dim b = Reader.GetDecimal("availablestock")
                Dim c = Reader.GetDecimal("minstocklimit")
                Dim d = Reader.GetString("measuringunit")
                Dim k = Reader.GetDecimal("supretprice")
                Dim f = Reader.GetDecimal("purchasecost")
                Dim g = Reader.GetDecimal("saleprice")
                Dim h = Reader.GetInt32("no")
                If k = 0 Then
                    ProductsDataGridView.Rows.Add(h, a, i, j, b, c, d, "", f, g)
                Else
                    ProductsDataGridView.Rows.Add(h, a, i, j, b, c, d, k, f, g)
                End If
            End While
            Reader.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        End Try
        ProductsDataGridView.ClearSelection()
        ProductNo = -1
        For i As Integer = 0 To ProductsDataGridView.Rows.Count - 1
            If ProductsDataGridView.Rows(i).Cells(4).Value <= ProductsDataGridView.Rows(i).Cells(5).Value Then
                ProductsDataGridView.Rows(i).Cells(4).Style.BackColor = Color.Red
            End If
        Next
        ProductPictureBox.Image = My.Resources.no_image_icon_13
        ProductNameLabel.Text = "Product name"
        ProductBrandLabel.Text = "Product brand"
        ProductDescriptionLabel.Text = "Product Description"
        MysqlCon.Close()
    End Sub

    Private Sub MenuStrip1_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles MenuStrip1.ItemClicked
        RefreshToolStripMenuItem_Click(sender, e)
    End Sub
End Class