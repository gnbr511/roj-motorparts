﻿Imports MySql.Data.MySqlClient
Public Class AddProductForm
    Private ProductPhotoPath As String
    Private Sub AddButton_Click(sender As Object, e As EventArgs) Handles AddButton.Click
        OpenConnection()
        Dim MessageResult As String = MessageBox.Show("Add this product?", "Please Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If MessageResult = DialogResult.Yes Then
            Dim ProdName, MeasureUnit, ProductBrand, ProductDescription As String
            Dim AvailStock, MinStockLimit, PurchaseCost, SalePrice, supretprice As Decimal
            Dim ProductNumber As Integer
            Try
                ProductNumber = Integer.Parse(ProductNoTextBox.Text)
                If ProductNameTextBox.Text <> "" Then
                    Try
                        AvailStock = Decimal.Parse(AvailStockTextBox.Text)
                        Try
                            MinStockLimit = Decimal.Parse(MinStockLimitTextBox.Text)
                            If MeasuringUnitComboBox.Text <> "" Then
                                Try
                                    PurchaseCost = Decimal.Parse(PurchaseCostTextBox.Text)
                                    Try
                                        SalePrice = Decimal.Parse(SalePriceTextBox.Text)

                                        ProdName = ProductNameTextBox.Text()
                                        MeasureUnit = MeasuringUnitComboBox.Text()
                                        ProductBrand = ProductBrandTextBox.Text()
                                        ProductDescription = ProductDescriptionTextBox.Text()
                                        If SupRetPriceTextBox.Text = "" Or IsNumeric(SupRetPriceTextBox.Text) = False Then
                                            supretprice = 0
                                        Else
                                            supretprice = SupRetPriceTextBox.Text()
                                        End If

                                        'ADD THE PRODUCT TO THE DATABASE
                                        Try
                                            Query = "insert into products values(" & ProductNumber & ", '" & ProdName & "', '" & ProductBrand & "','" _
                                                & ProductDescription & "', '" & AvailStock & "', '" _
                                                & MinStockLimit & "', '" & MeasureUnit & "', '" & supretprice & "', '" _
                                                & PurchaseCost & "', '" & SalePrice & "', '" & ProductPhotoPath & "')"
                                            Command = New MySqlCommand(Query, MysqlCon)
                                            Reader = Command.ExecuteReader
                                            Reader.Close()
                                            MessageBox.Show("New product added successfully.", "Product added", MessageBoxButtons.OK, MessageBoxIcon.Information)

                                            InventoryForm.InventoryForm_Activated(sender, e)
                                            'RESET ADD PRODUCT FORM
                                            ProductPhotoPath = ""
                                            ProductNameTextBox.Clear()
                                            MeasuringUnitComboBox.SelectedIndex = -1
                                            MeasuringUnitComboBox.Text = ""
                                            ProductBrandTextBox.Clear()
                                            ProductDescriptionTextBox.Clear()
                                            AvailStockTextBox.Clear()
                                            MinStockLimitTextBox.Clear()
                                            SupRetPriceTextBox.Clear()
                                            PurchaseCostTextBox.Clear()
                                            SalePriceTextBox.Clear()
                                            ProductPhotoPictureBox.Image = My.Resources.uploadphoto
                                            ProductNameTextBox.Focus()
                                        Catch mysqlex As MySqlException
                                            MessageBox.Show(mysqlex.Message)
                                        End Try

                                    Catch SalePriceex As Exception
                                        MessageBox.Show("Sale Price must be numeric!", "Invalid Input",
                                                        MessageBoxButtons.OK, MessageBoxIcon.Information)
                                        With SalePriceTextBox
                                            .Focus()
                                            .SelectAll()
                                        End With
                                    End Try
                                Catch PurchaseCostex As Exception
                                    MessageBox.Show("Purchase Cost must be numeric!", "Ivalid Input",
                                                    MessageBoxButtons.OK, MessageBoxIcon.Information)
                                    With PurchaseCostTextBox
                                        .Focus()
                                        .SelectAll()
                                    End With
                                End Try
                            Else
                                MessageBox.Show("Please select or input measuring unit!", "Invalid Input",
                                                MessageBoxButtons.OK, MessageBoxIcon.Information)
                                With MeasuringUnitComboBox
                                    .Focus()
                                End With
                            End If
                        Catch MinStockLimitex As Exception
                            MessageBox.Show("Minimum stock limit must be numeric!", "Invalid Entry",
                                            MessageBoxButtons.OK, MessageBoxIcon.Information)
                            With MinStockLimitTextBox
                                .Focus()
                                .SelectAll()
                            End With
                        End Try
                    Catch AvailStockex As FormatException
                        MessageBox.Show("Available Stock must be numeric!", "Invalid Entry",
                                        MessageBoxButtons.OK, MessageBoxIcon.Information)
                        With AvailStockTextBox
                            .Focus()
                            .SelectAll()
                        End With
                    End Try
                Else
                    MessageBox.Show("Product name should not be empty!", "Invalid Entry",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information)
                    ProductNameTextBox.Focus()
                End If
            Catch ex As Exception
                MessageBox.Show("Product number must be integer!", "Invalid Entry",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information)
                With ProductNoTextBox
                    .Focus()
                    .SelectAll()
                End With
            End Try
        Else
        End If
        MysqlCon.Close()
    End Sub

    Private Sub BrowsePicButton_Click(sender As Object, e As EventArgs) Handles BrowsePicButton.Click
        Try
            OpenFileDialog1.Filter = "JPEG|*.jpg|PNG|*.png|ALL FILES|*.*"
            If OpenFileDialog1.ShowDialog = DialogResult.OK Then
                ProductPhotoPictureBox.Image = Image.FromFile(OpenFileDialog1.FileName)
                ProductPhotoPath = OpenFileDialog1.FileName.Replace("\", "\\")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CancelButton_Click(sender As Object, e As EventArgs) Handles CancelAddProductButton.Click
        Me.Close()
    End Sub

    Private Sub AddProductForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ProductNameTextBox.Focus()
        'RESET ADD PRODUCT FORM
        ProductPhotoPath = ""
        ProductNameTextBox.Clear()
        MeasuringUnitComboBox.SelectedIndex = -1
        ProductBrandTextBox.Clear()
        ProductDescriptionTextBox.Clear()
        AvailStockTextBox.Clear()
        MinStockLimitTextBox.Clear()
        SupRetPriceTextBox.Clear()
        PurchaseCostTextBox.Clear()
        SalePriceTextBox.Clear()
        ProductPhotoPictureBox.Image = My.Resources.uploadphoto
    End Sub

    Private Sub AvailStockTextBox_TextChanged(sender As Object, e As EventArgs) Handles AvailStockTextBox.TextChanged
        Try
            If AvailStockTextBox.Text = "" Then
            ElseIf MeasuringUnitComboBox.Text.ToLower = "pcs" Or MeasuringUnitComboBox.Text.ToLower = "pack" Or MeasuringUnitComboBox.Text.ToLower = "pair" Then
                Integer.Parse(AvailStockTextBox.Text)
            End If
            If IsNumeric(AvailStockTextBox.Text) = True Or AvailStockTextBox.Text = "." Then

            Else
                AvailStockTextBox.Clear()
                AvailStockTextBox.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show("The measuring unit is " & MeasuringUnitComboBox.Text & ", your available stock should be a whole number!", "Check Inputs")
            AvailStockTextBox.Clear()
            AvailStockTextBox.Focus()
        End Try
    End Sub

    Private Sub MinStockLimitTextBox_TextChanged(sender As Object, e As EventArgs) Handles MinStockLimitTextBox.TextChanged
        Try
            If MinStockLimitTextBox.Text = "" Then
            ElseIf MeasuringUnitComboBox.Text.ToLower = "pcs" Or MeasuringUnitComboBox.Text.ToLower = "pack" Or MeasuringUnitComboBox.Text.ToLower = "pair" Then
                Integer.Parse(MinStockLimitTextBox.Text)
            End If
            If IsNumeric(MinStockLimitTextBox.Text) = True Or MinStockLimitTextBox.Text = "." Then

            Else
                MinStockLimitTextBox.Clear()
                MinStockLimitTextBox.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show("The measuring unit is " & MeasuringUnitComboBox.Text & ", your available stock should be a whole number!", "Check Inputs")
            MinStockLimitTextBox.Clear()
            MinStockLimitTextBox.Focus()
        End Try
    End Sub

    Private Sub SupRetPriceTextBox_TextChanged(sender As Object, e As EventArgs) Handles SupRetPriceTextBox.TextChanged
        Try
            If SupRetPriceTextBox.Text = "" Then
            Else
                If IsNumeric(SupRetPriceTextBox.Text) = False Then
                    If SupRetPriceTextBox.Text <> "." Then
                        SupRetPriceTextBox.Clear()
                        SupRetPriceTextBox.Focus()
                    End If
                ElseIf SupRetPriceTextBox.Text <= 0 Then
                    SupRetPriceTextBox.Clear()
                    SupRetPriceTextBox.Focus()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub MeasuringUnitComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles MeasuringUnitComboBox.SelectedIndexChanged
        AvailStockTextBox_TextChanged(sender, e)
        MinStockLimitTextBox_TextChanged(sender, e)
    End Sub

    Private Sub PurchaseCostTextBox_TextChanged(sender As Object, e As EventArgs) Handles PurchaseCostTextBox.TextChanged
        Try
            If PurchaseCostTextBox.Text = "" Then
            Else
                If IsNumeric(PurchaseCostTextBox.Text) = False Then
                    If PurchaseCostTextBox.Text <> "." Then
                        PurchaseCostTextBox.Clear()
                        PurchaseCostTextBox.Focus()
                    End If
                ElseIf PurchaseCostTextBox.Text <= 0 Then
                    PurchaseCostTextBox.Clear()
                    PurchaseCostTextBox.Focus()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SalePriceTextBox_TextChanged(sender As Object, e As EventArgs) Handles SalePriceTextBox.TextChanged
        Try
            If SalePriceTextBox.Text = "" Then
            Else
                If IsNumeric(SalePriceTextBox.Text) = False Then
                    If SalePriceTextBox.Text <> "." Then
                        SalePriceTextBox.Clear()
                        SalePriceTextBox.Focus()
                    End If
                ElseIf SalePriceTextBox.Text <= 0 Then
                        SalePriceTextBox.Clear()
                    SalePriceTextBox.Focus()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class