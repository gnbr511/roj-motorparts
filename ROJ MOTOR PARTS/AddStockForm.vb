﻿Imports MySql.Data.MySqlClient
Public Class AddStockForm
    Private ProdAvailStocks As Decimal
    Private Sub AddStockForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        AddStockTextBox.Focus()
    End Sub
    Private Sub AddButton_Click(sender As Object, e As EventArgs) Handles AddButton.Click
        ProdAvailStocks = InventoryForm.ProductsDataGridView.CurrentRow.Cells(4).Value
        Dim messagestring As String = MessageBox.Show("Add to stock?", "Please confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If messagestring = DialogResult.Yes Then
            Dim AddQuantity As Decimal
            Try
                OpenConnection()
                AddQuantity = Decimal.Parse(AddStockTextBox.Text)
                Query = "update products set availablestock = availablestock + " _
                & AddQuantity & " where no =" & InventoryForm.ProductNo
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                Reader.Close()
                Try
                    Query = "insert into stockshistory values(" _
                    & InventoryForm.ProductNo & ", (select sysdate()), " _
                    & AddQuantity & ", " & AddQuantity + ProdAvailStocks & ")"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    Reader.Close()
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
                MessageBox.Show("Successfully Added!")
                AddStockTextBox.Clear()
                Me.Close()
            Catch ex As Exception
                MessageBox.Show("Quantity to be added must be numeric!", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try
        Else
            With AddStockTextBox
                .Focus()
                .SelectAll()
            End With
        End If
        MysqlCon.Close()
    End Sub
    Private Sub AddStockForm_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        AddStockTextBox.Focus()
    End Sub
    Private Sub AddStockTextBox_TextChanged(sender As Object, e As EventArgs) Handles AddStockTextBox.TextChanged
        Dim MeasuringUnit As String = InventoryForm.ProductsDataGridView.CurrentRow.Cells(6).Value
        Try
            If AddStockTextBox.Text = "" Then
            ElseIf MeasuringUnit.ToLower = "pcs" Or MeasuringUnit.ToLower = "pack" Or MeasuringUnit.ToLower = "pair" Then
                If AddStockTextBox.Text = "-" Then
                ElseIf AddStockTextBox.Text < 0 Then
                    Integer.Parse(AddStockTextBox.Text)
                Else
                    Integer.Parse(AddStockTextBox.Text)
                End If
            End If
            If IsNumeric(AddStockTextBox.Text) = True Or AddStockTextBox.Text = "." Or AddStockTextBox.Text = "-" Then

            Else
                AddStockTextBox.Clear()
                AddStockTextBox.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show("The measuring unit is " & MeasuringUnit & ", number of stocks to be added should be a whole number!", "Check Inputs")
            AddStockTextBox.Clear()
            AddStockTextBox.Focus()
        End Try
    End Sub
End Class