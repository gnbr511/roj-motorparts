﻿Imports MySql.Data.MySqlClient
Public Class LoginForm
    Private Sub LoginButton_Click(sender As Object, e As EventArgs) Handles LoginButton.Click

        Try
            OpenConnection()

            Query = "select * from users where username = '" & UsernameTextBox.Text & "'" _
                & "and password = '" & PasswordTextBox.Text & "'"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            Dim Count As Integer = 0
            While Reader.Read
                Count += 1
            End While
            Reader.Close()

            If Count > 0 Then
                Me.Hide()
                StoreForm.Show()
            Else
                MessageBox.Show("Wrong username or password")
                With PasswordTextBox
                    .Focus()
                    .SelectAll()
                End With
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        MysqlCon.Close()
    End Sub
End Class
