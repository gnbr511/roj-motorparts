﻿Imports MySql.Data.MySqlClient
'Program Name: ePOSIRMS (Electronic Point of Sale, Inventory, and Record Management System)
'Program Description: This program is a thesis project of Ginber Candelario and Ian Jay Lomlom for ROJ Motorparts.
'Programmer: GINBER J. CANDELARIO, BSIT
Public Class UserSettingsForm
    Friend SelectedUser, NameSelecteduser, UserType As String
    Friend UserNumber As Integer
    Friend Sub UserSettingsForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            OpenConnection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Dim no As Integer = 0
        Try
            Query = "select * from users order by type, name"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim a = Reader.GetString("name")
                Dim b = Reader.GetString("username")
                Dim c = Reader.GetString("password")
                Dim d = Reader.GetString("Type")
                Dim f = Reader.GetString("no")
                no += 1
                UsersDataGridView.Rows.Add(no, f, a, b, c, d)
            End While
            Reader.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        MysqlCon.Close()
    End Sub

    Private Sub UserSettingsForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        UsersDataGridView.Rows.Clear()
        UsersDataGridView.ClearSelection()
    End Sub

    Private Sub AddUserButton_Click(sender As Object, e As EventArgs) Handles AddUserButton.Click
        AddUserForm.ShowDialog()
    End Sub

    Private Sub DeleteUserButton_Click(sender As Object, e As EventArgs) Handles DeleteUserButton.Click
        Try
            OpenConnection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Dim admincount As Integer = 0
        Try
            Query = "select * from users where type = 'Administrator'"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                admincount += 1
            End While
            Reader.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        If UserType.ToLower = "user" Then
            Dim Message As String = "Are you sure to delete " & NameSelecteduser & "?"
            Dim mesres As DialogResult = MessageBox.Show(Message, "Please Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If mesres = DialogResult.Yes Then
                Try
                    Query = "delete from users where no = '" & UserNumber & "'"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    Reader.Close()
                    UsersDataGridView.Rows.Clear()
                    UserSettingsForm_Load(sender, e)
                    UsersDataGridView.Rows(0).Selected = True
                    UserNumber = UsersDataGridView.CurrentRow.Cells(1).Value
                    NameSelecteduser = UsersDataGridView.CurrentRow.Cells(2).Value
                    SelectedUser = UsersDataGridView.CurrentRow.Cells(3).Value
                    UserType = UsersDataGridView.CurrentRow.Cells(5).Value
                    MessageBox.Show("The selected user has been successfully deleted!", "", MessageBoxButtons.OK)
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            End If
        ElseIf admincount < 2 Then
            MessageBox.Show("There should be at least one (1) Administrator user left. Try adding another Administrator user before deleting this user.", "Unable to delete this user.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            Dim Message As String = "Are you sure to delete " & NameSelecteduser & "?"
            Dim mesres As DialogResult = MessageBox.Show(Message, "Please Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If mesres = DialogResult.Yes Then
                Try
                    Query = "delete from users where no = '" & UserNumber & "'"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    Reader.Close()
                    UsersDataGridView.Rows.Clear()
                    UserSettingsForm_Load(sender, e)
                    UsersDataGridView.Rows(0).Selected = True
                    UserNumber = UsersDataGridView.CurrentRow.Cells(1).Value
                    NameSelecteduser = UsersDataGridView.CurrentRow.Cells(2).Value
                    SelectedUser = UsersDataGridView.CurrentRow.Cells(3).Value
                    UserType = UsersDataGridView.CurrentRow.Cells(5).Value
                    MessageBox.Show("The selected user has been successfully deleted!", "", MessageBoxButtons.OK)
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            End If
        End If
        MysqlCon.Close()
    End Sub
    Private Sub UsersDataGridView_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles UsersDataGridView.CellClick
        UserNumber = UsersDataGridView.CurrentRow.Cells(1).Value
        NameSelecteduser = UsersDataGridView.CurrentRow.Cells(2).Value
        SelectedUser = UsersDataGridView.CurrentRow.Cells(3).Value
        UserType = UsersDataGridView.CurrentRow.Cells(5).Value
    End Sub

    Private Sub UsersDataGridView_KeyDown(sender As Object, e As KeyEventArgs) Handles UsersDataGridView.KeyDown
        UserNumber = UsersDataGridView.CurrentRow.Cells(1).Value
        NameSelecteduser = UsersDataGridView.CurrentRow.Cells(2).Value
        SelectedUser = UsersDataGridView.CurrentRow.Cells(3).Value
        UserType = UsersDataGridView.CurrentRow.Cells(5).Value
    End Sub

    Private Sub EditButton_Click(sender As Object, e As EventArgs) Handles EditButton.Click
        EditUserForm.ShowDialog()
    End Sub

    Private Sub UsersDataGridView_KeyUp(sender As Object, e As KeyEventArgs) Handles UsersDataGridView.KeyUp
        UserNumber = UsersDataGridView.CurrentRow.Cells(1).Value
        NameSelecteduser = UsersDataGridView.CurrentRow.Cells(2).Value
        SelectedUser = UsersDataGridView.CurrentRow.Cells(3).Value
        UserType = UsersDataGridView.CurrentRow.Cells(5).Value
    End Sub

End Class