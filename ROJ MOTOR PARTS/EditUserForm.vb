﻿Imports MySql.Data.MySqlClient
Public Class EditUserForm
    Private Sub EditUserForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            OpenConnection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Dim name, username, userpassword, type As String
        Dim typeno As Integer = -1
        Try
            Query = "select * from users where no = '" _
            & UserSettingsForm.UserNumber & "'"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                name = Reader.GetString("name")
                username = Reader.GetString("username")
                userpassword = Reader.GetString("password")
                type = Reader.GetString("type")
            End While
            Reader.Close()
            If type.ToLower = "user" Then
                typeno = 0
            ElseIf type.ToLower = "administrator" Then
                typeno = 1
            Else
                typeno = 0
            End If
            NameOfUserTextBox.Text = name
            UsernameTextBox.Text = username
            PasswordTextBox.Text = userpassword
            UserTypeComboBox.Text = UserTypeComboBox.Items(typeno)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        MysqlCon.Close()
    End Sub

    Private Sub CancelButton_Click(sender As Object, e As EventArgs) Handles CancelButton.Click
        Me.Close()
    End Sub

    Private Sub UpdateUserButton_Click(sender As Object, e As EventArgs) Handles UpdateUserButton.Click

        Try
            OpenConnection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Dim admincount As Integer = 0
        Try
            Query = "select * from users where type = 'Administrator'"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                admincount += 1
            End While

            Reader.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        If NameOfUserTextBox.Text = "" Then
            MessageBox.Show("Name of the user should not be empty!", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information)
            NameOfUserTextBox.Focus()
        Else
            If UsernameTextBox.Text = "" Then
                MessageBox.Show("Username should not be empty!", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information)
                UsernameTextBox.Focus()
            Else
                If PasswordTextBox.Text = "" Then
                    MessageBox.Show("Password should not be empty!", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    PasswordTextBox.Focus()
                Else
                    If UserTypeComboBox.Text = "" Then
                        MessageBox.Show("Please select type of user!", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        UserTypeComboBox.Focus()
                    Else
                        If admincount < 2 And UserSettingsForm.UserType = "Administrator" Then
                            If UserTypeComboBox.Text <> "Administrator" Then
                                MessageBox.Show("There should be at least one (1) Administrator user left. Try adding another Administrator user before turning this Admin user to user.")
                            Else
                                Try
                                    Query = "update users set name = '" _
                                        & NameOfUserTextBox.Text & "', username = '" _
                                        & UsernameTextBox.Text _
                                        & "', password = '" & PasswordTextBox.Text _
                                        & "', type = '" & UserTypeComboBox.Text _
                                        & "' where no = '" _
                                        & UserSettingsForm.UserNumber & "'"
                                    Command = New MySqlCommand(Query, MysqlCon)
                                    Reader = Command.ExecuteReader
                                    Reader.Close()
                                    UserSettingsForm.UsersDataGridView.Rows.Clear()
                                    UserSettingsForm.UserSettingsForm_Load(sender, e)
                                    MessageBox.Show("Update successful!", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                    Me.Close()
                                Catch ex As Exception
                                    MessageBox.Show(ex.Message)
                                End Try
                            End If
                        Else
                            Try
                                Query = "update users set name = '" & NameOfUserTextBox.Text & "', username = '" & UsernameTextBox.Text _
                                    & "', password = '" & PasswordTextBox.Text & "', type = '" & UserTypeComboBox.Text & "' where no = '" _
                                    & UserSettingsForm.UserNumber & "'"
                                Command = New MySqlCommand(Query, MysqlCon)
                                Reader = Command.ExecuteReader
                                Reader.Close()
                                UserSettingsForm.UsersDataGridView.Rows.Clear()
                                UserSettingsForm.UserSettingsForm_Load(sender, e)
                                MessageBox.Show("Update successful!", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                Me.Close()
                            Catch ex As Exception
                                MessageBox.Show(ex.Message)
                            End Try
                        End If
                    End If
                End If
            End If
        End If
        MysqlCon.Close()
    End Sub
End Class