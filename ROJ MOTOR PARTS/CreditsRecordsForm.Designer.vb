﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CreditsRecordsForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CreditsRecordsForm))
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SearchCreditsTextBox = New System.Windows.Forms.TextBox()
        Me.PrintButton = New System.Windows.Forms.Button()
        Me.DeleteCreditsButton = New System.Windows.Forms.Button()
        Me.MoreInfoButton = New System.Windows.Forms.Button()
        Me.CreditsDataGridView = New System.Windows.Forms.DataGridView()
        Me.DebtorsID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CusNameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContactNoColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PaymentColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DiscountGiven = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AmountPaidColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RemainBalColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.MoreInfoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.LoanedProductsDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OrderNo2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductnameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductbrandColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductDescriptionColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PurchaseCost = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SalePriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QuantityColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MeasuringunitColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalPriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RdiscountColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RfinalPriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DebtorsID2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Debtor2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.UnpaidLoanedProdSearchTextBox = New System.Windows.Forms.TextBox()
        Me.PrintPreviewDialog1 = New System.Windows.Forms.PrintPreviewDialog()
        Me.CreditsPrintDocument = New System.Drawing.Printing.PrintDocument()
        Me.PrintDialog1 = New System.Windows.Forms.PrintDialog()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox7.SuspendLayout()
        CType(Me.CreditsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.LoanedProductsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox7
        '
        Me.GroupBox7.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox7.Controls.Add(Me.Label1)
        Me.GroupBox7.Controls.Add(Me.SearchCreditsTextBox)
        Me.GroupBox7.Controls.Add(Me.PrintButton)
        Me.GroupBox7.Controls.Add(Me.DeleteCreditsButton)
        Me.GroupBox7.Controls.Add(Me.MoreInfoButton)
        Me.GroupBox7.Controls.Add(Me.CreditsDataGridView)
        Me.GroupBox7.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox7.Location = New System.Drawing.Point(13, 9)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(714, 321)
        Me.GroupBox7.TabIndex = 30
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "CREDITS"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(338, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 18)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Search"
        '
        'SearchCreditsTextBox
        '
        Me.SearchCreditsTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SearchCreditsTextBox.Location = New System.Drawing.Point(402, 18)
        Me.SearchCreditsTextBox.Name = "SearchCreditsTextBox"
        Me.SearchCreditsTextBox.Size = New System.Drawing.Size(301, 23)
        Me.SearchCreditsTextBox.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.SearchCreditsTextBox, "Tip: You can searh by date with ""Year-Month-Date"" Format.")
        '
        'PrintButton
        '
        Me.PrintButton.Enabled = False
        Me.PrintButton.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PrintButton.Location = New System.Drawing.Point(122, 276)
        Me.PrintButton.Name = "PrintButton"
        Me.PrintButton.Size = New System.Drawing.Size(105, 33)
        Me.PrintButton.TabIndex = 4
        Me.PrintButton.Text = "&Print"
        Me.PrintButton.UseVisualStyleBackColor = True
        Me.PrintButton.Visible = False
        '
        'DeleteCreditsButton
        '
        Me.DeleteCreditsButton.Enabled = False
        Me.DeleteCreditsButton.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DeleteCreditsButton.Location = New System.Drawing.Point(234, 276)
        Me.DeleteCreditsButton.Name = "DeleteCreditsButton"
        Me.DeleteCreditsButton.Size = New System.Drawing.Size(105, 33)
        Me.DeleteCreditsButton.TabIndex = 3
        Me.DeleteCreditsButton.Text = "&Delete"
        Me.DeleteCreditsButton.UseVisualStyleBackColor = True
        Me.DeleteCreditsButton.Visible = False
        '
        'MoreInfoButton
        '
        Me.MoreInfoButton.Enabled = False
        Me.MoreInfoButton.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MoreInfoButton.Location = New System.Drawing.Point(10, 276)
        Me.MoreInfoButton.Name = "MoreInfoButton"
        Me.MoreInfoButton.Size = New System.Drawing.Size(105, 33)
        Me.MoreInfoButton.TabIndex = 2
        Me.MoreInfoButton.Text = "&More Info"
        Me.MoreInfoButton.UseVisualStyleBackColor = True
        '
        'CreditsDataGridView
        '
        Me.CreditsDataGridView.AllowUserToAddRows = False
        Me.CreditsDataGridView.AllowUserToDeleteRows = False
        Me.CreditsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.CreditsDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DebtorsID, Me.CusNameColumn, Me.ContactNoColumn, Me.PaymentColumn, Me.DiscountGiven, Me.AmountPaidColumn, Me.RemainBalColumn})
        Me.CreditsDataGridView.Location = New System.Drawing.Point(10, 50)
        Me.CreditsDataGridView.MultiSelect = False
        Me.CreditsDataGridView.Name = "CreditsDataGridView"
        Me.CreditsDataGridView.ReadOnly = True
        Me.CreditsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.CreditsDataGridView.Size = New System.Drawing.Size(693, 219)
        Me.CreditsDataGridView.TabIndex = 0
        Me.CreditsDataGridView.TabStop = False
        '
        'DebtorsID
        '
        Me.DebtorsID.HeaderText = "Debtors ID"
        Me.DebtorsID.Name = "DebtorsID"
        Me.DebtorsID.ReadOnly = True
        Me.DebtorsID.Width = 120
        '
        'CusNameColumn
        '
        Me.CusNameColumn.HeaderText = "Debtor"
        Me.CusNameColumn.Name = "CusNameColumn"
        Me.CusNameColumn.ReadOnly = True
        Me.CusNameColumn.Width = 130
        '
        'ContactNoColumn
        '
        Me.ContactNoColumn.HeaderText = "Contact No"
        Me.ContactNoColumn.Name = "ContactNoColumn"
        Me.ContactNoColumn.ReadOnly = True
        '
        'PaymentColumn
        '
        Me.PaymentColumn.HeaderText = "Payment"
        Me.PaymentColumn.Name = "PaymentColumn"
        Me.PaymentColumn.ReadOnly = True
        Me.PaymentColumn.Width = 75
        '
        'DiscountGiven
        '
        Me.DiscountGiven.HeaderText = "Discount"
        Me.DiscountGiven.Name = "DiscountGiven"
        Me.DiscountGiven.ReadOnly = True
        Me.DiscountGiven.Width = 75
        '
        'AmountPaidColumn
        '
        Me.AmountPaidColumn.HeaderText = "Amount Paid"
        Me.AmountPaidColumn.Name = "AmountPaidColumn"
        Me.AmountPaidColumn.ReadOnly = True
        Me.AmountPaidColumn.Width = 75
        '
        'RemainBalColumn
        '
        Me.RemainBalColumn.HeaderText = "Remaining Payment"
        Me.RemainBalColumn.Name = "RemainBalColumn"
        Me.RemainBalColumn.ReadOnly = True
        Me.RemainBalColumn.Width = 75
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MoreInfoToolStripMenuItem, Me.DeleteToolStripMenuItem, Me.PrintToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(168, 70)
        '
        'MoreInfoToolStripMenuItem
        '
        Me.MoreInfoToolStripMenuItem.Name = "MoreInfoToolStripMenuItem"
        Me.MoreInfoToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.M), System.Windows.Forms.Keys)
        Me.MoreInfoToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.MoreInfoToolStripMenuItem.Text = "More Info"
        '
        'DeleteToolStripMenuItem
        '
        Me.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem"
        Me.DeleteToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.DeleteToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.DeleteToolStripMenuItem.Text = "Delete"
        '
        'PrintToolStripMenuItem
        '
        Me.PrintToolStripMenuItem.Name = "PrintToolStripMenuItem"
        Me.PrintToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.PrintToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.PrintToolStripMenuItem.Text = "Print"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.LoanedProductsDataGridView)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.UnpaidLoanedProdSearchTextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 340)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(714, 308)
        Me.GroupBox1.TabIndex = 31
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "LOANED PRODUCTS"
        '
        'LoanedProductsDataGridView
        '
        Me.LoanedProductsDataGridView.AllowUserToAddRows = False
        Me.LoanedProductsDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.White
        Me.LoanedProductsDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.LoanedProductsDataGridView.ColumnHeadersHeight = 40
        Me.LoanedProductsDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.OrderNo2, Me.ProductNo, Me.ProductnameColumn, Me.ProductbrandColumn, Me.ProductDescriptionColumn, Me.PurchaseCost, Me.SalePriceColumn, Me.QuantityColumn, Me.MeasuringunitColumn, Me.TotalPriceColumn, Me.RdiscountColumn, Me.RfinalPriceColumn, Me.DebtorsID2, Me.Debtor2})
        Me.LoanedProductsDataGridView.Location = New System.Drawing.Point(10, 53)
        Me.LoanedProductsDataGridView.Name = "LoanedProductsDataGridView"
        Me.LoanedProductsDataGridView.ReadOnly = True
        Me.LoanedProductsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.LoanedProductsDataGridView.Size = New System.Drawing.Size(693, 241)
        Me.LoanedProductsDataGridView.TabIndex = 18
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Date"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 80
        '
        'OrderNo2
        '
        Me.OrderNo2.HeaderText = "Order No"
        Me.OrderNo2.Name = "OrderNo2"
        Me.OrderNo2.ReadOnly = True
        Me.OrderNo2.Width = 120
        '
        'ProductNo
        '
        Me.ProductNo.HeaderText = "Product No"
        Me.ProductNo.Name = "ProductNo"
        Me.ProductNo.ReadOnly = True
        Me.ProductNo.Width = 90
        '
        'ProductnameColumn
        '
        Me.ProductnameColumn.HeaderText = "Product Name"
        Me.ProductnameColumn.Name = "ProductnameColumn"
        Me.ProductnameColumn.ReadOnly = True
        Me.ProductnameColumn.Width = 120
        '
        'ProductbrandColumn
        '
        Me.ProductbrandColumn.HeaderText = "Product Brand"
        Me.ProductbrandColumn.Name = "ProductbrandColumn"
        Me.ProductbrandColumn.ReadOnly = True
        Me.ProductbrandColumn.Width = 110
        '
        'ProductDescriptionColumn
        '
        Me.ProductDescriptionColumn.HeaderText = "Product Description"
        Me.ProductDescriptionColumn.Name = "ProductDescriptionColumn"
        Me.ProductDescriptionColumn.ReadOnly = True
        Me.ProductDescriptionColumn.Width = 110
        '
        'PurchaseCost
        '
        Me.PurchaseCost.HeaderText = "Purchase Cost"
        Me.PurchaseCost.Name = "PurchaseCost"
        Me.PurchaseCost.ReadOnly = True
        Me.PurchaseCost.Width = 70
        '
        'SalePriceColumn
        '
        Me.SalePriceColumn.HeaderText = "Sale Price"
        Me.SalePriceColumn.Name = "SalePriceColumn"
        Me.SalePriceColumn.ReadOnly = True
        Me.SalePriceColumn.Width = 70
        '
        'QuantityColumn
        '
        Me.QuantityColumn.HeaderText = "Quantity"
        Me.QuantityColumn.Name = "QuantityColumn"
        Me.QuantityColumn.ReadOnly = True
        Me.QuantityColumn.Width = 70
        '
        'MeasuringunitColumn
        '
        Me.MeasuringunitColumn.HeaderText = "Measuring Unit"
        Me.MeasuringunitColumn.Name = "MeasuringunitColumn"
        Me.MeasuringunitColumn.ReadOnly = True
        Me.MeasuringunitColumn.Width = 70
        '
        'TotalPriceColumn
        '
        Me.TotalPriceColumn.HeaderText = "Total Price"
        Me.TotalPriceColumn.Name = "TotalPriceColumn"
        Me.TotalPriceColumn.ReadOnly = True
        Me.TotalPriceColumn.Width = 70
        '
        'RdiscountColumn
        '
        Me.RdiscountColumn.HeaderText = "Discount"
        Me.RdiscountColumn.Name = "RdiscountColumn"
        Me.RdiscountColumn.ReadOnly = True
        Me.RdiscountColumn.Width = 70
        '
        'RfinalPriceColumn
        '
        Me.RfinalPriceColumn.HeaderText = "Final Price"
        Me.RfinalPriceColumn.Name = "RfinalPriceColumn"
        Me.RfinalPriceColumn.ReadOnly = True
        Me.RfinalPriceColumn.Width = 70
        '
        'DebtorsID2
        '
        Me.DebtorsID2.HeaderText = "Debtors ID"
        Me.DebtorsID2.Name = "DebtorsID2"
        Me.DebtorsID2.ReadOnly = True
        Me.DebtorsID2.Width = 120
        '
        'Debtor2
        '
        Me.Debtor2.HeaderText = "Debtor"
        Me.Debtor2.Name = "Debtor2"
        Me.Debtor2.ReadOnly = True
        Me.Debtor2.Width = 130
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(338, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 18)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Search"
        '
        'UnpaidLoanedProdSearchTextBox
        '
        Me.UnpaidLoanedProdSearchTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UnpaidLoanedProdSearchTextBox.Location = New System.Drawing.Point(402, 21)
        Me.UnpaidLoanedProdSearchTextBox.Name = "UnpaidLoanedProdSearchTextBox"
        Me.UnpaidLoanedProdSearchTextBox.Size = New System.Drawing.Size(301, 23)
        Me.UnpaidLoanedProdSearchTextBox.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.UnpaidLoanedProdSearchTextBox, "Tip: You can searh by date with ""Year-Month-Date"" Format.")
        '
        'PrintPreviewDialog1
        '
        Me.PrintPreviewDialog1.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPreviewDialog1.Enabled = True
        Me.PrintPreviewDialog1.Icon = CType(resources.GetObject("PrintPreviewDialog1.Icon"), System.Drawing.Icon)
        Me.PrintPreviewDialog1.Name = "PrintPreviewDialog1"
        Me.PrintPreviewDialog1.Visible = False
        '
        'CreditsPrintDocument
        '
        '
        'PrintDialog1
        '
        Me.PrintDialog1.UseEXDialog = True
        '
        'CreditsRecordsForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.ROJBackground
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(741, 661)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox7)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "CreditsRecordsForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Credits Records"
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        CType(Me.CreditsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.LoanedProductsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents MoreInfoButton As Button
    Friend WithEvents CreditsDataGridView As DataGridView
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents DeleteCreditsButton As Button
    Friend WithEvents PrintButton As Button
    Friend WithEvents PrintPreviewDialog1 As PrintPreviewDialog
    Friend WithEvents CreditsPrintDocument As Printing.PrintDocument
    Friend WithEvents PrintDialog1 As PrintDialog
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents MoreInfoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DeleteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrintToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Label1 As Label
    Friend WithEvents SearchCreditsTextBox As TextBox
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents Label2 As Label
    Friend WithEvents UnpaidLoanedProdSearchTextBox As TextBox
    Friend WithEvents LoanedProductsDataGridView As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents OrderNo2 As DataGridViewTextBoxColumn
    Friend WithEvents ProductNo As DataGridViewTextBoxColumn
    Friend WithEvents ProductnameColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductbrandColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductDescriptionColumn As DataGridViewTextBoxColumn
    Friend WithEvents PurchaseCost As DataGridViewTextBoxColumn
    Friend WithEvents SalePriceColumn As DataGridViewTextBoxColumn
    Friend WithEvents QuantityColumn As DataGridViewTextBoxColumn
    Friend WithEvents MeasuringunitColumn As DataGridViewTextBoxColumn
    Friend WithEvents TotalPriceColumn As DataGridViewTextBoxColumn
    Friend WithEvents RdiscountColumn As DataGridViewTextBoxColumn
    Friend WithEvents RfinalPriceColumn As DataGridViewTextBoxColumn
    Friend WithEvents DebtorsID2 As DataGridViewTextBoxColumn
    Friend WithEvents Debtor2 As DataGridViewTextBoxColumn
    Friend WithEvents DebtorsID As DataGridViewTextBoxColumn
    Friend WithEvents CusNameColumn As DataGridViewTextBoxColumn
    Friend WithEvents ContactNoColumn As DataGridViewTextBoxColumn
    Friend WithEvents PaymentColumn As DataGridViewTextBoxColumn
    Friend WithEvents DiscountGiven As DataGridViewTextBoxColumn
    Friend WithEvents AmountPaidColumn As DataGridViewTextBoxColumn
    Friend WithEvents RemainBalColumn As DataGridViewTextBoxColumn
End Class
