﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UpdateInfoForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UpdateInfoForm))
        Me.BrowseDebtorsPhotoButton = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.DebtorsPhotoPictureBox = New System.Windows.Forms.PictureBox()
        Me.RemoveInfoButton = New System.Windows.Forms.Button()
        Me.CancelCreditsButton = New System.Windows.Forms.Button()
        Me.UpdateButton = New System.Windows.Forms.Button()
        Me.AddInfoButton = New System.Windows.Forms.Button()
        Me.MoreInfoListBox = New System.Windows.Forms.ListBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.MoreInfoTextBox = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CustomerContactTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CustomerFullNameTextBox = New System.Windows.Forms.TextBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        CType(Me.DebtorsPhotoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BrowseDebtorsPhotoButton
        '
        Me.BrowseDebtorsPhotoButton.Location = New System.Drawing.Point(281, 70)
        Me.BrowseDebtorsPhotoButton.Name = "BrowseDebtorsPhotoButton"
        Me.BrowseDebtorsPhotoButton.Size = New System.Drawing.Size(99, 31)
        Me.BrowseDebtorsPhotoButton.TabIndex = 27
        Me.BrowseDebtorsPhotoButton.Text = "Browse Photo"
        Me.BrowseDebtorsPhotoButton.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(25, 70)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(104, 18)
        Me.Label4.TabIndex = 26
        Me.Label4.Text = "Debtor's Photo"
        '
        'DebtorsPhotoPictureBox
        '
        Me.DebtorsPhotoPictureBox.Image = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.uploadphoto
        Me.DebtorsPhotoPictureBox.Location = New System.Drawing.Point(173, 10)
        Me.DebtorsPhotoPictureBox.Name = "DebtorsPhotoPictureBox"
        Me.DebtorsPhotoPictureBox.Size = New System.Drawing.Size(102, 91)
        Me.DebtorsPhotoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.DebtorsPhotoPictureBox.TabIndex = 25
        Me.DebtorsPhotoPictureBox.TabStop = False
        '
        'RemoveInfoButton
        '
        Me.RemoveInfoButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RemoveInfoButton.Location = New System.Drawing.Point(304, 181)
        Me.RemoveInfoButton.Name = "RemoveInfoButton"
        Me.RemoveInfoButton.Size = New System.Drawing.Size(76, 29)
        Me.RemoveInfoButton.TabIndex = 24
        Me.RemoveInfoButton.Text = "Remove"
        Me.RemoveInfoButton.UseVisualStyleBackColor = True
        '
        'CancelCreditsButton
        '
        Me.CancelCreditsButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.CancelCreditsButton.Location = New System.Drawing.Point(281, 358)
        Me.CancelCreditsButton.Name = "CancelCreditsButton"
        Me.CancelCreditsButton.Size = New System.Drawing.Size(99, 41)
        Me.CancelCreditsButton.TabIndex = 23
        Me.CancelCreditsButton.Text = "CANCEL"
        Me.CancelCreditsButton.UseVisualStyleBackColor = True
        '
        'UpdateButton
        '
        Me.UpdateButton.Location = New System.Drawing.Point(176, 358)
        Me.UpdateButton.Name = "UpdateButton"
        Me.UpdateButton.Size = New System.Drawing.Size(99, 41)
        Me.UpdateButton.TabIndex = 22
        Me.UpdateButton.Text = "&Update"
        Me.UpdateButton.UseVisualStyleBackColor = True
        '
        'AddInfoButton
        '
        Me.AddInfoButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AddInfoButton.Location = New System.Drawing.Point(223, 181)
        Me.AddInfoButton.Name = "AddInfoButton"
        Me.AddInfoButton.Size = New System.Drawing.Size(76, 29)
        Me.AddInfoButton.TabIndex = 21
        Me.AddInfoButton.Text = "Add Info"
        Me.AddInfoButton.UseVisualStyleBackColor = True
        '
        'MoreInfoListBox
        '
        Me.MoreInfoListBox.FormattingEnabled = True
        Me.MoreInfoListBox.ItemHeight = 14
        Me.MoreInfoListBox.Location = New System.Drawing.Point(173, 242)
        Me.MoreInfoListBox.Name = "MoreInfoListBox"
        Me.MoreInfoListBox.Size = New System.Drawing.Size(207, 102)
        Me.MoreInfoListBox.TabIndex = 20
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(25, 216)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(122, 18)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "More Information"
        '
        'MoreInfoTextBox
        '
        Me.MoreInfoTextBox.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MoreInfoTextBox.Location = New System.Drawing.Point(173, 213)
        Me.MoreInfoTextBox.Name = "MoreInfoTextBox"
        Me.MoreInfoTextBox.Size = New System.Drawing.Size(207, 26)
        Me.MoreInfoTextBox.TabIndex = 18
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(25, 139)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 18)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "Contact"
        '
        'CustomerContactTextBox
        '
        Me.CustomerContactTextBox.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CustomerContactTextBox.Location = New System.Drawing.Point(173, 136)
        Me.CustomerContactTextBox.Name = "CustomerContactTextBox"
        Me.CustomerContactTextBox.Size = New System.Drawing.Size(207, 26)
        Me.CustomerContactTextBox.TabIndex = 16
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(25, 110)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(131, 18)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Debtor's Full Name"
        '
        'CustomerFullNameTextBox
        '
        Me.CustomerFullNameTextBox.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CustomerFullNameTextBox.Location = New System.Drawing.Point(173, 107)
        Me.CustomerFullNameTextBox.Name = "CustomerFullNameTextBox"
        Me.CustomerFullNameTextBox.Size = New System.Drawing.Size(207, 26)
        Me.CustomerFullNameTextBox.TabIndex = 14
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'UpdateInfoForm
        '
        Me.AcceptButton = Me.AddInfoButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.ROJBackground
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(405, 409)
        Me.Controls.Add(Me.BrowseDebtorsPhotoButton)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.DebtorsPhotoPictureBox)
        Me.Controls.Add(Me.RemoveInfoButton)
        Me.Controls.Add(Me.CancelCreditsButton)
        Me.Controls.Add(Me.UpdateButton)
        Me.Controls.Add(Me.AddInfoButton)
        Me.Controls.Add(Me.MoreInfoListBox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.MoreInfoTextBox)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.CustomerContactTextBox)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CustomerFullNameTextBox)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "UpdateInfoForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Update Debtor's Information"
        CType(Me.DebtorsPhotoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BrowseDebtorsPhotoButton As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents DebtorsPhotoPictureBox As PictureBox
    Friend WithEvents RemoveInfoButton As Button
    Friend WithEvents CancelCreditsButton As Button
    Friend WithEvents UpdateButton As Button
    Friend WithEvents AddInfoButton As Button
    Friend WithEvents MoreInfoListBox As ListBox
    Friend WithEvents Label3 As Label
    Friend WithEvents MoreInfoTextBox As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents CustomerContactTextBox As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents CustomerFullNameTextBox As TextBox
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
End Class
