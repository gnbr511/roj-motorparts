﻿Public Class RestoreDataForm
    Public filepath As String
    Private Sub BrowseButton_Click(sender As Object, e As EventArgs) Handles BrowseButton.Click
        OpenFileDialog1.Filter = "SQL|*.sql|ALL FILES|*.*"
        If OpenFileDialog1.ShowDialog = DialogResult.OK Then
            FilePathTextBox.Text = OpenFileDialog1.FileName
            filepath = """" & OpenFileDialog1.FileName & """"
            ContinueButton.Enabled = True
        End If
    End Sub
    Private Sub ContinueButton_Click(sender As Object, e As EventArgs) Handles ContinueButton.Click
        Dim MesRes As DialogResult = MessageBox.Show("Restoring/loading data might delete all the current data, and username and password might also be changed! Click Yes if you're sure to continue.", "Please Confirm!", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        If MesRes = DialogResult.Yes Then
            GinberRestore()
            Me.Close()
        Else
        End If
    End Sub

    Private Sub RestoreDataForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        ContinueButton.Enabled = False
        filepath = ""
        FilePathTextBox.Clear()
    End Sub

    Private Sub BackupCancelButton_Click(sender As Object, e As EventArgs) Handles BackupCancelButton.Click
        Me.Close()
    End Sub
End Class