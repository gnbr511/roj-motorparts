﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class InventoryForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(InventoryForm))
        Me.ProductsDataGridView = New System.Windows.Forms.DataGridView()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.StocksHistoryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShowAllProductsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShowLowStockProductsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintDisplayedProductsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.AddProductToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddStockToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditProductDetailsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteProductToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddProductButton = New System.Windows.Forms.Button()
        Me.AddStockButton = New System.Windows.Forms.Button()
        Me.EditProductDetailsButton = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SearchProductTextBox = New System.Windows.Forms.TextBox()
        Me.DeleteProductButton = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ProductDescriptionLabel = New System.Windows.Forms.Label()
        Me.ProductBrandLabel = New System.Windows.Forms.Label()
        Me.ProductNameLabel = New System.Windows.Forms.Label()
        Me.ProductPictureBox = New System.Windows.Forms.PictureBox()
        Me.PrintPreviewDialog1 = New System.Windows.Forms.PrintPreviewDialog()
        Me.ProductsPrintDocument = New System.Drawing.Printing.PrintDocument()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.PrintDialog1 = New System.Windows.Forms.PrintDialog()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.RefreshToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.noColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductNameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BrandColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescriptionColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StockAvailableColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MinStockLimitColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MeasuringUnitColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SupRetPriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PurchaseCostColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SalePriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.ProductsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.ProductPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ProductsDataGridView
        '
        Me.ProductsDataGridView.AllowUserToAddRows = False
        Me.ProductsDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Gainsboro
        Me.ProductsDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.ProductsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ProductsDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.noColumn, Me.ProductNameColumn, Me.BrandColumn, Me.DescriptionColumn, Me.StockAvailableColumn, Me.MinStockLimitColumn, Me.MeasuringUnitColumn, Me.SupRetPriceColumn, Me.PurchaseCostColumn, Me.SalePriceColumn})
        Me.ProductsDataGridView.ContextMenuStrip = Me.ContextMenuStrip1
        Me.ProductsDataGridView.Location = New System.Drawing.Point(15, 54)
        Me.ProductsDataGridView.Name = "ProductsDataGridView"
        Me.ProductsDataGridView.ReadOnly = True
        Me.ProductsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ProductsDataGridView.Size = New System.Drawing.Size(1003, 371)
        Me.ProductsDataGridView.TabIndex = 0
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StocksHistoryToolStripMenuItem, Me.ToolStripSeparator1, Me.RefreshToolStripMenuItem, Me.ShowAllProductsToolStripMenuItem, Me.ShowLowStockProductsToolStripMenuItem, Me.PrintDisplayedProductsToolStripMenuItem, Me.ToolStripSeparator2, Me.AddProductToolStripMenuItem, Me.AddStockToolStripMenuItem, Me.EditProductDetailsToolStripMenuItem, Me.DeleteProductToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(232, 214)
        '
        'StocksHistoryToolStripMenuItem
        '
        Me.StocksHistoryToolStripMenuItem.Name = "StocksHistoryToolStripMenuItem"
        Me.StocksHistoryToolStripMenuItem.ShortcutKeyDisplayString = "Alt + H"
        Me.StocksHistoryToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.H), System.Windows.Forms.Keys)
        Me.StocksHistoryToolStripMenuItem.Size = New System.Drawing.Size(231, 22)
        Me.StocksHistoryToolStripMenuItem.Text = "Stocks &History"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(228, 6)
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.ShortcutKeyDisplayString = "F5"
        Me.RefreshToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(231, 22)
        Me.RefreshToolStripMenuItem.Text = "Refresh"
        '
        'ShowAllProductsToolStripMenuItem
        '
        Me.ShowAllProductsToolStripMenuItem.Name = "ShowAllProductsToolStripMenuItem"
        Me.ShowAllProductsToolStripMenuItem.Size = New System.Drawing.Size(231, 22)
        Me.ShowAllProductsToolStripMenuItem.Text = "Show All Products"
        Me.ShowAllProductsToolStripMenuItem.ToolTipText = "This will show all products."
        '
        'ShowLowStockProductsToolStripMenuItem
        '
        Me.ShowLowStockProductsToolStripMenuItem.Name = "ShowLowStockProductsToolStripMenuItem"
        Me.ShowLowStockProductsToolStripMenuItem.Size = New System.Drawing.Size(231, 22)
        Me.ShowLowStockProductsToolStripMenuItem.Text = "Show Low Stock Products"
        Me.ShowLowStockProductsToolStripMenuItem.ToolTipText = "Show all products that reached the minimum stock limit."
        '
        'PrintDisplayedProductsToolStripMenuItem
        '
        Me.PrintDisplayedProductsToolStripMenuItem.Name = "PrintDisplayedProductsToolStripMenuItem"
        Me.PrintDisplayedProductsToolStripMenuItem.ShortcutKeyDisplayString = "Alt + P"
        Me.PrintDisplayedProductsToolStripMenuItem.Size = New System.Drawing.Size(231, 22)
        Me.PrintDisplayedProductsToolStripMenuItem.Text = "&Print Shown Products"
        Me.PrintDisplayedProductsToolStripMenuItem.ToolTipText = "This will print all products shown in the datagrid."
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(228, 6)
        '
        'AddProductToolStripMenuItem
        '
        Me.AddProductToolStripMenuItem.Name = "AddProductToolStripMenuItem"
        Me.AddProductToolStripMenuItem.ShortcutKeyDisplayString = "Alt + A"
        Me.AddProductToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.AddProductToolStripMenuItem.Size = New System.Drawing.Size(231, 22)
        Me.AddProductToolStripMenuItem.Text = "&Add Product"
        '
        'AddStockToolStripMenuItem
        '
        Me.AddStockToolStripMenuItem.Name = "AddStockToolStripMenuItem"
        Me.AddStockToolStripMenuItem.ShortcutKeyDisplayString = "Alt + S"
        Me.AddStockToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.AddStockToolStripMenuItem.Size = New System.Drawing.Size(231, 22)
        Me.AddStockToolStripMenuItem.Text = "Add &Stock"
        '
        'EditProductDetailsToolStripMenuItem
        '
        Me.EditProductDetailsToolStripMenuItem.Name = "EditProductDetailsToolStripMenuItem"
        Me.EditProductDetailsToolStripMenuItem.ShortcutKeyDisplayString = "Alt + E"
        Me.EditProductDetailsToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.EditProductDetailsToolStripMenuItem.Size = New System.Drawing.Size(231, 22)
        Me.EditProductDetailsToolStripMenuItem.Text = "&Edit Product Details"
        '
        'DeleteProductToolStripMenuItem
        '
        Me.DeleteProductToolStripMenuItem.Name = "DeleteProductToolStripMenuItem"
        Me.DeleteProductToolStripMenuItem.ShortcutKeyDisplayString = "Alt + D"
        Me.DeleteProductToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.DeleteProductToolStripMenuItem.Size = New System.Drawing.Size(231, 22)
        Me.DeleteProductToolStripMenuItem.Text = "&Delete Product"
        '
        'AddProductButton
        '
        Me.AddProductButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AddProductButton.Location = New System.Drawing.Point(645, 122)
        Me.AddProductButton.Name = "AddProductButton"
        Me.AddProductButton.Size = New System.Drawing.Size(143, 43)
        Me.AddProductButton.TabIndex = 2
        Me.AddProductButton.Text = "&Add Product"
        Me.AddProductButton.UseVisualStyleBackColor = True
        '
        'AddStockButton
        '
        Me.AddStockButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AddStockButton.Location = New System.Drawing.Point(645, 171)
        Me.AddStockButton.Name = "AddStockButton"
        Me.AddStockButton.Size = New System.Drawing.Size(143, 43)
        Me.AddStockButton.TabIndex = 3
        Me.AddStockButton.Text = "Add &Stock"
        Me.AddStockButton.UseVisualStyleBackColor = True
        '
        'EditProductDetailsButton
        '
        Me.EditProductDetailsButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EditProductDetailsButton.Location = New System.Drawing.Point(794, 122)
        Me.EditProductDetailsButton.Name = "EditProductDetailsButton"
        Me.EditProductDetailsButton.Size = New System.Drawing.Size(143, 43)
        Me.EditProductDetailsButton.TabIndex = 4
        Me.EditProductDetailsButton.Text = "&Edit Product Details"
        Me.EditProductDetailsButton.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.SearchProductTextBox)
        Me.GroupBox1.Controls.Add(Me.ProductsDataGridView)
        Me.GroupBox1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 220)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1024, 436)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Products"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(452, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(108, 16)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Search Product"
        '
        'SearchProductTextBox
        '
        Me.SearchProductTextBox.Location = New System.Drawing.Point(563, 22)
        Me.SearchProductTextBox.Name = "SearchProductTextBox"
        Me.SearchProductTextBox.Size = New System.Drawing.Size(455, 23)
        Me.SearchProductTextBox.TabIndex = 5
        '
        'DeleteProductButton
        '
        Me.DeleteProductButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DeleteProductButton.Location = New System.Drawing.Point(794, 171)
        Me.DeleteProductButton.Name = "DeleteProductButton"
        Me.DeleteProductButton.Size = New System.Drawing.Size(143, 43)
        Me.DeleteProductButton.TabIndex = 8
        Me.DeleteProductButton.Text = "&Delete Product"
        Me.DeleteProductButton.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.ProductDescriptionLabel)
        Me.GroupBox2.Controls.Add(Me.ProductBrandLabel)
        Me.GroupBox2.Controls.Add(Me.ProductNameLabel)
        Me.GroupBox2.Controls.Add(Me.ProductPictureBox)
        Me.GroupBox2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(12, 8)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(623, 206)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Product Details"
        '
        'ProductDescriptionLabel
        '
        Me.ProductDescriptionLabel.Location = New System.Drawing.Point(324, 115)
        Me.ProductDescriptionLabel.Name = "ProductDescriptionLabel"
        Me.ProductDescriptionLabel.Size = New System.Drawing.Size(277, 74)
        Me.ProductDescriptionLabel.TabIndex = 3
        Me.ProductDescriptionLabel.Text = "Product Description"
        '
        'ProductBrandLabel
        '
        Me.ProductBrandLabel.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ProductBrandLabel.Location = New System.Drawing.Point(324, 70)
        Me.ProductBrandLabel.Name = "ProductBrandLabel"
        Me.ProductBrandLabel.Size = New System.Drawing.Size(277, 32)
        Me.ProductBrandLabel.TabIndex = 2
        Me.ProductBrandLabel.Text = "Product Brand"
        Me.ProductBrandLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ProductNameLabel
        '
        Me.ProductNameLabel.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ProductNameLabel.Location = New System.Drawing.Point(324, 28)
        Me.ProductNameLabel.Name = "ProductNameLabel"
        Me.ProductNameLabel.Size = New System.Drawing.Size(277, 32)
        Me.ProductNameLabel.TabIndex = 1
        Me.ProductNameLabel.Text = "Product Name"
        Me.ProductNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ProductPictureBox
        '
        Me.ProductPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ProductPictureBox.Image = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.no_image_icon_13
        Me.ProductPictureBox.Location = New System.Drawing.Point(25, 23)
        Me.ProductPictureBox.Name = "ProductPictureBox"
        Me.ProductPictureBox.Size = New System.Drawing.Size(275, 169)
        Me.ProductPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.ProductPictureBox.TabIndex = 0
        Me.ProductPictureBox.TabStop = False
        '
        'PrintPreviewDialog1
        '
        Me.PrintPreviewDialog1.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPreviewDialog1.Enabled = True
        Me.PrintPreviewDialog1.Icon = CType(resources.GetObject("PrintPreviewDialog1.Icon"), System.Drawing.Icon)
        Me.PrintPreviewDialog1.Name = "ProductsPrintPreviewDialog"
        Me.PrintPreviewDialog1.Visible = False
        '
        'ProductsPrintDocument
        '
        '
        'PrintDialog1
        '
        Me.PrintDialog1.UseEXDialog = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RefreshToolStripMenuItem1})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(966, 24)
        Me.MenuStrip1.TabIndex = 9
        Me.MenuStrip1.Text = "MenuStrip1"
        Me.MenuStrip1.Visible = False
        '
        'RefreshToolStripMenuItem1
        '
        Me.RefreshToolStripMenuItem1.Name = "RefreshToolStripMenuItem1"
        Me.RefreshToolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.F5
        Me.RefreshToolStripMenuItem1.Size = New System.Drawing.Size(55, 20)
        Me.RefreshToolStripMenuItem1.Text = "refresh"
        Me.RefreshToolStripMenuItem1.Visible = False
        '
        'noColumn
        '
        Me.noColumn.HeaderText = "no"
        Me.noColumn.Name = "noColumn"
        Me.noColumn.ReadOnly = True
        Me.noColumn.Width = 80
        '
        'ProductNameColumn
        '
        Me.ProductNameColumn.HeaderText = "Product Name"
        Me.ProductNameColumn.Name = "ProductNameColumn"
        Me.ProductNameColumn.ReadOnly = True
        Me.ProductNameColumn.Width = 195
        '
        'BrandColumn
        '
        Me.BrandColumn.HeaderText = "Brand"
        Me.BrandColumn.Name = "BrandColumn"
        Me.BrandColumn.ReadOnly = True
        Me.BrandColumn.Width = 150
        '
        'DescriptionColumn
        '
        Me.DescriptionColumn.HeaderText = "Description"
        Me.DescriptionColumn.Name = "DescriptionColumn"
        Me.DescriptionColumn.ReadOnly = True
        Me.DescriptionColumn.Width = 150
        '
        'StockAvailableColumn
        '
        Me.StockAvailableColumn.HeaderText = "Stock Available"
        Me.StockAvailableColumn.Name = "StockAvailableColumn"
        Me.StockAvailableColumn.ReadOnly = True
        Me.StockAvailableColumn.Width = 60
        '
        'MinStockLimitColumn
        '
        Me.MinStockLimitColumn.HeaderText = "Min Stock Limit"
        Me.MinStockLimitColumn.Name = "MinStockLimitColumn"
        Me.MinStockLimitColumn.ReadOnly = True
        Me.MinStockLimitColumn.Width = 60
        '
        'MeasuringUnitColumn
        '
        Me.MeasuringUnitColumn.HeaderText = "Measuring Unit"
        Me.MeasuringUnitColumn.Name = "MeasuringUnitColumn"
        Me.MeasuringUnitColumn.ReadOnly = True
        Me.MeasuringUnitColumn.Width = 70
        '
        'SupRetPriceColumn
        '
        Me.SupRetPriceColumn.HeaderText = "Sup. Retailer Price"
        Me.SupRetPriceColumn.Name = "SupRetPriceColumn"
        Me.SupRetPriceColumn.ReadOnly = True
        Me.SupRetPriceColumn.Width = 60
        '
        'PurchaseCostColumn
        '
        Me.PurchaseCostColumn.HeaderText = "Purchase Cost"
        Me.PurchaseCostColumn.Name = "PurchaseCostColumn"
        Me.PurchaseCostColumn.ReadOnly = True
        Me.PurchaseCostColumn.Width = 60
        '
        'SalePriceColumn
        '
        Me.SalePriceColumn.HeaderText = "Sale Price"
        Me.SalePriceColumn.Name = "SalePriceColumn"
        Me.SalePriceColumn.ReadOnly = True
        Me.SalePriceColumn.Width = 60
        '
        'InventoryForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.ROJBackground
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1048, 668)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.DeleteProductButton)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.AddProductButton)
        Me.Controls.Add(Me.EditProductDetailsButton)
        Me.Controls.Add(Me.AddStockButton)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "InventoryForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Inventory"
        CType(Me.ProductsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.ProductPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ProductsDataGridView As DataGridView
    Friend WithEvents AddProductButton As Button
    Friend WithEvents AddStockButton As Button
    Friend WithEvents EditProductDetailsButton As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents SearchProductTextBox As TextBox
    Friend WithEvents ProductPictureBox As PictureBox
    Friend WithEvents ProductDescriptionLabel As Label
    Friend WithEvents ProductBrandLabel As Label
    Friend WithEvents ProductNameLabel As Label
    Friend WithEvents DeleteProductButton As Button
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents AddProductToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AddStockToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EditProductDetailsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DeleteProductToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents StocksHistoryToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents PrintDisplayedProductsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents PrintPreviewDialog1 As PrintPreviewDialog
    Friend WithEvents ProductsPrintDocument As Printing.PrintDocument
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents PrintDialog1 As PrintDialog
    Friend WithEvents RefreshToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ShowAllProductsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ShowLowStockProductsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents RefreshToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents noColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductNameColumn As DataGridViewTextBoxColumn
    Friend WithEvents BrandColumn As DataGridViewTextBoxColumn
    Friend WithEvents DescriptionColumn As DataGridViewTextBoxColumn
    Friend WithEvents StockAvailableColumn As DataGridViewTextBoxColumn
    Friend WithEvents MinStockLimitColumn As DataGridViewTextBoxColumn
    Friend WithEvents MeasuringUnitColumn As DataGridViewTextBoxColumn
    Friend WithEvents SupRetPriceColumn As DataGridViewTextBoxColumn
    Friend WithEvents PurchaseCostColumn As DataGridViewTextBoxColumn
    Friend WithEvents SalePriceColumn As DataGridViewTextBoxColumn
End Class
