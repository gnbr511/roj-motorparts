﻿Imports MySql.Data.MySqlClient
Public Class AddSalesForm

    Private discount, discountedprice, totaldiscount As Decimal
    Friend MeasuringUnit As String
    Private Sub SelectProductButton_Click(sender As Object, e As EventArgs) Handles SelectProductButton.Click
        Try
            SelectProductForm.customername = CustomerTextBox.Text
            SelectProductForm.ShowDialog()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub AddToOrdersButton_Click(sender As Object, e As EventArgs) Handles AddToOrdersButton.Click
        Try
            OpenConnection()
        Catch ex As Exception
        End Try
        Dim ordernumber As String
        If StoreForm.OrdersDataGridView.RowCount = 0 Then
            ordernumber = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss").Replace("/", "").Replace(":", "").Remove(8, 1)
            StoreForm.OrderNoTextBox.Text = ordernumber
        End If

        Dim orderquantity, f, subtotal, grandtotal, stockavailable, orderedquantity, purchasecost As Decimal
        Dim a As Integer
        Dim b, c, d, g, p As String
        Dim enoughstock As Boolean = False

        If CustomerTextBox.Text <> "" Then
            If ProductNameTextBox.Text <> "" Then

                'Get ordered quantity of product
                Try
                    Query = "select * from orders where no = " & HiddenProductNoTextBox.Text
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    While Reader.Read
                        orderedquantity = Reader.GetDecimal("quantity")
                    End While
                    Reader.Close()
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try

                'Check product availability
                Try
                    orderquantity = Decimal.Parse(QuantityTextBox.Text)
                    Try
                        Query = "select * from products where no = " & HiddenProductNoTextBox.Text
                        Command = New MySqlCommand(Query, MysqlCon)
                        Reader = Command.ExecuteReader
                        While Reader.Read
                            a = Reader.GetInt32("no")
                            stockavailable = Reader.GetString("availablestock")
                        End While
                        Reader.Close()
                        ''''''''
                        If stockavailable >= orderquantity + orderedquantity Then
                            enoughstock = True
                        Else
                            enoughstock = False
                        End If
                        ''''''''
                        If enoughstock = True Then
                            Try
                                StoreForm.OrdersDataGridView.Rows.Clear()
                                Query = "select * from products where no = " & HiddenProductNoTextBox.Text
                                Command = New MySqlCommand(Query, MysqlCon)
                                Reader = Command.ExecuteReader
                                While Reader.Read
                                    a = Reader.GetInt32("no")
                                    b = Reader.GetString("productname")
                                    c = Reader.GetString("productbrand")
                                    d = Reader.GetString("productdescription")
                                    purchasecost = Reader.GetDecimal("purchasecost")
                                    f = Reader.GetDecimal("saleprice")
                                    g = Reader.GetString("measuringunit")
                                End While
                                Reader.Close()

                                'Get the discount
                                If DiscountTextBox.Text = "" Or IsNumeric(DiscountTextBox.Text) = False Then
                                    discount = 0
                                Else
                                    discount = DiscountTextBox.Text
                                End If

                                'Determine if product is already on the order
                                Dim count As Integer = 0
                                Query = "select * from orders where no = " & a
                                Command = New MySqlCommand(Query, MysqlCon)
                                Reader = Command.ExecuteReader
                                While Reader.Read
                                    count = 1
                                End While
                                Reader.Close()
                                'Add quantity to ordered product
                                If count = 0 Then
                                    Try
                                        Dim TotalPrice As Decimal = orderquantity * f
                                        Query = "insert into orders values(" & a & ",'" & CustomerTextBox.Text & "','" & b & "','" & c & "','" & d & "'," & purchasecost & "," & f & "," & orderquantity _
                                                        & ", '" & g & "', " & TotalPrice & ", " & discount & ", " & TotalPrice - discount & ", sysdate(), '" & StoreForm.OrderNoTextBox.Text & "')"
                                        Command = New MySqlCommand(Query, MysqlCon)
                                        Reader = Command.ExecuteReader
                                        Reader.Close()
                                        Try
                                            Query = "select * from orders order by time"
                                            Command = New MySqlCommand(Query, MysqlCon)
                                            Reader = Command.ExecuteReader
                                            While Reader.Read
                                                Dim h = Reader.GetInt32("no")
                                                p = Reader.GetString("customername")
                                                Dim i = Reader.GetString("productname")
                                                Dim j = Reader.GetString("productbrand")
                                                Dim k = Reader.GetString("productdescription")
                                                Dim purchasecost2 = Reader.GetDecimal("purchasecost")
                                                Dim l = Reader.GetDecimal("saleprice")
                                                Dim m = Reader.GetDecimal("quantity")
                                                Dim n = Reader.GetString("measuringunit")
                                                Dim o = Reader.GetDecimal("totalprice")
                                                Dim q = Reader.GetDecimal("discount")
                                                Dim time = Reader.GetString("time")
                                                Dim r = Reader.GetDecimal("discountedprice")
                                                Dim OrderNO As Integer = StoreForm.OrdersDataGridView.RowCount() + 1
                                                StoreForm.OrdersDataGridView.Rows.Add(h, OrderNO, i, j, k, l, m, n, o, q, r, time, purchasecost2)
                                                subtotal += o
                                                totaldiscount += q
                                            End While
                                            Reader.Close()

                                            grandtotal = subtotal - totaldiscount

                                            StoreForm.CustomerNameTextBox.Text = p.ToString()
                                            StoreForm.SubTotalTextBox.Text = subtotal.ToString("n2")
                                            StoreForm.DiscountTextBox.Text = totaldiscount.ToString("n2")
                                            StoreForm.GrandTotalTextBox.Text = grandtotal.ToString("n2")

                                            StoreForm.AmountReceivedTextBox_TextChanged(sender, e)
                                            ProductNameTextBox.Clear()
                                            BrandTextBox.Clear()
                                            DescriptionTextBox.Clear()
                                            MeasuringUnitTextBox.Clear()
                                            PriceTextBox.Text = ""
                                            TotalPriceTextBox.Text = ""
                                            customername = ""
                                            QuantityTextBox.Clear()
                                            SelectProductButton.Focus()
                                            DiscountTextBox.Clear()
                                            discount = 0
                                            discountedprice = 0
                                            totaldiscount = 0
                                            DiscountedPriceTextBox.Clear()
                                        Catch ex As Exception
                                            MessageBox.Show(ex.Message)
                                        End Try
                                    Catch ex As Exception
                                        MessageBox.Show(ex.Message)
                                    End Try

                                Else 'ELSE 
                                    'Display to orders datagrid
                                    Try
                                        Dim TotalPrice As Decimal = orderquantity * f
                                        Query = "update orders set quantity = quantity + " & orderquantity & ", " _
                                                        & "totalprice = totalprice + " & TotalPrice & ", discount = discount +" & discount & ", discountedprice = totalprice - discount" & " where no = " & a
                                        Command = New MySqlCommand(Query, MysqlCon)
                                        Reader = Command.ExecuteReader
                                        Reader.Close()
                                        Try
                                            Query = "select * from orders order by time"
                                            Command = New MySqlCommand(Query, MysqlCon)
                                            Reader = Command.ExecuteReader
                                            While Reader.Read
                                                Dim h = Reader.GetInt32("no")
                                                p = Reader.GetString("customername")
                                                Dim i = Reader.GetString("productname")
                                                Dim j = Reader.GetString("productbrand")
                                                Dim k = Reader.GetString("productdescription")
                                                Dim purchasecost3 = Reader.GetDecimal("purchasecost")
                                                Dim l = Reader.GetDecimal("saleprice")
                                                Dim m = Reader.GetDecimal("quantity")
                                                Dim n = Reader.GetString("measuringunit")
                                                Dim o = Reader.GetDecimal("totalprice")
                                                Dim q = Reader.GetDecimal("discount")
                                                Dim r = Reader.GetDecimal("discountedprice")
                                                Dim OrderNO As Integer = StoreForm.OrdersDataGridView.RowCount() + 1
                                                StoreForm.OrdersDataGridView.Rows.Add(h, OrderNO, i, j, k, l, m, n, o, q, r, purchasecost3)
                                                subtotal += o
                                                totaldiscount += q
                                            End While
                                            Reader.Close()

                                            grandtotal = subtotal - totaldiscount

                                            StoreForm.CustomerNameTextBox.Text = p.ToString()
                                            StoreForm.SubTotalTextBox.Text = subtotal.ToString("n2")
                                            StoreForm.DiscountTextBox.Text = totaldiscount.ToString("n2")
                                            StoreForm.GrandTotalTextBox.Text = grandtotal.ToString("n2")

                                            StoreForm.AmountReceivedTextBox_TextChanged(sender, e)
                                            ProductNameTextBox.Clear()
                                            BrandTextBox.Clear()
                                            DescriptionTextBox.Clear()
                                            MeasuringUnitTextBox.Clear()
                                            PriceTextBox.Text = ""
                                            TotalPriceTextBox.Text = ""
                                            customername = ""
                                            QuantityTextBox.Clear()
                                            SelectProductButton.Focus()
                                            DiscountTextBox.Clear()
                                            discount = 0
                                            discountedprice = 0
                                            totaldiscount = 0
                                            DiscountedPriceTextBox.Clear()
                                        Catch ex As Exception
                                            MessageBox.Show(ex.Message)
                                        End Try
                                    Catch ex As Exception
                                        MessageBox.Show(ex.Message)
                                    End Try

                                End If
                            Catch ex As Exception
                                MessageBox.Show(ex.Message)
                            End Try
                        Else
                            MessageBox.Show("Not enough stock for the order!", "Check product stock", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            With QuantityTextBox
                                .Focus()
                                .SelectAll()
                            End With
                        End If

                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try
                Catch ex As Exception
                    MessageBox.Show("Quantity must be numeric!", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    With QuantityTextBox
                        .Focus()
                        .SelectAll()
                    End With
                End Try
            Else
                MessageBox.Show("No product selected!", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information)
                ProductNameTextBox.Focus()
            End If
        Else
            MessageBox.Show("Customer name must not be empty!", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information)
            CustomerTextBox.Focus()
        End If

        If StoreForm.OrdersDataGridView.RowCount() > 0 Then
            CustomerTextBox.Enabled = False
        End If

        MysqlCon.Close()
    End Sub

    Private Sub AddSalesForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        StoreForm.OrdersDataGridView.ClearSelection()
        StoreForm.SelectedOrderNo = -1
        ProductNameTextBox.Clear()
        DescriptionTextBox.Clear()
        BrandTextBox.Clear()
        MeasuringUnitTextBox.Clear()
        PriceTextBox.Text = ""
        TotalPriceTextBox.Text = ""
        HiddenProductNoTextBox.Clear()
        QuantityTextBox.Clear()
        customername = ""
        DiscountedPriceTextBox.Clear()
        StoreForm.StoreForm_Load(sender, e)
    End Sub

    Private Sub AddSalesForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim ScreenResHeight, ScreenResWidth, yLocation, xLocation As Integer
        ScreenResHeight = Screen.PrimaryScreen.WorkingArea.Height
        ScreenResWidth = Screen.PrimaryScreen.WorkingArea.Width
        yLocation = (ScreenResHeight - 757) / 2 + 56
        xLocation = (ScreenResWidth - 1369) / 2 + 726
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New Point(xLocation, yLocation)
        If StoreForm.OrdersDataGridView.RowCount() > 0 Then
            CustomerTextBox.Enabled = False
        Else
            CustomerTextBox.Focus()
            CustomerTextBox.SelectAll()
        End If
    End Sub

    Friend customername As String
    Private Sub AddSalesForm_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        If StoreForm.OrdersDataGridView.RowCount() > 0 Then
            CustomerTextBox.Text = StoreForm.CustomerNameTextBox.Text
            CustomerTextBox.Enabled = False
        Else
            CustomerTextBox.Enabled = True
            If customername = "" Then
                CustomerTextBox.Text = "Walk-in"
                CustomerTextBox.Focus()
                CustomerTextBox.SelectAll()
            Else
                CustomerTextBox.Text = customername.ToString()
            End If
        End If
    End Sub

    Friend Sub QuantityTextBox_TextChanged(sender As Object, e As EventArgs) Handles QuantityTextBox.TextChanged
        Try
            If PriceTextBox.Text = "" Then

            Else
                If QuantityTextBox.Text <> "" Then
                    If MeasuringUnit.ToLower = "pcs" Or MeasuringUnit.ToLower = "pack" Or MeasuringUnit.ToLower = "pair" Then
                        Integer.Parse(QuantityTextBox.Text)
                    End If
                    Dim price As Decimal = PriceTextBox.Text
                    If QuantityTextBox.Text = "" Or IsNumeric(QuantityTextBox.Text) = False Or QuantityTextBox.Text = 0 Then
                        TotalPriceTextBox.Clear()
                        DiscountedPriceTextBox.Clear()
                        QuantityTextBox.Clear()
                    Else
                        Dim quantity As Decimal = QuantityTextBox.Text
                        Dim totalprice As Decimal = price * quantity
                        TotalPriceTextBox.Text = totalprice.ToString("n2")
                    End If
                Else
                    TotalPriceTextBox.Clear()
                    DiscountedPriceTextBox.Clear()
                End If
            End If
        Catch ex As Exception
            QuantityTextBox.Clear()
            If MeasuringUnit.ToLower = "pcs" Or MeasuringUnit.ToLower = "pack" Or MeasuringUnit.ToLower = "pair" Then
                MessageBox.Show("Quantity must be Integer!", "Invalid Input!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else
                MessageBox.Show("Quantity must be Numeric!", "Invalid Input!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End Try
    End Sub

    Private Sub TotalPriceTextBox_TextChanged(sender As Object, e As EventArgs) Handles TotalPriceTextBox.TextChanged
        DiscountTextBox_TextChanged(sender, e)
    End Sub

    Private Sub ProductNameTextBox_Click(sender As Object, e As EventArgs) Handles ProductNameTextBox.Click
        Try
            SelectProductForm.customername = CustomerTextBox.Text
            SelectProductForm.ShowDialog()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DiscountTextBox_TextChanged(sender As Object, e As EventArgs) Handles DiscountTextBox.TextChanged

        If TotalPriceTextBox.Text = "" Then
            DiscountTextBox.Text = ""
        Else
            Dim totalprice As Decimal = TotalPriceTextBox.Text
            If DiscountTextBox.Text = "" Or IsNumeric(DiscountTextBox.Text) = False Then
                DiscountedPriceTextBox.Clear()
                discount = 0
            Else
                discount = DiscountTextBox.Text
                DiscountedPrice = totalprice - discount
                DiscountedPriceTextBox.Text = DiscountedPrice.ToString("n2")
            End If
        End If
    End Sub
End Class