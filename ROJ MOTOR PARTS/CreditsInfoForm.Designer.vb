﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CreditsInfoForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CreditsInfoForm))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.FullNameLabel = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ContactLabel = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.LoanedProductsLabel = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.DiscountTextBox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.RemainingPaymentTextBox = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TotalPaidTextBox = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TotalPaymentTextBox = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.LoanedProductsDataGridView = New System.Windows.Forms.DataGridView()
        Me.NumberColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductNameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QuantityColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalPriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DiscountColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FinalPriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MesuringUnitColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductBrandColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductDescriptionColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.prodnumColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LoanedProdContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.OPTIONToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.DELETEToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MoreInfoListBox = New System.Windows.Forms.ListBox()
        Me.PaymentHistoryLabel = New System.Windows.Forms.Label()
        Me.PaymentHistoryDataGridView = New System.Windows.Forms.DataGridView()
        Me.CHDateColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CHPaidAmountColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.payhisdatetimeColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.paymenthistoryContextMenuStrip2 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.PaymentHisToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DebtorsPhotoPictureBox = New System.Windows.Forms.PictureBox()
        Me.UpdateinfoButton = New System.Windows.Forms.Button()
        Me.LoanDateLabel = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.LoanedProductsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LoanedProdContextMenuStrip1.SuspendLayout()
        CType(Me.PaymentHistoryDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.paymenthistoryContextMenuStrip2.SuspendLayout()
        CType(Me.DebtorsPhotoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(13, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 18)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Full Name:"
        '
        'FullNameLabel
        '
        Me.FullNameLabel.AutoSize = True
        Me.FullNameLabel.BackColor = System.Drawing.Color.Transparent
        Me.FullNameLabel.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FullNameLabel.ForeColor = System.Drawing.Color.Maroon
        Me.FullNameLabel.Location = New System.Drawing.Point(93, 21)
        Me.FullNameLabel.Name = "FullNameLabel"
        Me.FullNameLabel.Size = New System.Drawing.Size(161, 18)
        Me.FullNameLabel.TabIndex = 1
        Me.FullNameLabel.Text = "Ginber Juanillo Candelrio"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(13, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 18)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Contact:"
        '
        'ContactLabel
        '
        Me.ContactLabel.AutoSize = True
        Me.ContactLabel.BackColor = System.Drawing.Color.Transparent
        Me.ContactLabel.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContactLabel.ForeColor = System.Drawing.Color.Maroon
        Me.ContactLabel.Location = New System.Drawing.Point(76, 50)
        Me.ContactLabel.Name = "ContactLabel"
        Me.ContactLabel.Size = New System.Drawing.Size(58, 18)
        Me.ContactLabel.TabIndex = 3
        Me.ContactLabel.Text = "Contact"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(13, 81)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(156, 18)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Customer's More Info:"
        '
        'LoanedProductsLabel
        '
        Me.LoanedProductsLabel.AutoSize = True
        Me.LoanedProductsLabel.BackColor = System.Drawing.Color.DodgerBlue
        Me.LoanedProductsLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LoanedProductsLabel.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LoanedProductsLabel.ForeColor = System.Drawing.Color.Maroon
        Me.LoanedProductsLabel.Location = New System.Drawing.Point(16, 288)
        Me.LoanedProductsLabel.Name = "LoanedProductsLabel"
        Me.LoanedProductsLabel.Size = New System.Drawing.Size(128, 21)
        Me.LoanedProductsLabel.TabIndex = 7
        Me.LoanedProductsLabel.Text = "Loaned Products"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.DiscountTextBox)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.RemainingPaymentTextBox)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.TotalPaidTextBox)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.TotalPaymentTextBox)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(265, 103)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(226, 142)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Credits Summary"
        '
        'DiscountTextBox
        '
        Me.DiscountTextBox.Location = New System.Drawing.Point(117, 48)
        Me.DiscountTextBox.Name = "DiscountTextBox"
        Me.DiscountTextBox.ReadOnly = True
        Me.DiscountTextBox.Size = New System.Drawing.Size(100, 26)
        Me.DiscountTextBox.TabIndex = 12
        Me.DiscountTextBox.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 51)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 18)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Discount:"
        '
        'RemainingPaymentTextBox
        '
        Me.RemainingPaymentTextBox.Location = New System.Drawing.Point(117, 100)
        Me.RemainingPaymentTextBox.Name = "RemainingPaymentTextBox"
        Me.RemainingPaymentTextBox.ReadOnly = True
        Me.RemainingPaymentTextBox.Size = New System.Drawing.Size(100, 26)
        Me.RemainingPaymentTextBox.TabIndex = 10
        Me.RemainingPaymentTextBox.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(6, 108)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(111, 18)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Rem. Payment:"
        '
        'TotalPaidTextBox
        '
        Me.TotalPaidTextBox.Location = New System.Drawing.Point(117, 74)
        Me.TotalPaidTextBox.Name = "TotalPaidTextBox"
        Me.TotalPaidTextBox.ReadOnly = True
        Me.TotalPaidTextBox.Size = New System.Drawing.Size(100, 26)
        Me.TotalPaidTextBox.TabIndex = 8
        Me.TotalPaidTextBox.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 82)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(77, 18)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Total Paid:"
        '
        'TotalPaymentTextBox
        '
        Me.TotalPaymentTextBox.Location = New System.Drawing.Point(117, 22)
        Me.TotalPaymentTextBox.Name = "TotalPaymentTextBox"
        Me.TotalPaymentTextBox.ReadOnly = True
        Me.TotalPaymentTextBox.Size = New System.Drawing.Size(100, 26)
        Me.TotalPaymentTextBox.TabIndex = 6
        Me.TotalPaymentTextBox.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(6, 30)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(109, 18)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Total Payment:"
        '
        'LoanedProductsDataGridView
        '
        Me.LoanedProductsDataGridView.AllowUserToAddRows = False
        Me.LoanedProductsDataGridView.AllowUserToDeleteRows = False
        Me.LoanedProductsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.LoanedProductsDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NumberColumn, Me.ProductNameColumn, Me.PriceColumn, Me.QuantityColumn, Me.TotalPriceColumn, Me.DiscountColumn, Me.FinalPriceColumn, Me.MesuringUnitColumn, Me.ProductBrandColumn, Me.ProductDescriptionColumn, Me.prodnumColumn})
        Me.LoanedProductsDataGridView.ContextMenuStrip = Me.LoanedProdContextMenuStrip1
        Me.LoanedProductsDataGridView.Location = New System.Drawing.Point(16, 314)
        Me.LoanedProductsDataGridView.MultiSelect = False
        Me.LoanedProductsDataGridView.Name = "LoanedProductsDataGridView"
        Me.LoanedProductsDataGridView.ReadOnly = True
        Me.LoanedProductsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.LoanedProductsDataGridView.Size = New System.Drawing.Size(475, 198)
        Me.LoanedProductsDataGridView.TabIndex = 12
        Me.LoanedProductsDataGridView.TabStop = False
        '
        'NumberColumn
        '
        Me.NumberColumn.HeaderText = "#"
        Me.NumberColumn.Name = "NumberColumn"
        Me.NumberColumn.ReadOnly = True
        Me.NumberColumn.Width = 29
        '
        'ProductNameColumn
        '
        Me.ProductNameColumn.HeaderText = "Product Name"
        Me.ProductNameColumn.Name = "ProductNameColumn"
        Me.ProductNameColumn.ReadOnly = True
        Me.ProductNameColumn.Width = 110
        '
        'PriceColumn
        '
        Me.PriceColumn.HeaderText = "Price"
        Me.PriceColumn.Name = "PriceColumn"
        Me.PriceColumn.ReadOnly = True
        Me.PriceColumn.Width = 60
        '
        'QuantityColumn
        '
        Me.QuantityColumn.HeaderText = "Qty"
        Me.QuantityColumn.Name = "QuantityColumn"
        Me.QuantityColumn.ReadOnly = True
        Me.QuantityColumn.Width = 48
        '
        'TotalPriceColumn
        '
        Me.TotalPriceColumn.HeaderText = "Total Price"
        Me.TotalPriceColumn.Name = "TotalPriceColumn"
        Me.TotalPriceColumn.ReadOnly = True
        Me.TotalPriceColumn.Width = 60
        '
        'DiscountColumn
        '
        Me.DiscountColumn.HeaderText = "Discount"
        Me.DiscountColumn.Name = "DiscountColumn"
        Me.DiscountColumn.ReadOnly = True
        Me.DiscountColumn.Width = 65
        '
        'FinalPriceColumn
        '
        Me.FinalPriceColumn.HeaderText = "Final Price"
        Me.FinalPriceColumn.Name = "FinalPriceColumn"
        Me.FinalPriceColumn.ReadOnly = True
        Me.FinalPriceColumn.Width = 71
        '
        'MesuringUnitColumn
        '
        Me.MesuringUnitColumn.HeaderText = "Msrng Unit"
        Me.MesuringUnitColumn.Name = "MesuringUnitColumn"
        Me.MesuringUnitColumn.ReadOnly = True
        Me.MesuringUnitColumn.Width = 48
        '
        'ProductBrandColumn
        '
        Me.ProductBrandColumn.HeaderText = "Brand"
        Me.ProductBrandColumn.Name = "ProductBrandColumn"
        Me.ProductBrandColumn.ReadOnly = True
        Me.ProductBrandColumn.Width = 80
        '
        'ProductDescriptionColumn
        '
        Me.ProductDescriptionColumn.HeaderText = "Description"
        Me.ProductDescriptionColumn.Name = "ProductDescriptionColumn"
        Me.ProductDescriptionColumn.ReadOnly = True
        Me.ProductDescriptionColumn.Width = 80
        '
        'prodnumColumn
        '
        Me.prodnumColumn.HeaderText = "ProdNum"
        Me.prodnumColumn.Name = "prodnumColumn"
        Me.prodnumColumn.ReadOnly = True
        Me.prodnumColumn.Visible = False
        '
        'LoanedProdContextMenuStrip1
        '
        Me.LoanedProdContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OPTIONToolStripMenuItem, Me.ToolStripSeparator1, Me.DELETEToolStripMenuItem})
        Me.LoanedProdContextMenuStrip1.Name = "LoanedProdContextMenuStrip1"
        Me.LoanedProdContextMenuStrip1.Size = New System.Drawing.Size(137, 54)
        '
        'OPTIONToolStripMenuItem
        '
        Me.OPTIONToolStripMenuItem.Enabled = False
        Me.OPTIONToolStripMenuItem.Name = "OPTIONToolStripMenuItem"
        Me.OPTIONToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.OPTIONToolStripMenuItem.Text = "OPTION"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(133, 6)
        '
        'DELETEToolStripMenuItem
        '
        Me.DELETEToolStripMenuItem.Name = "DELETEToolStripMenuItem"
        Me.DELETEToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete
        Me.DELETEToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.DELETEToolStripMenuItem.Text = "DELETE"
        '
        'MoreInfoListBox
        '
        Me.MoreInfoListBox.FormattingEnabled = True
        Me.MoreInfoListBox.ItemHeight = 16
        Me.MoreInfoListBox.Location = New System.Drawing.Point(16, 111)
        Me.MoreInfoListBox.Name = "MoreInfoListBox"
        Me.MoreInfoListBox.Size = New System.Drawing.Size(227, 132)
        Me.MoreInfoListBox.TabIndex = 11
        Me.MoreInfoListBox.TabStop = False
        '
        'PaymentHistoryLabel
        '
        Me.PaymentHistoryLabel.AutoSize = True
        Me.PaymentHistoryLabel.BackColor = System.Drawing.Color.DodgerBlue
        Me.PaymentHistoryLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PaymentHistoryLabel.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PaymentHistoryLabel.Location = New System.Drawing.Point(152, 288)
        Me.PaymentHistoryLabel.Name = "PaymentHistoryLabel"
        Me.PaymentHistoryLabel.Size = New System.Drawing.Size(134, 21)
        Me.PaymentHistoryLabel.TabIndex = 13
        Me.PaymentHistoryLabel.Text = "Payments History"
        '
        'PaymentHistoryDataGridView
        '
        Me.PaymentHistoryDataGridView.AllowUserToAddRows = False
        Me.PaymentHistoryDataGridView.AllowUserToDeleteRows = False
        Me.PaymentHistoryDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.PaymentHistoryDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CHDateColumn, Me.CHPaidAmountColumn, Me.payhisdatetimeColumn})
        Me.PaymentHistoryDataGridView.ContextMenuStrip = Me.paymenthistoryContextMenuStrip2
        Me.PaymentHistoryDataGridView.Location = New System.Drawing.Point(16, 314)
        Me.PaymentHistoryDataGridView.Name = "PaymentHistoryDataGridView"
        Me.PaymentHistoryDataGridView.ReadOnly = True
        Me.PaymentHistoryDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.PaymentHistoryDataGridView.Size = New System.Drawing.Size(254, 182)
        Me.PaymentHistoryDataGridView.TabIndex = 14
        Me.PaymentHistoryDataGridView.Visible = False
        '
        'CHDateColumn
        '
        Me.CHDateColumn.HeaderText = "Date Paid"
        Me.CHDateColumn.Name = "CHDateColumn"
        Me.CHDateColumn.ReadOnly = True
        '
        'CHPaidAmountColumn
        '
        Me.CHPaidAmountColumn.HeaderText = "Paid Amount"
        Me.CHPaidAmountColumn.Name = "CHPaidAmountColumn"
        Me.CHPaidAmountColumn.ReadOnly = True
        Me.CHPaidAmountColumn.Width = 110
        '
        'payhisdatetimeColumn
        '
        Me.payhisdatetimeColumn.HeaderText = "DateTime"
        Me.payhisdatetimeColumn.Name = "payhisdatetimeColumn"
        Me.payhisdatetimeColumn.ReadOnly = True
        Me.payhisdatetimeColumn.Visible = False
        '
        'paymenthistoryContextMenuStrip2
        '
        Me.paymenthistoryContextMenuStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.ToolStripSeparator2, Me.PaymentHisToolStripMenuItem2})
        Me.paymenthistoryContextMenuStrip2.Name = "ContextMenuStrip1"
        Me.paymenthistoryContextMenuStrip2.Size = New System.Drawing.Size(137, 54)
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Enabled = False
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(136, 22)
        Me.ToolStripMenuItem1.Text = "OPTION"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(133, 6)
        '
        'PaymentHisToolStripMenuItem2
        '
        Me.PaymentHisToolStripMenuItem2.Name = "PaymentHisToolStripMenuItem2"
        Me.PaymentHisToolStripMenuItem2.ShortcutKeys = System.Windows.Forms.Keys.Delete
        Me.PaymentHisToolStripMenuItem2.Size = New System.Drawing.Size(136, 22)
        Me.PaymentHisToolStripMenuItem2.Text = "DELETE"
        '
        'DebtorsPhotoPictureBox
        '
        Me.DebtorsPhotoPictureBox.Image = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.no_image_icon_13
        Me.DebtorsPhotoPictureBox.Location = New System.Drawing.Point(382, 7)
        Me.DebtorsPhotoPictureBox.Name = "DebtorsPhotoPictureBox"
        Me.DebtorsPhotoPictureBox.Size = New System.Drawing.Size(109, 93)
        Me.DebtorsPhotoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.DebtorsPhotoPictureBox.TabIndex = 15
        Me.DebtorsPhotoPictureBox.TabStop = False
        '
        'UpdateinfoButton
        '
        Me.UpdateinfoButton.Location = New System.Drawing.Point(274, 68)
        Me.UpdateinfoButton.Name = "UpdateinfoButton"
        Me.UpdateinfoButton.Size = New System.Drawing.Size(99, 26)
        Me.UpdateinfoButton.TabIndex = 16
        Me.UpdateinfoButton.TabStop = False
        Me.UpdateinfoButton.Text = "Update Info"
        Me.UpdateinfoButton.UseVisualStyleBackColor = True
        '
        'LoanDateLabel
        '
        Me.LoanDateLabel.AutoSize = True
        Me.LoanDateLabel.BackColor = System.Drawing.Color.Transparent
        Me.LoanDateLabel.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LoanDateLabel.ForeColor = System.Drawing.Color.Maroon
        Me.LoanDateLabel.Location = New System.Drawing.Point(95, 258)
        Me.LoanDateLabel.Name = "LoanDateLabel"
        Me.LoanDateLabel.Size = New System.Drawing.Size(75, 18)
        Me.LoanDateLabel.TabIndex = 18
        Me.LoanDateLabel.Text = "Loan Date"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(13, 258)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(80, 18)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Loan Date:"
        '
        'CreditsInfoForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.ROJBackground
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(507, 527)
        Me.Controls.Add(Me.LoanDateLabel)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.UpdateinfoButton)
        Me.Controls.Add(Me.DebtorsPhotoPictureBox)
        Me.Controls.Add(Me.PaymentHistoryLabel)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.LoanedProductsLabel)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ContactLabel)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.FullNameLabel)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.MoreInfoListBox)
        Me.Controls.Add(Me.PaymentHistoryDataGridView)
        Me.Controls.Add(Me.LoanedProductsDataGridView)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "CreditsInfoForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Credits Information"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.LoanedProductsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LoanedProdContextMenuStrip1.ResumeLayout(False)
        CType(Me.PaymentHistoryDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.paymenthistoryContextMenuStrip2.ResumeLayout(False)
        CType(Me.DebtorsPhotoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents FullNameLabel As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents ContactLabel As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents LoanedProductsLabel As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents RemainingPaymentTextBox As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents TotalPaidTextBox As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents TotalPaymentTextBox As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents LoanedProductsDataGridView As DataGridView
    Friend WithEvents MoreInfoListBox As ListBox
    Friend WithEvents PaymentHistoryLabel As Label
    Friend WithEvents PaymentHistoryDataGridView As DataGridView
    Friend WithEvents LoanedProdContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents OPTIONToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents DELETEToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents paymenthistoryContextMenuStrip2 As ContextMenuStrip
    Friend WithEvents ToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents PaymentHisToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents CHDateColumn As DataGridViewTextBoxColumn
    Friend WithEvents CHPaidAmountColumn As DataGridViewTextBoxColumn
    Friend WithEvents payhisdatetimeColumn As DataGridViewTextBoxColumn
    Friend WithEvents NumberColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductNameColumn As DataGridViewTextBoxColumn
    Friend WithEvents PriceColumn As DataGridViewTextBoxColumn
    Friend WithEvents QuantityColumn As DataGridViewTextBoxColumn
    Friend WithEvents TotalPriceColumn As DataGridViewTextBoxColumn
    Friend WithEvents DiscountColumn As DataGridViewTextBoxColumn
    Friend WithEvents FinalPriceColumn As DataGridViewTextBoxColumn
    Friend WithEvents MesuringUnitColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductBrandColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductDescriptionColumn As DataGridViewTextBoxColumn
    Friend WithEvents prodnumColumn As DataGridViewTextBoxColumn
    Friend WithEvents DiscountTextBox As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents DebtorsPhotoPictureBox As PictureBox
    Friend WithEvents UpdateinfoButton As Button
    Friend WithEvents LoanDateLabel As Label
    Friend WithEvents Label9 As Label
End Class
