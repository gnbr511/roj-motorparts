﻿'Program Name: ePOSIRMS (Electronic Point of Sale, Inventory, and Record Management System)
'Program Description: This program is a thesis project of Ginber Candelario and Ian Jay Lomlom for ROJ Motorparts.
'Programmer: GINBER J. CANDELARIO, BSIT

Imports MySql.Data.MySqlClient
Imports System.Drawing.Printing
Public Class RecordsForm
    Private TOTGROSSSALE, TOTEXPENSES, TOTDISCOUNT, TOTNETSALE As Decimal

    Friend Sub RefreshButton_Click(sender As Object, e As EventArgs) Handles RefreshButton.Click
        StoreForm.StoreForm_Load(sender, e)
        RecordsForm_Load(sender, e)
    End Sub

    Private Sub SearchProductRecordsTextBox_TextChanged(sender As Object, e As EventArgs) Handles SearchProductRecordsTextBox.TextChanged
        Try
            OpenConnection()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "1")
        End Try
        'display searched sold products
        Try
            'Query = "select * from (select *, concat(productname, ' - ', productbrand) as concatnamebrand from
            '(select no, productname, productbrand, productdescription,customerfullname as customername, saleprice, quantity, 
            'measuringunit, totalprice, discount, concat('(', discountedprice, ')') as discountedprice, datepaid as date from paidloanedproducts
            'union all select * from todayssoldproducts) uniontspplp where date >='" & DateFromLabel.Text & "' and date <= '" _
            '& DateToLabel.Text & "') concatnamebrandselecteddate where productname like '%" _
            '& SearchProductRecordsTextBox.Text & "%' or productbrand like '%" _
            '& SearchProductRecordsTextBox.Text & "%' or concatnamebrand like '%" & SearchProductRecordsTextBox.Text & "%'" _
            '& "or date like '%" & SearchProductRecordsTextBox.Text & "%' order by date desc"

            'Query = "SELECT * FROM todayssoldproducts
            '         WHERE productname LIKE '%" & SearchProductRecordsTextBox.Text & "%' OR " _
            '         & "productbrand LIKE '%" & SearchProductRecordsTextBox.Text & "%' OR " _
            '         & "date LIKE '%" & SearchProductRecordsTextBox.Text & "%' OR " _
            '          & "no LIKE '%" & SearchProductRecordsTextBox.Text & "%' OR " _
            '         & "orderno LIKE '%" & SearchProductRecordsTextBox.Text & "%' order by date desc, time desc, productname asc"
            'Command = New MySqlCommand(Query, MysqlCon)
            'Reader = Command.ExecuteReader
            'While Reader.Read
            '    Dim a = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
            '    Dim orderno = Reader.GetString("orderno")
            '    Dim f = Reader.GetString("customername")
            '    Dim productno = Reader.GetInt32("no")
            '    Dim b = Reader.GetString("productname")
            '    Dim c = Reader.GetString("productbrand")
            '    Dim d = Reader.GetString("productdescription")
            '    Dim purchasecost = Reader.GetDecimal("purchasecost")
            '    Dim g = Reader.GetDecimal("saleprice")
            '    Dim h = Reader.GetDecimal("quantity")
            '    Dim i = Reader.GetString("measuringunit")
            '    Dim j = Reader.GetDecimal("totalprice")
            '    Dim k = Reader.GetDecimal("discount")
            '    Try
            '        Dim l = Reader.GetDecimal("discountedprice")
            '        SoldProductsDataGridView.Rows.Add(a, orderno, f, productno, b, c, d, purchasecost, g, h, i, j, k, l)
            '    Catch ex As Exception
            '        Dim l2 = Reader.GetString("discountedprice")
            '        SoldProductsDataGridView.Rows.Add(a, orderno, f, b, c, d, purchasecost, g, h, i, j, k, l2)
            '    End Try
            '    If No > 0 Then
            '        If orderno = SoldProductsDataGridView(1, No - 1).Value() Then
            '            SoldProductsDataGridView.Rows(No).DefaultCellStyle.BackColor = SoldProductsDataGridView.Rows(No - 1).DefaultCellStyle.BackColor
            '        Else
            '            If ColorAlternater = True Then
            '                SoldProductsDataGridView.Rows(No).DefaultCellStyle.BackColor = Color.White
            '                ColorAlternater = False
            '            Else
            '                SoldProductsDataGridView.Rows(No).DefaultCellStyle.BackColor = Color.Gray
            '                ColorAlternater = True
            '            End If
            '        End If
            '    End If
            '    No += 1
            'End While
            'Reader.Close()

            SoldProductsDataGridView.Rows.Clear()
            Dim ColorAlternater As Boolean = False
            Dim No As Integer = 0
            Query = "SELECT * FROM (SELECT * FROM (SELECT date, orderno, customername, no, productname, productbrand, productdescription, purchasecost, saleprice, quantity, measuringunit, totalprice, discount, discountedprice, time, no as no2 
                     FROM todayssoldproducts 
                     UNION ALL
                     SELECT date, orderno, debtor, no, productname, productbrand, productdescription, purchasecost, saleprice, quantity, measuringunit, totalprice, discount, discountedprice, time, no + no as no2 
                     FROM loanedproducts) AS unionedsoldandloanedproducts
                     WHERE date >= '" & DateFromLabel.Text & "' AND date <= '" & DateToLabel.Text & "') AS unionedsoldandloanedproductswithdates
                     WHERE date LIKE '%" & SearchProductRecordsTextBox.Text & "%' OR " _
                     & "orderno LIKE '%" & SearchProductRecordsTextBox.Text & "%' OR " _
                     & "customername LIKE '%" & SearchProductRecordsTextBox.Text & "%' OR " _
                     & "no LIKE '%" & SearchProductRecordsTextBox.Text & "%' OR " _
                     & "productname LIKE '%" & SearchProductRecordsTextBox.Text & "%' OR " _
                     & "productbrand LIKE '%" & SearchProductRecordsTextBox.Text & "%' OR " _
                     & "productdescription LIKE '%" & SearchProductRecordsTextBox.Text & "%' " _
                     & "ORDER BY date desc, time desc, productname asc"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim a = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                Dim OrderNo = Reader.GetString("orderno")
                Dim f = Reader.GetString("customername")
                Dim productno = Reader.GetInt32("no")
                Dim b = Reader.GetString("productname")
                Dim c = Reader.GetString("productbrand")
                Dim d = Reader.GetString("productdescription")
                Dim PurchaseCost = Reader.GetDecimal("purchasecost")
                Dim g = Reader.GetDecimal("saleprice")
                Dim h = Reader.GetDecimal("quantity")
                Dim i = Reader.GetString("measuringunit")
                Dim j = Reader.GetDecimal("totalprice")
                Dim k = Reader.GetDecimal("discount")
                Dim l = Reader.GetDecimal("discountedprice")
                Dim no2 = Reader.GetInt32("no2")
                Dim Loaned As Boolean = False
                If productno <> no2 Then
                    Loaned = True
                End If
                SoldProductsDataGridView.Rows.Add(a, OrderNo, f, productno, b, c, d, PurchaseCost, g, h, i, j, k, l)
                If No > 0 Then
                    If OrderNo = SoldProductsDataGridView(1, No - 1).Value() Then
                        SoldProductsDataGridView.Rows(No).DefaultCellStyle.BackColor = SoldProductsDataGridView.Rows(No - 1).DefaultCellStyle.BackColor
                    Else
                        If ColorAlternater = True Then
                            SoldProductsDataGridView.Rows(No).DefaultCellStyle.BackColor = Color.White
                            ColorAlternater = False
                        Else
                            SoldProductsDataGridView.Rows(No).DefaultCellStyle.BackColor = Color.Gray
                            ColorAlternater = True
                        End If
                    End If
                End If
                If Loaned = True Then
                    SoldProductsDataGridView.Rows(No).Cells(13).Style.BackColor = Color.Red
                End If
                No += 1
            End While
            Reader.Close()
            SoldProductsDataGridView.ClearSelection()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "2")
        End Try

        MysqlCon.Close()
    End Sub

    Private Sub ExpensesSearchTextBox_TextChanged(sender As Object, e As EventArgs) Handles ExpensesSearchTextBox.TextChanged
        Try
            OpenConnection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        'display searched expenses
        Try
            ExpensesDataGridView.Rows.Clear()
            Query = "select * from todaysexpenses where name like '%" & ExpensesSearchTextBox.Text & "%' OR " _
                & "description like '%" & ExpensesSearchTextBox.Text & "%' OR date like '%" & ExpensesSearchTextBox.Text & "%' order by date desc"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim a = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                Dim b = Reader.GetString("name")
                Dim c = Reader.GetString("description")
                Dim d = Reader.GetDecimal("cost")
                ExpensesDataGridView.Rows.Add(a, b, c, d)
            End While
            Reader.Close()
            ExpensesDataGridView.ClearSelection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        MysqlCon.Close()
    End Sub

    Private Sub CreditsRecordsButton_Click(sender As Object, e As EventArgs) Handles CreditsRecordsButton.Click
        CreditsRecordsForm.ShowDialog()
    End Sub

    Private Sub RefreshRecordsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RefreshRecordsToolStripMenuItem.Click
        StoreForm.StoreForm_Load(sender, e)
        RecordsForm_Load(sender, e)
    End Sub

    Private Sub SearchCreditsRecordsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SearchRecordsToolStripMenuItem.Click
        SearchRecordsButton_Click(sender, e)
    End Sub

    Private Sub CreditsRecordsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CreditsRecordsToolStripMenuItem.Click
        CreditsRecordsForm.ShowDialog()
    End Sub

    Private Sub BackUpToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BackUpToolStripMenuItem.Click
        SearchRecordsForm.BackUpORsearch = True
        SearchRecordsForm.DeleteORBackup = False
        SearchRecordsForm.PrintRecords = False
        SearchRecordsForm.Show()
    End Sub

    Private Sub BackUpRecordsButton_Click(sender As Object, e As EventArgs) Handles BackUpRecordsButton.Click
        BackUpToolStripMenuItem_Click(sender, e)
    End Sub

    Private Sub DeleteRecordsButton_Click(sender As Object, e As EventArgs) Handles DeleteRecordsButton.Click
        SearchRecordsForm.BackUpORsearch = False
        SearchRecordsForm.DeleteORBackup = True
        SearchRecordsForm.PrintRecords = False
        SearchRecordsForm.Show()
    End Sub

    Private Sub DeleteRecordsByDateToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeleteRecordsByDateToolStripMenuItem.Click
        DeleteRecordsButton_Click(sender, e)
    End Sub
    Private Sub RecordsForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        SearchRecordsForm.Close()
        StoreForm.StoreForm_Load(sender, e)
    End Sub
    Private Sub SearchRecordsButton_Click(sender As Object, e As EventArgs) Handles SearchRecordsButton.Click
        SearchRecordsForm.BackUpORsearch = False
        SearchRecordsForm.DeleteORBackup = False
        SearchRecordsForm.PrintRecords = False
        SearchRecordsForm.Show()
    End Sub

    'Private Sub SoldProductsDataGridView_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles SoldProductsDataGridView.CellClick
    '    SelectedRecordSource = "SoldProductsRecords"
    '    Del_A = SoldProductsDataGridView.CurrentRow.Cells(1).Value
    '    Del_B = SoldProductsDataGridView.CurrentRow.Cells(2).Value
    '    Del_C = SoldProductsDataGridView.CurrentRow.Cells(3).Value
    '    Del_D = SoldProductsDataGridView.CurrentRow.Cells(4).Value
    '    Del_E = SoldProductsDataGridView.CurrentRow.Cells(5).Value
    '    Del_F = SoldProductsDataGridView.CurrentRow.Cells(6).Value
    '    Del_G = SoldProductsDataGridView.CurrentRow.Cells(7).Value
    '    Del_H = SoldProductsDataGridView.CurrentRow.Cells(8).Value
    '    Del_I = SoldProductsDataGridView.CurrentRow.Cells(9).Value
    '    Del_J = SoldProductsDataGridView.CurrentRow.Cells(10).Value
    '    Del_K = SoldProductsDataGridView.CurrentRow.Cells(0).Value

    'End Sub

    Private Sub ExpensesDataGridView_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles ExpensesDataGridView.CellClick
        'SelectedRecordSource = "ExpensesRecords"
        'Del_x_A = ExpensesDataGridView.CurrentRow.Cells(0).Value
        'Del_x_B = ExpensesDataGridView.CurrentRow.Cells(1).Value
        'Del_x_C = ExpensesDataGridView.CurrentRow.Cells(2).Value
        'Del_x_D = ExpensesDataGridView.CurrentRow.Cells(3).Value
    End Sub

    Private Sub DebtPaymentsDataGridView_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DebtPaymentsDataGridView.CellClick
        'SelectedRecordSource = "DebtsPayments"
        'Del_PM_A = DebtPaymentsDataGridView.CurrentRow.Cells(0).Value
        'Del_PM_B = DebtPaymentsDataGridView.CurrentRow.Cells(2).Value
        'Del_PM_C = DebtPaymentsDataGridView.CurrentRow.Cells(3).Value
    End Sub

    Public datetimeofunpaidcredits As New List(Of DateTime)
    Private Sub RecordsForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TOTGROSSSALE = 0
        TOTDISCOUNT = 0
        TOTEXPENSES = 0
        TOTNETSALE = 0
        Dim flag As Integer = 0
        Dim datefrom, dateto As String

        Try
            OpenConnection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Try
            'Display daily sales
            DailysalesDataGridView.Rows.Clear()
            Query = "select * from todayssale order by date desc limit 300"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim a = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                Dim b = Reader.GetDecimal("grosssale")
                Dim c = Reader.GetDecimal("totaldiscount")
                Dim d = Reader.GetDecimal("totalexpenses")
                Dim netsale As Decimal = b - c - d
                DailysalesDataGridView.Rows.Add(a, b, c, d, netsale)
                If flag = 0 Then
                    dateto = a.ToString()
                End If
                datefrom = a.ToString()
                flag = 1
                TOTGROSSSALE += b
                TOTDISCOUNT += c
                TOTEXPENSES += d
                TOTNETSALE += netsale
            End While
            Reader.Close()

            'Display Sold products
            SoldProductsDataGridView.Rows.Clear()
            Dim ColorAlternater As Boolean = False
            Dim No As Integer = 0
            Query = "SELECT * FROM (SELECT date, orderno, customername, no, productname, productbrand, productdescription, purchasecost, saleprice, quantity, measuringunit, totalprice, discount, discountedprice, time, no as no2 
                     FROM todayssoldproducts 
                     UNION ALL
                     SELECT date, orderno, debtor, no, productname, productbrand, productdescription, purchasecost, saleprice, quantity, measuringunit, totalprice, discount, discountedprice, time, no + no as no2 
                     FROM loanedproducts) soldandloanedproducts
                     WHERE date >= '" & datefrom & "' AND date <= '" & dateto & "'
                     ORDER BY date desc, time desc, productname asc"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim a = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                Dim OrderNo = Reader.GetString("orderno")
                Dim f = Reader.GetString("customername")
                Dim productno = Reader.GetInt32("no")
                Dim b = Reader.GetString("productname")
                Dim c = Reader.GetString("productbrand")
                Dim d = Reader.GetString("productdescription")
                Dim PurchaseCost = Reader.GetDecimal("purchasecost")
                Dim g = Reader.GetDecimal("saleprice")
                Dim h = Reader.GetDecimal("quantity")
                Dim i = Reader.GetString("measuringunit")
                Dim j = Reader.GetDecimal("totalprice")
                Dim k = Reader.GetDecimal("discount")
                Dim l = Reader.GetDecimal("discountedprice")
                Dim no2 = Reader.GetInt32("no2")
                SoldProductsDataGridView.Rows.Add(a, OrderNo, f, productno, b, c, d, PurchaseCost, g, h, i, j, k, l)
                If No > 0 Then
                    If OrderNo = SoldProductsDataGridView(1, No - 1).Value() Then
                        SoldProductsDataGridView.Rows(No).DefaultCellStyle.BackColor = SoldProductsDataGridView.Rows(No - 1).DefaultCellStyle.BackColor
                    Else
                        If ColorAlternater = True Then
                            SoldProductsDataGridView.Rows(No).DefaultCellStyle.BackColor = Color.White
                            ColorAlternater = False
                        Else
                            SoldProductsDataGridView.Rows(No).DefaultCellStyle.BackColor = Color.Gray
                            ColorAlternater = True
                        End If
                    End If
                End If
                If productno <> no2 Then
                    SoldProductsDataGridView.Rows(No).Cells(13).Style.BackColor = Color.Red
                End If
                No += 1
            End While
            Reader.Close()

            'Display daily expenses
            ExpensesDataGridView.Rows.Clear()
            Query = "select * from todaysexpenses where date >= '" & datefrom & "' and date <= '" & dateto & "' order by date desc"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim a = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                Dim b = Reader.GetString("name")
                Dim c = Reader.GetString("description")
                Dim d = Reader.GetString("cost")
                ExpensesDataGridView.Rows.Add(a, b, c, d)
            End While
            Reader.Close()

            'Display Credits Payments
            DebtPaymentsDataGridView.Rows.Clear()
            Query = "SELECT * FROM paymentsmade 
                     INNER JOIN (SELECT debtor AS C_debtor, debtorsid FROM credits) credits
                     ON paymentsmade.customerid = credits.debtorsid 
                     WHERE date >= '" & datefrom & "' AND date <= '" & dateto & "' order by datetime desc"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim a = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                Dim b = Reader.GetString("orderno")
                Dim c = Reader.GetString("customerid")
                Dim d = Reader.GetString("c_debtor")
                Dim f = Reader.GetDecimal("paidamount")
                DebtPaymentsDataGridView.Rows.Add(a, b, c, d, f)
            End While
            Reader.Close()

            SoldProductsDataGridView.ClearSelection()
            ExpensesDataGridView.ClearSelection()
            DailysalesDataGridView.ClearSelection()
            DebtPaymentsDataGridView.ClearSelection()

            totgrossTextBox.Text = TOTGROSSSALE.ToString("n2")
            totexpTextBox.Text = TOTEXPENSES.ToString("n2")
            totdiscountTextBox.Text = TOTDISCOUNT.ToString("n2")
            totnetsaleTextBox.Text = TOTNETSALE.ToString("n2")
            DateFromLabel.Text = datefrom.ToString()
            DateToLabel.Text = dateto.ToString()
            Reader.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        SelectedRecordSource = ""
        MysqlCon.Close()
    End Sub

    'Private Sub recordsform_activated(sender As Object, e As EventArgs) Handles MyBase.Activated

    '    RecordsForm_Load(sender, e)
    '    SoldProductsDataGridView.ClearSelection()
    '    ExpensesDataGridView.ClearSelection()
    '    DailysalesDataGridView.ClearSelection()
    '    DebtPaymentsDataGridView.ClearSelection()

    'End Sub

    Private Sub DeleteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeleteToolStripMenuItem.Click
        If SelectedRecordSource = "" Then
            MessageBox.Show("No selected record!")
        Else
            DeleteSelectedRecord(sender, e)
        End If
    End Sub
    Private Sub PrintButton_Click(sender As Object, e As EventArgs) Handles PrintButton.Click
        SearchRecordsForm.BackUpORsearch = False
        SearchRecordsForm.DeleteORBackup = False
        SearchRecordsForm.PrintRecords = True
        SearchRecordsForm.Show()
    End Sub
    Private Sub ContextMenuStrip1_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles ContextMenuStrip1.Opening
        If SelectedRecordSource = "SoldProductsRecords" Then
            DeleteToolStripMenuItem.Enabled = True
        ElseIf SelectedRecordSource = "ExpensesRecords" Then
            DeleteToolStripMenuItem.Enabled = True
        ElseIf SelectedRecordSource = "DebtsPayments" Then
            DeleteToolStripMenuItem.Enabled = True
        Else
            DeleteToolStripMenuItem.Enabled = False
        End If
    End Sub
    Private SelectedRecordSource, Del_A, Del_B, Del_C, Del_D, Del_G, Del_x_B, Del_x_C As String

    Private Sub PaymentsSearchTextBox_TextChanged(sender As Object, e As EventArgs) Handles PaymentsSearchTextBox.TextChanged
        Try
            OpenConnection()
            DebtPaymentsDataGridView.Rows.Clear()
            Query = "SELECT * FROM paymentsmade " _
            & "WHERE date LIKE '%" & PaymentsSearchTextBox.Text & "%' " _
            & "OR orderno LIKE '%" & PaymentsSearchTextBox.Text & "%' " _
            & "OR customerid LIKE '%" & PaymentsSearchTextBox.Text & "%' " _
            & "OR debtor LIKE '%" & PaymentsSearchTextBox.Text & "%' " _
            & "ORDER BY datetime desc"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim a = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                Dim b = Reader.GetString("orderno")
                Dim c = Reader.GetString("customerid")
                Dim d = Reader.GetString("debtor")
                Dim f = Reader.GetDecimal("paidamount")
                DebtPaymentsDataGridView.Rows.Add(a, b, c, d, f)
            End While
            DebtPaymentsDataGridView.ClearSelection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Reader.Close()
        MysqlCon.Close()
    End Sub

    Private Sub SearchSaleTextBox_TextChanged(sender As Object, e As EventArgs) Handles SearchSaleTextBox.TextChanged
        DailysalesDataGridView.Rows.Clear()
        Try
            OpenConnection()
            Query = "select * from todayssale where date like '%" & SearchSaleTextBox.Text & "%' order by date desc"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim A = Reader.GetDateTime("date").ToString("yyyy-MM-dd")
                Dim B = Reader.GetDecimal("grosssale")
                Dim C = Reader.GetDecimal("totaldiscount")
                Dim D = Reader.GetDecimal("totalexpenses")
                Dim Netsale As Decimal = B - C - D
                DailysalesDataGridView.Rows.Add(A, B, C, D, Netsale)
            End While
            Reader.Close()
            DailysalesDataGridView.ClearSelection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        MysqlCon.Close()
    End Sub

    Private Sub PrintSpecificRecordsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PrintSpecificRecordsToolStripMenuItem.Click
        PrintButton_Click(sender, e)
    End Sub

    Private Del_E, Del_F, Del_H, Del_I, Del_J, Del_x_D As Decimal
    Private Del_K, Del_x_A As Date
    Private Del_PM_A As Date
    Private Del_PM_B As Decimal
    Private Del_PM_C As DateTime
    Private productnoInteger As Integer
    Private Sub DeleteSelectedRecord(sender As Object, e As EventArgs)
        'This will allow the user to delete 1 record.
        Try
            OpenConnection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Dim countrecord As Integer
        'Delete record from soldproducts
        If SelectedRecordSource = "SoldProductsRecords" Then
            Dim mesres As DialogResult = MessageBox.Show("Delete the selected record from sold products?", "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If mesres = DialogResult.Yes Then
                Try
                    Query = "select * from todayssoldproducts where " _
                    & "productname = '" & Del_A & "' and " _
                    & "productbrand = '" & Del_B & "' and " _
                    & "productdescription = '" & Del_C & "' and " _
                    & "customername = '" & Del_D & "' and " _
                    & "saleprice = " & Del_E & " and " _
                    & "quantity = " & Del_F & "  and " _
                    & "measuringunit = '" & Del_G & "' and " _
                    & "totalprice = " & Del_H & " and " _
                    & "discount = " & Del_I & " and " _
                    & "discountedprice = " & Del_J & " and " _
                    & "date = '" & Del_K.ToString("yyyy-MM-dd") & "'"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    While Reader.Read
                        countrecord += 1
                        productnoInteger = Reader.GetInt32("no")
                    End While
                    Reader.Close()
                    If countrecord >= 1 Then
                        Try
                            Query = "Delete from todayssoldproducts where " _
                            & "productname = '" & Del_A & "' and " _
                            & "productbrand = '" & Del_B & "' and " _
                            & "productdescription = '" & Del_C & "' and " _
                            & "customername = '" & Del_D & "' and " _
                            & "saleprice = " & Del_E & " and " _
                            & "quantity = " & Del_F & "  and " _
                            & "measuringunit = '" & Del_G & "' and " _
                            & "totalprice = " & Del_H & " and " _
                            & "discount = " & Del_I & " and " _
                            & "discountedprice = " & Del_J & " and " _
                            & "date = '" & Del_K.ToString("yyyy-MM-dd") & "'"
                            Command = New MySqlCommand(Query, MysqlCon)
                            Reader = Command.ExecuteReader
                            Reader.Close()
                        Catch ex As Exception
                            MessageBox.Show(ex.Message)
                        End Try
                        'just in case there are equally same record
                        If countrecord > 1 Then
                            Try
                                For i As Integer = 1 To countrecord - 1
                                    Query = "insert into todayssoldproducts values(" & productnoInteger & ", '" & Del_A & "', '" & Del_B & "', '" _
                                        & Del_C & "', '" & Del_D & "', " & Del_E & ", " & Del_F & ", '" & Del_G & "', " & Del_H & ", " & Del_I & ", " _
                                        & Del_J & ", '" & Del_K.ToString("yyyy-MM-dd") & "')"
                                    Command = New MySqlCommand(Query, MysqlCon)
                                    Reader = Command.ExecuteReader
                                    Reader.Close()
                                Next
                            Catch ex As Exception
                                MessageBox.Show(ex.Message)
                            End Try
                        End If
                        'Add back the quantity of product record to inventory
                        Try
                            Query = "update products set availablestock = availablestock + " & Del_F & "where no = " & productnoInteger
                            Command = New MySqlCommand(Query, MysqlCon)
                            Reader = Command.ExecuteReader
                            Reader.Close()
                        Catch ex As Exception
                            MessageBox.Show(ex.Message)
                        End Try
                        StoreForm.StoreForm_Load(sender, e)
                        RecordsForm_Load(sender, e)
                        MessageBox.Show("Record Deleted Successfully!")
                    End If
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            Else
                SelectedRecordSource = ""
            End If
        ElseIf SelectedRecordSource = "ExpensesRecords" Then
            Dim mesres As DialogResult = MessageBox.Show("Delete the selected record from expenses?", "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If mesres = DialogResult.Yes Then
                Try
                    Query = "select * from todaysexpenses where date = '" & Del_x_A.ToString("yyyy-MM-dd") _
                        & "' and name = '" & Del_x_B & "' and description = '" & Del_x_C & "' and cost = " & Del_x_D
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    While Reader.Read
                        countrecord += 1
                    End While
                    Reader.Close()
                    If countrecord >= 1 Then
                        Try
                            Query = "delete from todaysexpenses where date = '" & Del_x_A.ToString("yyyy-MM-dd") _
                                & "' and name = '" & Del_x_B & "' and description = '" & Del_x_C & "' and cost = " & Del_x_D
                            Command = New MySqlCommand(Query, MysqlCon)
                            Reader = Command.ExecuteReader
                            Reader.Close()
                        Catch ex As Exception
                            MessageBox.Show(ex.Message)
                        End Try
                    End If
                    If countrecord > 1 Then
                        Try
                            For i As Integer = 1 To countrecord - 1
                                Query = "insert into todaysexpenses values('" & Del_x_A.ToString("yyyy-MM-dd") & "', '" & Del_x_B & "', '" _
                                    & Del_x_C & "', " & Del_x_D & ")"
                                Command = New MySqlCommand(Query, MysqlCon)
                                Reader = Command.ExecuteReader
                                Reader.Close()
                            Next
                        Catch ex As Exception
                            MessageBox.Show(ex.Message)
                        End Try
                    End If
                    StoreForm.StoreForm_Load(sender, e)
                    RecordsForm_Load(sender, e)
                    MessageBox.Show("Record Deleted Successfully!")
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            Else
                SelectedRecordSource = ""
            End If
        ElseIf SelectedRecordSource = "DebtsPayments" Then
            Dim customerfullname As String
            Dim count As Integer
            Dim mesres As DialogResult = MessageBox.Show("Delete the selected record from debts payments?", "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If mesres = DialogResult.Yes Then
                Try
                    Query = "select * from paymentsmade where paidamount = " & Del_PM_B & " and date = '" & Del_PM_A.ToString("yyyy-MM-dd") & "' and datetime = '" & Del_PM_C.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    While Reader.Read
                        customerfullname = Reader.GetString("customerfullname")
                        count += 1
                    End While
                    Reader.Close()
                    Try
                        Query = "delete from paymentsmade where customerfullname = '" & customerfullname _
                       & "' and date = '" & Del_PM_A.ToString("yyyy-MM-dd") & "' and paidamount = " & Del_PM_B & " and datetime = '" & Del_PM_C.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                        Command = New MySqlCommand(Query, MysqlCon)
                        Reader = Command.ExecuteReader
                        Reader.Close()
                        Try
                            Dim i As Integer = 1
                            While count > i
                                Query = "insert into paymentsmade values('" & customerfullname & "', " & Del_PM_B _
                                    & ", '" & Del_PM_A.ToString("yyyy-MM-dd") & "', '" & Del_PM_C.ToString("yyyy-MM-dd HH:mm:ss") & "')"
                                Command = New MySqlCommand(Query, MysqlCon)
                                Reader = Command.ExecuteReader
                                Reader.Close()
                                i += 1
                            End While
                            Dim CountFromCredits As Integer
                            Try
                                Query = "select count(*) count from credits where datetime = '" & Del_PM_C.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                                Command = New MySqlCommand(Query, MysqlCon)
                                Reader = Command.ExecuteReader
                                While Reader.Read
                                    CountFromCredits = 1
                                End While
                                Reader.Close()
                                If CountFromCredits > 0 Then
                                    Query = "update credits set amountpaid = amountpaid - " & Del_PM_B & "where datetime = '" & Del_PM_C.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                                    Command = New MySqlCommand(Query, MysqlCon)
                                    Reader = Command.ExecuteReader
                                    Reader.Close()
                                    StoreForm.StoreForm_Load(sender, e)
                                    RecordsForm_Load(sender, e)
                                    MessageBox.Show("Record Deleted Successfully!")
                                End If
                            Catch ex As Exception
                                MessageBox.Show(ex.Message)
                            End Try
                        Catch ex As Exception
                            MessageBox.Show(ex.Message)
                        End Try
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            Else
                SelectedRecordSource = ""
            End If
        Else
            MessageBox.Show("No Record Selected!")
            End If

            MysqlCon.Close()
    End Sub

End Class