﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class AddInfoForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AddInfoForm))
        Me.CustomerFullNameTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CustomerContactTextBox = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.MoreInfoTextBox = New System.Windows.Forms.TextBox()
        Me.MoreInfoListBox = New System.Windows.Forms.ListBox()
        Me.AddInfoButton = New System.Windows.Forms.Button()
        Me.AddCreditsButton = New System.Windows.Forms.Button()
        Me.CancelCreditsButton = New System.Windows.Forms.Button()
        Me.RemoveInfoButton = New System.Windows.Forms.Button()
        Me.DebtorsPhotoPictureBox = New System.Windows.Forms.PictureBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.BrowseDebtorsPhotoButton = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.DebtorsIdTextBox = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ExistingDebtorCheckBox = New System.Windows.Forms.CheckBox()
        Me.SelectDebtorComboBox = New System.Windows.Forms.ComboBox()
        CType(Me.DebtorsPhotoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CustomerFullNameTextBox
        '
        Me.CustomerFullNameTextBox.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CustomerFullNameTextBox.Location = New System.Drawing.Point(174, 188)
        Me.CustomerFullNameTextBox.Name = "CustomerFullNameTextBox"
        Me.CustomerFullNameTextBox.Size = New System.Drawing.Size(282, 26)
        Me.CustomerFullNameTextBox.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(26, 191)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(131, 18)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Debtor's Full Name"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(26, 223)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 18)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Contact"
        '
        'CustomerContactTextBox
        '
        Me.CustomerContactTextBox.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CustomerContactTextBox.Location = New System.Drawing.Point(174, 220)
        Me.CustomerContactTextBox.Name = "CustomerContactTextBox"
        Me.CustomerContactTextBox.Size = New System.Drawing.Size(282, 26)
        Me.CustomerContactTextBox.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(26, 302)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(122, 18)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "More Information"
        '
        'MoreInfoTextBox
        '
        Me.MoreInfoTextBox.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MoreInfoTextBox.Location = New System.Drawing.Point(174, 299)
        Me.MoreInfoTextBox.Name = "MoreInfoTextBox"
        Me.MoreInfoTextBox.Size = New System.Drawing.Size(282, 26)
        Me.MoreInfoTextBox.TabIndex = 4
        '
        'MoreInfoListBox
        '
        Me.MoreInfoListBox.FormattingEnabled = True
        Me.MoreInfoListBox.ItemHeight = 14
        Me.MoreInfoListBox.Location = New System.Drawing.Point(174, 328)
        Me.MoreInfoListBox.Name = "MoreInfoListBox"
        Me.MoreInfoListBox.Size = New System.Drawing.Size(282, 102)
        Me.MoreInfoListBox.TabIndex = 6
        '
        'AddInfoButton
        '
        Me.AddInfoButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AddInfoButton.Location = New System.Drawing.Point(264, 266)
        Me.AddInfoButton.Name = "AddInfoButton"
        Me.AddInfoButton.Size = New System.Drawing.Size(93, 29)
        Me.AddInfoButton.TabIndex = 7
        Me.AddInfoButton.Text = "Add Info"
        Me.AddInfoButton.UseVisualStyleBackColor = True
        '
        'AddCreditsButton
        '
        Me.AddCreditsButton.Location = New System.Drawing.Point(249, 456)
        Me.AddCreditsButton.Name = "AddCreditsButton"
        Me.AddCreditsButton.Size = New System.Drawing.Size(99, 41)
        Me.AddCreditsButton.TabIndex = 8
        Me.AddCreditsButton.Text = "&ADD TO CREDITS"
        Me.AddCreditsButton.UseVisualStyleBackColor = True
        '
        'CancelCreditsButton
        '
        Me.CancelCreditsButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.CancelCreditsButton.Location = New System.Drawing.Point(357, 456)
        Me.CancelCreditsButton.Name = "CancelCreditsButton"
        Me.CancelCreditsButton.Size = New System.Drawing.Size(99, 41)
        Me.CancelCreditsButton.TabIndex = 9
        Me.CancelCreditsButton.Text = "CANCEL"
        Me.CancelCreditsButton.UseVisualStyleBackColor = True
        '
        'RemoveInfoButton
        '
        Me.RemoveInfoButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RemoveInfoButton.Location = New System.Drawing.Point(363, 266)
        Me.RemoveInfoButton.Name = "RemoveInfoButton"
        Me.RemoveInfoButton.Size = New System.Drawing.Size(93, 29)
        Me.RemoveInfoButton.TabIndex = 10
        Me.RemoveInfoButton.Text = "Remove"
        Me.RemoveInfoButton.UseVisualStyleBackColor = True
        '
        'DebtorsPhotoPictureBox
        '
        Me.DebtorsPhotoPictureBox.Image = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.uploadphoto
        Me.DebtorsPhotoPictureBox.Location = New System.Drawing.Point(174, 7)
        Me.DebtorsPhotoPictureBox.Name = "DebtorsPhotoPictureBox"
        Me.DebtorsPhotoPictureBox.Size = New System.Drawing.Size(130, 111)
        Me.DebtorsPhotoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.DebtorsPhotoPictureBox.TabIndex = 11
        Me.DebtorsPhotoPictureBox.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(26, 79)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(104, 18)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Debtor's Photo"
        '
        'BrowseDebtorsPhotoButton
        '
        Me.BrowseDebtorsPhotoButton.Location = New System.Drawing.Point(310, 79)
        Me.BrowseDebtorsPhotoButton.Name = "BrowseDebtorsPhotoButton"
        Me.BrowseDebtorsPhotoButton.Size = New System.Drawing.Size(99, 31)
        Me.BrowseDebtorsPhotoButton.TabIndex = 13
        Me.BrowseDebtorsPhotoButton.Text = "Browse Photo"
        Me.BrowseDebtorsPhotoButton.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'DebtorsIdTextBox
        '
        Me.DebtorsIdTextBox.Enabled = False
        Me.DebtorsIdTextBox.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DebtorsIdTextBox.Location = New System.Drawing.Point(174, 156)
        Me.DebtorsIdTextBox.Name = "DebtorsIdTextBox"
        Me.DebtorsIdTextBox.Size = New System.Drawing.Size(282, 26)
        Me.DebtorsIdTextBox.TabIndex = 14
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(26, 159)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(106, 18)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Debtor's ID No"
        '
        'ExistingDebtorCheckBox
        '
        Me.ExistingDebtorCheckBox.AutoSize = True
        Me.ExistingDebtorCheckBox.BackColor = System.Drawing.Color.Transparent
        Me.ExistingDebtorCheckBox.Font = New System.Drawing.Font("Tahoma", 11.25!)
        Me.ExistingDebtorCheckBox.Location = New System.Drawing.Point(24, 126)
        Me.ExistingDebtorCheckBox.Name = "ExistingDebtorCheckBox"
        Me.ExistingDebtorCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ExistingDebtorCheckBox.Size = New System.Drawing.Size(124, 22)
        Me.ExistingDebtorCheckBox.TabIndex = 15
        Me.ExistingDebtorCheckBox.Text = "Existing Debtor"
        Me.ExistingDebtorCheckBox.UseVisualStyleBackColor = False
        '
        'SelectDebtorComboBox
        '
        Me.SelectDebtorComboBox.Font = New System.Drawing.Font("Tahoma", 11.25!)
        Me.SelectDebtorComboBox.ForeColor = System.Drawing.Color.Black
        Me.SelectDebtorComboBox.FormattingEnabled = True
        Me.SelectDebtorComboBox.Location = New System.Drawing.Point(174, 124)
        Me.SelectDebtorComboBox.Name = "SelectDebtorComboBox"
        Me.SelectDebtorComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SelectDebtorComboBox.Size = New System.Drawing.Size(282, 26)
        Me.SelectDebtorComboBox.TabIndex = 16
        Me.SelectDebtorComboBox.Text = "SELECT DEBTOR"
        Me.SelectDebtorComboBox.Visible = False
        '
        'AddInfoForm
        '
        Me.AcceptButton = Me.AddInfoButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.ROJBackground
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.CancelButton = Me.CancelCreditsButton
        Me.ClientSize = New System.Drawing.Size(481, 507)
        Me.Controls.Add(Me.SelectDebtorComboBox)
        Me.Controls.Add(Me.ExistingDebtorCheckBox)
        Me.Controls.Add(Me.DebtorsIdTextBox)
        Me.Controls.Add(Me.BrowseDebtorsPhotoButton)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.DebtorsPhotoPictureBox)
        Me.Controls.Add(Me.RemoveInfoButton)
        Me.Controls.Add(Me.CancelCreditsButton)
        Me.Controls.Add(Me.AddCreditsButton)
        Me.Controls.Add(Me.AddInfoButton)
        Me.Controls.Add(Me.MoreInfoListBox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.MoreInfoTextBox)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.CustomerContactTextBox)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CustomerFullNameTextBox)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "AddInfoForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add Debtors Information"
        CType(Me.DebtorsPhotoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents CustomerFullNameTextBox As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents CustomerContactTextBox As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents MoreInfoTextBox As TextBox
    Friend WithEvents MoreInfoListBox As ListBox
    Friend WithEvents AddInfoButton As Button
    Friend WithEvents AddCreditsButton As Button
    Friend WithEvents CancelCreditsButton As Button
    Friend WithEvents RemoveInfoButton As Button
    Friend WithEvents DebtorsPhotoPictureBox As PictureBox
    Friend WithEvents Label4 As Label
    Friend WithEvents BrowseDebtorsPhotoButton As Button
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents DebtorsIdTextBox As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents ExistingDebtorCheckBox As CheckBox
    Friend WithEvents SelectDebtorComboBox As ComboBox
End Class
