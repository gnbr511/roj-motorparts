﻿Imports MySql.Data.MySqlClient
Public Class StocksHistoryForm
    Private Sub StocksHistoryForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            OpenConnection()

            Query = "select * from products where no = " & InventoryForm.ProductNo
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                NameLabel.Text = Reader.GetString("productname")
                BrandLabel.Text = Reader.GetString("productbrand")
                DescriptionLabel.Text = Reader.GetString("productdescription")
                CurrentStockLabel.Text = Reader.GetDecimal("availablestock")
            End While
            Reader.Close()

            stockHistoryDataGridView.Rows.Clear()
            Query = "select * from stockshistory where no = " & InventoryForm.ProductNo & " order by dateofadding desc"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                stockHistoryDataGridView.Rows.Add(Reader.GetDateTime("dateofadding").ToString("dd-MM-yyyy"), Reader.GetDecimal("stocksadded"), Reader.GetDecimal("totalstocks"))
            End While
            Reader.Close()
            stockHistoryDataGridView.ClearSelection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        MysqlCon.Close()
    End Sub

End Class