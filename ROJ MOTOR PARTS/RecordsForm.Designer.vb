﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class RecordsForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RecordsForm))
        Me.SoldProductsDataGridView = New System.Windows.Forms.DataGridView()
        Me.DateColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OrderNumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustomernameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductnameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductbrandColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductDescriptionColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PurchaseCost = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SalePriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QuantityColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MeasuringunitColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalPriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RdiscountColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RfinalPriceColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.SearchProductRecordsTextBox = New System.Windows.Forms.TextBox()
        Me.ExpensesDataGridView = New System.Windows.Forms.DataGridView()
        Me.DateexpColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ExpNameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ExpDescriptionColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ExpCostColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DailysalesDataGridView = New System.Windows.Forms.DataGridView()
        Me.SaleDateColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GrossSaleColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.totdiscountColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.totExpensesColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NetSaleColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.ExpensesSearchTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.SearchSaleTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.DateToLabel = New System.Windows.Forms.Label()
        Me.DateFromLabel = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.totnetsaleTextBox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.totdiscountTextBox = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.totexpTextBox = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.totgrossTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SearchRecordsButton = New System.Windows.Forms.Button()
        Me.RefreshButton = New System.Windows.Forms.Button()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.PrintButton = New System.Windows.Forms.Button()
        Me.DeleteRecordsButton = New System.Windows.Forms.Button()
        Me.BackUpRecordsButton = New System.Windows.Forms.Button()
        Me.CreditsRecordsButton = New System.Windows.Forms.Button()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.DebtPaymentsDataGridView = New System.Windows.Forms.DataGridView()
        Me.DebtpaymentDateColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OrderNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DebtorsIDNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DebtPaymentsCusFullNameColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DebtPaymentsPaidAmountColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.datetime = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PaymentsSearchTextBox = New System.Windows.Forms.TextBox()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.CONTROLSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.RefreshRecordsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SearchRecordsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintSpecificRecordsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CreditsRecordsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteRecordsByDateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BackUpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        CType(Me.SoldProductsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.ExpensesDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DailysalesDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        CType(Me.DebtPaymentsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SoldProductsDataGridView
        '
        Me.SoldProductsDataGridView.AllowUserToAddRows = False
        Me.SoldProductsDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.White
        Me.SoldProductsDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.SoldProductsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.SoldProductsDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DateColumn, Me.OrderNumber, Me.CustomernameColumn, Me.ProductNo, Me.ProductnameColumn, Me.ProductbrandColumn, Me.ProductDescriptionColumn, Me.PurchaseCost, Me.SalePriceColumn, Me.QuantityColumn, Me.MeasuringunitColumn, Me.TotalPriceColumn, Me.RdiscountColumn, Me.RfinalPriceColumn})
        Me.SoldProductsDataGridView.Location = New System.Drawing.Point(8, 38)
        Me.SoldProductsDataGridView.Name = "SoldProductsDataGridView"
        Me.SoldProductsDataGridView.ReadOnly = True
        Me.SoldProductsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.SoldProductsDataGridView.Size = New System.Drawing.Size(1257, 269)
        Me.SoldProductsDataGridView.TabIndex = 0
        '
        'DateColumn
        '
        Me.DateColumn.HeaderText = "Date"
        Me.DateColumn.Name = "DateColumn"
        Me.DateColumn.ReadOnly = True
        Me.DateColumn.Width = 70
        '
        'OrderNumber
        '
        Me.OrderNumber.HeaderText = "Order No"
        Me.OrderNumber.Name = "OrderNumber"
        Me.OrderNumber.ReadOnly = True
        '
        'CustomernameColumn
        '
        Me.CustomernameColumn.HeaderText = "Customer Name"
        Me.CustomernameColumn.Name = "CustomernameColumn"
        Me.CustomernameColumn.ReadOnly = True
        Me.CustomernameColumn.Width = 120
        '
        'ProductNo
        '
        Me.ProductNo.HeaderText = "Product No"
        Me.ProductNo.Name = "ProductNo"
        Me.ProductNo.ReadOnly = True
        Me.ProductNo.Width = 90
        '
        'ProductnameColumn
        '
        Me.ProductnameColumn.HeaderText = "Product Name"
        Me.ProductnameColumn.Name = "ProductnameColumn"
        Me.ProductnameColumn.ReadOnly = True
        Me.ProductnameColumn.Width = 120
        '
        'ProductbrandColumn
        '
        Me.ProductbrandColumn.HeaderText = "Product Brand"
        Me.ProductbrandColumn.Name = "ProductbrandColumn"
        Me.ProductbrandColumn.ReadOnly = True
        Me.ProductbrandColumn.Width = 110
        '
        'ProductDescriptionColumn
        '
        Me.ProductDescriptionColumn.HeaderText = "Product Description"
        Me.ProductDescriptionColumn.Name = "ProductDescriptionColumn"
        Me.ProductDescriptionColumn.ReadOnly = True
        Me.ProductDescriptionColumn.Width = 110
        '
        'PurchaseCost
        '
        Me.PurchaseCost.HeaderText = "Purchase Cost"
        Me.PurchaseCost.Name = "PurchaseCost"
        Me.PurchaseCost.ReadOnly = True
        Me.PurchaseCost.Width = 70
        '
        'SalePriceColumn
        '
        Me.SalePriceColumn.HeaderText = "Sale Price"
        Me.SalePriceColumn.Name = "SalePriceColumn"
        Me.SalePriceColumn.ReadOnly = True
        Me.SalePriceColumn.Width = 70
        '
        'QuantityColumn
        '
        Me.QuantityColumn.HeaderText = "Quantity"
        Me.QuantityColumn.Name = "QuantityColumn"
        Me.QuantityColumn.ReadOnly = True
        Me.QuantityColumn.Width = 70
        '
        'MeasuringunitColumn
        '
        Me.MeasuringunitColumn.HeaderText = "Measuring Unit"
        Me.MeasuringunitColumn.Name = "MeasuringunitColumn"
        Me.MeasuringunitColumn.ReadOnly = True
        Me.MeasuringunitColumn.Width = 70
        '
        'TotalPriceColumn
        '
        Me.TotalPriceColumn.HeaderText = "Total Price"
        Me.TotalPriceColumn.Name = "TotalPriceColumn"
        Me.TotalPriceColumn.ReadOnly = True
        Me.TotalPriceColumn.Width = 70
        '
        'RdiscountColumn
        '
        Me.RdiscountColumn.HeaderText = "Discount"
        Me.RdiscountColumn.Name = "RdiscountColumn"
        Me.RdiscountColumn.ReadOnly = True
        Me.RdiscountColumn.Width = 70
        '
        'RfinalPriceColumn
        '
        Me.RfinalPriceColumn.HeaderText = "Final Price"
        Me.RfinalPriceColumn.Name = "RfinalPriceColumn"
        Me.RfinalPriceColumn.ReadOnly = True
        Me.RfinalPriceColumn.Width = 70
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.SearchProductRecordsTextBox)
        Me.GroupBox1.Controls.Add(Me.SoldProductsDataGridView)
        Me.GroupBox1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(11, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1273, 316)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "SOLD PRODUCTS"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.Label7.Location = New System.Drawing.Point(875, 15)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(128, 14)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "Search Sold Product/s"
        '
        'SearchProductRecordsTextBox
        '
        Me.SearchProductRecordsTextBox.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SearchProductRecordsTextBox.Location = New System.Drawing.Point(1009, 12)
        Me.SearchProductRecordsTextBox.Name = "SearchProductRecordsTextBox"
        Me.SearchProductRecordsTextBox.Size = New System.Drawing.Size(257, 22)
        Me.SearchProductRecordsTextBox.TabIndex = 1
        '
        'ExpensesDataGridView
        '
        Me.ExpensesDataGridView.AllowUserToAddRows = False
        Me.ExpensesDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Gainsboro
        Me.ExpensesDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.ExpensesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ExpensesDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DateexpColumn, Me.ExpNameColumn, Me.ExpDescriptionColumn, Me.ExpCostColumn})
        Me.ExpensesDataGridView.Location = New System.Drawing.Point(8, 41)
        Me.ExpensesDataGridView.Name = "ExpensesDataGridView"
        Me.ExpensesDataGridView.ReadOnly = True
        Me.ExpensesDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ExpensesDataGridView.Size = New System.Drawing.Size(496, 142)
        Me.ExpensesDataGridView.TabIndex = 2
        '
        'DateexpColumn
        '
        Me.DateexpColumn.HeaderText = "Date"
        Me.DateexpColumn.Name = "DateexpColumn"
        Me.DateexpColumn.ReadOnly = True
        Me.DateexpColumn.Width = 70
        '
        'ExpNameColumn
        '
        Me.ExpNameColumn.HeaderText = "Expenses Name"
        Me.ExpNameColumn.Name = "ExpNameColumn"
        Me.ExpNameColumn.ReadOnly = True
        Me.ExpNameColumn.Width = 150
        '
        'ExpDescriptionColumn
        '
        Me.ExpDescriptionColumn.HeaderText = "Description"
        Me.ExpDescriptionColumn.Name = "ExpDescriptionColumn"
        Me.ExpDescriptionColumn.ReadOnly = True
        Me.ExpDescriptionColumn.Width = 150
        '
        'ExpCostColumn
        '
        Me.ExpCostColumn.HeaderText = "Cost"
        Me.ExpCostColumn.Name = "ExpCostColumn"
        Me.ExpCostColumn.ReadOnly = True
        Me.ExpCostColumn.Width = 80
        '
        'DailysalesDataGridView
        '
        Me.DailysalesDataGridView.AllowUserToAddRows = False
        Me.DailysalesDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro
        Me.DailysalesDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle3
        Me.DailysalesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DailysalesDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SaleDateColumn, Me.GrossSaleColumn, Me.totdiscountColumn, Me.totExpensesColumn, Me.NetSaleColumn})
        Me.DailysalesDataGridView.Location = New System.Drawing.Point(14, 41)
        Me.DailysalesDataGridView.Name = "DailysalesDataGridView"
        Me.DailysalesDataGridView.ReadOnly = True
        Me.DailysalesDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DailysalesDataGridView.Size = New System.Drawing.Size(513, 142)
        Me.DailysalesDataGridView.TabIndex = 3
        '
        'SaleDateColumn
        '
        Me.SaleDateColumn.HeaderText = "Date"
        Me.SaleDateColumn.Name = "SaleDateColumn"
        Me.SaleDateColumn.ReadOnly = True
        Me.SaleDateColumn.Width = 70
        '
        'GrossSaleColumn
        '
        Me.GrossSaleColumn.HeaderText = "Gross Sale"
        Me.GrossSaleColumn.Name = "GrossSaleColumn"
        Me.GrossSaleColumn.ReadOnly = True
        '
        'totdiscountColumn
        '
        Me.totdiscountColumn.HeaderText = "Total Discount"
        Me.totdiscountColumn.Name = "totdiscountColumn"
        Me.totdiscountColumn.ReadOnly = True
        '
        'totExpensesColumn
        '
        Me.totExpensesColumn.HeaderText = "Total Expenses"
        Me.totExpensesColumn.Name = "totExpensesColumn"
        Me.totExpensesColumn.ReadOnly = True
        '
        'NetSaleColumn
        '
        Me.NetSaleColumn.HeaderText = "Net Sale"
        Me.NetSaleColumn.Name = "NetSaleColumn"
        Me.NetSaleColumn.ReadOnly = True
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.ExpensesSearchTextBox)
        Me.GroupBox2.Controls.Add(Me.ExpensesDataGridView)
        Me.GroupBox2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(11, 526)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(512, 189)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "DAILY EXPENSES"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.Label8.Location = New System.Drawing.Point(190, 18)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(99, 14)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "Search Expenses"
        '
        'ExpensesSearchTextBox
        '
        Me.ExpensesSearchTextBox.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ExpensesSearchTextBox.Location = New System.Drawing.Point(295, 13)
        Me.ExpensesSearchTextBox.Name = "ExpensesSearchTextBox"
        Me.ExpensesSearchTextBox.Size = New System.Drawing.Size(209, 22)
        Me.ExpensesSearchTextBox.TabIndex = 3
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.DailysalesDataGridView)
        Me.GroupBox3.Controls.Add(Me.SearchSaleTextBox)
        Me.GroupBox3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(529, 526)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(541, 189)
        Me.GroupBox3.TabIndex = 5
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "DAILY SALES"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.Label9.Location = New System.Drawing.Point(242, 18)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(70, 14)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Search Sale"
        '
        'SearchSaleTextBox
        '
        Me.SearchSaleTextBox.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SearchSaleTextBox.Location = New System.Drawing.Point(318, 13)
        Me.SearchSaleTextBox.Name = "SearchSaleTextBox"
        Me.SearchSaleTextBox.Size = New System.Drawing.Size(209, 22)
        Me.SearchSaleTextBox.TabIndex = 5
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox4.Controls.Add(Me.DateToLabel)
        Me.GroupBox4.Controls.Add(Me.DateFromLabel)
        Me.GroupBox4.Controls.Add(Me.Label6)
        Me.GroupBox4.Controls.Add(Me.Label5)
        Me.GroupBox4.Controls.Add(Me.totnetsaleTextBox)
        Me.GroupBox4.Controls.Add(Me.Label4)
        Me.GroupBox4.Controls.Add(Me.totdiscountTextBox)
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Controls.Add(Me.totexpTextBox)
        Me.GroupBox4.Controls.Add(Me.Label2)
        Me.GroupBox4.Controls.Add(Me.totgrossTextBox)
        Me.GroupBox4.Controls.Add(Me.Label1)
        Me.GroupBox4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(529, 320)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(541, 204)
        Me.GroupBox4.TabIndex = 6
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "SALES RECORDS SUMMARY"
        '
        'DateToLabel
        '
        Me.DateToLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DateToLabel.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateToLabel.Location = New System.Drawing.Point(22, 114)
        Me.DateToLabel.Name = "DateToLabel"
        Me.DateToLabel.Size = New System.Drawing.Size(139, 23)
        Me.DateToLabel.TabIndex = 11
        Me.DateToLabel.Text = "DATE"
        Me.DateToLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'DateFromLabel
        '
        Me.DateFromLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DateFromLabel.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateFromLabel.Location = New System.Drawing.Point(22, 54)
        Me.DateFromLabel.Name = "DateFromLabel"
        Me.DateFromLabel.Size = New System.Drawing.Size(139, 23)
        Me.DateFromLabel.TabIndex = 10
        Me.DateFromLabel.Text = "DATE"
        Me.DateFromLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(19, 91)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(29, 16)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "TO:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(19, 31)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 16)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "FROM:"
        '
        'totnetsaleTextBox
        '
        Me.totnetsaleTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.totnetsaleTextBox.Location = New System.Drawing.Point(354, 153)
        Me.totnetsaleTextBox.Name = "totnetsaleTextBox"
        Me.totnetsaleTextBox.ReadOnly = True
        Me.totnetsaleTextBox.Size = New System.Drawing.Size(173, 23)
        Me.totnetsaleTextBox.TabIndex = 7
        Me.totnetsaleTextBox.TabStop = False
        Me.totnetsaleTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(193, 156)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(110, 16)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "TOTAL NET SALE"
        '
        'totdiscountTextBox
        '
        Me.totdiscountTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.totdiscountTextBox.Location = New System.Drawing.Point(354, 118)
        Me.totdiscountTextBox.Name = "totdiscountTextBox"
        Me.totdiscountTextBox.ReadOnly = True
        Me.totdiscountTextBox.Size = New System.Drawing.Size(173, 23)
        Me.totdiscountTextBox.TabIndex = 5
        Me.totdiscountTextBox.TabStop = False
        Me.totdiscountTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(193, 121)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(155, 16)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "TOTAL DISCOUNT GIVEN"
        '
        'totexpTextBox
        '
        Me.totexpTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.totexpTextBox.Location = New System.Drawing.Point(354, 84)
        Me.totexpTextBox.Name = "totexpTextBox"
        Me.totexpTextBox.ReadOnly = True
        Me.totexpTextBox.Size = New System.Drawing.Size(173, 23)
        Me.totexpTextBox.TabIndex = 3
        Me.totexpTextBox.TabStop = False
        Me.totexpTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(193, 90)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(114, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "TOTAL EXPENSES"
        '
        'totgrossTextBox
        '
        Me.totgrossTextBox.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.totgrossTextBox.Location = New System.Drawing.Point(354, 50)
        Me.totgrossTextBox.Name = "totgrossTextBox"
        Me.totgrossTextBox.ReadOnly = True
        Me.totgrossTextBox.Size = New System.Drawing.Size(173, 23)
        Me.totgrossTextBox.TabIndex = 1
        Me.totgrossTextBox.TabStop = False
        Me.totgrossTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(193, 53)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(130, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "TOTAL GROSS SALE"
        '
        'SearchRecordsButton
        '
        Me.SearchRecordsButton.BackColor = System.Drawing.SystemColors.Control
        Me.SearchRecordsButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SearchRecordsButton.ForeColor = System.Drawing.Color.Black
        Me.SearchRecordsButton.Location = New System.Drawing.Point(9, 76)
        Me.SearchRecordsButton.Name = "SearchRecordsButton"
        Me.SearchRecordsButton.Size = New System.Drawing.Size(189, 47)
        Me.SearchRecordsButton.TabIndex = 7
        Me.SearchRecordsButton.Text = "SEARCH RECORDS"
        Me.ToolTip1.SetToolTip(Me.SearchRecordsButton, "Lets you search records by date.")
        Me.SearchRecordsButton.UseVisualStyleBackColor = False
        '
        'RefreshButton
        '
        Me.RefreshButton.BackColor = System.Drawing.SystemColors.Control
        Me.RefreshButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RefreshButton.ForeColor = System.Drawing.Color.Black
        Me.RefreshButton.Location = New System.Drawing.Point(9, 23)
        Me.RefreshButton.Name = "RefreshButton"
        Me.RefreshButton.Size = New System.Drawing.Size(189, 47)
        Me.RefreshButton.TabIndex = 8
        Me.RefreshButton.Text = "REFRESH RECORDS"
        Me.RefreshButton.UseVisualStyleBackColor = False
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox5.Controls.Add(Me.PrintButton)
        Me.GroupBox5.Controls.Add(Me.DeleteRecordsButton)
        Me.GroupBox5.Controls.Add(Me.BackUpRecordsButton)
        Me.GroupBox5.Controls.Add(Me.CreditsRecordsButton)
        Me.GroupBox5.Controls.Add(Me.RefreshButton)
        Me.GroupBox5.Controls.Add(Me.SearchRecordsButton)
        Me.GroupBox5.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.ForeColor = System.Drawing.Color.Black
        Me.GroupBox5.Location = New System.Drawing.Point(1076, 319)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(208, 396)
        Me.GroupBox5.TabIndex = 9
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "CONTROLS"
        Me.ToolTip1.SetToolTip(Me.GroupBox5, "Right Click for Shortcut")
        '
        'PrintButton
        '
        Me.PrintButton.BackColor = System.Drawing.SystemColors.Control
        Me.PrintButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PrintButton.ForeColor = System.Drawing.Color.Black
        Me.PrintButton.Location = New System.Drawing.Point(9, 129)
        Me.PrintButton.Name = "PrintButton"
        Me.PrintButton.Size = New System.Drawing.Size(189, 47)
        Me.PrintButton.TabIndex = 12
        Me.PrintButton.Text = "PRINT RECORDS"
        Me.ToolTip1.SetToolTip(Me.PrintButton, "Lets you print specific records.")
        Me.PrintButton.UseVisualStyleBackColor = False
        '
        'DeleteRecordsButton
        '
        Me.DeleteRecordsButton.BackColor = System.Drawing.SystemColors.Control
        Me.DeleteRecordsButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DeleteRecordsButton.ForeColor = System.Drawing.Color.Black
        Me.DeleteRecordsButton.Location = New System.Drawing.Point(117, 258)
        Me.DeleteRecordsButton.Name = "DeleteRecordsButton"
        Me.DeleteRecordsButton.Size = New System.Drawing.Size(41, 19)
        Me.DeleteRecordsButton.TabIndex = 11
        Me.DeleteRecordsButton.Text = "DELETE RECORDS"
        Me.ToolTip1.SetToolTip(Me.DeleteRecordsButton, "Lets you delete records of specific dates.")
        Me.DeleteRecordsButton.UseVisualStyleBackColor = False
        Me.DeleteRecordsButton.Visible = False
        '
        'BackUpRecordsButton
        '
        Me.BackUpRecordsButton.BackColor = System.Drawing.SystemColors.Control
        Me.BackUpRecordsButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BackUpRecordsButton.ForeColor = System.Drawing.Color.Black
        Me.BackUpRecordsButton.Location = New System.Drawing.Point(116, 235)
        Me.BackUpRecordsButton.Name = "BackUpRecordsButton"
        Me.BackUpRecordsButton.Size = New System.Drawing.Size(42, 19)
        Me.BackUpRecordsButton.TabIndex = 9
        Me.BackUpRecordsButton.Text = "BACK UP RECORDS"
        Me.ToolTip1.SetToolTip(Me.BackUpRecordsButton, "Lets you back up data of specific dates.")
        Me.BackUpRecordsButton.UseVisualStyleBackColor = False
        Me.BackUpRecordsButton.Visible = False
        '
        'CreditsRecordsButton
        '
        Me.CreditsRecordsButton.BackColor = System.Drawing.SystemColors.Control
        Me.CreditsRecordsButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CreditsRecordsButton.ForeColor = System.Drawing.Color.Black
        Me.CreditsRecordsButton.Location = New System.Drawing.Point(9, 182)
        Me.CreditsRecordsButton.Name = "CreditsRecordsButton"
        Me.CreditsRecordsButton.Size = New System.Drawing.Size(189, 47)
        Me.CreditsRecordsButton.TabIndex = 8
        Me.CreditsRecordsButton.Text = "CREDITS RECORDS"
        Me.ToolTip1.SetToolTip(Me.CreditsRecordsButton, "Open credits records.")
        Me.CreditsRecordsButton.UseVisualStyleBackColor = False
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox6.Controls.Add(Me.Label10)
        Me.GroupBox6.Controls.Add(Me.DebtPaymentsDataGridView)
        Me.GroupBox6.Controls.Add(Me.PaymentsSearchTextBox)
        Me.GroupBox6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox6.Location = New System.Drawing.Point(11, 320)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(512, 204)
        Me.GroupBox6.TabIndex = 10
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "DEBT'S PAYMENTS"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.Label10.Location = New System.Drawing.Point(190, 21)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(101, 14)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "Search Payments"
        '
        'DebtPaymentsDataGridView
        '
        Me.DebtPaymentsDataGridView.AllowUserToAddRows = False
        Me.DebtPaymentsDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.Gainsboro
        Me.DebtPaymentsDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle4
        Me.DebtPaymentsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DebtPaymentsDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DebtpaymentDateColumn, Me.OrderNo, Me.DebtorsIDNo, Me.DebtPaymentsCusFullNameColumn, Me.DebtPaymentsPaidAmountColumn, Me.datetime})
        Me.DebtPaymentsDataGridView.Location = New System.Drawing.Point(8, 46)
        Me.DebtPaymentsDataGridView.Name = "DebtPaymentsDataGridView"
        Me.DebtPaymentsDataGridView.ReadOnly = True
        Me.DebtPaymentsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DebtPaymentsDataGridView.Size = New System.Drawing.Size(496, 150)
        Me.DebtPaymentsDataGridView.TabIndex = 0
        '
        'DebtpaymentDateColumn
        '
        Me.DebtpaymentDateColumn.HeaderText = "Date"
        Me.DebtpaymentDateColumn.Name = "DebtpaymentDateColumn"
        Me.DebtpaymentDateColumn.ReadOnly = True
        Me.DebtpaymentDateColumn.Width = 70
        '
        'OrderNo
        '
        Me.OrderNo.HeaderText = "Order No"
        Me.OrderNo.Name = "OrderNo"
        Me.OrderNo.ReadOnly = True
        '
        'DebtorsIDNo
        '
        Me.DebtorsIDNo.HeaderText = "Debtors ID"
        Me.DebtorsIDNo.Name = "DebtorsIDNo"
        Me.DebtorsIDNo.ReadOnly = True
        '
        'DebtPaymentsCusFullNameColumn
        '
        Me.DebtPaymentsCusFullNameColumn.HeaderText = "Debtor's Full Name"
        Me.DebtPaymentsCusFullNameColumn.Name = "DebtPaymentsCusFullNameColumn"
        Me.DebtPaymentsCusFullNameColumn.ReadOnly = True
        Me.DebtPaymentsCusFullNameColumn.Width = 110
        '
        'DebtPaymentsPaidAmountColumn
        '
        Me.DebtPaymentsPaidAmountColumn.HeaderText = "Paid Amount"
        Me.DebtPaymentsPaidAmountColumn.Name = "DebtPaymentsPaidAmountColumn"
        Me.DebtPaymentsPaidAmountColumn.ReadOnly = True
        Me.DebtPaymentsPaidAmountColumn.Width = 70
        '
        'datetime
        '
        Me.datetime.HeaderText = "datetime"
        Me.datetime.Name = "datetime"
        Me.datetime.ReadOnly = True
        Me.datetime.Visible = False
        '
        'PaymentsSearchTextBox
        '
        Me.PaymentsSearchTextBox.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PaymentsSearchTextBox.Location = New System.Drawing.Point(295, 18)
        Me.PaymentsSearchTextBox.Name = "PaymentsSearchTextBox"
        Me.PaymentsSearchTextBox.Size = New System.Drawing.Size(209, 22)
        Me.PaymentsSearchTextBox.TabIndex = 5
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CONTROLSToolStripMenuItem, Me.ToolStripSeparator1, Me.RefreshRecordsToolStripMenuItem, Me.SearchRecordsToolStripMenuItem, Me.DeleteToolStripMenuItem, Me.PrintSpecificRecordsToolStripMenuItem, Me.CreditsRecordsToolStripMenuItem, Me.DeleteRecordsByDateToolStripMenuItem, Me.BackUpToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(219, 186)
        '
        'CONTROLSToolStripMenuItem
        '
        Me.CONTROLSToolStripMenuItem.Enabled = False
        Me.CONTROLSToolStripMenuItem.Name = "CONTROLSToolStripMenuItem"
        Me.CONTROLSToolStripMenuItem.Size = New System.Drawing.Size(218, 22)
        Me.CONTROLSToolStripMenuItem.Text = "CONTROLS"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(215, 6)
        '
        'RefreshRecordsToolStripMenuItem
        '
        Me.RefreshRecordsToolStripMenuItem.Name = "RefreshRecordsToolStripMenuItem"
        Me.RefreshRecordsToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5
        Me.RefreshRecordsToolStripMenuItem.Size = New System.Drawing.Size(218, 22)
        Me.RefreshRecordsToolStripMenuItem.Text = "Refresh Records"
        '
        'SearchRecordsToolStripMenuItem
        '
        Me.SearchRecordsToolStripMenuItem.Name = "SearchRecordsToolStripMenuItem"
        Me.SearchRecordsToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.SearchRecordsToolStripMenuItem.Size = New System.Drawing.Size(218, 22)
        Me.SearchRecordsToolStripMenuItem.Text = "Search Records"
        '
        'DeleteToolStripMenuItem
        '
        Me.DeleteToolStripMenuItem.Enabled = False
        Me.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem"
        Me.DeleteToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete
        Me.DeleteToolStripMenuItem.Size = New System.Drawing.Size(218, 22)
        Me.DeleteToolStripMenuItem.Text = "Delete Selected Record"
        Me.DeleteToolStripMenuItem.ToolTipText = "Lets you delete one record."
        Me.DeleteToolStripMenuItem.Visible = False
        '
        'PrintSpecificRecordsToolStripMenuItem
        '
        Me.PrintSpecificRecordsToolStripMenuItem.Name = "PrintSpecificRecordsToolStripMenuItem"
        Me.PrintSpecificRecordsToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.PrintSpecificRecordsToolStripMenuItem.Size = New System.Drawing.Size(218, 22)
        Me.PrintSpecificRecordsToolStripMenuItem.Text = "Print Records"
        '
        'CreditsRecordsToolStripMenuItem
        '
        Me.CreditsRecordsToolStripMenuItem.Name = "CreditsRecordsToolStripMenuItem"
        Me.CreditsRecordsToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.CreditsRecordsToolStripMenuItem.Size = New System.Drawing.Size(218, 22)
        Me.CreditsRecordsToolStripMenuItem.Text = "Credits Records"
        '
        'DeleteRecordsByDateToolStripMenuItem
        '
        Me.DeleteRecordsByDateToolStripMenuItem.Enabled = False
        Me.DeleteRecordsByDateToolStripMenuItem.Name = "DeleteRecordsByDateToolStripMenuItem"
        Me.DeleteRecordsByDateToolStripMenuItem.Size = New System.Drawing.Size(218, 22)
        Me.DeleteRecordsByDateToolStripMenuItem.Text = "Delete Records by Date"
        Me.DeleteRecordsByDateToolStripMenuItem.Visible = False
        '
        'BackUpToolStripMenuItem
        '
        Me.BackUpToolStripMenuItem.Enabled = False
        Me.BackUpToolStripMenuItem.Name = "BackUpToolStripMenuItem"
        Me.BackUpToolStripMenuItem.Size = New System.Drawing.Size(218, 22)
        Me.BackUpToolStripMenuItem.Text = "Back Up Specific Records"
        Me.BackUpToolStripMenuItem.ToolTipText = "Make a back up file of sold products, debts payments, expenses, and daily sales r" &
    "ecords of specific dates."
        Me.BackUpToolStripMenuItem.Visible = False
        '
        'RecordsForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ROJ_MOTOR_PARTS.My.Resources.Resources.ROJBackground
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1296, 718)
        Me.ContextMenuStrip = Me.ContextMenuStrip1
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox5)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "RecordsForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Records"
        CType(Me.SoldProductsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.ExpensesDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DailysalesDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        CType(Me.DebtPaymentsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents SoldProductsDataGridView As DataGridView
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents ExpensesDataGridView As DataGridView
    Friend WithEvents DailysalesDataGridView As DataGridView
    Friend WithEvents SaleDateColumn As DataGridViewTextBoxColumn
    Friend WithEvents GrossSaleColumn As DataGridViewTextBoxColumn
    Friend WithEvents totdiscountColumn As DataGridViewTextBoxColumn
    Friend WithEvents totExpensesColumn As DataGridViewTextBoxColumn
    Friend WithEvents NetSaleColumn As DataGridViewTextBoxColumn
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents totnetsaleTextBox As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents totdiscountTextBox As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents totexpTextBox As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents totgrossTextBox As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents DateToLabel As Label
    Friend WithEvents DateFromLabel As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents SearchRecordsButton As Button
    Friend WithEvents RefreshButton As Button
    Friend WithEvents Label7 As Label
    Friend WithEvents SearchProductRecordsTextBox As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents ExpensesSearchTextBox As TextBox
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents DebtPaymentsDataGridView As DataGridView
    Friend WithEvents CreditsRecordsButton As Button
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents BackUpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RefreshRecordsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SearchRecordsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CreditsRecordsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CONTROLSToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents BackUpRecordsButton As Button
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents PrintButton As Button
    Friend WithEvents DeleteRecordsButton As Button
    Friend WithEvents DeleteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrintSpecificRecordsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DeleteRecordsByDateToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Label9 As Label
    Friend WithEvents SearchSaleTextBox As TextBox
    Friend WithEvents DateexpColumn As DataGridViewTextBoxColumn
    Friend WithEvents ExpNameColumn As DataGridViewTextBoxColumn
    Friend WithEvents ExpDescriptionColumn As DataGridViewTextBoxColumn
    Friend WithEvents ExpCostColumn As DataGridViewTextBoxColumn
    Friend WithEvents Label10 As Label
    Friend WithEvents DebtpaymentDateColumn As DataGridViewTextBoxColumn
    Friend WithEvents OrderNo As DataGridViewTextBoxColumn
    Friend WithEvents DebtorsIDNo As DataGridViewTextBoxColumn
    Friend WithEvents DebtPaymentsCusFullNameColumn As DataGridViewTextBoxColumn
    Friend WithEvents DebtPaymentsPaidAmountColumn As DataGridViewTextBoxColumn
    Friend WithEvents datetime As DataGridViewTextBoxColumn
    Friend WithEvents PaymentsSearchTextBox As TextBox
    Friend WithEvents DateColumn As DataGridViewTextBoxColumn
    Friend WithEvents OrderNumber As DataGridViewTextBoxColumn
    Friend WithEvents CustomernameColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductNo As DataGridViewTextBoxColumn
    Friend WithEvents ProductnameColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductbrandColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductDescriptionColumn As DataGridViewTextBoxColumn
    Friend WithEvents PurchaseCost As DataGridViewTextBoxColumn
    Friend WithEvents SalePriceColumn As DataGridViewTextBoxColumn
    Friend WithEvents QuantityColumn As DataGridViewTextBoxColumn
    Friend WithEvents MeasuringunitColumn As DataGridViewTextBoxColumn
    Friend WithEvents TotalPriceColumn As DataGridViewTextBoxColumn
    Friend WithEvents RdiscountColumn As DataGridViewTextBoxColumn
    Friend WithEvents RfinalPriceColumn As DataGridViewTextBoxColumn
End Class
