﻿Imports MySql.Data.MySqlClient
Public Class AddInfoForm
    Private DebtorsPhotoPath As String
    Private Sub AddInfoButton_Click(sender As Object, e As EventArgs) Handles AddInfoButton.Click
        If MoreInfoTextBox.Text = "" Then
        Else
            MoreInfoListBox.Items.Add(MoreInfoTextBox.Text)
            MoreInfoTextBox.Clear()
            MoreInfoTextBox.Focus()
        End If
    End Sub

    Private Sub RemoveInfoButton_Click(sender As Object, e As EventArgs) Handles RemoveInfoButton.Click
        If MoreInfoListBox.SelectedIndex < 0 Then
        Else
            MoreInfoListBox.Items.RemoveAt(MoreInfoListBox.SelectedIndex)
        End If
    End Sub
    Private Sub AddCreditsButton_Click(sender As Object, e As EventArgs) Handles AddCreditsButton.Click
        Dim ProductNo As Integer
        Dim LoanedDate As String = DateTime.Now.ToString("yyyy-MM-dd")
        Dim DebtorsID, Debtor, DebtorsContact, ProductName, ProductBrand, ProductDescription, MeasuringUnit, Time, OrderNo As String
        Dim PurchaseCost, SalePrice, Quantity, TotalPrice, Discount, DiscountedPrice, TotalPayment As Decimal
        Try
            OpenConnection()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Dim amountreceived As Decimal
        If StoreForm.AmountReceivedTextBox.Text = "" Or IsNumeric(StoreForm.AmountReceivedTextBox.Text) = False Then
            amountreceived = 0
        Else
            amountreceived = StoreForm.AmountReceivedTextBox.Text
        End If
        If CustomerFullNameTextBox.Text <> "" Then
            If CustomerContactTextBox.Text <> "" Then
                DebtorsID = DebtorsIdTextBox.Text
                Debtor = CustomerFullNameTextBox.Text
                OrderNo = StoreForm.OrderNoTextBox.Text
                DebtorsContact = CustomerContactTextBox.Text
                Try
                    'check if debtor already exist
                    Dim CountDebtorId As Integer = 0
                    Query = "SELECT * FROM credits WHERE debtorsid = '" & DebtorsIdTextBox.Text & "'"
                    Command = New MySqlCommand(Query, MysqlCon)
                    Reader = Command.ExecuteReader
                    While Reader.Read
                        CountDebtorId += 1
                    End While
                    Reader.Close()
                    If CountDebtorId > 0 Then
                        Dim Message1 As String = "Add these ordered products to " & CustomerFullNameTextBox.Text & "'s loans?"
                        Dim Mesres As DialogResult = MessageBox.Show(Message1, "Please Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                        If Mesres = DialogResult.Yes Then
                            'insert loaned products to existing debtor
                            For Gin As Integer = 0 To StoreForm.OrdersDataGridView.RowCount() - 1
                                ProductNo = StoreForm.OrdersDataGridView(0, Gin).Value()
                                ProductName = StoreForm.OrdersDataGridView(2, Gin).Value()
                                ProductBrand = StoreForm.OrdersDataGridView(3, Gin).Value()
                                ProductDescription = StoreForm.OrdersDataGridView(4, Gin).Value()
                                PurchaseCost = StoreForm.OrdersDataGridView(12, Gin).Value()
                                SalePrice = StoreForm.OrdersDataGridView(5, Gin).Value()
                                Quantity = StoreForm.OrdersDataGridView(6, Gin).Value()
                                MeasuringUnit = StoreForm.OrdersDataGridView(7, Gin).Value()
                                TotalPrice = StoreForm.OrdersDataGridView(8, Gin).Value()
                                Discount = StoreForm.OrdersDataGridView(9, Gin).Value()
                                DiscountedPrice = StoreForm.OrdersDataGridView(10, Gin).Value()
                                Time = StoreForm.OrdersDataGridView(11, Gin).Value()
                                TotalPayment += DiscountedPrice
                                'save ordered items to loaned products
                                Try
                                    Query = "INSERT INTO loanedproducts
                                            VALUES('" & DebtorsID & "', '" & Debtor & "', " & ProductNo & ", '" & ProductName & "', '" & ProductBrand & "', '" _
                                            & ProductDescription & "', " & PurchaseCost & ", " & SalePrice & ", " & Quantity & ", '" & MeasuringUnit & "', " _
                                            & TotalPrice & ", " & Discount & ", " & DiscountedPrice & ", '" & LoanedDate & "', '" & Time & "', '" & OrderNo & "')"
                                    Command = New MySqlCommand(Query, MysqlCon)
                                    Reader = Command.ExecuteReader
                                    Reader.Close()
                                    'Subtact order quantity to stock
                                    Try
                                        Query = "update products set availablestock = availablestock - " & Quantity & " where no = " & ProductNo
                                        Command = New MySqlCommand(Query, MysqlCon)
                                        Reader = Command.ExecuteReader
                                        Reader.Close()
                                    Catch ex As Exception
                                        MessageBox.Show(ex.Message)
                                    End Try
                                Catch ex As Exception
                                    MessageBox.Show(ex.Message)
                                End Try
                            Next
                            'if paid some amount
                            Try
                                'Update debtor's credits payment and amount paid
                                Query = "UPDATE credits SET payment = payment + " & TotalPayment & ", amountpaid = amountpaid + " & amountreceived & " where debtorsid = '" & DebtorsIdTextBox.Text & "'"
                                Command = New MySqlCommand(Query, MysqlCon)
                                Reader = Command.ExecuteReader
                                Reader.Close()
                                'If paid some amount, save to paymentsmade
                                Try
                                    If amountreceived > 0 Then
                                        Query = "INSERT INTO paymentsmade VALUES('" & OrderNo & "', '" & DebtorsID & "', '" & Debtor & "', " & amountreceived _
                                        & ", '" & LoanedDate & "', '" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
                                        Command = New MySqlCommand(Query, MysqlCon)
                                        Reader = Command.ExecuteReader
                                        Reader.Close()
                                    End If
                                    'Delete from orders
                                    Try
                                        Query = "DELETE FROM ORDERS"
                                        Command = New MySqlCommand(Query, MysqlCon)
                                        Reader = Command.ExecuteReader
                                        Reader.Close()
                                    Catch ex As Exception
                                        MessageBox.Show(ex.Message)
                                    End Try
                                Catch ex As Exception
                                    MessageBox.Show(ex.Message)
                                End Try
                            Catch ex As Exception
                                MessageBox.Show(ex.Message)
                            End Try
                            Dim Message As String = "The orders were successfully added to " & CustomerFullNameTextBox.Text & "'s loans record!"
                            MessageBox.Show(Message, "Successfully Added", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            DebtorsPhotoPictureBox.Image = My.Resources.uploadphoto
                            DebtorsPhotoPath = ""
                            DebtorsIdTextBox.Clear()
                            With SelectDebtorComboBox
                                .SelectedIndex = -1
                                .Text = " SELECT DEBTOR"
                            End With
                            StoreForm.OrdersDataGridView.Rows.Clear()
                            StoreForm.CustomerNameTextBox.Clear()
                            StoreForm.SubTotalTextBox.Clear()
                            StoreForm.DiscountTextBox.Clear()
                            StoreForm.GrandTotalTextBox.Clear()
                            StoreForm.AmountReceivedTextBox.Clear()
                            StoreForm.ChangeTextBox.Clear()
                            StoreForm.OrderNoTextBox.Clear()
                            CustomerFullNameTextBox.Clear()
                            CustomerContactTextBox.Clear()
                            MoreInfoListBox.Items.Clear()
                            Me.Close()
                        End If
                    Else 'NOT EXISTING DEBTOR
                        Dim Mesres As DialogResult = MessageBox.Show("Add to Credits?", "Please Confirm", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                        If Mesres = DialogResult.OK Then
                            'insert loaned products to existing debtor
                            For Gin As Integer = 0 To StoreForm.OrdersDataGridView.RowCount() - 1
                                ProductNo = StoreForm.OrdersDataGridView(0, Gin).Value()
                                ProductName = StoreForm.OrdersDataGridView(2, Gin).Value()
                                ProductBrand = StoreForm.OrdersDataGridView(3, Gin).Value()
                                ProductDescription = StoreForm.OrdersDataGridView(4, Gin).Value()
                                PurchaseCost = StoreForm.OrdersDataGridView(12, Gin).Value()
                                SalePrice = StoreForm.OrdersDataGridView(5, Gin).Value()
                                Quantity = StoreForm.OrdersDataGridView(6, Gin).Value()
                                MeasuringUnit = StoreForm.OrdersDataGridView(7, Gin).Value()
                                TotalPrice = StoreForm.OrdersDataGridView(8, Gin).Value()
                                Discount = StoreForm.OrdersDataGridView(9, Gin).Value()
                                DiscountedPrice = StoreForm.OrdersDataGridView(10, Gin).Value()
                                Time = StoreForm.OrdersDataGridView(11, Gin).Value()
                                TotalPayment += DiscountedPrice
                                'save ordered items to loaned products
                                Try
                                    Query = "INSERT INTO loanedproducts
                                            VALUES('" & DebtorsID & "', '" & Debtor & "', " & ProductNo & ", '" & ProductName & "', '" & ProductBrand & "', '" _
                                            & ProductDescription & "', " & PurchaseCost & ", " & SalePrice & ", " & Quantity & ", '" & MeasuringUnit & "', " _
                                            & TotalPrice & ", " & Discount & ", " & DiscountedPrice & ", '" & LoanedDate & "', '" & Time & "', '" & OrderNo & "')"
                                    Command = New MySqlCommand(Query, MysqlCon)
                                    Reader = Command.ExecuteReader
                                    Reader.Close()
                                    'Subtact order quantity to stock
                                    Try
                                        Query = "update products set availablestock = availablestock - " & Quantity & " where no = " & ProductNo
                                        Command = New MySqlCommand(Query, MysqlCon)
                                        Reader = Command.ExecuteReader
                                        Reader.Close()
                                    Catch ex As Exception
                                        MessageBox.Show(ex.Message)
                                    End Try
                                Catch ex As Exception
                                    MessageBox.Show(ex.Message)
                                End Try
                            Next
                            'save to credits
                            Try
                                Query = "INSERT INTO credits VALUES('" & DebtorsID & "', '" & Debtor & "', '" & DebtorsContact _
                                       & "', " & TotalPayment & ", " & amountreceived & ")"
                                Command = New MySqlCommand(Query, MysqlCon)
                                Reader = Command.ExecuteReader
                                Reader.Close()
                                'If paid some amount, save to payments made
                                Try
                                    If amountreceived > 0 Then
                                        Query = "INSERT INTO paymentsmade VALUES('" & OrderNo & "', '" & DebtorsID & "', '" & Debtor & "', " & amountreceived _
                                        & ", '" & LoanedDate & "', '" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
                                        Command = New MySqlCommand(Query, MysqlCon)
                                        Reader = Command.ExecuteReader
                                        Reader.Close()
                                    End If
                                    'Save More Information
                                    If MoreInfoListBox.Items.Count < 1 Then
                                    Else
                                        For Gin2 As Integer = 0 To MoreInfoListBox.Items.Count() - 1
                                            Try
                                                Query = "insert into moreinfo values('" & DebtorsID & "', '" & MoreInfoListBox.Items(Gin2).ToString() & "')"
                                                Command = New MySqlCommand(Query, MysqlCon)
                                                Reader = Command.ExecuteReader
                                                Reader.Close()
                                            Catch ex As Exception
                                                MessageBox.Show(ex.Message)
                                            End Try
                                        Next
                                    End If
                                    'Save debtor's photo
                                    Try
                                        Query = "insert into debtorsphotopath values('" & DebtorsID & "', '" & DebtorsPhotoPath & "')"
                                        Command = New MySqlCommand(Query, MysqlCon)
                                        Reader = Command.ExecuteReader
                                        Reader.Close()
                                        'Delete from orders
                                        Try
                                            Query = "DELETE FROM ORDERS"
                                            Command = New MySqlCommand(Query, MysqlCon)
                                            Reader = Command.ExecuteReader
                                            Reader.Close()
                                        Catch ex As Exception
                                            MessageBox.Show(ex.Message)
                                        End Try
                                    Catch ex As Exception
                                        MessageBox.Show(ex.Message)
                                    End Try
                                Catch ex As Exception
                                    MessageBox.Show(ex.Message)
                                End Try
                            Catch ex As Exception
                                MessageBox.Show(ex.Message)
                            End Try
                            Dim Message As String = "The orders were successfully saved as loans of " & CustomerFullNameTextBox.Text & "."
                            MessageBox.Show(Message, "Successfully Added", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Else
                            Return
                        End If
                        DebtorsPhotoPictureBox.Image = My.Resources.uploadphoto
                        DebtorsPhotoPath = ""
                        DebtorsIdTextBox.Clear()
                        With SelectDebtorComboBox
                            .SelectedIndex = -1
                            .Text = " SELECT DEBTOR"
                        End With
                        StoreForm.OrdersDataGridView.Rows.Clear()
                        StoreForm.CustomerNameTextBox.Clear()
                        StoreForm.SubTotalTextBox.Clear()
                        StoreForm.DiscountTextBox.Clear()
                        StoreForm.GrandTotalTextBox.Clear()
                        StoreForm.AmountReceivedTextBox.Clear()
                        StoreForm.ChangeTextBox.Clear()
                        StoreForm.OrderNoTextBox.Clear()
                        CustomerFullNameTextBox.Clear()
                        CustomerContactTextBox.Clear()
                        MoreInfoListBox.Items.Clear()
                        Me.Close()
                    End If
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
                'Dim datetime1 As String = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
                'Dim customerfullname As String = CustomerFullNameTextBox.Text
                'Dim customercontact As String = CustomerContactTextBox.Text

                'Try
                '    For Gin As Integer = 0 To StoreForm.OrdersDataGridView.RowCount() - 1
                '        Dim prodno As Integer = StoreForm.OrdersDataGridView(0, Gin).Value()
                '        Dim prodname As String = StoreForm.OrdersDataGridView(2, Gin).Value()
                '        Dim prodbrand As String = StoreForm.OrdersDataGridView(3, Gin).Value()
                '        Dim proddesc As String = StoreForm.OrdersDataGridView(4, Gin).Value()
                '        Dim price As Decimal = StoreForm.OrdersDataGridView(5, Gin).Value()
                '        Dim qty As Decimal = StoreForm.OrdersDataGridView(6, Gin).Value()
                '        Dim measunit As String = StoreForm.OrdersDataGridView(7, Gin).Value()
                '        Dim totprice As Decimal = StoreForm.OrdersDataGridView(8, Gin).Value()
                '        Discount = StoreForm.OrdersDataGridView(9, Gin).Value()
                '        Dim finalprice As Decimal = StoreForm.OrdersDataGridView(10, Gin).Value()
                '        TotalPayment += finalprice
                '        'save ordered items to loaned products
                '        Try
                '            Query = "insert into loanedproducts values(" & prodno & ", '" & customerfullname & "', '" & prodname & "', '" _
                '                & prodbrand & "', '" & proddesc & "', " & price & ", " & qty & ", '" & measunit & "', " & totprice & ", " _
                '                & discount & ", " & finalprice & ", '" & datetime1 & "')"
                '            Command = New MySqlCommand(Query, MysqlCon)
                '            Reader = Command.ExecuteReader
                '            Reader.Close()
                '        Catch ex As Exception

                '        End Try
                '        'Subtact order quantity to stock
                '        Try
                '            Query = "update products set availablestock = availablestock - " & qty & " where no = " & prodno
                '            Command = New MySqlCommand(Query, MysqlCon)
                '            Reader = Command.ExecuteReader
                '            Reader.Close()
                '        Catch ex As Exception
                '            MessageBox.Show(ex.Message)
                '        End Try
                '    Next
                '    'save credits details
                '    Try
                '        Query = "insert into credits values('" & customerfullname & "', '" & customercontact & "', " & TotalPayment _
                '            & ", " & amountreceived & ", '" & datetime1 & "')"
                '        Command = New MySqlCommand(Query, MysqlCon)
                '        Reader = Command.ExecuteReader
                '        Reader.Close()
                '    Catch ex As Exception
                '        MessageBox.Show(ex.Message)
                '    End Try
                '    'save more info
                '    If MoreInfoListBox.Items.Count < 1 Then
                '    Else
                '        For Gin2 As Integer = 0 To MoreInfoListBox.Items.Count() - 1
                '            Try
                '                Query = "insert into moreinfo values('" & datetime1 & "', '" & MoreInfoListBox.Items(Gin2).ToString() & "')"
                '                Command = New MySqlCommand(Query, MysqlCon)
                '                Reader = Command.ExecuteReader
                '                Reader.Close()
                '            Catch ex As Exception
                '                MessageBox.Show(ex.Message)
                '            End Try
                '        Next
                '    End If
                '    'save debtors photo
                '    Try
                '        Query = "insert into debtorsphotopath values('" & datetime1 & "', '" & DebtorsPhotoPath & "')"
                '        Command = New MySqlCommand(Query, MysqlCon)
                '        Reader = Command.ExecuteReader
                '        Reader.Close()
                '    Catch ex As Exception
                '        MessageBox.Show(ex.Message)
                '    End Try
                '    'save if paid some amount
                '    If amountreceived > 0 Then
                '        Query = "insert into paymentsmade values('" & customerfullname & "', " & amountreceived & ", (select convert(sysdate(), date))" _
                '            & ", '" & datetime1 & "')"
                '        Command = New MySqlCommand(Query, MysqlCon)
                '        Reader = Command.ExecuteReader
                '        Reader.Close()
                '        'DISPLAY SUMMARY
                '        Dim CountTodaysSale As Integer = 0
                '        Dim gross As Decimal
                '        Try
                '            Query = "select * from todayssale where date = (select convert(sysdate(), date))"
                '            Command = New MySqlCommand(Query, MysqlCon)
                '            Reader = Command.ExecuteReader
                '            While Reader.Read
                '                gross = Reader.GetDecimal("grosssale")
                '                CountTodaysSale = 1
                '            End While
                '            Reader.Close()
                '            If CountTodaysSale > 0 Then
                '                Query = "update todayssale set grosssale = " & gross + amountreceived & " where date = (select convert(sysdate(), date))"
                '                Command = New MySqlCommand(Query, MysqlCon)
                '                Reader = Command.ExecuteReader
                '                Reader.Close()
                '            Else
                '                Query = "insert into todayssale (date, grosssale) values((select convert(sysdate(), date)), " & amountreceived & ")"
                '                Command = New MySqlCommand(Query, MysqlCon)
                '                Reader = Command.ExecuteReader
                '                Reader.Close()
                '            End If
                '            'Display sales
                '            Dim SummaryGrossSale, SummaryTotalDiscount, SummaryTotalExpenses, SummaryNetSale As Decimal
                '            Try
                '                Query = "select * from todayssale where date = (select convert(sysdate(), date))"
                '                Command = New MySqlCommand(Query, MysqlCon)
                '                Reader = Command.ExecuteReader
                '                While Reader.Read
                '                    SummaryGrossSale = Reader.GetDecimal("grosssale")
                '                    SummaryTotalDiscount = Reader.GetDecimal("totaldiscount")
                '                    SummaryTotalExpenses = Reader.GetDecimal("totalexpenses")
                '                End While
                '                Reader.Close()
                '                SummaryNetSale = SummaryGrossSale - (SummaryTotalDiscount + SummaryTotalExpenses)
                '                StoreForm.GrossSaleTextBox.Text = SummaryGrossSale.ToString("n2")
                '                StoreForm.TotalDiscountTextBox.Text = SummaryTotalDiscount.ToString("n2")
                '                StoreForm.TotalExpensesTextBox.Text = SummaryTotalExpenses.ToString("n2")
                '                StoreForm.NetSaleTextBox.Text = SummaryNetSale.ToString("n2")
                '            Catch ex As Exception
                '                MessageBox.Show(ex.Message)
                '            End Try
                '        Catch ex As Exception
                '            MessageBox.Show(ex.Message)
                '        End Try
                '    End If
                '    'Display Credits
                '    Try
                '        StoreForm.CreditsDataGridView.Rows.Clear()
                '        Query = "select * from credits 
                '        inner join (select sum(discount) as totaldiscount, datetime as datetime2 from loanedproducts group by datetime) A 
                '        on credits.datetime = A.datetime2 order by customerfullname"
                '        Command = New MySqlCommand(Query, MysqlCon)
                '        Reader = Command.ExecuteReader
                '        While Reader.Read
                '            Dim a = Reader.GetString("debtor")
                '            Dim b = Reader.GetString("contact")
                '            Dim c = Reader.GetDecimal("payment")
                '            Dim Dscnt = Reader.GetDecimal("totaldiscount")
                '            Dim d = Reader.GetDecimal("amountpaid")
                '            Dim rempayment1 As Decimal = c - d
                '            Dim g1 = Reader.GetString("datetime")
                '            StoreForm.CreditsDataGridView.Rows.Add(a, b, c + Dscnt, Dscnt, d, rempayment1, g1)
                '        End While
                '        Reader.Close()
                '        StoreForm.CreditsDataGridView.ClearSelection()
                '    Catch ex As Exception
                '        MessageBox.Show(ex.Message)
                '    End Try
                '    'display paid amount
                '    StoreForm.TodaysPaidCreditsDataGridView.Rows.Clear()
                '    Try
                '        Query = "select * from paymentsmade where date = (select convert(sysdate(), date))"
                '        Command = New MySqlCommand(Query, MysqlCon)
                '        Reader = Command.ExecuteReader
                '        While Reader.Read
                '            Dim a = Reader.GetString("debtor")
                '            Dim b = Reader.GetDecimal("paidamount")
                '            StoreForm.TodaysPaidCreditsDataGridView.Rows.Add(a, b)
                '        End While
                '        Reader.Close()
                '    Catch ex As Exception
                '        MessageBox.Show(ex.Message)
                '    End Try
                '    'CLEAR
                '    StoreForm.OrdersDataGridView.Rows.Clear()
                '    StoreForm.CustomerNameTextBox.Clear()
                '    StoreForm.SubTotalTextBox.Clear()
                '    StoreForm.DiscountTextBox.Clear()
                '    StoreForm.GrandTotalTextBox.Clear()
                '    StoreForm.AmountReceivedTextBox.Clear()
                '    StoreForm.ChangeTextBox.Clear()
                '    CustomerFullNameTextBox.Clear()
                '    CustomerContactTextBox.Clear()
                '    MoreInfoListBox.Items.Clear()
                '    Try
                '        Query = "delete from orders"
                '        Command = New MySqlCommand(Query, MysqlCon)
                '        Reader = Command.ExecuteReader
                '        Reader.Close()
                '    Catch ex As Exception
                '        MessageBox.Show(ex.Message)
                '    End Try
                '    Me.Close()
                'Catch ex As Exception
                '    MessageBox.Show(ex.Message)
                'End Try

            Else
                MessageBox.Show("Customer Contact should not be empty!", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information)
                CustomerContactTextBox.Focus()
            End If
        Else
            MessageBox.Show("Customer Full Name should not be empty!", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information)
            CustomerFullNameTextBox.Focus()
        End If
        MysqlCon.Close()
    End Sub

    Private Sub AddInfoForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        StoreForm.CreditsDataGridView.ClearSelection()
        StoreForm.PayCreditButton.Enabled = False
        StoreForm.MoreInfoButton.Enabled = False
        MoreInfoTextBox.Clear()
        DebtorsPhotoPictureBox.Image = My.Resources.uploadphoto
        DebtorsPhotoPath = ""
        DebtorsIdTextBox.Clear()
        With SelectDebtorComboBox
            .SelectedIndex = -1
            .Text = " SELECT DEBTOR"
            .Visible = False
        End With
        ExistingDebtorCheckBox.Checked = False
        CustomerFullNameTextBox.Clear()
        CustomerContactTextBox.Clear()
        MoreInfoListBox.Items.Clear()
        StoreForm.StoreForm_Load(sender, e)
    End Sub
    Private Sub AddInfoForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DebtorsPhotoPictureBox.Image = My.Resources.uploadphoto
        DebtorsPhotoPath = ""
        DebtorsIdTextBox.Text = StoreForm.OrderNoTextBox.Text
    End Sub

    Private Sub BrowseDebtorsPhotoButton_Click(sender As Object, e As EventArgs) Handles BrowseDebtorsPhotoButton.Click
        Try
            OpenFileDialog1.Filter = "JPEG|*.jpg|PNG|*.png|ALL FILES|*.*"
            If OpenFileDialog1.ShowDialog = DialogResult.OK Then
                DebtorsPhotoPictureBox.Image = Image.FromFile(OpenFileDialog1.FileName)
                DebtorsPhotoPath = OpenFileDialog1.FileName.Replace("\", "\\")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub CancelCreditsButton_Click(sender As Object, e As EventArgs) Handles CancelCreditsButton.Click
        Me.Close()
    End Sub

    Private Sub ExistingDebtorCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles ExistingDebtorCheckBox.CheckedChanged
        If ExistingDebtorCheckBox.Checked = True Then
            SelectDebtorComboBox.Visible = True
            SelectDebtorComboBox.Text = " SELECT DEBTOR"
            BrowseDebtorsPhotoButton.Enabled = False
            CustomerFullNameTextBox.Enabled = False
            CustomerContactTextBox.Enabled = False
            AddInfoButton.Enabled = False
            RemoveInfoButton.Enabled = False
            MoreInfoTextBox.Enabled = False
            MoreInfoListBox.Enabled = False
            CustomerFullNameTextBox.Clear()
            CustomerContactTextBox.Clear()
            MoreInfoListBox.Items.Clear()
            Try
                OpenConnection()
                Query = "SELECT CONCAT(debtorsid, '-', debtor) as debtorsidandname from credits"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                While Reader.Read
                    SelectDebtorComboBox.Items.Add(Reader.GetString("debtorsidandname"))
                End While
                Reader.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
            MysqlCon.Close()
        Else
            SelectDebtorComboBox.Text = ""
            SelectDebtorComboBox.Items.Clear()
            SelectDebtorComboBox.Visible = False
            BrowseDebtorsPhotoButton.Enabled = True
            CustomerFullNameTextBox.Enabled = True
            CustomerContactTextBox.Enabled = True
            AddInfoButton.Enabled = True
            RemoveInfoButton.Enabled = True
            MoreInfoTextBox.Enabled = True
            MoreInfoListBox.Enabled = True
            DebtorsPhotoPictureBox.Image = My.Resources.uploadphoto
            DebtorsPhotoPath = ""
            DebtorsIdTextBox.Text = StoreForm.OrderNoTextBox.Text
            CustomerFullNameTextBox.Clear()
            CustomerContactTextBox.Clear()
            MoreInfoListBox.Items.Clear()

        End If
    End Sub

    Private Sub SelectDebtorComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles SelectDebtorComboBox.SelectedIndexChanged
        Try
            OpenConnection()
            Query = "select A.debtorsid, A.debtor, A.contact, A.payment, A.Amountpaid, A.concatdebtornameid, debtorsphotopath.debtorsphotopath from (SELECT * FROM (SELECT *, CONCAT(DEBTORSID, '-', DEBTOR) AS concatdebtornameid FROM CREDITS) 
            AS creditswithconcatidname WHERE concatdebtornameid = '" & SelectDebtorComboBox.Text & "') A inner join debtorsphotopath on A.debtorsid = debtorsphotopath.debtorsid"
            Command = New MySqlCommand(Query, MysqlCon)
            Reader = Command.ExecuteReader
            While Reader.Read
                DebtorsIdTextBox.Text = Reader.GetString("debtorsid")
                CustomerFullNameTextBox.Text = Reader.GetString("debtor")
                CustomerContactTextBox.Text = Reader.GetString("contact")
                If System.IO.File.Exists(Reader.GetString("debtorsphotopath")) Then
                    DebtorsPhotoPictureBox.Image = Image.FromFile(Reader.GetString("debtorsphotopath"))
                Else
                    DebtorsPhotoPictureBox.Image = My.Resources.uploadphoto
                End If
            End While
            Reader.Close()
            Try
                MoreInfoListBox.Items.Clear()
                Query = "SELECT * FROM moreinfo where debtorsid = '" & DebtorsIdTextBox.Text & "'"
                Command = New MySqlCommand(Query, MysqlCon)
                Reader = Command.ExecuteReader
                While Reader.Read
                    MoreInfoListBox.Items.Add(Reader.GetString("info"))
                End While
                Reader.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        MysqlCon.Close()
    End Sub
End Class